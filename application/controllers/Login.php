<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


public function __construct(){
	parent::__construct();
 /*	
     if($this->session->logged == 'y'){
     if ($this->session->info == 'Dosen') {
     	redirect('dosen');
       
     }else if ($this->session->info == 'Koor MK') {
       redirect('koor_mk');
     }else if ($this->session->info == 'Koor PA') {
       redirect('koor_pa');
     }else if ($this->session->info == 'Ka. Prodi'){
       redirect('kaprodi');
     };
	 } else if($this->session->logged == 't') {
	 redirect('login/index');
	 }
 */ 
}
	
	public function index()

	{
	 if($this->session->logged == 'y'){
     if ($this->session->info == 'Dosen') {
     	redirect('dosen');
       
     }else if ($this->session->info == 'Koor MK') {
       redirect('koor_mk');
     }else if ($this->session->info == 'Koor PA') {
       redirect('koor_pa');
     }else if ($this->session->info == 'Ka. Prodi'){
       redirect('kaprodi');
     };
	 } else if($this->session->logged == 't') {
	 redirect('login/index');
	 }
		
		$data = array(
            'title' => 'Login Page',
            'action' => site_url('login/check')
            );
		$this->load->view('login',$data);
	}

	public function check(){

 	 $this->load->model('Auth','login');
     $login = $this->login->login($this->input->post('username'), md5($this->input->post('passwd')));
	
	
	$resp = array('accessGranted' => false, 'errors' => '','level' => ''); // For ajax response
	
	if(isset($_POST['do_login']))
	{
		$given_username = $_POST['username'];
		$given_password = $_POST['passwd'];
		
		if($login == 1)
		{

			$row = $this->login->data_login($this->input->post('username'), md5($this->input->post('passwd')));
 			
			
			  $data = array(
                'logged' => TRUE,
                'id_dosen' => $row->id_dosen,
                'username' => $row->nip,
                'nama' => $row->nama,
                'foto' => $row->picture,
                'level' => $row->level,
                'info' => get_level($row->level)
            );
            $this->session->set_userdata($data);

			$resp['accessGranted'] = true;
			setcookie('failed-attempts', 0);
			// $resp['level'] = '1';
			if($this->session->level == "1"){
            	 //redirect(site_url('dosen'));
            $resp['level'] = '1';
            }else if($this->session->level == "2"){
            	 //redirect(site_url('koor_mk'));
            $resp['level'] = '2';
            } 
            else if($this->session->level == "3"){
            	 //redirect(site_url('koor_pa'));
			$resp['level'] = '3';
            } 
            else if($this->session->level == "4"){
            	// redirect(site_url('kaprodi'));
            $resp['level'] = '4';
            } else if($this->session->level == "5"){
            	// redirect(site_url('admin'));
            $resp['level'] = '5';
            } 
		}
		else
		{
			// Failed Attempts
			$fa = isset($_COOKIE['failed-attempts']) ? $_COOKIE['failed-attempts'] : 0;
			$fa++;
			
			setcookie('failed-attempts', $fa);
			
			// Error message
			$resp['errors'] = '<strong>Invalid login!</strong><br />Please enter valid username and password .<br />Failed attempts: ' . $fa;
		}
	}
	
	echo json_encode($resp);
	
	}

	function logout() {
//        destroy session
		$this->session->set_userdata("logged","t");
        $this->session->sess_destroy();
        
//        redirect ke halaman login
        redirect('login');
    }
    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    
public function power_ranger(){
    $this->load->view('our_team');
}
 
}
