<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kaprodi extends CI_Controller {

	public function __construct(){
		 parent::__construct();
		
        if(!isset($_SESSION['username'])){
            redirect('login');
        };
        $this->load->model('M_kaprodi', 'yeah');
		$this->load->helper('raj');
		$this->session->set_userdata("info","Ka. Prodi");
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->helper(array('form', 'url'));
		if($this->session->level != '4'){
			redirect('dosen');
		};
		
	}

	public function index()

	{
		  $this->load->model('M_kaprodi', 'yeah');

		$rows = $this->yeah->getById('nip',$this->session->username,'t_dosen')->result();
		$data = array(
            'title' => 'Dashboard Kaprodi',
            'action' => site_url('login/check'),
            'rows' => $rows,
            'ind' =>  $this->yeah->getListKPI()
            );
		$this->load->view('kaprodi/dashboard_kaprodi',$data);
	}

	//rapat
	public function list_rapat(){
		$rows1 = $this->yeah->all_undangan('id_undangan','t_undangan',1)->result();
		$rows2 = $this->yeah->all_undangan('id_undangan','t_undangan',2)->result();
		$nama  = $this->yeah->nama_dosen()->result();
		
		$data = array(
            'title' => 'List Undangan',
            'rows1' => $rows1,
            'rows2' => $rows2,
            'nama' => $nama
            );
		$this->load->view('kaprodi/rapat/list_rapat',$data);
	}

	public function add_rapat(){
		$data = array(
			'agenda' => html_escape($this->input->post('agenda')),
			'ruangan' => html_escape($this->input->post('ruangan')),
			'tanggal_rapat' => html_escape($this->input->post('tanggal')),
			'tipe' => '2',
			'bukti' => 'x',
			'id_semester' => html_escape(getsemester_aktif())
			);
		$this->yeah->add($data,'t_undangan');
		$id = $this->yeah->last_data('t_undangan','id_undangan')->row();
		$peserta = $this->input->post('peserta');
		
		
		$all_dosen = $this->yeah->all_dosen('id_dosen','t_dosen','id_dosen')->result();
		
		foreach ($all_dosen as $x ) {
		
		if($x->id_dosen == $this->session->id_dosen){
			$data3 = array(
			'id_undangan' => html_escape($id->id_undangan),
			'id_dosen' => html_escape($this->session->id_dosen),
			'status' => 'HADIR',
			'keterangan' => ' ',
			'tipe_p' => '1'
			);
		hitungnilai_rapat_dosen($this->session->id_dosen);	
		$this->yeah->add($data3,'t_kehadiran');
		}else {
			$data2 = array(
			'id_undangan' => $id->id_undangan,
			'id_dosen' => $x->id_dosen,
			'status' => 'TERKIRIM',
			'keterangan' => ' ',
			'tipe_p' => '2'
		);
		$this->yeah->add($data2,'t_kehadiran');
		};
		hitungnilai_rapat_dosen($x->id_dosen);
		}
		

		redirect('kaprodi/list_rapat');
	}

	public function add_bukti(){
		$id = $this->uri->segment(3);

		if (!empty($_FILES)) {
	        $path = $_FILES['file']['name'];
	   		$ext = pathinfo($path, PATHINFO_EXTENSION);
	   		$name = pathinfo($path, PATHINFO_FILENAME);
	   	
	        $tempFile = $_FILES['file']['tmp_name'];
	        $fileName = str_replace(" ", "-",'Bukti-'.$name . '_' . date("Y-m-d") . '_' . date("H-i-s") . '.' .$ext);
	        $date = date("Y-m-d");
	        $targetPath = getcwd() . '/uploadfile/rapat/';
	        $targetFile = $targetPath . $fileName ;
	        ini_set('upload_max_filesize', '90M');
	   		ini_set('post_max_size', '90M');
	   		$cek = $this->yeah->getbukti($id);
	   		if($cek == 'x'){
	   		$this->yeah->update(array('bukti'=>$fileName),'t_undangan',$id,'id_undangan');
	        move_uploaded_file($tempFile, $targetFile);
			}else{
			$this->load->helper("file");
			unlink('./uploadfile/rapat/'.$cek);
			$this->yeah->update(array('bukti'=>$fileName),'t_undangan',$id,'id_undangan');
	        move_uploaded_file($tempFile, $targetFile);
			}
			
        	
        }
 
       
	}

	public function view_gambar(){
		$rows  = $this->yeah->gambar_bukti($this->input->get('id'));
        echo json_encode($rows);
	}


	public function del_rapat($id){
		$cek = $this->yeah->getbukti($id);
		$this->load->helper("file");
		if($cek != 'x')
		{
		unlink('./uploadfile/rapat/'.$cek);
		}
		$daftar_dosen = $this->db->query("SELECT * FROM t_kehadiran where id_undangan='$id'")->result();
		$this->yeah->delete('id_undangan',$id,'t_undangan');
		$this->yeah->delete('id_undangan',$id,'t_kehadiran');
		foreach($daftar_dosen as $rows)
		{hitungnilai_rapat_dosen($rows->id_dosen);}
		
		redirect('kaprodi/list_rapat');

	}

	public function del_rapat_koor($id,$di){
		$id_koor = getiddosen_rapat($id);

		$cek = $this->yeah->getbukti($id);
		$this->load->helper("file");
		if($cek != 'x')
		{
			unlink('./uploadfile/rapat/'.$cek);
		}
		$id_dosen =  $this->db->query("SELECT id_dosen FROM t_kehadiran WHERE id_undangan = '$id'")->result();
		$this->yeah->delete('id_undangan',$id,'t_undangan');
		foreach($id_dosen as $x){
			hitungnilai_rapat_koor_dosen($x->id_dosen);
		}
		hitung_nilai_koor($di,$id_koor);
		redirect('kaprodi/list_rapat');
	}



	public function edit_rapat($id){
		$rows = $this->yeah->getById('id_undangan',$id,'t_undangan')->result();
		$data = array(
			'title' => 'Edit Data',
			'rows' => $rows
		);
		$this->load->view("kaprodi/rapat/edit_rapat",$data);

	}
	public function proses_edit_rapat(){
		$data = array(
			'agenda' => $this->input->post('agenda'),
			'ruangan' => $this->input->post('ruangan'),
			'tanggal_rapat' =>  $this->input->post('tanggal'),
			'tipe' => $this->input->post('radio')
			);

		$where = $this->input->post('id');
		$this->yeah->update(html_escape($data),'t_undangan',$where,'id_undangan');
		redirect('kaprodi/list_rapat');

	}
	public function rapat_hadir($id,$val,$id2,$iddosen){
		

		$this->yeah->update_status_rapat($id2,html_escape(urldecode($val)),'HADIR');

		$q = $this->yeah->cek_rapat($id);
		if($q == '1'){
		hitungnilai_rapat_koor_dosen($iddosen);
		} else if($q == '2'){
		hitungnilai_rapat_dosen($iddosen);
		}
		redirect('kaprodi/detail_rapat/'.$id);
	}

	public function rapat_alpha($id,$val,$id2,$iddosen){
		$this->yeah->update_status_rapat($id2,html_escape(urldecode($val)),'ALPHA');
		$q = $this->yeah->cek_rapat($id);
		if($q == '1'){
		hitungnilai_rapat_koor_dosen($iddosen);
		} else if($q == '2'){
		hitungnilai_rapat_dosen($iddosen);
		}
		redirect('kaprodi/detail_rapat/'.$id);
	}

	public function rapat_izin($id,$val,$id2,$iddosen){
		$this->yeah->update_status_rapat($id2,html_escape(urldecode($val)),'IZIN');


		$q = $this->yeah->cek_rapat($id);
		if($q == '1'){
		hitungnilai_rapat_koor_dosen($iddosen);
		} else if($q == '2'){
		hitungnilai_rapat_dosen($iddosen);
		}
		redirect('kaprodi/detail_rapat/'.$id);
	}

	public function detail_rapat($id){
		$rows = $this->yeah->getById('id_undangan',$id,'t_undangan')->result();
		$res = $this->yeah->getById('id_undangan',$id,'t_kehadiran')->result();
		$data = array(
			'title' => 'Detail Rapat',
			'rows' => $rows,
			'res' => $res
		);
		$this->load->view("kaprodi/rapat/detail_rapat",$data);
	}

	public function view_nilai(){
		$rows = $this->yeah->all('id_nilai','t_nilai')->result();
			
		$data = array(
            'title' => 'Hasil Penilaian',
            'rows' => $rows,
           
            );
		$this->load->view('kaprodi/nilai/nilai_rapat_dosen',$data);
	}

	public function proses_edit_nilai(){
		$data = array(
			
			'nilai_rapat_prodi2' => htmlspecialchars($this->input->post('nilai_prodi2')),
			'nilai_rapat_koor2' => htmlspecialchars($this->input->post('nilai_koor2'))
		);
		$this->yeah->update($data,'t_nilai',$this->input->post('id'),'id_nilai');
		redirect('kaprodi/view_nilai');
	}

	public function view_nilai_koordinator(){
		$rows = $this->yeah->all('id_nilai','t_nilai')->result();
			
		$data = array(
            'title' => 'Hasil Penilaian',
            'rows' => $rows,
           
            );
		$this->load->view('kaprodi/nilai/hasil_nilai',$data);
	}

	public function ListIndikator(){
		$rows = $this->yeah->all('id_kpi','t_kpi')->result();

		$rows1 = $this->yeah->getById('id_kpi',1,'t_kpi')->result();
		$ros1 = $this->yeah->getKategori('id_kpi',1,'t_kategori')->result();

		$rows2 = $this->yeah->getById('id_kpi',2,'t_kpi')->result();

		$rows3 = $this->yeah->getById('id_kpi',3,'t_kpi')->result();

		$rows4 = $this->yeah->getById('id_kpi',4,'t_kpi')->result();
		$ros4 = $this->yeah->getKategori('id_kpi',4,'t_kategori')->result();

		$rows5 = $this->yeah->getById('id_kpi',5,'t_kpi')->result();
		$ros5 = $this->yeah->getKategori('id_kpi',5,'t_kategori')->result();

		$rows12 = $this->yeah->getById('id_kpi',12,'t_kpi')->result();
		$ros12 = $this->yeah->getKategori('id_kpi',12,'t_kategori')->result();

		$rows13 = $this->yeah->getById('id_kpi',13,'t_kpi')->result();
		$ros13 = $this->yeah->getKategori('id_kpi',13,'t_kategori')->result();

		$rows14 = $this->yeah->getById('id_kpi',14,'t_kpi')->result();
		$ros14 = $this->yeah->getKategori('id_kpi',14,'t_kategori')->result();

		$rows15 = $this->yeah->getById('id_kpi',15,'t_kpi')->result();
		$ros15 = $this->yeah->getKategori('id_kpi',15,'t_kategori')->result();

		$rows6 = $this->yeah->getById('id_kpi',6,'t_kpi')->result();
		$ros6 = $this->yeah->getKategori('id_kpi',6,'t_kategori')->result();

		$rows7 = $this->yeah->getById('id_kpi',7,'t_kpi')->result();
		$ros7 = $this->yeah->getKategori('id_kpi',7,'t_kategori')->result();

		$rows8 = $this->yeah->getById('id_kpi',8,'t_kpi')->result();
		$ros8 = $this->yeah->getKategori('id_kpi',8,'t_kategori')->result();

		$rows9 = $this->yeah->getById('id_kpi',9,'t_kpi')->result();
		$ros9 = $this->yeah->getKategori('id_kpi',9,'t_kategori')->result();

		$rows10 = $this->yeah->getById('id_kpi',10,'t_kpi')->result();
		$ros10 = $this->yeah->getKategori('id_kpi',10,'t_kategori')->result();

		$rows11 = $this->yeah->getById('id_kpi',11,'t_kpi')->result();
		$ros11 = $this->yeah->getKategori('id_kpi',11,'t_kategori')->result();

		$rows16 = $this->yeah->getById('id_kpi',16,'t_kpi')->result();
		$ros16 = $this->yeah->getKategori('id_kpi',16,'t_kategori')->result();
		
		$rows17 = $this->yeah->getById('id_kpi',17,'t_kpi')->result();
		$ros17  = $this->yeah->getKategori('id_kpi',17,'t_kategori')->result();

		$data = array(
			'title' => 'Kelola KPI',
			'rows' => $rows,
			'rows1' =>$rows1,
			'ros1' => $ros1,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'ros4' => $ros4,
			'rows5' => $rows5,
			'ros5' => $ros5,
			'rows6' => $rows6,
			'ros6' => $ros6,
			'rows7' => $rows7,
			'ros7' => $ros7,
			'rows8' => $rows8,
			'ros8' => $ros8,
			'rows9' => $rows9,
			'ros9' => $ros9,
			'rows10' => $rows10,
			'ros10' => $ros10,
			'rows11' => $rows11,
			'ros11' => $ros11,
			'rows12' => $rows12,
			'ros12' => $ros12,
			'rows13' => $rows13,
			'ros13' => $ros13,
			'rows14' => $rows14,
			'ros14' => $ros14,
			'rows15' => $rows15,
			'ros15' => $ros15,
			'rows16' => $rows16,
			'ros16' => $ros16,
			'rows17' => $rows17,
			'ros17' => $ros17
		);
		$this->load->view('kaprodi/kpi/view_kpi',$data);
	}

	public function off_kpi($id){
	
	$data = array(
		'status' => 'OFF'
	);
	$this->yeah->update($data,'t_kpi',$id,'id_kpi');
	redirect('kaprodi/ListIndikator');
	}

	public function on_kpi($id){
	
	$data = array(
		'status' => 'ON'
	);
	$this->yeah->update($data,'t_kpi',$id,'id_kpi');
	redirect('kaprodi/ListIndikator');
	}

	public function edit_kpi($id){
		$rows = $this->yeah->getKategori('id_kpi',$id,'t_kategori')->result();
		$rows2 = $this->yeah->getById('id_kpi',$id,'t_kpi')->result();
		$data = array(
			'title' => 'Edit KPI',
			'kpi' => $rows2,
			'penilaian' => $rows,
			'id' => '1'
		);
		$this->load->view('kaprodi/kpi/edit_kpi',$data);
	}

	public function add_nilai(){
		$id = $this->input->post('id_kpi');
		$kon = $this->input->post('kondisi');
		$nil = $this->input->post('nilai');
		$where = "id_kpi='$id' AND (kondisi ='$kon' OR nilai='$nil')";
		$this->db->select('*')
		->where($where)
		->from('t_kategori');
		$q = $this->db->get();
		if($q->num_rows() > 0){
		$this->session->set_flashdata('msg','not ok');
		redirect('kaprodi/edit_kpi/'.$id);
		}else{
		$data = array(
			'kondisi' => $kon,
			'nilai' => $nil,
			'id_kpi' => $id
		);
		$this->yeah->add($data,'t_kategori');
		$this->session->set_flashdata('msg','ok');
		redirect('kaprodi/edit_kpi/'.$id);
		}
	}

	public function edit_nilai(){

			$where3 = $this->input->post('id');
			$where2 = $this->input->post('id2');

			$kon = $this->input->post('kondisi');
			$nil = $this->input->post('nilai');
			$where = "id_kpi='$where2' AND nilai='$nil'";
			$this->db->select('*')
					 ->where($where)
					 ->from('t_kategori');
			$q = $this->db->get();
			if($q->num_rows() > 0){
			$this->session->set_flashdata('msg','not ok nilai');
			redirect('kaprodi/edit_kpi/'.$where2);
			}else{
			$data = array(
				'nilai' => html_escape($this->input->post('nilai')),
			);
			
			$this->yeah->update($data,'t_kategori',$where3,'id_kategori');
			$this->session->set_flashdata('msg','ok');
			redirect('kaprodi/edit_kpi/'.$where2);
			}


	}


	public function edit_nilai2(){

			$where3 = $this->input->post('id');
			$where2 = $this->input->post('id2');

			$kon = $this->input->post('kondisi');
			$nil = $this->input->post('nilai');
			$where = "id_kpi='$where2' AND kondisi ='$kon'";
			$this->db->select('*')
					 ->where($where)
					 ->from('t_kategori');
			$q = $this->db->get();
			if($q->num_rows() > 0){
			$this->session->set_flashdata('msg','not ok deadline');
			redirect('kaprodi/edit_kpi/'.$where2);
			}else{
			$data = array(
				'kondisi' => html_escape($this->input->post('kondisi')),
			);
			
			$this->yeah->update($data,'t_kategori',$where3,'id_kategori');
			$this->session->set_flashdata('msg','ok');
			redirect('kaprodi/edit_kpi/'.$where2);
			}


	}

	public function del_nilai($id,$di){
		$this->yeah->delete('id_kategori',$id,'t_kategori');
		$this->edit_kpi($di);
	}
	public function proses_kpi(){
		$data = array(
			'indikator' => html_escape($this->input->post('indikator'))
		);
		$where = $this->input->post('id');
		$this->yeah->update($data,'t_kpi',$where,'id_kpi');

		redirect('kaprodi/edit_kpi/'.$where);

	}

	//kelola edom dosen
	public function unggah_edom(){
		$data = array(
			'title' => 'Unggah EDOM'
		);
		$this->load->view('kaprodi/edom/unggah_edom', $data);
	}
	public function unggah_edom_aksi(){

		$fileName = $this->input->post('file', TRUE);
     
	    $config['upload_path'] = './uploadfile/edom';  //buat folder dengan nama arsip di root folder
	    $config['file_name'] = $fileName;
	    $config['allowed_types'] = 'xls|xlsx|csv';
	    $config['max_size'] = 100000;

	    $this->load->library('upload', $config);
		$this->upload->initialize($config); 
	     
	    if(! $this->upload->do_upload('file')){
		    $error = array('error' => $this->upload->display_errors());
		    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
			redirect('kaprodi/unggah_edom');
		} else {
		   	$media = $this->upload->data();
		    $inputFileName = './uploadfile/edom/'.$media['file_name'];
	        
	        try {
	            $inputFileType = IOFactory::identify($inputFileName);
	            $objReader = IOFactory::createReader($inputFileType);
	            $objPHPExcel = $objReader->load($inputFileName);
	        } catch(Exception $e) {
	            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	        }

	        $sheet = $objPHPExcel->getSheet(0);
	        $highestRow = $sheet->getHighestRow();
	        $highestColumn = $sheet->getHighestColumn();
	        $cek = $this->yeah->getEdom()->num_rows();
	        for ($row = 2; $row <= $highestRow; $row++){//  Read a row of data into an array                 
		        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	                                            NULL,
	                                            TRUE,
	                                            FALSE);
		        if($rowData[0][0]){
		        	if (checkcode($rowData[0][1])) {
		        		$cek2 = $this->yeah->get_nilai_edom(array('id_dosen' => convertkode($rowData[0][1]), 'id_semester'=> getsemester_aktif()));
				       if ($cek  > 0) {
				       		$data = array(
					                "jml_kelas" => $rowData[0][4],
					                "jml_mahasiswa"=> $rowData[0][5],
					                "presentase" => $rowData[0][6]
				            );
				        	$this->yeah->update_edom(convertkode($rowData[0][1]),$data);
				       } else{
				       		 //Sesuaikan sama nama kolom tabel di database   
				          	$data = array(
					                "id_dosen"=> convertkode($rowData[0][1]),
					                "jml_kelas" => $rowData[0][4],
					                "jml_mahasiswa"=> $rowData[0][5],
					                "presentase" => $rowData[0][6],
					                "semester" => getsemester_aktif()
				            );
				        	$this->db->insert('t_edom', $data);
				       }
				       if($cek2 > 0){
				       		$data2 = array(
					                "nilai_edom" => $rowData[0][6]
				            );
				        	$this->yeah->update_edom2(convertkode($rowData[0][1]),getsemester_aktif(),$data2);
				        } else{
				        	$data2 = array(
					                "id_dosen"=> convertkode($rowData[0][1]),
					                "nilai_edom" => $rowData[0][6],
					                "id_semester" => getsemester_aktif()
				            );
				        	$this->db->insert('t_nilai', $data2);
				        }
		        	}
			       
			    }    
	        }
	        redirect('kaprodi/lihat_edom');
	    }
	    
	}

	public function lihat_edom(){
		// $rows = $this->yeah->getEdom();
		$rows = $this->yeah->getEdom()->result();
		$data = array(
			'title' => 'Lihat EDOM',
			'rows' => $rows
		);
		$this->load->view('kaprodi/edom/lihat_edom', $data);
	}

	public function download_edom(){
		$this->load->helper('download');
		$fileName = 'EDOM.xlsx';
	    force_download('uploadfile/template/'.$fileName, NULL);

	}

	//kelola bap
	public function unggah_bap(){
		$data = array(
			'title' => 'Unggah BAP'
		);
		$this->load->view('kaprodi/bap/unggah_bap', $data);
	}

	public function unggah_bap_aksi(){

		$fileName = $this->input->post('file', TRUE);
     
	    $config['upload_path'] = './uploadfile/bap';  //buat folder dengan nama arsip di root folder
	    $config['file_name'] = $fileName;
	    $config['allowed_types'] = 'xls|xlsx|csv';
	    $config['max_size'] = 100000;

	    $this->load->library('upload', $config);
		$this->upload->initialize($config); 
	     
	    if(! $this->upload->do_upload('file') ){
		    $error = array('error' => $this->upload->display_errors());
		    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
			redirect('kaprodi/unggah_bap');
		} else {
		   	$media = $this->upload->data();
		    $inputFileName = './uploadfile/bap/'.$media['file_name'];
	        
	        try {
	            $inputFileType = IOFactory::identify($inputFileName);
	            $objReader = IOFactory::createReader($inputFileType);
	            $objPHPExcel = $objReader->load($inputFileName);
	        } catch(Exception $e) {
	            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	        }

	        $sheet = $objPHPExcel->getSheet(0);
	        $highestRow = $sheet->getHighestRow();
	        $highestColumn = $sheet->getHighestColumn();
	        $cek = $this->yeah->getBap()->num_rows();
	        for ($row = 2; $row <= $highestRow; $row++){//  Read a row of data into an array                 
		        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	                                            NULL,
	                                            TRUE,
	                                            FALSE);
		        
		        if($rowData[0][0]) {
		        	if (checkcode($rowData[0][0])) {
		        		  $cek2 = $this->yeah->get_nilai_bap(array('id_dosen' => convertkode($rowData[0][0]), 'id_semester'=> getsemester_aktif()));
				       if ($cek  > 0) {
				       		
				       		
				       			$pres_rfid = round(($rowData[0][1]/$rowData[0][2])*100,2);
					       		$pres_inputbap = round(($rowData[0][6]/($rowData[0][5]+$rowData[0][6]))*100,2);
					       		$numberset = 4;
					       		$sum = round($pres_rfid+$pres_inputbap+$rowData[0][8]+$rowData[0][10],2);
					       		$kinerja_dosen = round($sum/$numberset,2);
					       		$data = array(
					                "rfid" => $rowData[0][2],
					                "total"=> $rowData[0][3],
					                "non_rfid" => $rowData[0][4],
					                "belum_submit" => $rowData[0][6],
					                "sudah_submit" => $rowData[0][7],
					                "pres_waktubap" => $rowData[0][8],
					                "pres_kehadiran" => $rowData[0][10],
					                "pres_rfid"	=> $pres_rfid,
					                "pres_inputbap" => $pres_inputbap,
					                "kinerja_dosen" => $kinerja_dosen,
					                "semester" => getsemester_aktif()
					            );
					        	$this->yeah->update_bap(convertkode($rowData[0][0]),$data);
				       		
				       		
				       } else{
				       		 //Sesuaikan sama nama kolom tabel di database  
				       		//proses hitung
				       		$pres_rfid = round(($rowData[0][1]/$rowData[0][2])*100,2);
				       		$pres_inputbap = round(($rowData[0][6]/($rowData[0][5]+$rowData[0][6]))*100,2);
				       		$sum = round($pres_rfid+$pres_inputbap+$rowData[0][8]+$rowData[0][10],2);
				       		$numberset = 4;
				       		$kinerja_dosen = round($sum/$numberset,2);
				          	$data = array(
					                "id_dosen"=> convertkode($rowData[0][0]),
					                "rfid" => $rowData[0][1],
					                "total"=> $rowData[0][2],
					                "non_rfid" => $rowData[0][3],
					                "belum_submit" => $rowData[0][5],
					                "sudah_submit" => $rowData[0][6],
					                "pres_waktubap" => $rowData[0][8],
					                "pres_kehadiran" => $rowData[0][10],
					                "pres_rfid"	=> $pres_rfid,
					                "pres_inputbap" => $pres_inputbap,
					                "kinerja_dosen" => $kinerja_dosen,
					                "semester" => getsemester_aktif()
				            );
				        	$this->db->insert('t_bap', $data);
				       }
				       if($cek2 > 0){
				       		$data2 = array(
					                "nilai_bap" => $kinerja_dosen
				            );
				        	$this->yeah->update_bap2(convertkode($rowData[0][0]),getsemester_aktif(),$data2);
				        } else{
				        	$data2 = array(
					                "id_dosen"=> convertkode($rowData[0][0]),
					                "nilai_bap" => $kinerja_dosen,
					                "id_semester" => getsemester_aktif()
				            );
				        	$this->db->insert('t_nilai', $data2);
				        }
		        	}
			     
			    }    
	        }
	        redirect('kaprodi/lihat_bap');
	    }
	    
	}

	public function lihat_bap(){
		$rows = $this->yeah->getBap()->result();
		$data = array(
			'title' => 'Lihat BAP',
			'rows' => $rows
		);
		$this->load->view('kaprodi/bap/lihat_bap', $data);
	}

	public function download_bap(){
		$this->load->helper('download');
		$fileName = 'BAP.xlsx';
	    force_download('uploadfile/template/'.$fileName, NULL);

	}
	//akhir kelola bap

	public function nilai_rapat_dosen(){
		$rows  = $this->yeah->NilaiRapat()->result();
        echo json_encode($rows);
	}
	public function edit_nilai_rapat(){
		$rows  = $this->yeah->EditNilaiRapat($this->input->get('id'));
        echo json_encode($rows);
	}

	
/*	public function edit_kontribusi(){
	$rows = $this->yeah->EditKontribusi($this->input->get('id'));
	echo json_encode($rows);
	}*/







public function hitungnilai_kontribusi($id,$pro){

if(cek_kpi('5')){

	$kategori = $this->yeah->getKPI('5')->result();
	foreach($kategori as $res){
		if($pro > $res->kondisi){
		
			$nil = $res->nilai;
			$this->yeah->update_nilai($nil,$id,'nilai_kk',getsemester_aktif());
			break;
		}else{
			
			$nil = 0;
			$this->yeah->update_nilai($nil,$id,'nilai_kk',getsemester_aktif());
		}
	}
	$this->session->set_flashdata('msg','Sukses ...!!');
	}
}

function delete_files($path){
	   		$files = glob($path.'*'); // get all file names
			  foreach($files as $file){ // iterate files
		      if(is_file($file))
		        unlink($file); // delete file
		}
	       
	}



//Bagian farizt
//lihat rps
public function lihat_rps(){
	$rows = $this->yeah->getrps();
	$data = array(
		'title' => 'List RPS',
		'rows' => $rows
	);
	$this->load->view('kaprodi/rps/lihat_rps', $data);
}

//view rps using ajax
public function view_rps(){
	$data['results'] = $this->yeah->getrps();
	$data['jumlah'] = count($data['results']); 
	echo json_encode($data);
}
public function download_rps(){
	$this->load->helper('download');
	$fileName = $this->uri->segment(3);
    force_download('uploadfile/rps/'.$fileName, NULL);

}
public function lihat_status_rps(){
	$rows = $this->yeah->getStatusRps();
	$data = array(
		'rows' => $rows
	);
}
public function updateStatusRps($id){
	$data = array(
		'status_rps' => 'APPROVE'
	);
	$data2 = array(
		'status' => 'SETUJU'
	);
	$this->yeah->update_data_rps($id, $data, $data2);
	$this->hitungnilai_rps($id);
	redirect('kaprodi/lihat_rps');
	//nilai id dosen, itung tanggal, insert nilai 

}
public function revisi_rps(){
	$id_revisi = $this->input->post('identitas');
	$input = $this->input->post('keterangan');
	$this->yeah->update_revisi_rps($id_revisi,html_escape(urldecode($input)));

}

// hitung nilai rps
public function hitungnilai_rps($idrps){
// ubah
if(cek_kpi('6')){ 
		$kategori = $this->yeah->getKPI('6')->result();
		$tgl_upload = $this->yeah->TglUpload('id_rps',$idrps,'t_rps','t_matakuliah','t_matakuliah.id_matakuliah = t_rps.id_matakuliah','t_pengajaran','t_pengajaran.id_matakuliah = t_matakuliah.id_matakuliah','status_koordinator','y');  //ngambil tgl upload
			
				foreach($tgl_upload as $res2){
					foreach($kategori as $res){
					$now = strtotime($res->kondisi); // tanggal deadline

					$your_date = strtotime($res2->date);  //tgl upload
					$datediff = $now - $your_date;

					$selisih =  round($datediff / (60 * 60 * 24));
					//echo $selisih.'<br>';
					
					$nilai = $res->nilai; //100

						if($selisih < 0){
							$nilai = $res->nilai - (10*$selisih*-1);
							if($nilai <= 60){
								$nilai = 60;
							}		
						}
					}

				   	$where = 'nilai_rps'; 
					$idd = $res2->id_dosen;
					$idsem  = getsemester_aktif();
					$idmatkul = $res2->id_matakuliah;
					$this->yeah->update_nilai_koor($nilai,$idd,$where,$idsem,$idmatkul);
					}

		}
}


//lihat modul praktikum
public function lihat_modul(){
	$rows  = $this->yeah->getmodul();
	$data = array(
		'title' => 'List Modul Praktikum',
		'rows' => $rows
	);
	$this->load->view('kaprodi/modul/lihat_modul', $data);
}
//vuew modul using ajax
public function view_modul(){
	$data['results'] = $this->yeah->getmodul();
	$data['jumlah'] = count($data['results']); 
	echo json_encode($data);
}
public function lihat_status_modul(){
	$rows = $this->yeah->getStatusModul();
	$data = array(
		'rows' => $rows
	);
}
public function updateStatusModul($id){
	$data = array(
		'status_modul' => 'APPROVE'
	);
	$data2 = array(
		'status' => 'SETUJU'
	);
	$this->yeah->update_data_modul($id, $data, $data2);
	$this->hitungnilai_modul($id);
	redirect('kaprodi/lihat_modul');
}
public function download_modul(){
	$this->load->helper('download');
    $fileName = $this->uri->segment(3);
    force_download('uploadfile/modul/'.$fileName, NULL);

}
public function revisi_modul(){
	$id_revisi = $this->input->post('identitas');
	$input = $this->input->post('keterangan');
	$this->yeah->update_revisi_modul($id_revisi,html_escape(urldecode($input)));
}

//hitung nilai modul
public function hitungnilai_modul($idmodul){
	if(cek_kpi('7')){ 

		$kategori = $this->yeah->getKPI('7')->result();
		$tgl_upload = $this->yeah->TglUpload('id_modul',$idmodul,'t_modul','t_matakuliah','t_matakuliah.id_matakuliah = t_modul.id_matakuliah','t_pengajaran','t_pengajaran.id_matakuliah = t_matakuliah.id_matakuliah','status_koordinator','y');
		foreach($tgl_upload as $res2){
		foreach($kategori as $res){
			$now = strtotime($res->kondisi); // tanggal deadline

			$your_date = strtotime($res2->date);  //tgl upload
			$datediff = $now - $your_date;

			$selisih =  round($datediff / (60 * 60 * 24));
			//echo $selisih.'<br>';

			$nilai = $res->nilai;

				if($selisih < 0){
					$nilai = $res->nilai - (10*$selisih*-1);
					if($nilai <= 60){
					$nilai = 60;
					}
						
				}
			}
		   	$where = 'nilai_modul'; 
			$idd = $res2->id_dosen;
			$idsem  = getsemester_aktif();
			$idmatkul = $res2->id_matakuliah;
			$this->yeah->update_nilai_koor($nilai,$idd,$where,$idsem,$idmatkul);
		}
	}
}


//lihat soal asessment
public function lihat_soal(){
	$rows = $this->yeah->getsoal();
	$data = array(
		'title' => 'List Soal Asessment',
		'rows' => $rows
	);
	$this->load->view('kaprodi/soal/lihat_soal', $data);
}
//view soal ajax
public function view_soal(){
	$data['results'] = $this->yeah->getsoal();
	$data['jumlah'] = count($data['results']); 
	echo json_encode($data);
}
public function lihat_status_soal(){
	$rows = $this->yeah->getStatusSoal();
	$data = array(
		'rows' => $rows
	);
}
public function updateStatusSoal($id){
	$data = array(
		'status_soal' => 'APPROVE'
	);
	$data2 = array(
		'status' => 'SETUJU'
	);
	$this->yeah->update_data_soal($id, $data, $data2);
	$this->hitungnilai_soal($id);
	redirect('kaprodi/lihat_soal');
}
public function download_soal(){
	$this->load->helper('download');
	//$data = file_get_contents(base_url('/uploadfile/soal/'.$fileName));
    //force_download($fileName, $data);
    $fileName = $this->uri->segment(3);
    force_download('uploadfile/soal/'.$fileName, NULL);

}
public function revisi_soal(){
	$id_revisi = $this->input->post('identitas');
	$input = $this->input->post('keterangan');
	$this->yeah->update_revisi_soal($id_revisi,html_escape(urldecode($input)));
}

//hitung nilai soal
public function hitungnilai_soal($idsoal){
	if(cek_kpi('8')){ 

		$kategori = $this->yeah->getKPI('8')->result();
		$tgl_upload = $this->yeah->TglUpload('id_soal',$idsoal,'t_soal','t_matakuliah','t_matakuliah.id_matakuliah = t_soal.id_matakuliah','t_pengajaran','t_pengajaran.id_matakuliah = t_matakuliah.id_matakuliah','status_koordinator','y');
		foreach($tgl_upload as $res2){
		foreach($kategori as $res){
			$now = strtotime($res->kondisi); // tanggal deadline

			$your_date = strtotime($res2->date);  //tgl upload
			$datediff = $now - $your_date;

			$selisih =  round($datediff / (60 * 60 * 24));
			//echo $selisih.'<br>';

			$nilai = $res->nilai;

				if($selisih < 0){
					$nilai = $res->nilai - (10*$selisih*-1);
					if($nilai <= 60){
					$nilai = 60;
					}
						
				}
			}
		   	$where = 'nilai_soal'; 
			$idd = $res2->id_dosen;
			$idsem  = getsemester_aktif();
			$idmatkul = $res2->id_matakuliah;
			$this->yeah->update_nilai_koor($nilai,$idd,$where,$idsem,$idmatkul);
		}
	}
}

//bahan perkuliahan
public function lihat_bahan(){
	$rows  = $this->yeah->getbahan();
	$data = array(
		'title' => 'List Bahan Perkuliahan',
		'rows' => $rows
	);
	$this->load->view('kaprodi/bahan/lihat_bahan', $data);
}
public function view_bahan(){
	$data['results'] = $this->yeah->getbahan();
	$data['jumlah'] = count($data['results']); 
	echo json_encode($data);
}
public function lihat_status_bahan(){
	$rows = $this->yeah->getStatusBahan();
	$data = array(
		'rows' => $rows
	);
}
public function updateStatusBahan($id){
	$data = array(
		'status_bahan' => 'APPROVE'
	);
	$data2 = array(
		'status' => 'SETUJU'
	);
	$this->yeah->update_data_bahan($id, $data, $data2);
	$this->hitungnilai_bahan($id);
	redirect('kaprodi/lihat_bahan');
}
public function download_bahan(){
	$this->load->helper('download');
    $fileName = $this->uri->segment(3);
    force_download('uploadfile/bahan/'.$fileName, NULL);

}
public function revisi_bahan(){
	$id_revisi = $this->input->post('identitas');
	$input = $this->input->post('keterangan');
	$this->yeah->update_revisi_bahan($id_revisi,html_escape(urldecode($input)));
}

//hitung nilai bahan perkuliahan
public function hitungnilai_bahan($idbahan){
	if(cek_kpi('9')){ 

		$kategori = $this->yeah->getKPI('9')->result();
		$tgl_upload = $this->yeah->TglUpload('id_bahan',$idbahan,'t_bahanperkuliahan','t_matakuliah','t_matakuliah.id_matakuliah = t_bahanperkuliahan.id_matakuliah','t_pengajaran','t_pengajaran.id_matakuliah = t_matakuliah.id_matakuliah','status_koordinator','y');
		foreach($tgl_upload as $res2){
		foreach($kategori as $res){
			$now = strtotime($res->kondisi); // tanggal deadline

			$your_date = strtotime($res2->date);  //tgl upload
			$datediff = $now - $your_date;

			$selisih =  round($datediff / (60 * 60 * 24));
			//echo $selisih.'<br>';

			$nilai = $res->nilai;

				if($selisih < 0){
					$nilai = $res->nilai - (10*$selisih*-1);
					if($nilai <= 60){
					$nilai = 60;
					}
						
				}
			}
		   	$where = 'nilai_bahan'; 
			$idd = $res2->id_dosen;
			$idsem  = getsemester_aktif();
			$idmatkul = $res2->id_matakuliah;
			$this->yeah->update_nilai_koor($nilai,$idd,$where,$idsem,$idmatkul);
		}
	}
}

//get ajax notif
public function data_notif(){
	//$data['results_rps'] = $this->yeah->get_data_rps();
	$data['jumlah_rps'] = count($this->yeah->get_data_rps()); 
	

	//$data['results_modul'] = $this->yeah->get_data_modul();
	$data['jumlah_modul'] = count( $this->yeah->get_data_modul()); 


	//$data['results_perwalian'] = $this->yeah->get_data_perwalian();
	$data['jumlah_perwalian'] = count($this->yeah->get_data_perwalian()); 

	//$data['results_lks'] = $this->yeah->get_data_lks();
	$data['jumlah_lks'] = count($this->yeah->get_data_lks()); 

	//$data['results_soal'] = $this->yeah->get_data_soal();
	$data['jumlah_soal'] = count($this->yeah->get_data_soal()); 


	//$data['results_bahan'] = $this->yeah->get_data_bahan();
	$data['jumlah_bahan'] = count($this->yeah->get_data_bahan()); 
	echo json_encode($data);
}


public function logout(){
	session_destroy();
	redirect('login');
}


public function LaporanPerwalian(){
	$this->db->select('*')
		 	 ->where('id_semester',getsemester_aktif());
	$x = $this->db->get('t_perwalian');

	$data = array(
		'title' => 'Data Perwalian',
		'rows' => $x->result()

	);
	$this->load->view('kaprodi/perwalian/laporan_perwalian',$data);
}

public function revisi_perwalian($id,$val){
		$this->yeah->update_status_file($id,html_escape(urldecode($val)),'rejected','t_perwalian','id_perwalian');

		redirect('kaprodi/LaporanPerwalian/'.$id);
	}

public function revisi_perwalian2($id){
		$this->yeah->update_status_file($id,NULL,'approved','t_perwalian','id_perwalian');
		hitungnilai_perwalian(getiddosen_perwalian($id));
		redirect('kaprodi/LaporanPerwalian/'.$id);
	}

public function LKS(){
	$sem = getsemester_aktif();

	$this->db->select('*')
		 	 ->where('id_semester',getsemester_aktif());
	$x = $this->db->get('t_lks');

	$data = array(
		'title' => 'LKS',
		'rows' => $x->result()

	);
	$this->load->view('kaprodi/lks/laporan_lks',$data);
}

//kelola angkatan
function lihat_angkatan(){
	$rows = $this->yeah->getAngkatan();
	$data = array(
		'title' => 'List Angkatan',
		'rows' => $rows 
	);
	$this->load->view('kaprodi/angkatan/tambah_angkatan', $data);
}
function tambah_angkatan(){
	$id = $this->db->query("select id_prodi from t_prodi")->row();
	$angkatan = $this->input->post('angkatan');
	$cek = $this->db->query("select * from t_angkatan where angkatan = '$angkatan'")->num_rows();
	if ($cek > 0) {
		$this->session->set_flashdata('msg','not ok');
		redirect('kaprodi/lihat_angkatan');
	}else{
		$data = array(
			'id_prodi' => html_escape($id->id_prodi),
			'angkatan' => html_escape($this->input->post('angkatan')),
			'jumlah' => html_escape($this->input->post('jumlah'))
		);
		$this->yeah->add($data,'t_angkatan');
		$this->session->set_flashdata('msg','ok');
		redirect('kaprodi/lihat_angkatan');
	}
}
function update_angkatan(){
	$id_angkatan = $this->input->post('id_angkatan');
	$angkatan = $this->input->post('angkatan');
	$jumlah = $this->input->post('jumlah');
	//$cek = $this->db->query("select * from t_angkatan where angkatan = '$angkatan'")->num_rows();
	
		$data = array(
			'angkatan' => $angkatan,
			'jumlah' => $jumlah
		);

		$where = array('id_angkatan' => $id_angkatan);
		$this->yeah->update_angkatan($where,$data,'t_angkatan');
		$this->session->set_flashdata('msg','ok');
		redirect('kaprodi/lihat_angkatan');
}
public function hapus_angkatan($id_angkatan){
	$where = array('id_angkatan' => $id_angkatan);
	$this->yeah->hapus_angkatan($where, 't_angkatan');
	redirect('kaprodi/lihat_angkatan');
}
//batas kelola angkatan

public function revisi_lks($id,$val){
		$this->yeah->update_status_file($id,html_escape(urldecode($val)),'rejected','t_lks','id_lks');

		redirect('kaprodi/LKS/'.$id);
	}
public function revisi_lks2($id){
		$this->yeah->update_status_file($id,NULL,'approved','t_lks','id_lks');

		hitungnilai_lks($id);
		redirect('kaprodi/LKS/'.$id);
	}


}