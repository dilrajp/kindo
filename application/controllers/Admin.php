<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		 parent::__construct();
		 
        if(!isset($_SESSION['username'])){
            redirect('login');
        }
		$this->load->helper('raj');
		$this->load->model('M_admin','yeah');
		$this->session->set_userdata('info','Admin');
		if($this->session->level != '5'){
			redirect('dosen');
		};
	}

	public function index()
	{
		//$rows = $this->yeah->getById('id_matakuliah',$this->session->username,'t_matakuliah')->result();
		//$rows = $this->yeah->getById($this->session->username)->result();
		$rows = $this->yeah->all()->result();
		$rows2 = $this->yeah->all2()->result();
		$data = array(
            'title' => 'Dashboard Admin',
            'rows' => $rows,
            'rows2'=> $rows2
            );
		$this->load->view('admin/dashboard_admin',$data);
	}	
	public function tambah_dosen(){
		$data = array(
			'title' => 'Add Dosen'
		);
		$this->load->view('admin/tambah_dosen', $data);
	}

	public function tambah_dosen_aksi(){
		$kode_dosen = $this->input->post('kode_dosen');
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$email_dosen = $this->input->post('email_dosen');
		$no_telp = $this->input->post('no_telp');
		$status = $this->input->post('status');
		$level = $this->input->post('level');
		$password = md5($this->input->post('password'));
		
		$config['upload_path']='./assets/images/foto';
		$config['allowed_types']='gif|jpg|png';
		$config['max_size']='1024';
		$config['max_width']='1024';
		$config['max_height']='1024';

		$this->load->library('upload',$config);
		
		if(! $this->upload->do_upload()){
			$this->load->view('admin/tambah_dosen');
		}else{
			$gambar = $this->upload->data();
			$data = array(
				'kode_dosen' => $kode_dosen,
				'nip' => $nip,
				'nama_dosen' => $nama_dosen,
				'email_dosen' => $email_dosen,
				'no_telp' => $no_telp,
				'status' => $status,
				'level' => $level,
				'password' => $password,
				'foto' => $gambar['file_name']
			);
		$this->yeah->input_data2($data, 't_dosen');
		redirect('admin/lihat_dosen');
		}
	}

	public function lihat_dosen(){
		$rows2 = $this->yeah->all2()->result();

		$data = array(
            'title' => 'List Dosen',
            'rows' => $rows2
            );
		$this->load->view('admin/lihat_dosen', $data);
	}

	public function update_dosen(){
		$id_dosen = $this->input->post('id_dosen');
		$kode_dosen = $this->input->post('kode_dosen');
		$nip = $this->input->post('nip');
		$nama_dosen = $this->input->post('nama_dosen');
		$email_dosen = $this->input->post('email_dosen');
		$no_telp = $this->input->post('no_telp');
		$status = $this->input->post('status');

			$data = array(
				'kode_dosen' => $kode_dosen,
				'nip' => $nip,
				'nama_dosen' => $nama_dosen,
				'email_dosen' => $email_dosen,
				'no_telp' => $no_telp,
				'status' => $status
			);

			$where = array('id_dosen' => $id_dosen);
			$this->yeah->update_data2($where,$data,'t_dosen');
			redirect('admin/lihat_dosen');
	}

	public function hapus_dosen($id_dosen){
		$where = array('id_dosen' => $id_dosen);
		$this->yeah->hapus_data2($where, 't_dosen');
		redirect('admin/lihat_dosen');
	}

}