<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koor_PA extends CI_Controller {

	public function __construct(){
		 parent::__construct();
		 
        if(!isset($_SESSION['username'])){
            redirect('login');
        }
		$this->load->helper('raj');
		$this->load->model('M_koorpa','yeah');
		$this->session->set_userdata("info","Koor PA");
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->helper(array('form', 'url'));

		if($this->session->level != '3'){
			if($this->session->level != '4')
			{
				redirect('dosen');
			}
		};
	}

	public function index()
	{	
		$rows = $this->yeah->getById('nip',$this->session->username,'t_dosen')->result();
		$tahun = $this->db->query('SELECT DISTINCT tahunajaran from t_kontribusi join t_tahunajaran on(t_tahunajaran.id_tahunajaran = t_kontribusi.tahun) order by tahunajaran ASC');
		$jumlah = $this->db->query('SELECT sum(total_mhs) as tot from t_kontribusi join t_tahunajaran on(t_tahunajaran.id_tahunajaran = t_kontribusi.tahun) group by tahun order by tahunajaran ASC')->result();

		$data = array(
            'title' => 'Dashboard Koordinator MK',
            'action' => site_url('login/check'),
            'rows' => $rows,
            'tahun' => $tahun,
            'jumlah' => $jumlah,
            );
		$this->load->view('koor_pa/dashboard_koorpa',$data);
	}

	public function plotting(){

		$all_dosen = $this->yeah->all_dosen()->result();
		$data = array(
			'title' => 'Plotting Dosen Pembimbing',
			'rows' => $all_dosen
		);
		$this->load->view('koor_pa/plotting/view_plotting',$data);
	}
	public function download_template_pa(){
		$this->load->helper('download');
		force_download('arsip/Plotting Pembimbing PA.xlsx',NULL);
	}
	public function download_template_de(){
		$this->load->helper('download');
		force_download('arsip/Template_Jadwal DE.xlsx',NULL);
	}
	public function download_template_sidang(){
		$this->load->helper('download');
		force_download('arsip/Template_Jadwal Sidang.xlsx',NULL);
	}
	public function download_template_sk(){
		$this->load->helper('download');
		force_download('arsip/Template_Pengajuan SK.xlsx',NULL);
	}
	public function download_template_yudisium(){
		$this->load->helper('download');
		force_download('arsip/Template_Yudisium.xlsx',NULL);
	}
	public function view_doping(){
		$rows  = $this->yeah->all('id_dosen','t_dosen')->result();
		
		$data = array(
			'title' => 'Data Dosen Pembimbing',
			'rows' => $rows
			);
		$this->load->view('koor_pa/plotting/view_doping',$data);
	}
	public function view_mhs(){
		$rows  = $this->yeah->all('id_mhs','t_mhs_pa')->result();
		$nama = $this->yeah->all_dosen('id_dosen','t_dosen')->result();
		$tahun = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$data = array(
			'title' => 'Data Mahasiswa Proyek Akhir',
			'rows' => $rows,
			'nama' => $nama,
			'tahun' => $tahun,
			'alert' => ''
			);
		$this->load->view('koor_pa/view_mahasiswa',$data);
	}
	public function ListMahasiswaPA(){
		$rows  = $this->yeah->all('id_mhs','t_mhs_pa')->result();
		$nama = $this->yeah->all_dosen('id_dosen','t_dosen')->result();
		$tahun = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$data = array(
			'title' => 'Data Mahasiswa Proyek Akhir',
			'rows' => $rows,
			'nama' => $nama,
			'tahun' => $tahun,
			'alert' => ''
			);
		$this->load->view('koor_pa/view_mahasiswa2',$data);
	}

	public function data_mhs(){
    	$rows  = $this->yeah->data_mhs()->result();
        echo json_encode($rows);
    }
    public function edit_mhss(){
    	$rows  = $this->yeah->edit_mhs($this->input->get('id'));
        echo json_encode($rows);
    }

    public function data_jadwal_de(){
    	$rows  = $this->yeah->data_jadwal_de()->result();
        echo json_encode($rows);
    }
    public function edit_jadwal_de(){
    	$rows = $this->yeah->edit_jadwal_de($this->input->get('id'));
    	echo json_encode($rows);
    }

    public function proses_edit_de(){
    	if($this->input->post('puj1') == $this->input->post('puj2')){
		$this->session->set_flashdata('msg','puj1=puj2'); 
		redirect('koor_pa/DeskEvaluation');

		} else {
		$data = array(
			'periode' => html_escape($this->input->post('periode')),
			'tanggal' => html_escape($this->input->post('tanggal')),
			'PUJ1' =>  html_escape($this->input->post('puj1')),
			'PUJ2' =>	html_escape($this->input->post('puj2'))
			);
		$this->yeah->update($data,'t_jadwal_de',$this->input->post('id'),'id_jadwal_de');
		$this->hitungnilai_de2( html_escape($this->input->post('puj1')));
		$this->hitungnilai_de2( html_escape($this->input->post('puj2')));
		$this->hitungnilai_de2( html_escape($this->input->post('puj1-lama')));
		$this->hitungnilai_de2( html_escape($this->input->post('puj2-lama')));
		redirect('koor_pa/DeskEvaluation');
		}
    }


    public function proses_edit_sidang(){
    	if($this->input->post('puj1') == $this->input->post('puj2')){
		$this->session->set_flashdata('msg','puj1=puj2'); 
		redirect('koor_pa/Sidang');
		} else {
		$data = array(
			'waktu' => html_escape($this->input->post('waktu')),
			'tanggal' => html_escape($this->input->post('tanggal')),
			'PUJ1' =>  html_escape($this->input->post('puj1')),
			'PUJ2' =>	html_escape($this->input->post('puj2')),
			'ruangan' =>	html_escape($this->input->post('ruangan'))
			);
		$this->yeah->update($data,'t_jadwal_sidang',$this->input->post('id'),'id_jadwal_sidang');
		$this->hitungnilai_sidang2( html_escape($this->input->post('puj1')));
		$this->hitungnilai_sidang2( html_escape($this->input->post('puj2')));
		$this->hitungnilai_sidang2( html_escape($this->input->post('puj1-lama')));
		$this->hitungnilai_sidang2( html_escape($this->input->post('puj2-lama')));
		redirect('koor_pa/Sidang');
		}
    }

    public function proses_add_de(){
    	if($this->input->post('puj1') == $this->input->post('puj2')){
		$this->session->set_flashdata('msg','puj1=puj2'); 
		redirect('koor_pa/DeskEvaluation');

		} else {
		$data = array(
			'id_mhs' => html_escape($this->input->post('id_mhs')),
			'periode' => html_escape($this->input->post('periode')),
			'tanggal' => html_escape($this->input->post('tanggal')),
			'PUJ1' =>  html_escape($this->input->post('puj1')),
			'PUJ2' =>	html_escape($this->input->post('puj2')),
		    'tahun' => gettahun_aktif(),
			);
		$this->yeah->add($data,'t_jadwal_de',$data);
		$this->yeah->update(array('tahap' => '2'), 't_mhs_pa',$this->input->post('id_mhs'),'id_mhs');
		$this->hitungnilai_de2( html_escape($this->input->post('puj1')));
		$this->hitungnilai_de2( html_escape($this->input->post('puj2')));
		redirect('koor_pa/DeskEvaluation');
		}
    }

    public function proses_add_sidang(){
    	if($this->input->post('puj1') == $this->input->post('puj2')){
		$this->session->set_flashdata('msg','puj1=puj2'); 
		redirect('koor_pa/Sidang');

		} else {
		$data = array(
			'id_mhs' => html_escape($this->input->post('id_mhs')),
			'waktu' => html_escape($this->input->post('waktu')),
			'tanggal' => html_escape($this->input->post('tanggal')),
			'PUJ1' =>  html_escape($this->input->post('puj1')),
			'PUJ2' =>	html_escape($this->input->post('puj2')),
		    'tahun' => gettahun_aktif(),
		    'ruangan' =>	html_escape($this->input->post('ruangan'))
			);
		$this->yeah->add($data,'t_jadwal_sidang',$data);
		$this->yeah->update(array('tahap' => '6'), 't_mhs_pa',$this->input->post('id_mhs'),'id_mhs');
		$this->hitungnilai_sidang2( html_escape($this->input->post('puj1')));
		$this->hitungnilai_sidang2( html_escape($this->input->post('puj2')));
		redirect('koor_pa/Sidang');
		}
    }


    public function data_jadwal_sidang(){
    	$rows  = $this->yeah->data_jadwal_sidang()->result();
        echo json_encode($rows);
    }

    public function edit_jadwal_sidang(){
    	$rows = $this->yeah->edit_jadwal_sidang($this->input->get('id'));
    	echo json_encode($rows);
    }

	public function add_mhs(){
		if($this->input->post('p1') == $this->input->post('p2')){
		$rows  = $this->yeah->all('id_mhs','t_mhs_pa')->result();
		$nama = $this->yeah->all_dosen('id_dosen','t_dosen')->result();
		$tahun = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$data = array(
			'title' => 'Data Dosen Pembimbing',
			'rows' => $rows,
			'nama' => $nama,
			'tahun' => $tahun,
			'alert' => 'fail'
			);
		$this->load->view('koor_pa/view_mahasiswa2',$data);
		} else {
		$data = array(
			'nim_mhs' => html_escape($this->input->post('nim-add')),
			'nama_mhs' => html_escape($this->input->post('nama-add')),
			'judul_pa' =>  html_escape($this->input->post('judul-add')),
			'grup' =>	html_escape($this->input->post('grup-add')),
			'id_doping1' =>  html_escape($this->input->post('p1')),
			'id_doping2' => html_escape( $this->input->post('p2')),
			'id_tahunajaran' => html_escape($this->input->post('p3')),
			'tahap' => html_escape($this->input->post('tahap')),
			'angkatan' => html_escape($this->input->post('angkatan-add'))
			);
		$this->yeah->add($data,'t_mhs_pa');
		$this->hitungnilaibimb2($this->input->post('p1'));
		$this->hitungnilaibimb2($this->input->post('p2'));
		redirect('koor_pa/ListMahasiswaPA');
		}
	}

	public function edit_mhs(){
		if($this->input->post('p1') == $this->input->post('p2')){
		$rows  = $this->yeah->all('id_mhs','t_mhs_pa')->result();
		$nama = $this->yeah->all_dosen('id_dosen','t_dosen')->result();
		$tahun = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$data = array(
			'title' => 'Data Dosen Pembimbing',
			'rows' => $rows,
			'nama' => $nama,
			'tahun' => $tahun,
			'alert' => 'fail edit'
			);
		$this->load->view('koor_pa/view_mahasiswa2',$data);
		} else {
		$data = array(
			'nim_mhs' => html_escape($this->input->post('nim')),
			'nama_mhs' => html_escape($this->input->post('nama')),
			'judul_pa' =>  html_escape($this->input->post('judul')),
			'grup' =>	html_escape($this->input->post('grup')),
			'id_doping1' =>  html_escape($this->input->post('p1')),
			'id_doping2' => html_escape( $this->input->post('p2')),
			'id_tahunajaran' => html_escape($this->input->post('p3')),
			'tahap' => html_escape($this->input->post('tahap')),
			'angkatan' => html_escape($this->input->post('angkatan'))
			);
		$this->yeah->update($data,'t_mhs_pa',$this->input->post('id'),'id_mhs');
		//update nilai kk
		if(html_escape($this->input->post('tahap')) == 7){
		$taun = date("Y");
		$yea = gettahun_aktif();
		$aang = html_escape($this->input->post('angkatan'));
		$x = $taun - $aang;
		if($x <= 3){
		$p1 = html_escape($this->input->post('p1'));
		$p2 = html_escape($this->input->post('p2'));
		$jml_mhs_lulus_p1 = $this->db->query("SELECT COUNT(*) as 'jum' FROM t_mhs_pa WHERE (id_doping1 = '$p1' or id_doping2 = '$p1' ) AND tahap = '7' AND angkatan = '$aang'  ")->row_array();
		$jml_mhs_lulus_p2 = $this->db->query("SELECT COUNT(*) as 'jum' FROM t_mhs_pa WHERE (id_doping1 = '$p2' or id_doping2 = '$p2' ) AND tahap = '7' AND angkatan = '$aang'  ")->row_array();
		$jum1 = $jml_mhs_lulus_p1['jum'];
		$jum2 = $jml_mhs_lulus_p2['jum'];
		$cek1 = $this->db->query("SELECT * from t_kontribusi WHERE id_dosen = '$p1' and tahun = '$yea'");
		$cek2 = $this->db->query("SELECT * from t_kontribusi WHERE id_dosen = '$p1' and tahun = '$yea'");
		if($cek1->num_rows() > 0 ){
		$this->db->query("UPDATE `t_kontribusi` SET `total_mhs` = '$jum1' WHERE `t_kontribusi`.`id_dosen` = '$p1'  and tahun = '$yea'");
		} else{
		$this->db->query("INSERT INTO `t_kontribusi` ( `id_dosen`, `total_mhs`, `jml_bimbingan`, `prosentase`, `tahun`) VALUES ('$p1', '$jum1', '0', '0', '$yea')");
		}
		if($cek2->num_rows() > 0 ){
		$this->db->query("UPDATE `t_kontribusi` SET `total_mhs` = '$jum2' WHERE `t_kontribusi`.`id_dosen` = '$p2' and tahun = '$yea'");
		}else{
		$this->db->query("INSERT INTO `t_kontribusi` ( `id_dosen`, `total_mhs`, `jml_bimbingan`, `prosentase`, `tahun`) VALUES ('$p2', '$jum2', '0', '0', '$yea')");
		}
		 $this->hitungnilai_kontribusi($p1,$jum1);
		 $this->hitungnilai_kontribusi($p2,$jum2);
		}

		}

		//end nilai kk	
		$this->hitungnilaibimb2($this->input->post('p1'));
		//$this->hitungnilaibimb();
		$this->hitungnilaibimb2($this->input->post('p2'));
		redirect('koor_pa/ListMahasiswaPA');
		}
	}

	public function hitungnilai_kontribusi($id,$jum){

	if(cek_kpi('5')){

		$kategori = $this->yeah->getKPI('5')->result();
		foreach($kategori as $res){
			$total_bimb = total_bimb_tahun($id);
			$yea = gettahun_aktif();
			$this->db->query("UPDATE `t_kontribusi` SET `jml_bimbingan` = '$total_bimb' WHERE `t_kontribusi`.`id_dosen` = '$id'  and tahun = '$yea'");
			$pro = round(($jum *100 )/$total_bimb,2);
			if($pro > $res->kondisi){
			
				$nil = $res->nilai;
				$this->db->query("UPDATE `t_kontribusi` SET `prosentase` = '$pro' WHERE `t_kontribusi`.`id_dosen` = '$id'  and tahun = '$yea'");
				$this->yeah->update_nilai($id,'t_nilai',$nil,'nilai_kk',getsemester_aktif());
				break;
			}else{
				
				$nil = 0;
				$this->db->query("UPDATE `t_kontribusi` SET `prosentase` = '$pro' WHERE `t_kontribusi`.`id_dosen` = '$id'  and tahun = '$yea'");
				$this->yeah->update_nilai($id,'t_nilai',$nil,'nilai_kk',getsemester_aktif());
			}
		}
		//$this->session->set_flashdata('msg','Sukses ...!!');
		}
	}

	public function detail_doping($id){
		$rows = $this->yeah->getById('id_dosen',$id,'t_dosen')->result();
		$rows2 = $this->yeah->getmhsby($id)->result();
		$data = array(
			'title' => 'Detail Dosen Pembimbing',
			'rows' => $rows,
			'rows2' => $rows2,
			'id_dosen' => $id
			);
		$this->load->view('koor_pa/plotting/detail_doping',$data);
	}

	public function createdata(){
	
	$fileName = $this->input->post('file', TRUE);
     
    $config['upload_path'] = './uploadfile/';  //buat folder dengan nama arsip di root folder
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'xls|xlsx|csv';
    $config['max_size'] = 100000;

    $this->load->library('upload', $config);
	$this->upload->initialize($config); 
     
    if(! $this->upload->do_upload('file') ){
	    $error = array('error' => $this->upload->display_errors());
	    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
		redirect('koor_pa/plotting');
	} else {
	   	$media = $this->upload->data();
	    $inputFileName = './uploadfile/'.$media['file_name'];
        
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
         
    	for ($row = 2; $row <= $highestRow; $row++){
    	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
    	if($rowData[0][0]){
    		if($rowData[0][0] == 'No.' && $rowData[0][1] == 'NIM' && $rowData[0][2] == "NAMA" && $rowData[0][3] == "NO GROUP" && $rowData[0][4] == "JUDUL PA" && $rowData[0][5]  == "PBB 1" && $rowData[0][6] == "PBB 2" && $rowData[0][7] == "Angkatan"  )
    	
  				  {   
     for ($row = 4; $row <= $highestRow; $row++){//  Read a row of data into an array                 
    
    	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
    	        if($rowData[0][0]){
    		        //Sesuaikan sama nama kolom tabel di database                                
    	          	
    
    	          	$data = array(
    		                "nim_mhs"=> $rowData[0][1],
    		                "nama_mhs"=> $rowData[0][2],
    		                "judul_pa" =>  $rowData[0][4],
    		                "id_doping1" =>  convertkode($rowData[0][5]),
    		                "id_doping2" => convertkode($rowData[0][6]),
    		                "tahap" => 1,
    		                "angkatan" => $rowData[0][7],
    		                "id_tahunajaran" => gettahun_aktif(),
    		                "grup" =>  $rowData[0][3],
    	            	);
    		       
     		        //delete_files($upload_data['file_path']);
    		        $nim_mhs_pa = $rowData[0][1]; 
    	          	$cek = $this->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs = '$nim_mhs_pa'");
    	          	if($cek->num_rows() > 0){
    	          	$qas = $cek->row();
    	          	$this->yeah->update($data,'t_mhs_pa',$qas->id_mhs,'id_mhs');
    	            }else{
    	            $this->db->insert('t_mhs_pa', $data);
    	        	}
    
    
    	        }
    
            }}else {
            $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
            redirect('koor_pa/plotting');
            }
    	}
        $this->hitungnilaibimb();
        $this->delete_files($media['file_path']); 
       
        	$this->session->set_flashdata('msg','ok');
        redirect('koor_pa/plotting');
    }
}
}
public function tes(){
	$data_tanggal_form = "13 Juni 2017";
	echo $this->jin_date_sql($data_tanggal_form);
	}

public function createdata_de(){
	
	$fileName = $this->input->post('file', TRUE);
     
    $config['upload_path'] = './uploadfile/';  //buat folder dengan nama arsip di root folder
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'xls|xlsx|csv';
    $config['max_size'] = 100000;

    $this->load->library('upload', $config);
	$this->upload->initialize($config); 
     
    if(! $this->upload->do_upload('file') ){
	    $error = array('error' => $this->upload->display_errors());
	    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
		redirect('koor_pa/DeskEvaluation');
	//print_r ($error);
	} else {
	   	$media = $this->upload->data();
	    $inputFileName = './uploadfile/'.$media['file_name'];
        
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($aw = 1; $aw <= $highestRow; $aw++){
    	$awData = $sheet->rangeToArray('A' . $aw . ':' . $highestColumn . $aw,
                                            NULL,
                                            TRUE,
                                            FALSE);
    	if($awData[0][0]){
    		if($awData[0][4] == 'NIM' && $awData[0][17] == "PUJ 1" && $awData[0][18] == "PUJ 2" )
    	
  				  {  
         
        for ($row = 2; $row <= $highestRow; $row++){//  Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
	        if($rowData[0][0]){
		        //Sesuaikan sama nama kolom tabel di database                                
				$nima = $rowData[0][4]; 
		        $check_mahasiswa = $this->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs ='$nima'")->num_rows();
	          	if($check_mahasiswa > 0 ){
	        	$tgl = ($rowData[0][3] - 25569) * 86400;
	        	$tgl2 = gmdate("M-y", $tgl);
	          	$data = array(
		                "periode" => $tgl2,
		                "id_mhs"  => convertidmhs($rowData[0][4]),
		               	"tanggal" => $rowData[0][13],
		               	"PUJ1"    => convertkode($rowData[0][17]),
		               	"PUJ2"    => convertkode($rowData[0][18]),
		                "tahun" => gettahun_aktif(),
	            	);
		       
 		        //delete_files($upload_data['file_path']);
	                $nim_mhs_pa =convertidmhs($rowData[0][4]); 
    	          	$cek = $this->db->query("SELECT id_jadwal_de FROM t_jadwal_de WHERE id_mhs = '$nim_mhs_pa'");
    	          	if($cek->num_rows() > 0){
    	          	$qas = $cek->row();
    	          	$this->yeah->update($data,'t_jadwal_de',$qas->id_jadwal_de,'id_jadwal_de');
    	            }else{
    	            $this->db->insert('t_jadwal_de', $data);
	            	$this->yeah->update(array('tahap' => '2'), 't_mhs_pa',convertidmhs($rowData[0][4]),'id_mhs');
    	        	}


	        }
	    }// end cek maha
        }
      	$this->hitungnilai_de();
        $this->delete_files($media['file_path']); 
        $this->session->set_flashdata('msg','ok'); 
        redirect('koor_pa/DeskEvaluation');
        //print_r ($rowData);
    	}else{
    		$this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
            //print_r ($rowData);
            redirect('koor_pa/DeskEvaluation');	
    	}
	}
	}
    }
}

public function createdata_sidang(){
	
	$fileName = $this->input->post('file', TRUE);
     
    $config['upload_path'] = './uploadfile/';  //buat folder dengan nama arsip di root folder
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'xls|xlsx|csv';
    $config['max_size'] = 100000;

    $this->load->library('upload', $config);
	$this->upload->initialize($config); 
     
    if(! $this->upload->do_upload('file') ){
	    $error = array('error' => $this->upload->display_errors());
	    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
		redirect('koor_pa/Sidang');
	} else {
	   	$media = $this->upload->data();
	    $inputFileName = './uploadfile/'.$media['file_name'];
        
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        for ($aw = 1; $aw <= $highestRow; $aw++){
    	$awData = $sheet->rangeToArray('A' . $aw . ':' . $highestColumn . $aw,
                                            NULL,
                                            TRUE,
                                            FALSE);
    	if($awData[0][0]){
    		if($awData[0][1] == 'NIM' && $awData[0][20] == "PUJ 1" && $awData[0][21] == "PUJ 2" )
    	
  				  {  
         
        for ($row = 2; $row <= $highestRow; $row++){//  Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
	        if($rowData[0][0]){
		        //Sesuaikan sama nama kolom tabel di database   
		        $nima = $rowData[0][1]; 
		        $check_mahasiswa = $this->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs ='$nima'")->num_rows();
	          	if($check_mahasiswa > 0 ){
	        	$tgl = $this->jin_date_sql($rowData[0][18]);
	          	$data = array(
		                "id_mhs"  => convertidmhs($rowData[0][1]),
		               	"tanggal" => $tgl,
		               	"waktu"   => $rowData[0][19],
		               	"PUJ1"    => convertkode($rowData[0][20]),
		               	"PUJ2"    => convertkode($rowData[0][21]),
		               	"ruangan" => $rowData[0][22],
		                "tahun" => gettahun_aktif(),
	            	);
		       	
 		        //delete_files($upload_data['file_path']);
 		        	$nim_mhs_pa =convertidmhs($rowData[0][1]); 
 		        /*	$cek_maha = $this->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs = '$nim_mhs_pa'")->num_rows();
		       		if($cek_maha > 0){*/
    	          	$cek = $this->db->query("SELECT id_jadwal_sidang FROM t_jadwal_sidang WHERE id_mhs = '$nim_mhs_pa'");
    	          	if($cek->num_rows() > 0){
    	          	$qas = $cek->row();
    	          	$this->yeah->update($data,'t_jadwal_sidang',$qas->id_jadwal_sidang,'id_jadwal_sidang');
    	            }else{
    	           $this->db->insert('t_jadwal_sidang', $data);
	            	$this->yeah->update(array('tahap' => '6'), 't_mhs_pa',convertidmhs($rowData[0][1]),'id_mhs');
    	        	}

    	        	//}
    	  
	        	}//end if cek mahas

	        }

        }
        $this->hitungnilai_sidang();
        $this->delete_files($media['file_path']); 
        $this->session->set_flashdata('msg','ok'); 
        redirect('koor_pa/Sidang');
       	}else{
    		$this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
            //print_r ($rowData);
            redirect('koor_pa/Sidang');	
    	}
	}
	}
}
}

public function createdata_sk(){
	
	$fileName = $this->input->post('file', TRUE);
     
    $config['upload_path'] = './uploadfile/';  //buat folder dengan nama arsip di root folder
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'xls|xlsx|csv';
    $config['max_size'] = 100000;

    $this->load->library('upload', $config);
	$this->upload->initialize($config); 
     
    if(! $this->upload->do_upload('file') ){
	    $error = array('error' => $this->upload->display_errors());
	    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
		redirect('koor_pa/plotting');
	} else {
	   	$media = $this->upload->data();
	    $inputFileName = './uploadfile/'.$media['file_name'];
        
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        
        for ($aw = 1; $aw <= $highestRow; $aw++){
    	$awData = $sheet->rangeToArray('A' . $aw . ':' . $highestColumn . $aw,
                                            NULL,
                                            TRUE,
                                            FALSE);
    	if($awData[0][0]){
    		if($awData[0][0] == 'NIM' )
    	
  				  {  

        for ($row = 2; $row <= $highestRow; $row++){//  Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
	        if($rowData[0][0]){
		        //Sesuaikan sama nama kolom tabel di database                                
	          	
		       
 		        //delete_files($upload_data['file_path']);
		           
	        	$nima = $rowData[0][0]; 
		        $check_mahasiswa = $this->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs ='$nima'")->num_rows();
	          	if($check_mahasiswa > 0 ){
	            $this->yeah->update(array('tahap' => '4'), 't_mhs_pa',convertidmhs($rowData[0][0]),'id_mhs');
	        	}


	        }

        }
       
        $this->delete_files($media['file_path']); 
        $this->session->set_flashdata('msg','ok'); 
        redirect('koor_pa/PengajuanSK');
    	}else{
    		$this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
            //print_r ($rowData);
            redirect('koor_pa/PengajuanSK');	
    	}
	}
	}
}
}

public function createdata_yudisium(){
	
	$fileName = $this->input->post('file', TRUE);
     
    $config['upload_path'] = './uploadfile/';  //buat folder dengan nama arsip di root folder
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'xls|xlsx|csv';
    $config['max_size'] = 100000;

    $this->load->library('upload', $config);
	$this->upload->initialize($config); 
     
    if(! $this->upload->do_upload('file') ){
	    $error = array('error' => $this->upload->display_errors());
	    $this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
		redirect('koor_pa/plotting');
	} else {
	   	$media = $this->upload->data();
	    $inputFileName = './uploadfile/'.$media['file_name'];
        
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        
        for ($aw = 1; $aw <= $highestRow; $aw++){
    	$awData = $sheet->rangeToArray('A' . $aw . ':' . $highestColumn . $aw,
                                            NULL,
                                            TRUE,
                                            FALSE);
    	if($awData[0][0]){
    		if($awData[0][0] == 'NIM' )
    	
  				  { 

        for ($row = 2; $row <= $highestRow; $row++){//  Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
	       	$nima = $rowData[0][0]; 
		        $check_mahasiswa = $this->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs ='$nima'")->num_rows();
	         
	        if($check_mahasiswa > 0 ){
	        if($rowData[0][0]){
		        //Sesuaikan sama nama kolom tabel di database                                
	          	
		       	
 		        //delete_files($upload_data['file_path']);
		        $this->session->set_flashdata('msg','Sukses');
		     
		     

	        	if($rowData[0][18] == 'LULUS'){
	            $this->yeah->update(array('tahap' => '7'), 't_mhs_pa',convertidmhs($rowData[0][0]),'id_mhs');


        		//update nilai kk
				
				$taun = date("Y");
				$yea = gettahun_aktif();
				$aang = getangkatan(convertidmhs($rowData[0][0]));
				$x = $taun - $aang;
				if($x <= 3){
				$p1 = getp1(convertidmhs($rowData[0][0]));
				$p2 = getp2(convertidmhs($rowData[0][0]));
				$jml_mhs_lulus_p1 = $this->db->query("SELECT COUNT(*) as 'jum' FROM t_mhs_pa WHERE (id_doping1 = '$p1' or 
					id_doping2 = '$p1' ) AND tahap = '7' AND angkatan = '$aang'  ")->row_array();
				$jml_mhs_lulus_p2 = $this->db->query("SELECT COUNT(*) as 'jum' FROM t_mhs_pa WHERE (id_doping1 = '$p2' or id_doping2 = '$p2' ) AND tahap = '7' AND angkatan = '$aang'  ")->row_array();
				$jum1 = $jml_mhs_lulus_p1['jum'];
				$jum2 = $jml_mhs_lulus_p2['jum'];
				$cek1 = $this->db->query("SELECT * from t_kontribusi WHERE id_dosen = '$p1' and tahun = '$yea'");
				$cek2 = $this->db->query("SELECT * from t_kontribusi WHERE id_dosen = '$p2' and tahun = '$yea'");
				if($cek1->num_rows() > 0 ){
				$this->db->query("UPDATE `t_kontribusi` SET `total_mhs` = '$jum1' WHERE `t_kontribusi`.`id_dosen` = '$p1'  and tahun = '$yea'");
				} else{
				$this->db->query("INSERT INTO `t_kontribusi` ( `id_dosen`, `total_mhs`, `jml_bimbingan`, `prosentase`, `tahun`) VALUES ('$p1', '0', '$jum1', '0', '$yea')");
				}
				if($cek2->num_rows() > 0 ){
				$this->db->query("UPDATE `t_kontribusi` SET `total_mhs` = '$jum2' WHERE `t_kontribusi`.`id_dosen` = '$p2' and tahun = '$yea'");
				}else{
				$this->db->query("INSERT INTO `t_kontribusi` ( `id_dosen`, `total_mhs`, `jml_bimbingan`, `prosentase`, `tahun`) VALUES ('$p2', '0', '$jum2', '0', '$yea')");
				}
				 $this->hitungnilai_kontribusi($p1,$jum1);
				 $this->hitungnilai_kontribusi($p2,$jum2);
				}

				
				//end nilai kk	
	            
	            }
	            }else{
    		$this->session->set_flashdata('msg','Ada kesalahan dalam proses upload'); 
            //print_r ($rowData);
            redirect('koor_pa/Yudisium');	
		    	}
	      
	               
			}// CEK MAHA
		
			}

				
		    }
		}
			

        }
       		$this->delete_files($media['file_path']); 
			$this->session->set_flashdata('msg','ok'); 

        	redirect('koor_pa/Yudisium');
        
   	 }
	}



public function jin_date_sql($date){
	$exp = explode(' ',$date);
	if(count($exp) == 3) {
		if($exp[1] == 'Januari'){
			$bln = '01';
		}else if($exp[1] == 'Februari'){
			$bln = '02';
		}else if($exp[1] == 'Maret'){
			$bln = '03';
		}else if($exp[1] == 'April'){
			$bln = '04';
		}else if($exp[1] == 'Mei'){
			$bln = '05';
		}else if($exp[1] == 'Juni'){
			$bln = '06';
		}else if($exp[1] == 'Juli'){
			$bln = '07';
		}else if($exp[1] == 'Agustus'){
			$bln = '08';
		}else if($exp[1] == 'September'){
			$bln = '09';
		}else if($exp[1] == 'Oktober'){
			$bln = '10';
		}else if($exp[1] == 'November'){
			$bln = '11';
		}else {
			$bln = '12';
		}

		$date = $exp[2].'-'.$bln.'-'.$exp[0];
	}
	return $date;
}
 

public function del_mhs($id){
	$cek = $this->db->query("SELECT id_doping1,id_doping2 from t_mhs_pa WHERE id_mhs ='$id'")->row();
	$this->yeah->delete($id,'t_mhs_pa','id_mhs');
	$this->hitungnilaibimb2($cek->id_doping1);
	$this->hitungnilaibimb2($cek->id_doping2);
	redirect('koor_pa/ListMahasiswaPA');
}
public function del_jadwal($tipe,$tabel,$id){
	if($tabel == 't_jadwal_sidang'){
		$where = 'id_jadwal_sidang';
	}else if($tabel == 't_jadwal_de'){
		$where = 'id_jadwal_de';
	}
	$this->yeah->delete($id,$tabel,$where);
	redirect('koor_pa/'.$tipe);
}

public function hitungnilaibimb(){
	$all_dosen = $this->yeah->all_dosen()->result();

	foreach($all_dosen as $row){

		if(cek_kpi('1')){

			$kategori = $this->yeah->getKPI('1')->result();
			foreach($kategori as $res){

				$total = total_bimb_all($row->id_dosen);
				if($total >= $res->kondisi){
					$hasil = array(
						'nilai_jml_bimb' => $res->nilai
					);
					$nil = $res->nilai;
					$where= 'nilai_jml_bimb';
					$this->yeah->update_nilai($row->id_dosen,'t_nilai',$nil,$where,getsemester_aktif());
					
					break;
				}else{
					$hasil = array(
						'nilai_jml_bimb' => 0
					);
					$nil = 0;
					$where= 'nilai_jml_bimb';
					$this->yeah->update_nilai($row->id_dosen,'t_nilai',$nil,$where,getsemester_aktif());
				}
			}
		//$this->session->set_flashdata('msg','Sukses ...!!');
		}
	}
}



public function hitungnilaibimb2($id){

if(cek_kpi('1')){

	$kategori = $this->yeah->getKPI('1')->result();
	foreach($kategori as $res){

		$total = total_bimb_all($id);
		$where = 'nilai_jml_bimb';
		if($total >= $res->kondisi){
			$hasil = array(
				'nilai_jml_bimb' => $res->nilai
			);
			$nil = $res->nilai;
			
			$this->yeah->update_nilai($id,'t_nilai',$nil,$where,getsemester_aktif());
			
			break;
		}else{
			$hasil = array(
				'nilai_jml_bimb' => 0
			);
			$nil = 0;
			$this->yeah->update_nilai($id,'t_nilai',$nil,$where,getsemester_aktif());
		}
	}
	$this->session->set_flashdata('msg','Sukses ...!!');
}
}

public function hitungnilai_de(){
	$all_dosen = $this->yeah->all_dosen()->result();

	foreach($all_dosen as $row){

		if(cek_kpi('12')){

			$kategori = $this->yeah->getKPI('12')->result();
			foreach($kategori as $res){

				$total = total_menguji($row->id_dosen,'t_jadwal_de');
				if($total >= $res->kondisi){
					
					$nil = $res->nilai;
					$where = 'nilai_jml_de';
					$this->yeah->update_nilai($row->id_dosen,'t_nilai',$nil,$where,getsemester_aktif());
					
					break;
				}else{
					
					$nil = 0;
					$where = 'nilai_jml_de';
					$this->yeah->update_nilai($row->id_dosen,'t_nilai',$nil,$where,getsemester_aktif());
				}
			}
			$this->session->set_flashdata('msg','Sukses ...!!');
		}
	}
}

public function hitungnilai_de2($id){
	$all_dosen = $this->yeah->all_dosen()->result();


		if(cek_kpi('1')){

			$kategori = $this->yeah->getKPI('12')->result();
			foreach($kategori as $res){

				$total = total_menguji($id,'t_jadwal_de');
				if($total >= $res->kondisi){
					
					$nil = $res->nilai;
					$where = 'nilai_jml_de';
					$this->yeah->update_nilai($id,'t_nilai',$nil,$where,getsemester_aktif());
					
					break;
				}else{
					
					$nil = 0;
					$where = 'nilai_jml_de';
					$this->yeah->update_nilai($id,'t_nilai',$nil,$where,getsemester_aktif());
				}
			}
		//$this->session->set_flashdata('msg','Sukses ...!!');
		}
	
}

public function hitungnilai_sidang(){
	$all_dosen = $this->yeah->all_dosen()->result();

	foreach($all_dosen as $row){

		if(cek_kpi('13')){

			$kategori = $this->yeah->getKPI('13')->result();
			foreach($kategori as $res){

				$total = total_menguji($row->id_dosen,'t_jadwal_sidang');
				if($total >= $res->kondisi){
					
					$nil = $res->nilai;
					$where = 'nilai_jml_sidang';
					$this->yeah->update_nilai($row->id_dosen,'t_nilai',$nil,$where,getsemester_aktif());
					
					break;
				}else{
					
					$nil = 0;
					$where = 'nilai_jml_sidang';
					$this->yeah->update_nilai($row->id_dosen,'t_nilai',$nil,$where,getsemester_aktif());
				}
			}
		//$this->session->set_flashdata('msg','Sukses ...!!');
		}
	}
}

public function hitungnilai_sidang2($id){
	$all_dosen = $this->yeah->all_dosen()->result();


		if(cek_kpi('13')){

			$kategori = $this->yeah->getKPI('13')->result();
			foreach($kategori as $res){

				$total = total_menguji($id,'t_jadwal_sidang');
				if($total >= $res->kondisi){
					
					$nil = $res->nilai;
					$where = 'nilai_jml_sidang';
					$this->yeah->update_nilai($id,'t_nilai',$nil,$where,getsemester_aktif());
					
					break;
				}else{
					
					$nil = 0;
					$where = 'nilai_jml_sidang';
					$this->yeah->update_nilai($id,'t_nilai',$nil,$where,getsemester_aktif());
				}
			}
		//$this->session->set_flashdata('msg','Sukses ...!!');
		}
	
}

public function delete_files($path){
	   		$files = glob($path.'*'); // get all file names
			  foreach($files as $file){ // iterate files
		      if(is_file($file))
		        unlink($file); // delete file
		        //echo $file.'file deleted';
		  	
		}
	       
	}

public function DeskEvaluation(){
	$nama = $this->yeah->all_dosen('id_dosen','t_dosen')->result();
	$listde = $this->db->query("SELECT *, t1.id_mhs as ini FROM `t_mhs_pa` as t1
								left join `t_jadwal_de` as t2 on t1.id_mhs = t2.id_mhs
								where tahap = '1' 
								and t2.id_mhs is NULL ")->result();
	$data = array(
			'title' => 'Desk Evaluation',
			'nama' => $nama,
			'listde' => $listde,
			'alert' => ' '
			);
	$this->load->view('koor_pa/jadwal/view_de',$data);
}

public function Sidang(){
	$nama = $this->yeah->all_dosen('id_dosen','t_dosen')->result();
	$listsidang = $this->db->query("SELECT *, t1.id_mhs as ini FROM `t_mhs_pa` as t1
								left join `t_jadwal_sidang` as t2 on t1.id_mhs = t2.id_mhs
								where tahap = '5' 
								and t2.id_mhs is NULL ")->result();
	$data = array(
			'title' => 'Sidang',
			'nama' => $nama,
			'listsidang' => $listsidang
			);
	$this->load->view('koor_pa/jadwal/view_sidang',$data);
}

//pengajuan sk

public function PengajuanSK(){
	$data = array(
		'title' => "Unggah Pengajuan SK"
	);
	$this->load->view('koor_pa/view_pengajuansk',$data);
}

//yudisium 

public function Yudisium(){
	$data = array(
		'title' => "Unggah Yudisium"
	);
	$this->load->view('koor_pa/view_yudisium',$data);
}

public function KontribusiKelulusan(){
	//$row = $this->yeah->KontribusiKelulusan()->result();
	$tahun = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
	$data = array(
		'title' => 'Laporan Kontribusi Kelulusan',
		'tahun' => $tahun
	);
	$this->load->view('kaprodi/pa/data_kontribusi',$data);

}

function getKontribusiKel(){
	$data['total'] =  $this->yeah->KontribusiKelulusan($this->input->post('tahun'))->num_rows();
	$data['row'] = $this->yeah->KontribusiKelulusan($this->input->post('tahun'))->result();
	echo json_encode($data);
}

public function edit_kontribusi(){
$rows = $this->yeah->EditKontribusi($this->input->get('id'));
echo json_encode($rows);
}

public function proses_edit_kontribusi(){
	$total = html_escape($this->input->post('total'));
	$prosentase = round($total/html_escape($this->input->post('jumlah')),2)*100;
	$data = array(
		'prosentase' => $prosentase,
		'jml_bimbingan' => html_escape($this->input->post('jumlah')),
		'total_mhs' => html_escape($this->input->post('total'))
	);
	$this->yeah->update($data,'t_kontribusi',$this->input->post('id'),'id_kl');
	$this->hitungnilai_kontribusi2($this->input->post('id_dosen'),$prosentase);
	//redirect('kaprodi/view_data_kontribusi');
}

public function hitungnilai_kontribusi2($id,$pro){

if(cek_kpi('5')){

	$kategori = $this->yeah->getKPI('5')->result();
	foreach($kategori as $res){
		if($pro >= $res->kondisi){
		
			$nil = $res->nilai;
			$this->yeah->update_nilai($id,'t_nilai',$nil,'nilai_kk',getsemester_aktif());
			break;
		}else{
			
			$nil = 0;
			$this->yeah->update_nilai($id,'t_nilai',$nil,'nilai_kk',getsemester_aktif());
		}
	}
	$this->session->set_flashdata('msg','Sukses ...!!');
	}
}

}
