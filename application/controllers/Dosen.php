<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	
	public function __construct(){
		 parent::__construct();
		 
        if(!isset($_SESSION['username'])){
            redirect('login');
        }

        $this->load->helper('raj');
		$this->load->model('M_dosen','yeah');
		$this->session->set_userdata("info","Dosen");

		
	}

	public function index()

	{
			//$rows2 = $this->yeah->getusulan2($this->session->userdata('id_dosen'));
			$data = array(
	            'title' => 'Dashboard Dosen',
	            'action' => site_url('login/check'),
	            //'rows2' => $rows2,
	            'ind' =>  $this->yeah->getListKPI()
            );

		$this->load->view('dosen/dashboard2_dosen',$data);
	}

	public function rapat_hadir($id){
		$status = $id;
		$this->yeah->update_rapat('HADIR',$status,$this->session->id_dosen);
		redirect('dosen/ListRapat');
	}

	public function rapat_alpha($id){
		$status = $id;
		$this->yeah->update_rapat('ALPHA',$status,$this->session->id_dosen);
		
		redirect('dosen/ListRapat');
	}


	public function rapat_izin($id,$val){
		$status = $id;
		$this->yeah->update_rapat_izin($status,html_escape(urldecode($val)));
		redirect('dosen/ListRapat');
	}

	public function ListRapat(){
		$rows = $this->yeah->all_rapat($this->session->id_dosen,'id_dosen','id_undangan','t_undangan','t_kehadiran')->result();
		//$rows2 = $this->yeah->getusulan2($this->session->userdata('id_dosen'));
		$data = array(
            'title' => 'List Undangan',
            'rows' => $rows,
            //'rows2' =>$rows2
            );
		$this->load->view('dosen/rapat/list_rapat',$data);
	}
	public function dashboard(){

		$data = array(
            'title' => 'Dashboard',
            'ind' =>  $this->yeah->getListKPI()
            );
		$this->load->view('dosen/dashboard2_dosen',$data);
	}

	//lihat rps
	public function lihat_rps(){
		$all = $this->yeah->getAllRps($this->session->userdata('id_dosen'));
		$idrps = array();
		foreach ($all as $allin) {
			$idrps[] = $allin->id_rps;
		}
		if (count($idrps)) {
			$data2 = array(
				'status' => 'READ'
			);
			$this->yeah->updateAllRps($this->session->userdata('id_dosen'),$idrps,$data2);
		}
		$rows = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List RPS',
			'rows' => $rows
		);
		$this->load->view('dosen/rps/lihat_rps', $data);
	}
	public function download_rps(){
		$this->load->helper('download');
		//$data = file_get_contents(base_url('/uploadfile/rps/'.$fileName));
	    //force_download($fileName, $data);
		$fileName = $this->uri->segment(3);
	    force_download('uploadfile/rps/'.$fileName, NULL);

	}
	public function lihat_status_rps(){
		$rows = $this->yeah->getStatusRps($this->session->userdata('id_dosen'));
		$data = array(
			'rows' => $rows
		);
	}
	public function updateStatusRps($id){
		$data = array(
			'status' => 'READ'
		);
		$this->yeah->update_data_rps($id, $data);
		redirect('dosen/lihat_rps');
	}

	//lihat modul praktikum
	public function lihat_modul(){
		$all = $this->yeah->getAllModul($this->session->userdata('id_dosen'));
		$idmodul = array();
		foreach ($all as $allin) {
			$idmodul[] = $allin->id_modul;
		}
		if (count($idmodul)>0) {
			$data2 = array(
				'status' => 'READ'
			);
			$this->yeah->updateAllModul($this->session->userdata('id_dosen'),$idmodul,$data2);
		}
		$rows = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List Modul Praktikum',
			'rows' => $rows
		);
		$this->load->view('dosen/modul/lihat_modul', $data);
	}
	public function lihat_status_modul(){
		$rows = $this->yeah->getStatusModul($this->session->userdata('id_dosen'));
		$data = array(
			'rows' => $rows
		);
	}
	public function updateStatusModul($id){

		$data = array(
			'status' => 'READ'
		);
		$this->yeah->update_data_modul($id, $data);
		redirect('dosen/lihat_modul');
	}
	public function download_modul(){
		$this->load->helper('download');
		//$data = file_get_contents(base_url('/uploadfile/modul/'.$fileName));
	    //force_download($fileName, $data);
	    $fileName = $this->uri->segment(3);
	    force_download('uploadfile/modul/'.$fileName, NULL);

	}

	//lihat soal asessment
	public function lihat_soal(){
		$all = $this->yeah->getAllSoal($this->session->userdata('id_dosen'));
		$idsoal = array();
		foreach ($all as $allin) {
			$idsoal[] = $allin->id_soal;
		}
		if (count($idsoal)>0) {
			$data2 = array(
				'status' => 'READ'
			);
			$this->yeah->updateAllSoal($this->session->userdata('id_dosen'),$idsoal,$data2);	
		}
		$rows = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List Soal Asessment',
			'rows' => $rows
		);
		$this->load->view('dosen/soal/lihat_soal', $data);
	}
	public function lihat_status_soal(){
		$rows = $this->yeah->getStatusSoal($this->session->userdata('id_dosen'));
		$data = array(
			'rows' => $rows
		);
	}
	public function updateStatusSoal($id){
		$data = array(
			'status' => 'READ'
		);
		$this->yeah->update_data_soal($id, $data);
		redirect('dosen/lihat_soal');
	}
	public function download_soal(){
		$this->load->helper('download');
		//$data = file_get_contents(base_url('/uploadfile/soal/'.$fileName));
	    //force_download($fileName, $data);
	    $fileName = $this->uri->segment(3);
	    force_download('uploadfile/soal/'.$fileName, NULL);

	}

	//lihat bahan perkuliahan
	public function lihat_bahan(){
		$all = $this->yeah->getAllBahan($this->session->userdata('id_dosen'));
		$idbahan = array();
		foreach ($all as $allin) {
			$idbahan[] = $allin->id_bahan;
		}
		if (count($idbahan)) {
			$data2 = array(
				'status' => 'READ'
			);
			$this->yeah->updateAllBahan($this->session->userdata('id_dosen'),$idbahan,$data2);
		}
		$rows = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'Bahan Perkuliahan',
			'rows' => $rows
		);
		$this->load->view('dosen/bahan/lihat_bahan', $data);
	}
	public function lihat_status_bahan(){
		$rows = $this->yeah->getStatusBahan($this->session->userdata('id_dosen'));
		$data = array(
			'rows' => $rows
		);
	}
	public function updateStatusBahan($id){
		$data = array(
			'status' => 'READ'
		);
		$this->yeah->update_data_bahan($id, $data);
		redirect('dosen/lihat_bahan');
	}
	public function download_bahan(){
		$this->load->helper('download');
		//$data = file_get_contents(base_url('/uploadfile/bahan/'.$fileName));
	    //force_download($fileName, $data);
	    $fileName = $this->uri->segment(3);
	    force_download('uploadfile/bahan/'.$fileName, NULL);

	}

	//filter semester dan matakulkiah
	// public function listsemester(){
	// 	echo $id = $this->input->post('id', TRUE);
	// 	$data['semester'] = $this->yeah->getselectedsemester($id); 
	// 	$output = '<option value="">-Pilih Semester-</option>';

	// 	foreach ($data['semester'] as $row){  
	//        $output .= "<option value='".$row->id_semester."'>".$row->semester."</option>";  
	//     }

	//     echo $output;
	// }
	// public function listmatakuliah(){  
	//     echo $id = $this->input->post('id', TRUE);
	//     $data['matakuliah'] = $this->yeah->getselectedmatakuliah($id, $this->session->userdata('id_dosen'));  
	//     $output = '<option value="">-Pilih Mata Kuliah-</option>';

	//     foreach ($data['matakuliah'] as $row){    
	//        $output .= "<option value='".$row->id_matakuliah."'>".$row->nama_matakuliah."</option>";  
	//     }

	//     echo $output;  
	// }

	//unggah usulan
	public function getassessment2(){
		$id_matakuliah = $this->input->post('id');
		$data['assessment'] = $this->yeah->getassessment2($id_matakuliah); 
		if (count($data['assessment']) <= 0) {
			$output = '<option value="">-Deadline Belum Ditentukan-</option>';
		}else{
			$output = '<option value="">-Pilih Assessment-</option>';
			foreach ($data['assessment'] as $row){  
	      		$output .= "<option value='".$row->jenis_nilai."'>".$row->jenis_nilai."</option>";  
	  		}
		}
		
	    echo $output;
	}
	public function unggah_usulan(){
		$rows = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$matkul = $this->yeah->getselectedmatakuliah2($this->session->userdata('id_dosen'));
		$kondisi = $this->yeah->getKondisi();
		//$usulan = $this->yeah->getDeadlineUsulan();
		$data = array(
			'title' => 'Unggah Usulan Soal',
			'rows' => $rows,
			'matkul' => $matkul,
			'kondisi' => $kondisi
			//'usulan' => $usulan
		);
		$this->load->view('dosen/soal/unggah_usulan', $data);
	}
	public function unggah_usulan_aksi(){
		$asessment = $this->input->post('asessment');
		$cek = $this->db->get_where('t_usulansoal', array('id_matakuliah' => $this->input->post('id_matakuliah'), 'id_dosen' => $this->session->userdata('id_dosen'), 'asessment' => $asessment));
		if ($cek->num_rows() > 0) {
			$id_usulan =  $this->db->get_where('t_usulansoal', array('id_matakuliah' => $this->input->post('id_matakuliah'), 'id_dosen' => $this->session->userdata('id_dosen'), 'asessment' => 'asessment'))->row()->id_usulan;
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/usulan/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['max_size'] = '1000000';

	        $this->load->library('upload',$config);
	   		if(! $this->upload->do_upload()){
	   			$error = array('error' => $this->upload->display_errors());
		 	    $this->session->set_flashdata('msg','gagal'); 
				redirect('dosen/unggah_usulan');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'date' => $date,
					'fileName' => $dokumen['file_name'],
					'asessment' => $asessment,
					'status_usulan' => 'WAITING',
					'id_dosen' => $this->session->userdata('id_dosen')
				);
				$this->yeah->update_data_usulan($id_usulan, $data);
				$id = $this->yeah->last_datausulan('t_usulansoal','id_usulan')->row();
				// $matakuliah = $this->yeah->tampungUsulan($id_matakuliah);
				// foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $this->session->userdata('id_dosen'),
						'id_usulan' => $id->id_usulan,
						'status' => 'UNREAD'
					);
					$this->yeah->update_status_usulan($id_usulan,$data2);
				//$this->hitungnilai_asessment($id->id_nilaiasessment);
				redirect('dosen/lihat_usulan');
			}
		}else{
			$id_matakuliah = $this->input->post('id_matakuliah');
			$asessment = $this->input->post('asessment');
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/usulan/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['max_size'] = '10000';

	   		$this->load->library('upload',$config);
	   		if(! $this->upload->do_upload()){
	   			$error = array('error' => $this->upload->display_errors());
		 	    $this->session->set_flashdata('msg','gagal'); 
				redirect('dosen/unggah_usulan');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'id_matakuliah' => $id_matakuliah,
					'date' => $date,
					'fileName' => $dokumen['file_name'],
					'asessment' => $asessment,
					'status_usulan' => 'WAITING',
					'id_dosen' => $this->session->userdata('id_dosen')
				);
				$this->yeah->input_data_usulan($data, 't_usulansoal');
				$id = $this->yeah->last_datausulan('t_usulansoal','id_usulan')->row();
				// $matakuliah = $this->yeah->tampungAsessment($id_matakuliah);
				 //foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $this->session->userdata('id_dosen'),
						'id_usulan' => $id->id_usulan,
						'status' => 'UNREAD'
					);
					$this->yeah->input_status_usulan($data2, 't_statususulan');
				//$this->hitungnilai_asessment($id->id_nilaiasessment);
				redirect('dosen/lihat_usulan');
			}
		}
	}
	public function lihat_usulan(){
		$id_usulan = $this->yeah->getidusulan();
		$listidusulan = array();
		foreach ($id_usulan as $row) {
				$listidusulan[] = $row->id_usulan;
		}
		if ($listidusulan) {
			$rows = $this->yeah->getusulan($listidusulan);
		}else{
			$rows = array();
		}
		$data = array(
			'title' => 'List Usulan Soal',
			'rows' => $rows
		);
		$this->load->view('dosen/soal/lihat_usulan', $data);
	}
	public function hapus_usulan($id_usulan,$fileName){
		$this->load->helper("file");
		//$path = $this->yeah->getPath($id_rps);
		unlink('./uploadfile/usulan/'.$fileName);
		$where = array('id_usulan' => $id_usulan);
		$this->yeah->hapus_data_usulan($where, 't_usulansoal');
		redirect('dosen/lihat_usulan');
	}
	public function download_usulan($fileName){
		$this->load->helper('download');
	    //$data = file_get_contents(base_url('/uploadfile/usulan/'.$fileName));
	    //force_download($fileName, $data);
	    $fileName = $this->uri->segment(3);
	    force_download('uploadfile/usulan/'.$fileName, NULL);
	}

//Unggah nilai asessment
	public function getassessment(){
		$id_matakuliah = $this->input->post('id');
		$data['assessment'] = $this->yeah->getassessment($id_matakuliah); 
		if (count($data['assessment']) <= 0) {
			$output = '<option value="">-Deadline Belum Ditentukan-</option>';
		}else{
			$output = '<option value="">-Pilih Assessment-</option>';
			foreach ($data['assessment'] as $row){  
	      		$output .= "<option value='".$row->jenis_nilai."'>".$row->jenis_nilai."</option>";  
	  		}
		}
		
	    echo $output;
	}
	public function unggah_nilai(){
		$rows = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$matkul = $this->yeah->getselectedmatakuliah2($this->session->userdata('id_dosen'));
		$kondisi = $this->yeah->getKondisi2();
		//$nilai = $this->yeah->getDeadlineNilai();
		$data = array(
			'title' => 'Unggah Nilai Assessment',
			'rows' => $rows,
			'matkul' => $matkul,
			'kondisi' => $kondisi,
			
		);
		$this->load->view('dosen/asessment/unggah_nilai', $data);
	}
	public function unggah_nilai_aksi(){
		$asessment = $this->input->post('asessment');
		$cek = $this->db->get_where('t_nilaiasessment', array('id_matakuliah' => $this->input->post('id_matakuliah'), 'id_dosen' => $this->session->userdata('id_dosen'), 'asessment' => $asessment));
		if ($cek->num_rows() > 0) {
			$id_nilaiasessment =  $this->db->get_where('t_nilaiasessment', array('id_matakuliah' => $this->input->post('id_matakuliah'), 'id_dosen' => $this->session->userdata('id_dosen'), 'asessment' => 'asessment'))->row()->id_nilaiasessment;
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/nilai/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['max_size'] = '1000000';

	        $this->load->library('upload',$config);
	   		if(! $this->upload->do_upload()){
	   			$error = array('error' => $this->upload->display_errors());
		 	    $this->session->set_flashdata('msg','gagal'); 
				redirect('dosen/unggah_nilai');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'date' => $date,
					'fileName' => $dokumen['file_name'],
					'asessment' => $asessment,
					'status_nilai' => 'WAITING',
					'id_dosen' => $this->session->userdata('id_dosen')
				);
				$this->yeah->update_data_nilai($id_nilaiasessment, $data);
				$id = $this->yeah->last_nilaiasessment('t_nilaiasessment','id_nilaiasessment')->row();
				// $matakuliah = $this->yeah->tampungUsulan($id_matakuliah);
				// foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $this->session->userdata('id_dosen'),
						'id_nilaiasessment' => $id->id_nilaiasessment,
						'status' => 'UNREAD'
					);
					$this->yeah->update_status_nilai($id_nilaiasessment,$data2);
				//$this->hitungnilai_asessment($id->id_nilaiasessment);
				redirect('dosen/lihat_nilai');
			}
		}else{
			$id_matakuliah = $this->input->post('id_matakuliah');
			$asessment = $this->input->post('asessment');
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/nilai/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['max_size'] = '10000';

	   		$this->load->library('upload',$config);
	   		if(! $this->upload->do_upload()){
	   			$error = array('error' => $this->upload->display_errors());
		 	    $this->session->set_flashdata('msg','gagal'); 
				redirect('dosen/unggah_nilai');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'id_matakuliah' => $id_matakuliah,
					'date' => $date,
					'fileName' => $dokumen['file_name'],
					'asessment' => $asessment,
					'status_nilai' => 'WAITING',
					'id_dosen' => $this->session->userdata('id_dosen')
				);
				$this->yeah->input_nilai_asessment($data, 't_nilaiasessment');
				$id = $this->yeah->last_nilaiasessment('t_nilaiasessment','id_nilaiasessment')->row();
				// $matakuliah = $this->yeah->tampungAsessment($id_matakuliah);
				 //foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $this->session->userdata('id_dosen'),
						'id_nilaiasessment' => $id->id_nilaiasessment,
						'status' => 'UNREAD'
					);
					$this->yeah->input_status_nilai($data2, 't_statusnilaiasessment');
				//$this->hitungnilai_asessment($id->id_nilaiasessment);
				redirect('dosen/lihat_nilai');
			}
		}
	}
	public function lihat_nilai(){
		$id_nilaiasessment = $this->yeah->getidnilai();
		$listidnilai = array();
		foreach ($id_nilaiasessment as $row) {
				$listidnilai[] = $row->id_nilaiasessment;
		}
		if ($listidnilai) {
			$rows = $this->yeah->getnilaiasessment($listidnilai);
		}else{
			$rows = array();
		}
		$data = array(
			'title' => 'List Nilai Asessment',
			'rows' => $rows
		);
		$this->load->view('dosen/asessment/lihat_nilai', $data);
	}
	public function hapus_nilai($id_nilaiasessment){
		$fileName = $this->db->query("SELECT fileName FROM t_nilaiasessment WHERE id_nilaiasessment = '$id_nilaiasessment'")->row('fileName');
		$this->load->helper("file");
		unlink('./uploadfile/nilai/'.$fileName);
		$where = array('id_nilaiasessment' => $id_nilaiasessment);
		$this->yeah->hapus_nilai_asessment($where, 't_nilaiasessment');
		//$this->hitungnilai_asessment($id_nilaiasessment);
		redirect('dosen/lihat_nilai');
	}
	public function download_nilai($fileName){
		$this->load->helper('download');
	    $fileName = $this->uri->segment(3);
	    force_download('uploadfile/nilai/'.$fileName, NULL);
	}

// proyek akhir	
	function DosenPembimbing(){
	$rows = $this->yeah->getById('id_dosen',$this->session->id_dosen,'t_dosen')->result();
	$data = array(
            'title' => 'Dashboard Dosen Pembimbing',
            'rows' => $rows
            );
	$this->load->view('dosen/pa/view_doping',$data);
	}

	function data_mhs(){
    	$rows  = $this->yeah->data_mhs($this->session->id_dosen)->result();
   
        echo json_encode($rows);
    }
    function edit_mhss(){
    	$rows  = $this->yeah->edit_mhs($this->input->get('id'));
        echo json_encode($rows);
    }

	public function logout(){
		session_destroy();
		redirect('login');
	}

	public function edit_mhs(){
		$id = $this->input->post('id');
	$ros2 = $this->db->query("select tahap from t_mhs_pa where id_mhs = '$id'")->row('tahap');	
	if($ros2 > html_escape($this->input->post('tahap'))){
		$this->session->set_flashdata('msg','gagal');
		redirect('dosen/DosenPembimbing');
	}else{
		$data = array(
			'tahap' => html_escape($this->input->post('tahap'))
			);
		$this->yeah->update($data,'t_mhs_pa',$this->input->post('id'),'id_mhs');
		$this->hitungnilaibimb2($this->session->id_dosen);
		$this->session->set_flashdata('msg','ok');
		redirect('dosen/DosenPembimbing');
	}
	}

	public function hitungnilaibimb2($id){

	if(cek_kpi('1')){

		$kategori = $this->yeah->getKPI('1')->result();
		foreach($kategori as $res){

			$total = total_bimb_all($id);
			$where = 'nilai_jml_bimb';
			if($total >= $res->kondisi){
				$hasil = array(
					'nilai_jml_bimb' => $res->nilai
				);
				$nil = $res->nilai;
				
				update_nilai($nil,$this->session->id_dosen,$where,getsemester_aktif());
				
				break;
			}else{
				$hasil = array(
					'nilai_jml_bimb' => 0
				);
				$nil = 0;
				update_nilai(0,$this->session->id_dosen,$where,getsemester_aktif());
			}
		}
		$this->session->set_flashdata('msg','Sukses ...!!');
	}
	}
	//lihat kpi dosen
	public function ListIndikator(){

		$rows = $this->yeah->all('id_kpi','t_kpi')->result();

		$rows1 = $this->yeah->getById('id_kpi',1,'t_kpi')->result();
		$ros1 = $this->yeah->getKategori('id_kpi',1,'t_kategori')->result();

		$rows2 = $this->yeah->getById('id_kpi',2,'t_kpi')->result();

		$rows3 = $this->yeah->getById('id_kpi',3,'t_kpi')->result();

		$rows4 = $this->yeah->getById('id_kpi',4,'t_kpi')->result();
		$ros4 = $this->yeah->getKategori('id_kpi',4,'t_kategori')->result();

		$rows5 = $this->yeah->getById('id_kpi',5,'t_kpi')->result();
		$ros5 = $this->yeah->getKategori('id_kpi',5,'t_kategori')->result();

		$rows12 = $this->yeah->getById('id_kpi',12,'t_kpi')->result();
		$ros12 = $this->yeah->getKategori('id_kpi',12,'t_kategori')->result();

		$rows13 = $this->yeah->getById('id_kpi',13,'t_kpi')->result();
		$ros13 = $this->yeah->getKategori('id_kpi',13,'t_kategori')->result();

		$rows14 = $this->yeah->getById('id_kpi',14,'t_kpi')->result();
		$ros14 = $this->yeah->getKategori('id_kpi',14,'t_kategori')->result();

		$rows15 = $this->yeah->getById('id_kpi',15,'t_kpi')->result();
		$ros15 = $this->yeah->getKategori('id_kpi',15,'t_kategori')->result();

		$rows6 = $this->yeah->getById('id_kpi',6,'t_kpi')->result();
		$ros6 = $this->yeah->getKategori('id_kpi',6,'t_kategori')->result();

		$rows7 = $this->yeah->getById('id_kpi',7,'t_kpi')->result();
		$ros7 = $this->yeah->getKategori('id_kpi',7,'t_kategori')->result();

		$rows8 = $this->yeah->getById('id_kpi',8,'t_kpi')->result();
		$ros8 = $this->yeah->getKategori('id_kpi',8,'t_kategori')->result();

		$rows9 = $this->yeah->getById('id_kpi',9,'t_kpi')->result();
		$ros9 = $this->yeah->getKategori('id_kpi',9,'t_kategori')->result();

		$rows10 = $this->yeah->getById('id_kpi',10,'t_kpi')->result();
		$ros10 = $this->yeah->getKategori('id_kpi',10,'t_kategori')->result();

		$rows11 = $this->yeah->getById('id_kpi',11,'t_kpi')->result();
		$ros11 = $this->yeah->getKategori('id_kpi',11,'t_kategori')->result();


		$rows16 = $this->yeah->getById('id_kpi',16,'t_kpi')->result();
		$ros16 = $this->yeah->getKategori('id_kpi',16,'t_kategori')->result();
		
		$rows17 = $this->yeah->getById('id_kpi',17,'t_kpi')->result();
		$ros17  = $this->yeah->getKategori('id_kpi',17,'t_kategori')->result();

		$data = array(
			'title' => 'Kelola KPI',
			'rows' => $rows,
			'rows1' =>$rows1,
			'ros1' => $ros1,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'ros4' => $ros4,
			'rows5' => $rows5,
			'ros5' => $ros5,
			'rows6' => $rows6,
			'ros6' => $ros6,
			'rows7' => $rows7,
			'ros7' => $ros7,
			'rows8' => $rows8,
			'ros8' => $ros8,
			'rows9' => $rows9,
			'ros9' => $ros9,
			'rows10' => $rows10,
			'ros10' => $ros10,
			'rows11' => $rows11,
			'ros11' => $ros11,
			'rows12' => $rows12,
			'ros12' => $ros12,
			'rows13' => $rows13,
			'ros13' => $ros13,
			'rows14' => $rows14,
			'ros14' => $ros14,
			'rows15' => $rows15,
			'ros15' => $ros15,
			'rows16' => $rows16,
			'ros16' => $ros16,
			'rows17' => $rows17,
			'ros17' => $ros17
		);
		$this->load->view('dosen/view_kpi',$data);
	}
	//hitung nilai usulan soal
	

	//hitung nilai  ass
	
//notif dosen
	public function data_notif(){
	//$data['results'] = $this->yeah->cek_notif_rapat($this->session->userdata('id_dosen'));
	$data['jumlah_rapat'] = count($this->yeah->cek_notif_rapat($this->session->userdata('id_dosen'))); 
	

	$data['results_rps'] = $this->yeah->cek_notif_rps($this->session->userdata('id_dosen'));
	$data['jumlah_rps'] = count($this->yeah->get_data_rps($this->session->userdata('id_dosen'))); 



	$data['results_modul'] = $this->yeah->cek_notif_modul($this->session->userdata('id_dosen'));
	$data['jumlah_modul'] = count( $this->yeah->get_data_modul($this->session->userdata('id_dosen'))); 

	$data['results_soal'] = $this->yeah->cek_notif_soal($this->session->userdata('id_dosen'));;
	$data['jumlah_soal'] = count($this->yeah->get_data_soal($this->session->userdata('id_dosen'))); 


	$data['results_bahan'] = $this->yeah->cek_notif_bahan($this->session->userdata('id_dosen'));
	$data['jumlah_bahan'] = count($this->yeah->get_data_bahan($this->session->userdata('id_dosen'))); 
	echo json_encode($data);
}
	

//Jadwal Menguji
public function JadwalMenguji(){
	$jadwal_de = $this->yeah->jadwal_menguji('t_jadwal_de');
	$jadwal_sidang = $this->yeah->jadwal_menguji('t_jadwal_sidang');
	$data = array(
		'title' => 'Jadwal Menguji',
		'jadwal_de' => $jadwal_de,
		'jadwal_sidang' => $jadwal_sidang
	);
	$this->load->view('dosen/pa/view_menguji',$data);
}

public function Perwalian(){
	$this->db->select('*')
		 	 ->where('id_semester',getsemester_aktif())
		 	 ->where('id_dosen',$this->session->id_dosen);
	$x = $this->db->get('t_perwalian');

	$data = array(
		'title' => 'Data Perwalian',
		'rows' => $x->result()

	);
	$this->load->view('dosen/view_perwalian',$data);
}

function file_perwalian(){
	
	$rows  = $this->yeah->gambar_perwalian($this->input->get('id'));
    echo json_encode($rows);
}

function file_lks(){
	
	$rows  = $this->yeah->gambar_lks($this->input->get('id'));
    echo json_encode($rows);
}

public function add_perwalian(){
	$config['upload_path']          = './uploadfile/perwalian/';
	$config['allowed_types']        = 'gif|jpg|png|jpeg';
	$config['max_size']             = 10000;
	$config['max_width']            = 10240;
	$config['max_height']           = 7680;
	$config['file_name'] = 'Perwalian_'.$this->input->post('kelas').'_'.getkode($this->session->id_dosen).'_'.date("Y-m-d_h-i-s");
 
	$this->load->library('upload', $config);
 	
	if ( ! $this->upload->do_upload('berkas')){
	$this->session->set_flashdata('msg','gagal');
	redirect('dosen/Perwalian');	
	}else{
		$w = $this->upload->data();
		$nama_file = $w['file_name'];
		$data= array(
		'kelas' => $this->input->post('kelas'),
		'tanggal' => date("Y-m-d"),
		'file_perwalian' => $nama_file,
		'id_dosen' => $this->session->id_dosen,
		'id_semester'=> getsemester_aktif()
		);
		$this->db->insert('t_perwalian',html_escape($data));;
		redirect('dosen/Perwalian');
	}
}

function edit_perwalian(){
	$rows  = $this->yeah->edit_perwalian($this->input->get('id'));
    echo json_encode($rows);
}
function edit_lks(){
	$rows  = $this->yeah->edit_lks($this->input->get('id'));
    echo json_encode($rows);
}

public function proses_edit_perwalian(){
	$config['upload_path']          = './uploadfile/perwalian/';
	$config['allowed_types']        = 'gif|jpg|png|jpeg';
	$config['max_size']             = 1000;
	$config['max_width']            = 10240;
	$config['max_height']           = 7680;
	$config['file_name'] = 'Perwalian_'.$this->input->post('kelas').'_'.getkode($this->session->id_dosen).'_'.date("Y-m-d_h-i-s");
 
	$this->load->library('upload', $config);
 
	if ( ! $this->upload->do_upload('berkas')){
	$this->session->set_flashdata('msg','gagal');
	redirect('dosen/Perwalian');
	}else{
		$w = $this->upload->data();
		
		$data= array(
		'file_perwalian' => $w['file_name'],
		'status' => 'waiting'
		);

		$where = $this->input->post('id_perwalian');
		$this->yeah->update($data,'t_perwalian',$where,'id_perwalian');

		unlink('./uploadfile/perwalian/'.$this->input->post('pile'));
		//$this->hitungnilai_perwalian(getiddosen_perwalian($where));
		redirect('dosen/Perwalian');
	}
}


public function proses_edit_lks(){
	$config['upload_path']          = './uploadfile/lks/';
	$config['allowed_types']        = 'gif|jpg|png|jpeg';
	$config['max_size']             = 1000;
	$config['max_width']            = 10240;
	$config['max_height']           = 7680;
	$config['file_name'] = 'LKS_'.getkode($this->session->id_dosen).'_'.date("Y-m-d_h-i-s");
 
	$this->load->library('upload', $config);
 
	if ( ! $this->upload->do_upload('berkas')){
	
	$this->session->set_flashdata('msg','gagal');
	redirect('dosen/LaporanLKS');
	}else{
		$w = $this->upload->data();
		
		$data= array(
		'file_lks' => $w['file_name'],
		'status' => 'waiting'
		);

		$where = $this->input->post('id_lks');
		$this->yeah->update($data,'t_lks',$where,'id_lks');

		unlink('./uploadfile/lks/'.$this->input->post('pile'));
		
		redirect('dosen/LaporanLKS');
	}
}
public function del_perwalian($id,$iddosen){
	$file = $this->db->query("SELECT file_perwalian from t_perwalian WHERE id_perwalian='$id'")->row();
	
	$this->load->helper("file");
	unlink('./uploadfile/perwalian/'.$file->file_perwalian);
	$this->db->delete('t_perwalian',array('id_perwalian' => $id));
	hitungnilai_perwalian($iddosen);
    redirect('dosen/Perwalian');
}
public function del_lks($id,$iddosen){
	$file = $this->db->query("SELECT file_lks from t_lks WHERE id_lks='$id'")->row();
	
	$this->load->helper("file");
	unlink('./uploadfile/lks/'.$file->file_lks);
	$this->db->delete('t_lks',array('id_lks' => $id));
	hitungnilai_lks_dosen($iddosen);
    redirect('dosen/LaporanLKS');
}

public function LaporanLKS(){
	$this->db->select('*')
		 	 ->where('id_semester',getsemester_aktif())
		 	 ->where('id_dosen',$this->session->id_dosen);
	$x = $this->db->get('t_lks');

	$data = array(
		'title' => 'Data LKS',
		'rows' => $x->result()

	);
	$this->load->view('dosen/view_lks',$data);
}

public function add_lks(){
	$config['upload_path']          = './uploadfile/lks/';
	$config['allowed_types']        = 'gif|jpg|png|jpeg';
	$config['max_size']             = 1000;
	$config['max_width']            = 10240;
	$config['max_height']           = 7680;
	$config['file_name'] = 'LKS_'.getkode($this->session->id_dosen).'_'.date("Y-m-d_h-i-s");
 
	$this->load->library('upload', $config);
 
	if ( ! $this->upload->do_upload('berkas')){
	$this->session->set_flashdata('msg','gagal');
	redirect('dosen/LaporanLKS');
	}else{
		$w = $this->upload->data();
		$nama_file = $w['file_name'];
		$data= array(
		'tgl_lks' => date("Y-m-d"),
		'file_lks' => $nama_file,
		'id_dosen' => $this->session->id_dosen,
		'id_semester'=> getsemester_aktif()
		);
		$this->db->insert('t_lks',html_escape($data));;
		redirect('dosen/LaporanLKS');
	}
}

}