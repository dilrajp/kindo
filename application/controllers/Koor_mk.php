<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koor_MK extends CI_Controller {

	public function __construct(){
		 parent::__construct();
		 
        if(!isset($_SESSION['username'])){
            redirect('login');
        }
		$this->load->helper('raj');
		$this->load->model('M_koormk','yeah');
		$this->session->set_userdata("info","Koor MK");
	}

	public function view_gambar(){
		$rows  = $this->yeah->gambar_bukti($this->input->get('id'));
        echo json_encode($rows);
	}

	public function index()
	{
		$rows = $this->yeah->getById('nip',$this->session->username,'t_dosen')->result();
		$id_matakuliah = $this->yeah->koormatkul();
		$listidmatakuliah = array();
		foreach ($id_matakuliah as $row) {
				$listidmatakuliah[] = $row->id_matakuliah;
		}
		if ($listidmatakuliah) {
			$progres1 = $this->yeah->progresnilai($listidmatakuliah);
			// $progres2 = $this->yeah->progresusulan($listidmatakuliah);
		}else{
			$progres1 = array();
			// $progres2 = array();
		}
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
            'title' => 'Dashboard Koordinator MK',
            'action' => site_url('login/check'),
            'rows' => $rows,
            'rows2' => $rows2,
            'rows3' => $rows3,
            'rows4' => $rows4,
            'rows5' => $rows5,
            'rows6' => $rows6,
            'rows7' => $rows7,
            'progres1' => $progres1
            );
		$this->load->view('koormk/dashboard_koormk',$data);
	}

	public function unggah_rps(){
		$rows = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$matkul = $this->yeah->getselectedmatakuliah($this->session->userdata('id_dosen'));
		$kondisi = $this->yeah->getKondisi(6);
		$data = array(
			'title' => 'Unggah RPS',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7,
			'matkul' => $matkul,
			'kondisi' => $kondisi
		);
		$this->load->view('koormk/rps/unggah_rps', $data);
	}

	
	//unggah rps
	public function unggah_rps_aksi(){
		$cek = $this->db->get_where('t_rps', array('id_matakuliah' => $this->input->post('id_matakuliah')));
		if ($cek->num_rows() > 0) {
			$this->session->set_flashdata('msg','done'); 
			redirect('koor_mk/unggah_rps');
		}else{
			$id_matakuliah = $this->input->post('id_matakuliah');
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/rps/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['max_size'] = 10000;

	   		$this->load->library('upload', $config);
			$this->upload->initialize($config); 
	   		
	   		if(! $this->upload->do_upload('file')){
	   			$error = array('error' => $this->upload->display_errors());
		   		$this->session->set_flashdata('msg','gagal');
				redirect('koor_mk/unggah_rps');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'id_matakuliah' => $id_matakuliah,
					'date' => $date,
					'status_rps' => 'WAITING',
					'fileName' => $dokumen['file_name']
				);
				$this->yeah->input_data_rps($data, 't_rps');
				$id = $this->yeah->last_datarps('t_rps','id_rps')->row();
				$matakuliah = $this->yeah->tampungRps($id_matakuliah);
				foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $matkul->id_dosen,
						'id_rps' => $id->id_rps,
						'status' => 'UNREAD'
					);
					$this->yeah->input_status_rps($data2, 't_statusrps');
				}
				redirect('koor_mk/lihat_rps');
			}
		}
		
	}

	//lihat rps
	public function lihat_rps(){
		$rows = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List RPS',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows6' => $rows6,
			'rows7' => $rows7
		);
		$this->load->view('koormk/rps/lihat_rps', $data);
	}

	public function hapus_rps($id_rps,$fileName){
		$this->load->helper("file");
		//$path = $this->yeah->getPath($id_rps);
		unlink('./uploadfile/rps/'.$fileName);
		$where = array('id_rps' => $id_rps);
		$this->yeah->hapus_data_rps($where, 't_rps');
		redirect('koor_mk/lihat_rps');
	}
	public function download_rps($fileName){
		$this->load->helper('download');
	    $data = file_get_contents(base_url('/uploadfile/rps/'.$fileName));
	    force_download($fileName, $data);
	}

	//revisi rps
	public function revisiRps($id){
		$date = date('Y-m-d');
       	$config['upload_path'] = './uploadfile/rps/';
       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
        $config['max_size'] = '10000';

        $this->load->library('upload',$config);
   		if(! $this->upload->do_upload()){
			redirect('koor_mk/lihat_rps');
		}else{
			$dokumen = $this->upload->data();
			$data = array(
				'date' => $date,
				'status_rps' => 'WAITING',
				'keterangan' => Null,
				'fileName' => $dokumen['file_name']
			);
			$this->yeah->update_revisi_rps($id,$data);
			$id_matakuliah = $this->input->post('id_matakuliah');
			$matakuliah = $this->yeah->tampungStatus($id_matakuliah);
			foreach ($matakuliah as $matkul) {
				$data2 = array(
					'status' => 'UNREAD'

				);
				$this->yeah->revisi_status_rps($id,$data2);
			}
			redirect('koor_mk/lihat_rps');
		}
	}

	//lihat modul
	public function lihat_modul(){
		$rows = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List Modul',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7
		);
		$this->load->view('koormk/modul/lihat_modul', $data);
	}
	public function unggah_modul(){
		$rows = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$matkul = $this->yeah->getselectedmatakuliah($this->session->userdata('id_dosen'));
		$kondisi = $this->yeah->getKondisi(7);
		$data = array(
			'title' => 'Unggah Modul',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7,
			'matkul' => $matkul,
			'kondisi' => $kondisi
		);
		$this->load->view('koormk/modul/unggah_modul', $data);
	}
	public function unggah_modul_aksi(){
		$cek = $this->db->get_where('t_modul', array('id_matakuliah' => $this->input->post('id_matakuliah')));
		if ($cek->num_rows() > 0) {
			// $id_modul =  $this->db->get_where('t_modul', array('id_matakuliah' => $this->input->post('id_matakuliah')))->row()->id_modul;
			// $date = date('Y-m-d');
	  //      	$config['upload_path'] = './uploadfile/modul/';
	  //      	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	  //       $config['max_size'] = 10000;

	  //       $this->load->library('upload', $config);
			// $this->upload->initialize($config); 

	  //  		if(! $this->upload->do_upload('file')){
	  //  			$error = array('error' => $this->upload->display_errors());
		 // 	    $this->session->set_flashdata('msg','gagal'); 
			// 	redirect('koor_mk/lihat_modul');
			// }else{
			// 	$dokumen = $this->upload->data();
			// 	$data = array(
			// 		'date' => $date,
			// 		'status_modul' => 'WAITING',
			// 		'fileName' => $dokumen['file_name']
			// 	);
			// 	$this->yeah->update_revisi_modul($id_modul,$data);
			// 	$id_matakuliah = $this->input->post('id_matakuliah');
			// 	$matakuliah = $this->yeah->tampungStatus($id_matakuliah);
			// 	foreach ($matakuliah as $matkul) {
			// 		$data2 = array(
			// 			'status' => 'UNREAD'

			// 		);
			// 		$this->yeah->revisi_status_modul($id_modul,$data2);
			// 	}
				$this->session->set_flashdata('msg','done'); 
				redirect('koor_mk/unggah_modul');
			// }
		}else{
			$id_matakuliah = $this->input->post('id_matakuliah');
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/modul/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['max_size'] = 10000;

	   		$this->load->library('upload', $config);
			$this->upload->initialize($config); 

	   		if(! $this->upload->do_upload('file')){
	   			$error = array('error' => $this->upload->display_errors());
		 	    $this->session->set_flashdata('msg','gagal');
				redirect('koor_mk/unggah_modul');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'id_matakuliah' => $id_matakuliah,
					'date' => $date,
					'status_modul' => 'WAITING',
					'fileName' => $dokumen['file_name']
				);
				$this->yeah->input_data_modul($data, 't_modul');
				$id = $this->yeah->last_data2('t_modul','id_modul')->row();
				$matakuliah = $this->yeah->tampungStatus($id_matakuliah);
				foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $matkul->id_dosen,
						'id_modul' => $id->id_modul,
						'status' => 'UNREAD'

					);
					$this->yeah->input_status_modul($data2, 't_statusmodul');
				}
				redirect('koor_mk/lihat_modul');
			}
		}	
	}

	public function hapus_modul($id_modul,$fileName){
		$this->load->helper("file");
		//$path = $this->yeah->getPath($id_modul);
		unlink('./uploadfile/modul/'.$fileName);
		$where = array('id_modul' => $id_modul);
		$this->yeah->hapus_data_modul($where, 't_modul');
		redirect('koor_mk/lihat_modul');
	}
	public function download_modul($fileName){
		$this->load->helper('download');

	    $data = file_get_contents(base_url('/uploadfile/modul/'.$fileName));
	    force_download($fileName, $data);
	}

	//revisi modul
	function revisiModul($id){
		$date = date('Y-m-d');
       	$config['upload_path'] = './uploadfile/modul/';
       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
        $config['max_size'] = '10000';

        $this->load->library('upload',$config);
   		if(! $this->upload->do_upload()){
			redirect('koor_mk/lihat_modul');
		}else{
			$dokumen = $this->upload->data();
			$data = array(
				'date' => $date,
				'status_modul' => 'WAITING',
				'fileName' => $dokumen['file_name']
			);
			$this->yeah->update_revisi_modul($id,$data);
			$id_matakuliah = $this->input->post('id_matakuliah');
			$matakuliah = $this->yeah->tampungStatus($id_matakuliah);
			foreach ($matakuliah as $matkul) {
				$data2 = array(
					'status' => 'UNREAD'

				);
				$this->yeah->revisi_status_modul($id,$data2);
			}
			redirect('koor_mk/lihat_modul');
		}
	}



	//lihat usulan soal
	public function lihat_usulan(){
		$rows = $this->yeah->getusulan($this->session->userdata('id_dosen'));
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List Usulan Soal',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7
		);
		$this->load->view('koormk/usulan/lihat_usulan', $data);
	}
	//view usulan using ajax
	public function view_usulan(){
		$data['results'] = $this->yeah->getusulan();
		$data['jumlah'] = count($data['results']); 
		echo json_encode($data);
	}
	public function lihat_status_usulan(){
		$rows = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$data = array(
			'rows' => $rows
		);
	}
	public function updateStatusUsulan($id,$iddosen){
		$data = array(
		'status_usulan' => 'APPROVE'
		);
		$data2 = array(
			'status' => 'SETUJU'
		);
		$this->yeah->update_data_usulan($id, $data, $data2);
		$this->hitungnilai_usulan($iddosen);
		redirect('koor_mk/lihat_usulan');
	}
	public function revisi_usulan(){
		$id_usulan = $this->input->post('identitas');
		$input = $this->input->post('keterangan');
		$this->yeah->update_revisi_usulan($id_usulan,html_escape(urldecode($input)));
	}

	public function download_usulan(){
		$this->load->helper('download');
		$fileName = $this->uri->segment(3);
	    force_download('uploadfile/usulan/'.$fileName, NULL);
	}
	
	//hitung nilai usulan
	public function hitungnilai_usulan($iddos){
		$matkul = $this->yeah->getselectedmatakuliah2($iddos);
		$nilai_per_matkul = 0;
		$nilai_per_asessment = 0;
		$total_matkul = 0;
			foreach($matkul as $row){
				$idmat = $row->id_matakuliah;			
				$cek_asessment = $this->db->query("SELECT * FROM t_usulansoal WHERE id_matakuliah ='$idmat' and id_dosen='$iddos'")->result();
					
				foreach($cek_asessment as $row2){
					
					$tgl_upload = $row2->date;
					$jenis = $row2->asessment;
					$tgl_deadline = $this->db->query("SELECT tgl_deadlineusulan FROM t_deadlineusulan WHERE id_matakuliah ='$idmat' and jenis_nilai = '$jenis' ")->row('tgl_deadlineusulan');

					$now = strtotime($tgl_upload);
					$dead = strtotime($tgl_deadline);
					$datediff = $dead - $now;

					$selisih =  round($datediff / (60 * 60 * 24));

					$nilai = 100;
					if($selisih < 0){ //start if
					$nilai = 100 - (10*$selisih*-1);
					if($nilai <= 60){
					$nilai = 60;
					}
					}

					$nilai_per_asessment += $nilai;
					}
				$total_matkul += 1;
				//echo "x";
				
			}
			$nilai_per_asessment = $nilai_per_asessment/3;
			$nilai_per_matkul = round($nilai_per_asessment/$total_matkul,2);
			$where = 'nilai_usulan'; 
			$idsem  = getsemester_aktif();
			$this->yeah->update_nilai2($nilai_per_matkul,$iddos,$where,$idsem);
			//echo $total_matkul.'<br>';
			//echo $nilai_per_matkul.'<br>';
			//echo $iddos.'<br>';
			//echo $where.'<br>';
			//echo $idsem.'<br>';

	}
	//batas hitung nilai usulan

	//lihat nilai asessment
	public function lihat_nilai(){
		$rows = $this->yeah->getnilaiasessment($this->session->userdata('id_dosen'));
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List Nilai Assessment',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7
		);
		$this->load->view('koormk/asessment/lihat_nilai', $data);
	}
	public function view_nilai(){
		$data['results'] = $this->yeah->getnilaiasessment();
		$data['jumlah'] = count($data['results']); 
		echo json_encode($data);
	}
	public function lihat_status_nilai(){
		$rows = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$data = array(
			'rows' => $rows
		);
	}
	public function updateStatusNilai($id,$iddosen){
		$data = array(
		'status_nilai' => 'APPROVE'
		);
		$data2 = array(
			'status' => 'SETUJU'
		);
		$this->yeah->update_data_nilai($id, $data, $data2);
		$this->hitungnilai_asessment($iddosen);
		redirect('koor_mk/lihat_nilai');
	}
	public function revisi_nilai(){
		$id_nilaiasessment = $this->input->post('identitas');
		$input = $this->input->post('keterangan');
		$this->yeah->update_revisi_nilai($id_nilaiasessment,html_escape(urldecode($input)));

	}

	public function download_nilai(){
		$this->load->helper('download');
		$fileName = $this->uri->segment(3);
	    force_download('uploadfile/nilai/'.$fileName, NULL);
	}


	//hitung nilai asessment
	public function hitungnilai_asessment($iddos){
	$matkul = $this->yeah->getselectedmatakuliah2($iddos);
	$nilai_per_matkul = 0;
	$nilai_per_asessment = 0;
	$total_matkul = 0;
		foreach($matkul as $row){
			$idmat = $row->id_matakuliah;			
			$cek_asessment = $this->db->query("SELECT * FROM t_nilaiasessment WHERE id_matakuliah ='$idmat' and id_dosen='$iddos'")->result();
				
			foreach($cek_asessment as $row2){
				
				$tgl_upload = $row2->date;
				$jenis = $row2->asessment;
				$tgl_deadline = $this->db->query("SELECT tgl_deadlinenilai FROM t_deadlinenilai WHERE id_matakuliah ='$idmat' and jenis_nilai = '$jenis' ")->row('tgl_deadlinenilai');

				$now = strtotime($tgl_upload);
				$dead = strtotime($tgl_deadline);
				$datediff = $dead - $now;

				$selisih =  round($datediff / (60 * 60 * 24));

				$nilai = 100;
				if($selisih < 0){ //start if
				$nilai = 100 - (10*$selisih*-1);
				if($nilai <= 60){
				$nilai = 60;
				}
				}

				$nilai_per_asessment += $nilai;
				}
			$total_matkul += 1;
			//echo "x";
			
		}
		$nilai_per_asessment = $nilai_per_asessment/3;
		$nilai_per_matkul = round($nilai_per_asessment/$total_matkul,2);
		$where = 'nilai_penyerahan'; 
		$idsem  = getsemester_aktif();
		$this->yeah->update_nilai2($nilai_per_matkul,$iddos,$where,$idsem);
		//echo $total_matkul.'<br>';
		//echo round($nilai_per_asessment/$total_matkul,2).'<br>';

	}
	//batas hitung
	//unggah soal asessment fix
	public function unggah_soal(){
		$rows = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$matkul = $this->yeah->getselectedmatakuliah($this->session->userdata('id_dosen'));
		$kondisi = $this->yeah->getKondisi(8);
		$data = array(
			'title' => 'Unggah Soal Asessment',
			'rows' => $rows,
			'rows2' =>$rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7,
			'matkul' => $matkul,
			'kondisi' => $kondisi
		);
		$this->load->view('koormk/soal/unggah_soal', $data);
	}
	public function unggah_soal_aksi(){
		$cek = $this->db->get_where('t_soal', array('id_matakuliah' => $this->input->post('id_matakuliah')));
		if ($cek->num_rows() > 0) {			
			$this->session->set_flashdata('msg','done');
			redirect('koor_mk/unggah_soal');
		}else{
			$id_matakuliah = $this->input->post('id_matakuliah');
			$date = date('Y-m-d');

			$this->load->library('upload');
	        //config untuk upload gambar
	        $config['upload_path'] = './uploadfile/soal/';
	        $config['allowed_types'] = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
	        $config['overwrite'] = FALSE;
	        $config['max_size'] = '5000';
	        $this->upload->initialize($config);
	        if (!$this->upload->do_upload('userfile1')) {
	            $this->session->set_flashdata('msg','gagal');
				redirect('koor_mk/unggah_soal');
	        } else {
	            unset($config);
	            $config['upload_path'] = './uploadfile/soal/';
	            // $config['upload_path'] = './assets/download/news/';
	            $config['allowed_types'] = 'txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp|';
	            $config['overwrite'] = FALSE;
	            $config['max_size'] = '15000';
	            $this->upload->initialize($config);
	            if (!$this->upload->do_upload('userfile2')) {
	                $this->session->set_flashdata('msg','gagal');
					redirect('koor_mk/unggah_soal');
	            } else {
	                $data = array(
						'id_matakuliah' => $id_matakuliah,
						'date' => $date,
						'status_soal' => 'WAITING',
						'bukti'	=> $_FILES['userfile1']['name'],
						'fileName' => $_FILES['userfile2']['name']
					);
					$this->yeah->input_data_soal($data, 't_soal');
					$id = $this->yeah->last_datasoal('t_soal','id_soal')->row();
					$matakuliah = $this->yeah->tampungSoal($id_matakuliah);
					foreach ($matakuliah as $matkul) {
						$data2 = array(
							'id_dosen' => $matkul->id_dosen,
							'id_soal' => $id->id_soal,
							'status' => 'UNREAD'
						);
						$this->yeah->input_status_soal($data2, 't_statussoal');
					}
					redirect('koor_mk/lihat_soal');
	            }
	        }
		}
	}

	//lihat soal
	public function lihat_soal(){
		$rows = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List Soal',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows7' => $rows7
		);
		$this->load->view('koormk/soal/lihat_soal', $data);
	}
	public function download_soal($fileName){
		$this->load->helper('download');
	    $data = file_get_contents(base_url('/uploadfile/soal/'.$fileName));
	    force_download($fileName, $data);
	}
	public function hapus_soal($id_soal,$fileName){
		$this->load->helper("file");
		//$path = $this->yeah->getPath($id_modul);
		unlink('./uploadfile/soal/'.$fileName);
		$where = array('id_soal' => $id_soal);
		$this->yeah->hapus_data_soal($where, 't_soal');
		redirect('koor_mk/lihat_soal');
	}

	//revisi soal
	function revisiSoal($id){
		$date = date('Y-m-d');
       	$config['upload_path'] = './uploadfile/soal/';
        $config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp|gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
        $config['max_size'] = '10000';

        $this->load->library('upload',$config);
   		if(!$this->upload->do_upload('userfile1') && !$this->upload->do_upload('userfile2')){
			redirect('koor_mk/lihat_soal');
		}else{
			$dokumen = $this->upload->data();
			$data = array(
				'date' => $date,
				'status_soal' => 'WAITING',
				'bukti'	=> $_FILES['userfile1']['name'],
				'fileName' => $_FILES['userfile2']['name']
			);
			$this->yeah->update_revisi_soal($id,$data);
			$id_matakuliah = $this->input->post('id_matakuliah');
			$matakuliah = $this->yeah->tampungStatus($id_matakuliah);
			foreach ($matakuliah as $matkul) {
				$data2 = array(
					'status' => 'UNREAD'

				);
				$this->yeah->revisi_status_soal($id,$data2);
			}
			redirect('koor_mk/lihat_soal');
		}
	}

	//unggh bahan perkuliahan
	public function unggah_bahan(){
		$rows = $this->yeah->all('id_tahunajaran','t_tahunajaran')->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$matkul = $this->yeah->getselectedmatakuliah($this->session->userdata('id_dosen'));
		$kondisi = $this->yeah->getKondisi(9);
		$data = array(
			'title' => 'Unggah Bahan Perkuliahan',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7,
			'matkul' => $matkul,
			'kondisi' => $kondisi
		);
		$this->load->view('koormk/bahan/unggah_bahan', $data);
	}
	public function unggah_bahan_aksi(){
		$cek = $this->db->get_where('t_bahanperkuliahan', array('id_matakuliah' => $this->input->post('id_matakuliah')));
		if ($cek->num_rows() > 0) {
			$this->session->set_flashdata('msg','done');
			redirect('koor_mk/unggah_bahan');
		}else{
			$id_matakuliah = $this->input->post('id_matakuliah');
			$date = date('Y-m-d');
	       	$config['upload_path'] = './uploadfile/bahan/';
	       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
	        $config['upload_max_filesize'] = '90M';
	   		$config['post_max_size'] =  '90M';

	   		$this->load->library('upload', $config);
			$this->upload->initialize($config);

	   		$this->load->library('upload',$config);
	   		if(! $this->upload->do_upload('file')){
	   			$error = array('error' => $this->upload->display_errors());
		    	$this->session->set_flashdata('msg','gagal');
				redirect('koor_mk/unggah_bahan');
			}else{
				$dokumen = $this->upload->data();
				$data = array(
					'id_matakuliah' => $id_matakuliah,
					'date' => $date,
					'status_bahan' => 'WAITING',
					'fileName' => $dokumen['file_name']
				);
				$this->yeah->input_data_bahan($data, 't_bahanperkuliahan');
				$id = $this->yeah->last_databahan('t_bahanperkuliahan','id_bahan')->row();
				$matakuliah = $this->yeah->tampungBahan($id_matakuliah);
				foreach ($matakuliah as $matkul) {
					$data2 = array(
						'id_dosen' => $matkul->id_dosen,
						'id_bahan' => $id->id_bahan,
						'status' => 'UNREAD'
					);
					$this->yeah->input_status_bahan($data2, 't_statusbahan');
				}
				redirect('koor_mk/lihat_bahan');
			}
		}
	}

	//lihat bahan perkuliahan
	public function lihat_bahan(){
		$rows = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'List bahan',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6
		);
		$this->load->view('koormk/bahan/lihat_bahan', $data);
	}
	public function download_bahan($fileName){
		$this->load->helper('download');
	    $data = file_get_contents(base_url('/uploadfile/bahan/'.$fileName));
	    force_download($fileName, $data);
	}
	public function hapus_bahan($id_bahan,$fileName){
		$this->load->helper("file");
		//$path = $this->yeah->getPath($id_modul);
		unlink('./uploadfile/bahan/'.$fileName);
		$where = array('id_bahan' => $id_bahan);
		$this->yeah->hapus_data_bahan($where, 't_bahanperkuliahan');
		redirect('koor_mk/lihat_bahan');
	}

	//revisi bahan perkuliahan
	function revisiBahan($id){
		$date = date('Y-m-d');
       	$config['upload_path'] = './uploadfile/bahan/';
       	$config['allowed_types'] ='txt|pdf|docx|doc|ppt|pptx|xls|xlsx|odp';
        $config['max_size'] = '10000';

        $this->load->library('upload',$config);
   		if(! $this->upload->do_upload()){
			redirect('koor_mk/lihat_bahan');
		}else{
			$dokumen = $this->upload->data();
			$data = array(
				'date' => $date,
				'status_bahan' => 'WAITING',
				'fileName' => $dokumen['file_name']
			);
			$this->yeah->update_revisi_bahan($id,$data);
			$id_matakuliah = $this->input->post('id_matakuliah');
			$matakuliah = $this->yeah->tampungStatus($id_matakuliah);
			foreach ($matakuliah as $matkul) {
				$data2 = array(
					'status' => 'UNREAD'

				);
				$this->yeah->revisi_status_bahan($id,$data2);
			}
			redirect('koor_mk/lihat_bahan');
		}
	}

	//list rapat kehadiran dosen
	public function list_rapat(){
		$rows = $this->yeah->rapat_koormk($this->session->id_dosen)->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$nama  = $this->yeah->nama_matkul()->result();
		$data = array(
            'title' => 'List Undangan',
            'rows' => $rows,
            'rows2' => $rows2,
            'rows3' => $rows3,
            'rows4' => $rows4,
            'rows5' => $rows5,
            'rows6' => $rows6,
            'rows7' => $rows7,
            'nama' => $nama
        );
		$this->load->view('koormk/rapat/list_rapat', $data);
	}

	//add rapat
public function add_rapat(){
		$matkul = $this->input->post('mk');
		$data = array(
			'agenda' => html_escape($this->input->post('agenda')),
			'ruangan' => html_escape($this->input->post('ruangan')),
			'tanggal_rapat' =>  html_escape($this->input->post('tanggal')),
			'tipe' => $this->input->post('radio'),
			'bukti' => 'x',
			'id_matakuliah' => html_escape($matkul),
			'id_semester' => getsemester_aktif()
			);
		$this->yeah->add($data,'t_undangan');
		$id = $this->yeah->last_data('t_undangan','id_undangan')->row();
		$matkul = $this->input->post('mk');
		$peserta = $this->yeah->getpesertarapat($this->input->post('mk'))->result();	
		foreach ($peserta as $x ) {
		if($this->session->id_dosen == $x->id_dosen) {
		$data3 = array(
			'id_undangan' => $id->id_undangan,
			'id_dosen' => $this->session->id_dosen,
			'status' => 'HADIR',
			'tipe_p' => '1',
			'keterangan' => ' ',
		);
		$this->yeah->add($data3,'t_kehadiran');
		hitungnilai_rapat_koor_dosen($this->session->id_dosen);
		hitung_nilai_koor($matkul,$this->session->id_dosen);
		} else {
		$data2 = array(
			'id_undangan' => $id->id_undangan,
			'id_dosen' => $x->id_dosen,
			'status' => 'TERKIRIM',
			'tipe_p' => '2',
			'keterangan' => ' ',
		);
		$this->yeah->add($data2,'t_kehadiran');

		hitungnilai_rapat_koor_dosen($x->id_dosen);
		}
		}

	redirect('koor_mk/list_rapat');
}

	public function add_bukti(){
		$id = $this->uri->segment(3);

		if (!empty($_FILES)) {
	        $path = $_FILES['file']['name'];
	   		$ext = pathinfo($path, PATHINFO_EXTENSION);
	   		$name = pathinfo($path, PATHINFO_FILENAME);
	        $tempFile = $_FILES['file']['tmp_name'];
	        $fileName = str_replace(" ", "-",'Bukti-'.$name . '_' . date("Y-m-d") . '_' . date("H-i-s") . '.' .$ext);
	        $date = date("Y-m-d");
	        $targetPath = getcwd() . '/uploadfile/rapat/';
	        $targetFile = $targetPath . $fileName ;
	        ini_set('upload_max_filesize', '90M');
	   		ini_set('post_max_size', '90M');
	   		$cek = $this->yeah->getbukti($id);
	   		if($cek == 'x'){
	   		$this->yeah->update(array('bukti'=>$fileName),'t_undangan',$id,'id_undangan');
	        move_uploaded_file($tempFile, $targetFile);
			}else{
			$this->load->helper("file");
			unlink('./uploadfile/rapat/'.$cek);
			$this->yeah->update(array('bukti'=>$fileName),'t_undangan',$id,'id_undangan');
	        move_uploaded_file($tempFile, $targetFile);
			}
		
        	
        }
 
       
	}
	public function del_rapat($id,$di){
		$cek = $this->yeah->getbukti($id);
		$id_koor = getiddosen_rapat($id);
		$this->load->helper("file");
		if($cek != 'x')
		{
		unlink('./uploadfile/rapat/'.$cek);
		}
		$id_dosen =  $this->db->query("SELECT id_dosen FROM t_kehadiran WHERE id_undangan = '$id'")->result();
		$this->yeah->delete('id_undangan',$id,'t_undangan');
		$this->yeah->delete('id_undangan',$id,'t_kehadiran');
		foreach($id_dosen as $x){
			hitungnilai_rapat_koor_dosen($x->id_dosen);
		}
		hitung_nilai_koor($di,$id_koor);
		redirect('koor_mk/list_rapat');
	}

	public function edit_rapat($id){
		$rows = $this->yeah->getById('id_undangan',$id,'t_undangan')->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$data = array(
			'title' => 'Edit Data',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7
		);
		$this->load->view("koormk/rapat/edit_rapat",$data);

	}
	public function proses_edit_rapat(){
		$data = array(
			'agenda' => $this->input->post('agenda'),
			'ruangan' => $this->input->post('ruangan'),
			'tanggal_rapat' =>  $this->input->post('tanggal'),
			'tipe' => $this->input->post('radio')
			);

		$where = $this->input->post('id');
		$this->yeah->update($data,'t_undangan',$where,'id_undangan');
		redirect('koor_mk/list_rapat');

	}
	public function rapat_hadir($id,$val,$id2,$iddosen){
		
		$this->yeah->update_status_rapat($id2,html_escape(urldecode($val)),'HADIR');
		hitungnilai_rapat_koor_dosen($iddosen);
		redirect('koor_mk/detail_rapat/'.$id);
	}

	public function rapat_alpha($id,$val,$id2,$iddosen){
		$this->yeah->update_status_rapat($id2,html_escape(urldecode($val)),'ALPHA');
		hitungnilai_rapat_koor_dosen($iddosen);
		redirect('koor_mk/detail_rapat/'.$id);
	}
	public function rapat_izin($id,$val,$id2,$iddosen){

		$this->yeah->update_status_rapat($id2,html_escape(urldecode($val)),'IZIN');
		hitungnilai_rapat_koor_dosen($iddosen);
		redirect('koor_mk/detail_rapat/'.$id);
	}


	public function detail_rapat($id){
		$rows = $this->yeah->getById('id_undangan',$id,'t_undangan')->result();
		$rows2 = $this->yeah->getStatusUsulan($this->session->userdata('id_dosen'));
		$rows3 = $this->yeah->getStatusNilai($this->session->userdata('id_dosen'));
		$rows4 = $this->yeah->getmodul($this->session->userdata('id_dosen'));
		$rows5 = $this->yeah->getrps($this->session->userdata('id_dosen'));
		$rows6 = $this->yeah->getsoal($this->session->userdata('id_dosen'));
		$rows7 = $this->yeah->getbahan($this->session->userdata('id_dosen'));
		$res = $this->yeah->getById('id_undangan',$id,'t_kehadiran')->result();
		$data = array(
			'title' => 'Detail Rapat',
			'rows' => $rows,
			'rows2' => $rows2,
			'rows3' => $rows3,
			'rows4' => $rows4,
			'rows5' => $rows5,
			'rows6' => $rows6,
			'rows7' => $rows7,
			'res' => $res
		);
		$this->load->view("koormk/rapat/detail_rapat",$data);
	}

//get ajax notif
public function notifKoor(){
	$id_matakuliah = $this->yeah->koormatkul();
	$listidmatakuliah = array();
	foreach ($id_matakuliah as $row) {
			$listidmatakuliah[] = $row->id_matakuliah;
	}
	if ($listidmatakuliah) {
		//$data['results_usulan'] = $this->yeah->get_data_usulan($listidmatakuliah);
		$data['jumlah_usulan'] = count($this->yeah->get_data_usulan($listidmatakuliah)); 

		//$data['results_nilai'] = $this->yeah->get_data_nilai($listidmatakuliah);
		$data['jumlah_nilai'] = count($this->yeah->get_data_nilai($listidmatakuliah)); 
	}else{
		//$data['results_usulan'] = array();
		$data['jumlah_usulan'] = 0;
		//$data['results_nilai'] = array();
		$data['jumlah_nilai'] = 0; 
	}
	echo json_encode($data);
}


//deadlineusulan
		function lihat_deadline(){
			$rows = $this->yeah->getkpiusulannilai();
			$rows10 = $this->yeah->getById('id_kpi',10,'t_kpi')->result();
			$rows11 = $this->yeah->getById('id_kpi',11,'t_kpi')->result();
			$ros10 = $this->yeah->getDeadlineUsulan();
			$ros11 = $this->yeah->getDeadlineNilai();
			// $rows11 = $this->yeah->getById('id_kpi',11,'t_kpi')->result();
			// $ros11 = $this->yeah->getKategori('id_kpi',11,'t_kategori')->result();
			$data = array(
				'title' => 'Deadline',
				'rows' => $rows,
				'rows10' => $rows10,
				'ros10' => $ros10,
				'rows11' => $rows11,
				'ros11' => $ros11
			);
			$this->load->view('koormk/kpi/batas_unggah', $data);
		}
		function tambah_deadline_usulan(){
			$id_matakuliah = $this->input->post('id_matakuliah');
			$nilai_usulan = $this->input->post('nilai_usulan');
			$tgl_deadlineusulan = $this->input->post('tgl_deadlineusulan');
			$jenis_nilai = $this->input->post('jenis_nilai');

			$cek = $this->db->get_where('t_deadlineusulan', array('id_matakuliah' => $this->input->post('id_matakuliah')));
			$cek = $this->db->query("select * from t_deadlineusulan where jenis_nilai = '$jenis_nilai' and id_matakuliah= '$id_matakuliah'")->num_rows();
			if ($cek > 0)  {
			$cek = $this->db->query("select id_deadlineusulan from t_deadlineusulan where jenis_nilai = '$jenis_nilai' and id_matakuliah= '$id_matakuliah'")->row();
			$data = array(
					'tgl_deadlineusulan' => $tgl_deadlineusulan,
					);
				$this->yeah->update_deadline_usulan($cek->id_deadlineusulan, $data);
				redirect('koor_mk/lihat_deadline');
			}else{
				
			$data = array(
					'id_matakuliah' => $id_matakuliah,
					'nilai_usulan' => $nilai_usulan,
					'tgl_deadlineusulan' => $tgl_deadlineusulan,
					'jenis_nilai' => $jenis_nilai
			);
			$this->yeah->tambah_deadline_usulan($data, 't_deadlineusulan');
			redirect('koor_mk/lihat_deadline');
			}
		}
		public function edit_kpiusulan($id){
			$matkul = $this->yeah->getselectedmatakuliah($this->session->userdata('id_dosen'));
			$rows = $this->yeah->getDeadlineUsulan();
			$rows2 = $this->yeah->getById('id_kpi',$id,'t_kpi')->result();
			$rows3 = $this->yeah->getDeadlineNilai();
			$data = array(
				'title' => 'Edit KPI',
				'kpi' => $rows2,
				'matkul' => $matkul,
				'penilaian' => $rows,
				'penilaian2' => $rows3,
				'id' => '1'
			);
			$this->load->view('koormk/kpi/edit_kpi',$data);
		}
		public function edit_nilai(){

			$where3 = $this->input->post('id');
			$where2 = $this->input->post('id2');

			$kon = $this->input->post('tgl_deadlineusulan');
			$nil = $this->input->post('nilai_usulan');
			$where = "tgl_deadlineusulan ='$kon'";
			$this->db->select('*')
					 ->where($where)
					 ->from('t_deadlineusulan');
			$q = $this->db->get();
			if($q->num_rows() > 0){
			$this->session->set_flashdata('msg','not ok deadline');
			redirect('koor_mk/edit_kpiusulan/'.$where2);
			}else{
			$data = array(
				'tgl_deadlineusulan' => html_escape($this->input->post('tgl_deadlineusulan')),
			);
			
			$this->yeah->update($data,'t_deadlineusulan',$where3,'id_deadlineusulan');
			$this->session->set_flashdata('msg','ok');
			redirect('koor_mk/edit_kpiusulan/'.$where2);
			}
		}
		public function edit_nilai2(){

			$where3 = $this->input->post('id');
			$where2 = $this->input->post('id2');

			$kon = $this->input->post('tgl_deadlinenilai');
			$nil = $this->input->post('nilai_assessment');
			$cek = $this->db->query("select * from t_deadlinenilai where jenis_nilai = '$jenis_nilai' and id_matakuliah= '$id_matakuliah'")->num_rows();
			if($cek > 0){
			$this->session->set_flashdata('msg','not ok deadline');
			redirect('koor_mk/edit_kpiusulan/'.$where2);
			}else{
			$data = array(
				'tgl_deadlinenilai' => html_escape($this->input->post('tgl_deadlinenilai')),
			);
			
			$this->yeah->update($data,'t_deadlinenilai',$where3,'id_deadlinenilai');
			$this->session->set_flashdata('msg','ok');
			redirect('koor_mk/edit_kpiusulan/'.$where2);
			}
		}
		//add nilai asessment
		function tambah_deadline_nilai(){

			$id_matakuliah = $this->input->post('id_matakuliah');
			$nilai_asessment = $this->input->post('nilai_assessment');
			$tgl_deadlinenilai = $this->input->post('tgl_deadlinenilai');
			$jenis_nilai = $this->input->post('jenis_nilai');

			$cek = $this->db->get_where('t_deadlinenilai', array('id_matakuliah' => $this->input->post('id_matakuliah')));
			$cek = $this->db->query("select * from t_deadlinenilai where jenis_nilai = '$jenis_nilai' and id_matakuliah= '$id_matakuliah'")->num_rows();
			if ($cek > 0)  {
			$cek = $this->db->query("select id_deadlinenilai from t_deadlinenilai where jenis_nilai = '$jenis_nilai' and id_matakuliah= '$id_matakuliah'")->row();
			$data = array(
					'tgl_deadlinenilai' => $tgl_deadlinenilai,
					);
				$this->yeah->update_deadline_nilai($cek->id_deadlinenilai, $data);
				redirect('koor_mk/lihat_deadline');
			}else{
				
			$data = array(
					'id_matakuliah' => $id_matakuliah,
					'nilai_assessment' => $nilai_asessment,
					'tgl_deadlinenilai' => $tgl_deadlinenilai,
					'jenis_nilai' => $jenis_nilai
			);
			$this->yeah->tambah_deadline_nilai($data, 't_deadlinenilai');
			redirect('koor_mk/lihat_deadline');
			}
		}
public function logout(){
	session_destroy();
	redirect('login');
}

}