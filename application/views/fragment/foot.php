	<div class="page-loading-overlay">
	<div class="loader-2"></div>
	</div>
	<!-- Bottom Scripts -->


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>">
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>"></script>
	

</body>
</html>