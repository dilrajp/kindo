
					<li class="dropdown hover-line">
						<a href="#" data-toggle="dropdown" id="getTotal">
							
						</a>
					
						<ul class="dropdown-menu notifications">
							<li class="top">
								<p class="small" id="getTotal2">
									
								
								</p>
							</li>
						
							<!-- notif rapat -->
								<li>
								<ul class="dropdown-menu-list list-unstyled ps-scrollbar">
									<li class="notification-info" id= "getModul">
										
									</li>
									<li class="notification-danger" id= "getSoal">
										
									</li>
									<li class="notification-success" id="getRps">
										
									</li>
									
									<li class="notification-warning" id="getBahan">
										
									</li>
									<li class="notification-success" id="getPerwalian">
										
									</li>
									<li class="notification-success" id="getLKS">
										
									</li>
									
								</ul>
								</li>
						</ul>
					</li>

					<script type="text/javascript">
					
						viewNotif();
						//val untuk input type
						function viewNotif(){
							var text = "";
							var text2 = "";
							var text3 = "";
							var text4 = "";
							var text5 = "";
							var text6 = "";
							var total =0;
							  $.ajax({
						        url: '<?php echo base_url('kaprodi/data_notif'); ?>',
						        dataType:'json',
						        type: "post",
						        success: function(data){
						        	var no = 1;
						        	if (data.jumlah_modul > 0) {
						        		text += '<a href="<?php echo base_url('kaprodi/lihat_modul');?>"> <i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>New Modul</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#getModul').html(text);
						          		total++;
						        	}

						     

						        	if (data.jumlah_soal > 0) {
						        		text2 += '<a href="<?php echo base_url('kaprodi/lihat_soal');?>"><i class="fa-file-text-o"></i>\
									        		<span class="line">\
															<strong>New Soal</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#getSoal').html(text2);
						          		total++;
						        	}
						           //console.log(data);

						           if (data.jumlah_rps > 0) {
						        		text3 += '<a href="<?php echo base_url('kaprodi/lihat_rps');?>"><i class="fa-file-powerpoint-o"></i>\
									        		<span class="line">\
															<strong>New Rps</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#getRps').html(text3);
						          		total++;
						        	}

						        	if (data.jumlah_bahan > 0) {
						        		text4 += '<a href="<?php echo base_url('kaprodi/lihat_bahan');?>"><i class="fa-file-archive-o"></i>\
									        		<span class="line">\
															<strong>New Bahan</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#getBahan').html(text4);
						          		total++;
						        	}
						        	if (data.jumlah_perwalian > 0) {
						        		text5 += '<a href="<?php echo base_url('kaprodi/LaporanPerwalian');?>"> <i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>Laporan Perwalian</strong>\
													</span>\
														<span class="line small time">\
															<strong>'+data.jumlah_perwalian+'</strong> Laporan Perwalian Baru\
													</span>\
												</a>';
						          		$('#getPerwalian').html(text5);
						          		total++;
						        	}

						        	if (data.jumlah_lks > 0) {
						        		text5 += '<a href="<?php echo base_url('kaprodi/LKS');?>"> <i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>Laporan LKS</strong>\
													</span>\
														<span class="line small time">\
															<strong>'+data.jumlah_lks+'</strong> Laporan LKS Baru\
													</span>\
												</a>';
						          		$('#getPerwalian').html(text5);
						          		total++;
						        	}

						        	if (total > 0) {
						        		$('#getTotal').html('<i class="fa-bell-o"></i><span class="badge badge-purple">'+total+'</span>');
						        		$('#getTotal2').html('You have <strong>'+total+'</strong> new notifications.');
						        	}else{
						        		$('#getTotal').html('<i class="fa-bell-o"></i><span class="badge badge-purple"></span>');
						        		$('#getTotal2').html('You have <strong>'+0+'</strong> new notifications.');
						        	}
						           //console.log(data);
						        }
						      });
						}
						setInterval(function(){ viewNotif(); }, 2000);

						
					</script>