<li class="dropdown user-profile">
						<a href="#" data-toggle="dropdown">
							<img src="<?php echo base_url('assets/images/foto/').getfoto($this->session->id_dosen); ?>" alt="user-image" class="img-circle img-inline userpic-32" width="28" />
							<span>
								<?php echo $this->session->userdata('nama');?>
								<i class="fa-angle-down"></i>
							</span>
						</a>
						
						<ul class="dropdown-menu user-profile-menu list-unstyled">					
							<li>
								<a href="<?php echo base_url('login/power_ranger/') ?>" target="_blank">
									<i class="fa-user"></i>
									Dev Team
								</a>
							</li>
							<li>
								<a href="#help">
									<i class="fa-paper-plane-o"></i>
									Go To APPD
								</a>
							</li>	
							<li>
								<a href="#help">
									<i class="fa-paper-plane"></i>
									Go To AP MODO
								</a>
							</li>
							<li class="last">
								<a href="<?php echo site_url('login/logout'); ?>">
									<i class="fa-lock"></i>
									<b>Logout</b>
								</a>
							</li>
						</ul>
					</li>
					
					<!-- <li>
						<a href="#" data-toggle="chat">
							<i class="fa-comments-o"></i>
						</a>
					</li> -->
					
				</ul>
				