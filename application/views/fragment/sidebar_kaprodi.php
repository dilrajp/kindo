<div class="settings-pane">
			
		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>
			<?php 
		$iddosen = $this->session->id_dosen;
		$qak = $this->db->query("SELECT * from t_dosen where id_dosen = '$iddosen'")->result();

		?>	
		<div class="settings-pane-inner">
			
			<div class="row">
				
				<div class="col-md-4">
					
					<div class="user-info">
						
						<div class="user-image">
							<a href="extra-profile.html">
								<img src="<?php echo base_url('assets/images/foto/').getfoto($this->session->id_dosen); ?>" class="img-responsive img-circle" />
							</a>
						</div>
						
						<div class="user-details">
							
							<h3>
								<a href="extra-profile.html"><?php echo $this->session->userdata('nama'); ?></a>
								
								<!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
								<span class="user-status is-online"></span>
							</h3>
							
							<p class="user-title"><?php echo get_level($this->session->level); ?></p>
							
							<div class="user-links">
							<a href="extra-profile.html" class="btn btn-success">Edit Profile</a>
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="col-md-8 link-blocks-env">
					
					<div class="links-block left-sep">
						<h4>
							<span>Profil Dosen</span>
						</h4>

						
						<ul class="list-unstyled">
								<?php foreach($qak as $rows){ ?>

							<li>
								<label for="sp-chk1"><font color="#68b828" face="Verdana">NIP</font> :<font style="font-weight: bold"> <?= $rows->nip ?></font></label>
							</li>
							<li>
								<label for="sp-chk1"><font color="#68b828" face="Verdana">Email</font> :<font style="font-weight: bold"><?= $rows->email_dosen ?></font></label>
							</li>
							<li>
								<label for="sp-chk3"><font color="#68b828" face="Verdana">Jabatan Akademik</font> :<font style="font-weight: bold"> <?= $rows->jabatan_akademik ?> </font></label>
							</li>
							<?php } ?>
							<li>
								<label for="sp-chk4"><font color="#68b828" face="Verdana"> Mata Kuliah Aktif</font> :
							<?php 
					
							$sem = getsemester_aktif();
							$daftar_matkul = $this->db->query("SELECT * FROM `t_pengajaran` join t_matakuliah using(id_matakuliah)
								join t_semester using(id_semester) where id_dosen= '$iddosen' and t_matakuliah.id_semester = '$sem'");
							foreach($daftar_matkul->result() as $rows){
							echo $rows->nama_matakuliah.' | ';
							}
							?> 	</label>
							</li>
						</ul>
					</div>
					

					
				</div>
				
			</div>
		
		</div>
		
	</div>
	
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="#" class="logo-expanded">
							<img src="<?php echo base_url('assets/images/kindo.png'); ?>" width="80" alt="" />
						</a>
						
						<a href="#" class="logo-collapsed">
							<img src="<?php echo base_url('assets/images/kindo.png'); ?>" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					<div class="settings-icon">
						<a href="#" data-toggle="settings-pane" data-animate="true">
							<i class="linecons-cog"></i>
						</a>
					</div>
								
				</header>		
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li class="active">
						<a href="<?php echo base_url().'kaprodi'?>">
							<i class="linecons-user"></i>
							<span class="title">Ketua Prodi</span>
						</a>
					</li>
					<li >
						<a href="<?php echo base_url().'kaprodi/ListIndikator'?>">
							<i class="linecons-cog"></i>
							<span class="title">Indikator Kinerja</span>
						</a>
					</li>
					<li >
						<a >
							<i class="linecons-calendar"></i>
							<span class="title">Agenda Program Studi</span>
						</a>

						<ul>
							<li >
								<a href="<?php echo base_url().'kaprodi/list_rapat';?>">
								
									<span class="title">Kelola Undangan Rapat</span>
								</a>
							</li>

							<li>
								<a href="<?php echo base_url().'kaprodi/view_nilai';?>">
									<span class="title">Verifikasi Nilai</span>
								</a>
							</li>
						</ul>

					</li>


					<li >
						<a href="<?php echo base_url('kaprodi/LaporanPerwalian'); ?>">
							<i class="linecons-doc"></i>
							<span class="title">Laporan Perwalian</span>
						</a>
					</li>
				<li >
						<a href="<?php echo base_url('kaprodi/LKS'); ?>">
							<i class="linecons-note"></i>
							<span class="title">Laporan LKS</span>
						</a>
					</li>



					<li>
						<a href="#">
							<i class="fa-file-o"></i>
							<span class="title">Kelola EDOM</span>
						</a>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/lihat_edom'); ?>">
									<span class="title">Lihat EDOM</span>
								</a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/unggah_edom'); ?>">
									<span class="title">Unggah EDOM</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">
							<i class="fa-file-o"></i>
							<span class="title">Kelola BAP</span>
						</a>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/lihat_bap'); ?>">
									<span class="title">Lihat BAP</span>
								</a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/unggah_bap'); ?>">
									<span class="title">Unggah BAP</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">
							<i class="fa-file-text-o"></i>
							<span class="title">Approval</span>
						</a>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/lihat_modul'); ?>">
										<span class="title">Modul Praktikum</span>
								</a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/lihat_soal'); ?>">
									<span class="title">Soal Asessment</span>
								</a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/lihat_rps'); ?>">
									<span class="title">RPS</span>
								</a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="<?php echo base_url('kaprodi/lihat_bahan'); ?>">
									<span class="title">Bahan Perkuliahan</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="<?php echo base_url('kaprodi/lihat_angkatan'); ?>">
							<i class="fa-file-o"></i>
							<span class="title">Tambah Angkatan</span>
						</a>
					</li>
				</ul>	
			</div>
		</div>