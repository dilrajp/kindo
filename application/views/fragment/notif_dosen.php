
					<li class="dropdown hover-line">
						<a href="#" data-toggle="dropdown" id="getTotal">
							
						</a>
					
						<ul class="dropdown-menu notifications">
							<li class="top">
								<p class="small" id="getTotal2">
									
								</p>
							</li>
						
							<!-- notif rapat -->
								<li>
									<ul class="dropdown-menu-list list-unstyled ps-scrollbar">
										<li class="active notification-danger" id="ambilRapat">
											
										</li>
								
										<li class="notification-info" id="ambilModul">
											
										</li>
									
										<li class="notification-danger" id="ambilSoal">
											
										</li>
										
										<li class="notification-success" id="ambilRps">
											
										</li>
										
										<li class="notification-warning" id="ambilBahan">
											
										</li>
										
									</ul>
								</li>
						</ul>
					</li>

					<script type="text/javascript">
						viewNotif();
						//val untuk input type
						function viewNotif(){
							var text = "";
							var text2 = "";
							var text3 = "";
							var text4 = "";
							var text5 = "";
							var text6 = "";
							var total =0;
							  $.ajax({
						        url: '<?php echo base_url('Dosen/data_notif'); ?>',
						        dataType:'json',
						        type: "post",
						        success: function(data){
						        	var no = 1;
						        	if (data.jumlah_rapat > 0) {
						        		text += '<a href="<?php echo base_url('Dosen/ListRapat');?>">\
						        		 <i class="fa-calendar"></i>\
									        		<span class="line">\
															<strong>Anda memiliki undangan rapat</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#ambilRapat').html(text);
						          		total++;
						        	}

						     
						        	if (data.jumlah_modul > 0) {
						        		text2 += '<a href="<?php echo base_url('dosen/updateStatusModul/');?>'+data.results_modul[0].id_statusmodul+'">\
						        		 <i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>New Modul</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#ambilModul').html(text2);
						          		total++;
						        	}
						           //console.log(data);

						        	if (data.jumlah_rps > 0) {
						        		text3 += '<a href="<?php echo base_url('dosen/updateStatusRps/');?>'+data.results_rps[0].id_statusrps+'">\
						        		 <i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>New RPS</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#ambilRps').html(text3);
						          		total++;
						        	}
						        	if (data.jumlah_soal > 0) {
						        		text4 += '<a href="<?php echo base_url('dosen/updateStatusSoal/');?>'+data.results_soal[0].id_statussoal+'">\
						        		<i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>New Soal Asessment</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#ambilSoal').html(text4);
						          		total++;
						        	}
						        	if (data.jumlah_bahan > 0) {
						        		text5 += '<a href="<?php echo base_url('dosen/updateStatusBahan/');?>'+data.results_bahan[0].id_statusbahan+'"> <i class="fa-file-pdf-o"></i>\
									        		<span class="line">\
															<strong>New Bahan Perkuliahan</strong>\
													</span>\
														<span class="line small time">\
															Mohon segera ditindak lanjuti\
													</span>\
												</a>';
						          		$('#ambilBahan').html(text5);
						          		total++;
						        	}

						        	if (total > 0) {
						        		$('#getTotal').html('<i class="fa-bell-o"></i><span class="badge badge-purple">'+total+'</span>');
						        		$('#getTotal2').html('You have <strong>'+total+'</strong> new notifications.');
						        	}else{
						        		$('#getTotal').html('<i class="fa-bell-o"></i><span class="badge badge-purple"></span>');
						        		$('#getTotal2').html('You have <strong>'+0+'</strong> new notifications.');
						        	}
						           //console.log(data);
						        }
						      });
						}
						setInterval(function(){ viewNotif(); }, 2000);

						
					</script>
