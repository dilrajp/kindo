<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Hasil Kontribusi Kelulusan</h1>
					<p class="description">Tahun <?php echo nama_tahun(gettahun_aktif()); ?></p>
				</div>
				
					<div class="breadcrumb-env">
					
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>

					<li class="active">
					<a href="forms-native.html">Kontribusi Kelulusan</a>
					</li>
					</ol>
								
				</div>
					
			</div>
	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">List Hasil Kontribusi Dosen Tahun </h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
					
					<table class="table table-striped table-bordered" id="example-3"  style="word-break:normal">
						<thead>
							<tr class="replace-inputs">
								
								<th>NIP</th>
								<th>Nama</th>
								<th width="10%">Jumlah PBB1 Sidang</th>
								<th width="10%">Jumlah PBB2 Sidang</th>
								<th width="10%">Total Pembimbing Sidang</th>
								<th width="10%">Jumlah Bimbingan</th>
								<th>Prosentase</th>
								<th>Tahun</th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        
					        foreach ($rows as $row) {
					        ?>
							<tr>
								
								<td><?php echo $row->nip; ?>	</td>
								<td><?php echo $row->nama_dosen; ?></td>
								<td class="center"><?php echo $row->jml_pbb1; ?></td>
								<td class="center"><?php echo $row->jml_pbb2; ?></td>
								<td ><?php echo $row->jml_pbb1+$row->jml_pbb2; ?></td>
								<td class="center"><?php echo $row->jml_bimbingan; ?></td>
								<td class="center"><?php echo $row->prosentase."%"; ?></td>
								<td class="center"><?php echo nama_tahun($row->tahun); ?></td>
								
							</tr>
							<?php } ?>
						</tbody>	
							
					
					</table>
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1" id="kek">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2018
						<strong>Manajemen Informatika</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
			
	
		
		
	</div>

	
    

	<?php
	$this->load->view("fragment/foot");
	?>

 
	<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 0,filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							{column_number : 4, filter_type: 'text'},
							{column_number : 5, },
							{column_number : 6, },
							
						]);
						$('#kek').removeAttr('style');
					});
    </script>