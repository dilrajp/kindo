<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>

<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Hasil Kontribusi Kelulusan</h1>
					<p class="description"><!-- Tahun <?php echo nama_tahun(gettahun_aktif()); ?> --></p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1">
									<li>
							<a href="index"><i class="fa-home"></i>Home</a>
						</li>
								<li class="active">
						
										<a href="forms-native.html">Kontribusi Kelulusan</a>
								</li>
						
								
				</div>
					
			</div>
		
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Pilih Tahun Ajaran</h3>
					
				
				</div>
				<div class="panel-body">
				<script type="text/javascript">
				jQuery(document).ready(function($)
				{

				$("#tahunn").select2({
				    placeholder: "Select a state",
				    allowClear: true
				});
						
					});
					</script>
				<form method="post" id="form-ajax">
				<select class="form-control" id="tahunn" name="tahun" onchange="getval(this);" >
					<optgroup label="Tahun Ajaran ">
						<option value=""> -Pilih Tahun Ajaran-</option>
					<?php foreach ($tahun as $row) {		?>	
						<option value="<?php echo $row->id_tahunajaran ?>" ><?php echo nama_tahun($row->id_tahunajaran); ?></option>
					<?php } ?>
					</optgroup>
				</select>		
				</form>
				</div>
			</div>	
			
<script type="text/javascript">
	setInterval(function(){getval($('#tahunn'))},1000);
	function getval(sel)
{
   // alert(sel.value);

   var tahun = $(sel).val();
    		if(tahun){  
                $.ajax({  
                    url:"<?php echo base_url('koor_pa/getKontribusiKel'); ?>",  
                    data: $("#form-ajax").serialize(),
                    type: "POST",  
                    dataType: "json",
				    success: function(data){
				    	
				        if (data.total > 0) {
				        	$("#show_data").empty();

				        	var no = 1;
				        	for (var i = 0; i < data.total; i++) {
				        		var teks = 	$('<tr>\
												<td>'+data.row[i].nip+'</td>\
												<td>'+data.row[i].nama+'</td>\
												<td>'+data.row[i].jml_bimbingan+'</td>\
												<td>'+data.row[i].total_mhs+'</td>\
												<td>'+data.row[i].prosentase+'</td>\
												<td>'+data.row[i].tahunajaran+'</td>\
												<td style="text-align:right;"><a href="javascript:;" class="btn btn-secondary fa-refresh icon-left item_edit" data="'+data.row[i].id_kl+'"></a></td>\
												</tr>');
				        	
				        		$("#show_data").append(teks);

				        		no++;
					        }   
				        } else {
				        	$("#show_data").empty();
			            	var teks = 	$('<tr>">\
											<td colspan="7">\
												Tidak ada data\
											</td>\
										</tr>');
			            	$("#show_data").html(teks);
				        }
				    }  
            	});
            } else {
            	$("#show_data").empty();
            	var teks = 	$('<tr>">\
								<td colspan="7">\
									Silahkan tentukan Tahun\
								</td>\
							</tr>');
            	$("#show_data").html(teks);
            }  


}
</script>
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Laporan Kontribusi Kelulusan Dosen </h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
					
						<table class="table table-striped table-bordered" id="mydata">
						<thead>
							<tr class="replace-inputs">
								
								<th>NIP</th>
								<th>Nama</th>
								<th width="20%">Jumlah Bimbingan</th>
								<th width="20%">Mahasiswa Lulus</th>
								<th>Prosentase</th>
								<th>Tahun</th>
								<th>Aksi</th>
							</tr>
						</thead>
						 
						<tbody id="show_data">
								
						</tbody>
							
					
					</table>
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
	
		
		
	</div>
   

	<?php
	$this->load->view("fragment/foot");
	?>



<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Data </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						 <form role="form" method="POST" class="form-horizontal"  id="myForm" >
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIP</label>
									
									<div class="col-sm-10">
										<input name="nip2" type="text" class="form-control" id="nip2" disabled>
										<input name="id_dosen" type="hidden" class="form-control" id="nip" >
										<input name="id" type="hidden" id="id">
										<input name="jumlah" type="hidden" id="jumlah">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama </label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama" disabled>
								
									</div>
								</div>
							
								<div class="form-group">
									<label class="col-sm-2 control-label">Jumlah Bimbingan </label>
									<div class="col-sm-10">
										<input name="jumlah" class="form-control" type="number" id="jumlah" required>
								
									</div>
								</div>
		      					
		      					<div class="form-group">
									<label class="col-sm-2 control-label">Total Mahasiswa Lulus </label>
									<div class="col-sm-10">
										<input name="total" class="form-control" type="number" id="total" required>
								
									</div>
								</div>
		      					
					
							
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info" id="btn_update" >Simpan</button>

				</div>
			</form>		
					
    </div>
  </div>
</div>


	<script type="text/javascript">
$(document).ready(function(){
   // tampil_data_barang();   //pemanggilan fungsi tampil barang.
setInterval(getval($('#tahunn')),5000);
$("#show_data").empty();
var teks = 	$('<tr>">\
				<td colspan="7">\
					Silahkan tentukan Tahun\
				</td>\
			</tr>');
$("#show_data").html(teks);


	//GET UPDATE
	$('#show_data').on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url  : "<?php echo base_url('koor_pa/edit_kontribusi')?>",
            dataType : "JSON",
            data : {id:id},
            success: function(data){
            	$.each(data,function(id,nip,total,nama,jumlah,id_dosen){
                	$('#ModalaEdit').modal('show');
        			$('[name="id_dosen"]').val(data.id_dosen);
        			$('[name="nip2"]').val(data.nip);
        			$('[name="nama"]').val(data.nama);
        			$('[name="id"]').val(data.id);
        			$('[name="jumlah"]').val(data.jumlah);
        			$('[name="total"]').val(data.total);
        		
        		});
            }
        });
        return false;
    });

	

	 //Update Barang
		$('#btn_update').on('click',function(){
			
           	var data = $('#myForm').serialize();
            $.ajax({
               		type : "POST",
              		url: '<?php echo base_url(). 'koor_pa/proses_edit_kontribusi';?>',
					data :data,
				success: function(){
                    console.log($('#tahunn').val());
                   // alert("What follows is blank: ");
                    $('#ModalaEdit').modal('hide');
                    $('#show_data').empty();
                    getval($('#tahunn'));
                }
            });
            
            return false;
        });

    });		
 </script>
