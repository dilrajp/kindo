<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Unggah Data Kontribusi Kelulusan</h1>
					<p class="description">Halaman Upload File Kontribusi Kelulusan, Pada Tahun <?php echo nama_tahun(gettahun_aktif()) ?></p>
				</div>
				
				<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>

					<li class="active">
					<a href="forms-native.html">Upload KK</a>
					</li>
					</ol>
				</div>
					
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">File Excel<br></h3><br>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<h4 style="color:grey"> Download Data Kontribusi Kelulusan  <?php echo nama_tahun(gettahun_aktif()) ?> (Excel) untuk diunggah kedalam Aplikasi </h4>
					<a href="<?php echo base_url('kaprodi/export_kontribusi'); ?>" class="btn btn-success btn-icon btn-icon-standalone">
					<i class="fa-download"></i><span>Unduh Data</span>
					</a>
			
				</div>
			</div>	


			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Upload File Excel</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form role="form" class="form-horizontal" method="post" action="<?php echo base_url('kaprodi/createdata'); ?>" enctype="multipart/form-data" accept-charset="utf-8">
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="field-4">Unggah File (.xls/.xlxs)</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" id="field-4" name="file" required>
						</div>
					</div>
					<div class="form-group">
					<div class="col-sm-10">
					<?php echo $this->session->flashdata('msg'); echo "<br>" ?>
					<button type="submit" class="btn btn-info">Upload File</button>
					</div>
					</div>
					</form>			
				</div>
			</div>	

			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2018
						<strong>Manajemen Informatika</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
	</div>

	
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>


	<?php
	$this->load->view("fragment/foot");
	?>


	<?php if($this->session->flashdata('msg') == 'Sukses ...!!') { ?>	
	<script type="text/javascript">
		alertify
		.alert("Data berhasil di upload !", function(){
		});
	</script>
	<?php } ?>
	<?php if($this->session->flashdata('msg') == 'Ada kesalahan dalam proses upload') { ?>	
	<script type="text/javascript">
		alertify
		.alert("Error<br> Data tidak berhasil diupload... cek template file excel sesuai template.", function(){
		});
	</script>
	<?php  } ?>