<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_kaprodi");
?>

<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Soal Asessment</h1>
					<p class="description">Daftar Soal Asessment</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('kaprodi');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Soal Asessment</strong>
						</li>
						</ol>
				</div>		

			</div>

				<!-- modal animation -->
					<style>
						{
						box-sizing: border-box;
						}
						.muncul{
							height: 400px;
							width: 400px;
						}
						html, body {
						  min-height: 100%;
						  height: 100%;
						  background-image: url();
						  background-size: cover;
						  background-position: top center;
						  font-family: helvetica neue, helvetica, arial, sans-serif;
						  font-weight: 200;
						}
						html.modal-active, body.modal-active {
						  overflow: hidden;
						}

						#modal-container {
						  position: fixed;
						  display: table;
						  height: 100%;
						  width: 100%;
						  top: 0;
						  left: 0;
						  transform: scale(0);
						  z-index: 5;
						}
						#modal-container.one {
						  transform: scaleY(0.01) scaleX(0);
						  animation: unfoldIn 1s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.one .modal-background .modal {
						  transform: scale(0);
						  animation: zoomIn 0.5s 0.8s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.one.out {
						  transform: scale(1);
						  animation: unfoldOut 1s 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.one.out .modal-background .modal {
						  animation: zoomOut 0.5s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
					
						#modal-container.seven {
						  transform: scale(1);
						}
						#modal-container.seven .modal-background {
						  background: rgba(0, 0, 0, 0);
						  animation: fadeIn 0.5s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.seven .modal-background .modal {
						  height: 75px;
						  width: 75px;
						  border-radius: 75px;
						  overflow: hidden;
						  animation: bondJamesBond 1.5s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.seven .modal-background .modal h2, #modal-container.seven .modal-background .modal p {
						  opacity: 0;
						  position: relative;
						  animation: modalContentFadeIn .5s 1.4s linear forwards;
						}
						#modal-container.seven.out {
						  animation: slowFade .5s 1.5s linear forwards;
						}
						#modal-container.seven.out .modal-background {
						  background-color: rgba(0, 0, 0, 0.7);
						  animation: fadeToRed 2s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.seven.out .modal-background .modal {
						  border-radius: 3px;
						  height: 162px;
						  width: 227px;
						  animation: killShot 1s cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container.seven.out .modal-background .modal h2, #modal-container.seven.out .modal-background .modal p {
						  animation: modalContentFadeOut 0.5s 0.5 cubic-bezier(0.165, 0.84, 0.44, 1) forwards;
						}
						#modal-container .modal-background {
						  display: table-cell;
						  background: rgba(0, 0, 0, 0.8);
						  text-align: center;
						  vertical-align: middle;
						}
						#modal-container .modal-background .modal {
						  background: white;
						  padding: 100px;
						  display: inline-block;
						  border-radius: 3px;
						  font-weight: 300;
						  position: relative;
						}
						#modal-container .modal-background .modal h2 {
						  font-size: 25px;
						  line-height: 25px;
						  margin-bottom: 15px;
						}
						#modal-container .modal-background .modal p {
						  font-size: 18px;
						  line-height: 22px;
						}
						#modal-container .modal-background .modal .modal-svg {
						  position: absolute;
						  top: 0;
						  left: 0;
						  height: 100%;
						  width: 100%;
						  border-radius: 3px;
						}
						#modal-container .modal-background .modal .modal-svg rect {
						  stroke: #fff;
						  stroke-width: 2px;
						  stroke-dasharray: 778;
						  stroke-dashoffset: 778;
						}

						.content {
						  min-height: 100%;
						  height: 100%;
						  background: white;
						  position: relative;
						  z-index: 0;
						}
						.content h1 {
						  padding: 75px 0 30px 0;
						  text-align: center;
						  font-size: 30px;
						  line-height: 30px;
						}
						.content .buttons {
						  max-width: 800px;
						  margin: 0 auto;
						  padding: 0;
						  text-align: center;
						}
						.content .buttons .button {
						  display: inline-block;
						  text-align: center;
						  padding: 10px 15px;
						  margin: 10px;
						  background: red;
						  font-size: 18px;
						  background-color: #efefef;
						  border-radius: 3px;
						  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3);
						  cursor: pointer;
						}
						.content .buttons .button:hover {
						  color: white;
						  background: #009bd5;
						}

						@keyframes unfoldIn {
						  0% {
						    transform: scaleY(0.005) scaleX(0);
						  }
						  50% {
						    transform: scaleY(0.005) scaleX(1);
						  }
						  100% {
						    transform: scaleY(1) scaleX(1);
						  }
						}
						@keyframes unfoldOut {
						  0% {
						    transform: scaleY(1) scaleX(1);
						  }
						  50% {
						    transform: scaleY(0.005) scaleX(1);
						  }
						  100% {
						    transform: scaleY(0.005) scaleX(0);
						  }
						}
						@keyframes zoomIn {
						  0% {
						    transform: scale(0);
						  }
						  100% {
						    transform: scale(1);
						  }
						}
						@keyframes zoomOut {
						  0% {
						    transform: scale(1);
						  }
						  100% {
						    transform: scale(0);
						  }
						}
						@keyframes fadeIn {
						  0% {
						    background: rgba(0, 0, 0, 0);
						  }
						  100% {
						    background: rgba(0, 0, 0, 0.7);
						  }
						}
						@keyframes fadeOut {
						  0% {
						    background: rgba(0, 0, 0, 0.7);
						  }
						  100% {
						    background: rgba(0, 0, 0, 0);
						  }
						}
						@keyframes scaleUp {
						  0% {
						    transform: scale(0.8) translateY(1000px);
						    opacity: 0;
						  }
						  100% {
						    transform: scale(1) translateY(0px);
						    opacity: 1;
						  }
						}
						@keyframes scaleDown {
						  0% {
						    transform: scale(1) translateY(0px);
						    opacity: 1;
						  }
						  100% {
						    transform: scale(0.8) translateY(1000px);
						    opacity: 0;
						  }
						}
						@keyframes scaleBack {
						  0% {
						    transform: scale(1);
						  }
						  100% {
						    transform: scale(0.85);
						  }
						}
						@keyframes scaleForward {
						  0% {
						    transform: scale(0.85);
						  }
						  100% {
						    transform: scale(1);
						  }
						}
						@keyframes quickScaleDown {
						  0% {
						    transform: scale(1);
						  }
						  99.9% {
						    transform: scale(1);
						  }
						  100% {
						    transform: scale(0);
						  }
						}
						@keyframes slideUpLarge {
						  0% {
						    transform: translateY(0%);
						  }
						  100% {
						    transform: translateY(-100%);
						  }
						}
						@keyframes slideDownLarge {
						  0% {
						    transform: translateY(-100%);
						  }
						  100% {
						    transform: translateY(0%);
						  }
						}
						@keyframes moveUp {
						  0% {
						    transform: translateY(150px);
						  }
						  100% {
						    transform: translateY(0);
						  }
						}
						@keyframes moveDown {
						  0% {
						    transform: translateY(0px);
						  }
						  100% {
						    transform: translateY(150px);
						  }
						}
						@keyframes blowUpContent {
						  0% {
						    transform: scale(1);
						    opacity: 1;
						  }
						  99.9% {
						    transform: scale(2);
						    opacity: 0;
						  }
						  100% {
						    transform: scale(0);
						  }
						}
						@keyframes blowUpContentTwo {
						  0% {
						    transform: scale(2);
						    opacity: 0;
						  }
						  100% {
						    transform: scale(1);
						    opacity: 1;
						  }
						}
						@keyframes blowUpModal {
						  0% {
						    transform: scale(0);
						  }
						  100% {
						    transform: scale(1);
						  }
						}
						@keyframes blowUpModalTwo {
						  0% {
						    transform: scale(1);
						    opacity: 1;
						  }
						  100% {
						    transform: scale(0);
						    opacity: 0;
						  }
						}
						@keyframes roadRunnerIn {
						  0% {
						    transform: translateX(-1500px) skewX(30deg) scaleX(1.3);
						  }
						  70% {
						    transform: translateX(30px) skewX(0deg) scaleX(0.9);
						  }
						  100% {
						    transform: translateX(0px) skewX(0deg) scaleX(1);
						  }
						}
						@keyframes roadRunnerOut {
						  0% {
						    transform: translateX(0px) skewX(0deg) scaleX(1);
						  }
						  30% {
						    transform: translateX(-30px) skewX(-5deg) scaleX(0.9);
						  }
						  100% {
						    transform: translateX(1500px) skewX(30deg) scaleX(1.3);
						  }
						}
						@keyframes sketchIn {
						  0% {
						    stroke-dashoffset: 778;
						  }
						  100% {
						    stroke-dashoffset: 0;
						  }
						}
						@keyframes sketchOut {
						  0% {
						    stroke-dashoffset: 0;
						  }
						  100% {
						    stroke-dashoffset: 778;
						  }
						}
						@keyframes modalFadeIn {
						  0% {
						    background-color: transparent;
						  }
						  100% {
						    background-color: white;
						  }
						}
						@keyframes modalFadeOut {
						  0% {
						    background-color: white;
						  }
						  100% {
						    background-color: transparent;
						  }
						}
						@keyframes modalContentFadeIn {
						  0% {
						    opacity: 0;
						    top: -20px;
						  }
						  100% {
						    opacity: 1;
						    top: 0;
						  }
						}
						@keyframes modalContentFadeOut {
						  0% {
						    opacity: 1;
						    top: 0px;
						  }
						  100% {
						    opacity: 0;
						    top: -20px;
						  }
						}
						@keyframes bondJamesBond {
						  0% {
						    transform: translateX(1000px);
						  }
						  80% {
						    transform: translateX(0px);
						    border-radius: 75px;
						    height: 75px;
						    width: 75px;
						  }
						  90% {
						    border-radius: 3px;
						    height: 182px;
						    width: 247px;
						  }
						  100% {
						    border-radius: 3px;
						    height: 162px;
						    width: 227px;
						  }
						}
						@keyframes killShot {
						  0% {
						    transform: translateY(0) rotate(0deg);
						    opacity: 1;
						  }
						  100% {
						    transform: translateY(300px) rotate(45deg);
						    opacity: 0;
						  }
						}
						@keyframes fadeToRed {
						  0% {
						    box-shadow: inset 0 0 0 rgba(201, 24, 24, 0.8);
						  }
						  100% {
						    box-shadow: inset 0 2000px 0 rgba(201, 24, 24, 0.8);
						  }
						}
						@keyframes slowFade {
						  0% {
						    opacity: 1;
						  }
						  99.9% {
						    opacity: 0;
						    transform: scale(1);
						  }
						  100% {
						    transform: scale(0);
						  }
						}
					</style>

			<!-- Body Page-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Soal</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div id="modal-container">
				<div class="modal-background">
					<div class="modal">
						<?php foreach ($rows as $row) { ?>
						<img class="muncul" src="<?php echo base_url() . 'uploadfile/soal/'.$row->bukti  ?>"/>
						<?php } ?>
				    </div>
				</div>
				</div>
				<div class="panel-body">

					<table class="table table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								<th style="width: 10px;">No</th>
								<th style="width: 80px;">Matakuliah</th>
								<th style="width: 70px;">Tanggal</th>
								<th style="width: 50px;">Bukti Penyerahan</th>
								<th style="width: 50px;">Status</th>
								<th style="width: 120px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody>
							<?php if($rows){$no=1; foreach($rows as $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->nama_matakuliah; ?></td>
								<td><?php echo $row->date; ?></td>
								<td>
									<div class="content">
										<div class="content">
										  	<div class="buttons">
								   		 		<div id="one" class="button" style="height: 30px; width: 100px; font-size: 12px;">Lihat Bukti</div>
								  			</div>
										</div>
									</div>
								</td>
								<td><label style="color: #ffffff;" class="
									<?php 
										if ($row->status_soal=='WAITING') {
											echo  "label label-warning";
										}elseif ($row->status_soal=='REVISI') {
											echo "label label-danger";
										}elseif ($row->status_soal=='APPROVE') {
											echo "label label-secondary";
										}
									 ?>
								"><?php echo $row->status_soal; ?></label></td>
								<td align="center">
									<button onClick="Approval(<?= $row->id_soal; ?>);" class="col-xs-3 btn btn-secondary btn-lg fa-check icon-left"></button>
									<a href="<?php echo base_url('kaprodi/download_soal/'.$row->fileName); ?>" class="col-xs-3 btn btn-info btn-lg fa-download"></a>
									<button onClick="Notice(<?= $row->id_soal; ?>);" class="col-xs-3 btn btn-danger btn-lg fa-tag icon-left"></button>
								</td>
							</tr>
							<?php  $no++; } } ?>
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->


<!-- Bottom Scripts -->

</div>
</div>

<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>

<?php
$this->load->view("fragment/foot");
?>

<script type="text/javascript">
	function Approval(id) {
 	alertify.confirm("Confirmation Message","Yakin Approval Soal ?",
    function(input) {
      if (input) {
        alertify.success('Approval Soal');
        window.location.href = "<?= base_url() ?>kaprodi/updateStatusSoal/"+id;
      } else {
        alertify.error('Cancel');
      }
   	}, function(){  alertify.error('Cancel !');});
 	}	

 	function Notice(id){
 		alertify.prompt("Confirmation Message","Beri Tanggapan Revisi!!!", "Mohon Diperbaiki",
    	function(evt,value) {
    	var input = decodeURI(value);
        alertify.success('Status: Revisi');
       	$.ajax({
		  type: "POST",
		  url: "<?= base_url() ?>kaprodi/revisi_soal/",
		  data: {identitas:id,keterangan:input},
		  success:function(data){
		  	window.location.href="<?php echo base_url('kaprodi/lihat_soal'); ?>"
		  }
		});
    }, function(){  alertify.error('Cancel !');});
 	}
</script>

<script type="text/javascript">
	$('.button').click(function(){
		  var buttonId = $(this).attr('id');
		  $('#modal-container').removeAttr('class').addClass(buttonId);
		  $('body').addClass('modal-active');
		})

		$('#modal-container').click(function(){
		  $(this).addClass('out');
		  $('body').removeClass('modal-active');
		});
</script>