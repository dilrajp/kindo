<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_kaprodi");
?>

<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Daftar Nilai BAP Dosen</h1>
					<p class="description">Berita Acara Perkuliahan</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('kaprodi');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Nilai BAP</strong>
						</li>
						</ol>
				</div>		

			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Nilai BAP Dosen</h3>
				</div>
	
				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>


					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr style="">
								<th style="width: 10px;">No</th>
								<th style="width: 50px;">Kode Dosen</th>
								<th style="width: 50px;">Dosen</th>
								<th style="width: 50px;">Presesntase RFID</th>
								<th style="width: 50px;">Presentase Input BAP</th>
								<th style="width: 50px;">Presentase Waktu Input BAP</th>
								<th style="width: 50px;">Presentase Kehadiran Dosen</th>
								<th style="width: 50px;">Kinerja Dosen</th>
							</tr>
						</thead>
						

						<tbody>
							<?php $no=1; foreach ($rows as $row): ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $row->kode_dosen ?></td>
								<td><?php echo $row->nama ?></td>
								<td><?php echo $row->pres_rfid ?></td>
								<td><?php echo $row->pres_inputbap ?></td>
								<td><?php echo $row->pres_waktubap ?></td>
								<td><?php echo $row->pres_kehadiran ?></td>
								<td><?php echo $row->kinerja_dosen ?></td>
							</tr>	
							<?php $no++?>
							<?php endforeach ?>
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->

	<!-- Bottom Scripts -->
	<footer class="main-footer sticky footer-type-1">
		
		<div class="footer-inner">
		
			<!-- Add your copyright text here -->
			<div class="footer-text">
				&copy; 2014 
				<strong>Xenon</strong> 
				theme by <a href="http://laborator.co" target="_blank">Laborator</a>
			</div>
			
			
			<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
			<div class="go-up">
			
				<a href="#" rel="go-top">
					<i class="fa-angle-up"></i>
				</a>
				
			</div>
			
		</div>
		
	</footer>
	</div>
	</div>
	<?php
	$this->load->view("fragment/foot");
	?>