<?php 
$this->load->view("fragment/head");
?>

<body class="page-body">
						
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">
					
					<li class="search-form"><!-- You can add "always-visible" to show make the search input visible -->
						
						<form method="get" action="extra-search.html">
							<input type="text" name="s" class="form-control search-field" placeholder="Type to search..." />
							
							<button type="submit" class="btn btn-link">
								<i class="linecons-search"></i>
							</button>
						</form>
						
					</li>
					
					<li class="dropdown user-profile">
						<a href="#" data-toggle="dropdown">
							<img src="<?php echo base_url('assets/images/foto/').getfoto($this->session->username); ?>" alt="user-image" class="img-circle img-inline userpic-32" width="28" />
							<span>
								<?php echo $this->session->userdata('nama');?>
								<i class="fa-angle-down"></i>
							</span>
						</a>
						
						<ul class="dropdown-menu user-profile-menu list-unstyled">
							<li>
								<a href="#edit-profile">
									<i class="fa-edit"></i>
									New Post
								</a>
							</li>
							<li>
								<a href="#settings">
									<i class="fa-wrench"></i>
									Settings
								</a>
							</li>
							<li>
								<a href="#profile">
									<i class="fa-user"></i>
									Profile
								</a>
							</li>
							<li>
								<a href="#help">
									<i class="fa-info"></i>
									Help
								</a>
							</li>
							<li class="last">
								<a href="<?php echo site_url('login/logout'); ?>">
									<i class="fa-lock"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
					
					<!-- <li>
						<a href="#" data-toggle="chat">
							<i class="fa-comments-o"></i>
						</a>
					</li> -->
					
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Undangan Rapat</h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen</p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1">
									<li>
							<a href="dosen"><i class="fa-home"></i>Home</a>
						</li>
								<li class="active">
						
										<a href="forms-native.html">Rapat</a>
								</li>
						
								
				</div>
					
			</div>
			<a href="javascript:;" 
			onclick="jQuery('#modal-1').modal('show', {backdrop: 'fade'});" 
			class="btn btn-red btn-icon btn-icon-standalone">
				<i class="fa-pencil"></i>
				<span>Add Undangan</span>
			</a>
		

	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">List Undangan</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
					
					<table class="table table-striped table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								
								<th>Rapat</th>
								<th>Agenda</th>
								<th>Ruangan</th>
								<th>Tanggal</th>
								
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        
					        foreach ($rows as $row) {
					        ?>
							<tr>
								
								<td><?php echo namarapat($row->tipe); ?>	</td>
								<td><?php echo $row->agenda; ?></td>
								<td class="center"><?php echo $row->ruangan; ?></td>
								<td class="center"><?php 
									echo date('Y-m-d H:i:s', strtotime($row->tanggal_rapat)); ?></td>
								<td>
							
									<a href="<?php echo base_url() ?>kaprodi/edit_rapat/<?php echo $row->id_undangan;?>"  class="btn btn-secondary btn-sm el-arrows-cw icon-left"> </a>
									
									
									<button onClick="CheckDelete(<?= $row->id_undangan; ?>);" class="btn btn-danger btn-sm el-trash icon-left">
										</button>
									
									<a href="<?php echo base_url() ?>kaprodi/detail_rapat/<?php echo $row->id_undangan; ?>" class="btn btn-info btn-sm el-search icon-left"> </a>
										
									</a>
								
								</td>
							</tr>
							<?php } ?>
						</tbody>	
							
						<tfoot>
						<tr>
								
								<th>Rapat</th>
								<th>Agenda</th>
								<th>Ruangan</th>
								<th>Tanggal</th>
								
								<th> Action </th>
							</tr>
						</tfoot>
					</table>
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2018
						<strong>Manajemen Informatika</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
			
	
		
		
	</div>

	<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 0},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							
						]);
					});
					</script>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
     <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.uk.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>

<div class="modal" tabindex="-1" role="dialog" id="modal-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Undangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/add_rapat'; ?>">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Agenda</label>
									
									<div class="col-sm-10">
										<input name="agenda" type="text" class="form-control" id="field-1" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tipe</label>
									<div class="col-sm-10">
									<label class="radio-inline">
									<input type="radio" name="radio" value="1" checked required="required">
											Koordinasi MK
									</label>
									<label class="radio-inline">
									<input type="radio" name="radio" value="2" required="required">
											Prodi
									</label>
								</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Ruangan</label>
									<div class="col-sm-10">
										<input name="ruangan" type="text" class="form-control" id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
									<div class="col-sm-10">
								<div class="input-append date form_datetime">
							    <input name="tanggal"  class="form control"  type="text"  readonly required="required">
							    <span class="add-on"><i class="icon-th"></i></span>
								</div>
							</div>
						</div>

						<div class="form-group">
									<label class="col-sm-2 control-label">Peserta</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-2").select2({
												placeholder: 'Daftar Peserta yang tersedia',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
									
									<select name="peserta[]" class="form-control" id="s2example-2" multiple required="required">
										<option value="all">All</option>
										<optgroup label="Daftar Dosen">
										<?php foreach ($nama as $row) {		?>	
											<option value="<?php echo $row->nama_dosen ?>" ><?php echo $row->nama_dosen ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								
						
				
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Save changes</button>

				</div>

					</form>
    </div>
  </div>
</div>
	<?php
	$this->load->view("fragment/foot");
	?>

	<script type="text/javascript">
		 $(document).ready(function() {
        // Untuk sunting
        $('#edit-data').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)

            // Isi nilai pada field
            modal.find('#tipe').attr("value",div.data('tipe'));
            modal.find('#agenda').attr("value",div.data('agenda'));
            modal.find('#ruangan').html(div.data('ruangan'));
            modal.find('#tanggal').attr("value",div.data('tanggal'));
        });
    });
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii:ss"
    });


function CheckDelete(id) {
  alertify.confirm("Data Undangan akan dihapus ?",
    function(input) {
      if (input) {
       
        window.location.href = "<?php echo base_url(); ?>kaprodi/del_rapat/"+id;
      } else {
        alertify.error('Cancel');
      }
    });
}

function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }

    
</script>  