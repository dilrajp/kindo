<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Hasil Penilaian Indikator Agenda Program Studi</h1>
					<p class="description">Tahun <?php echo nama_tahun(gettahun_aktif()); ?></p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1">
									<li>
							<a href="dosen"><i class="fa-home"></i>Home</a>
						</li>
								<li class="active">
						
										<a href="forms-native.html">Verifikasi Nilai</a>
								</li>
							</ol>
						
								
				</div>
					
			</div>
	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Nilai Kinerja</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
					
						<table class="table table-striped table-bordered" id="mydata">
						<thead>
							<tr class="replace-inputs">
								
								<th>NIP</th>
								<th>Nama Dosen</th>
								<th>Undangan Prodi/ Verfikasi</th>
								<th>Rapat Koordinasi/ Verfikasi</th>
								<th> Aksi </th>
						</thead>
						 
						<tbody id="show_data">
								
						</tbody>
							
					
					</table>
					
				</div>
			</div>

			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>
	

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Nilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						 <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/proses_edit_nilai';?>" >
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIP</label>
									
									<div class="col-sm-10">
										<input name="nip" type="text" class="form-control" id="nip" disabled>
										<input name="id" type="hidden" id="id">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama </label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama_mhs" disabled>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nilai Rapat Prodi </label>
									<div class="col-xs-3">
									<input type="number" max="100" min="0" class="form-control" name="nilai_prodi" id="nilai_prodi" disabled>
									</div>
									<label class="col-xs-3 control-label">Verifikasi Nilai </label>
									<div class="col-xs-3">
									<input type="number" max="100" min="0" class="form-control" name="nilai_prodi2" id="nilai_prodi2" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nilai Rapat Koordinasi MK </label>
									<div class="col-xs-3">
									<input type="number" max="100" min="0" class="form-control" name="nilai_koor" id="nilai_koor" disabled>
								
									</div>
									<label class="col-xs-3 control-label">Verifikasi Nilai </label>
									<div class="col-xs-3">
									<input type="number"  max="100" min="0" class="form-control" name="nilai_koor2" id="nilai_koor2" >
								
									</div>
								</div>
							
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info" >Simpan</button>

				</div>
			</form>		
					
    </div>
  </div>
</div>

	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>

	<!-- Edit -->



	<script type="text/javascript">
$(document).ready(function(){
    tampil_data_barang();   //pemanggilan fungsi tampil barang.
	
	$("#mydata").dataTable().yadcf([
						
							{column_number : 0,filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'range_number'},
							{column_number : 3, filter_type: 'range_number'},
							
	]);
				

    function tampil_data_barang(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo base_url()?>kaprodi/nilai_rapat_dosen',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i= 0; i<data.length; i++){
                	var nilai_prodi = ' '
                	if(data[i].nilai_rapat_prodi == null){
                		nilai_prodi = ' - ';
                	}else{
                		nilai_prodi = data[i].nilai_rapat_prodi;
                	}
                	var nilai_koor = ' '
                	if(data[i].nilai_rapat_koor == null){
                		nilai_koor = ' - ';
                	}else{
                		nilai_koor = data[i].nilai_rapat_koor;
                	}
                	var nilai_prodi2 = ' '
                	if(data[i].nilai_rapat_prodi2 == null){
                		nilai_prodi2 = ' - ';
                	}else{
                		nilai_prodi2 = data[i].nilai_rapat_prodi2;
                	}
                	var nilai_koor2 = ' '
                	if(data[i].nilai_rapat_koor2 == null){
                		nilai_koor2 = ' - ';
                	}else{
                		nilai_koor2 = data[i].nilai_rapat_koor2;
                	}
                    html += '<tr>'+
                            '<td>'+data[i].nip+'</td>'+
                            '<td>'+data[i].nama+'</td>'+
                            '<td>'+nilai_prodi+' / '+nilai_prodi2+'</td>'+
                            '<td>'+nilai_koor+' / '+nilai_koor2+'</td>'+
                            '<td style="text-align:right;">'+
                            '<a href="javascript:;" class="btn btn-secondary fa-refresh icon-left item_edit" data="'+data[i].id_nilai+'"></a>'+
                            '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }


		//GET UPDATE
		$('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('kaprodi/edit_nilai_rapat')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(id,nip,nama,nilai_prodi,nilai_koor,nilai_prodi2,nilai_koor2){
                    	$('#ModalaEdit').modal('show');
            			$('[name="nip"]').val(data.nip);
            			$('[name="id"]').val(data.id);
            			$('[name="nama"]').val(data.nama);
            			$('[name="nilai_prodi"]').val(data.nilai_prodi);
            			$('[name="nilai_koor"]').val(data.nilai_koor);
            			$('[name="nilai_prodi2"]').val(data.nilai_prodi2);
            			$('[name="nilai_koor2"]').val(data.nilai_koor2);
            		
            		});
                }
            });
            return false;
        });

		
    });		
 </script>
