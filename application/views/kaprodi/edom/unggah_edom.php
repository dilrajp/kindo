<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_kaprodi");
?>

<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Unggah File EDOM</h1>
					<p class="description">Evaluasi Dosen Oleh Mahasiswa</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('kaprodi');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>EDOM</strong>
						</li>
						</ol>
				</div>		

			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">File Excel<br></h3><br>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<h4 style="color:grey"> Download Template EDOM</h4>
					<a href="<?php echo base_url('kaprodi/download_edom'); ?>" class="btn btn-success btn-icon btn-icon-standalone">
					<i class="el-download-alt"></i><span>Unduh Template</span>
					</a>
			
				</div>
			</div>	

			<!-- Body Page-->
				<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Upload File Excel</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form role="form" class="form-horizontal" method="post" action="<?php echo base_url('kaprodi/unggah_edom_aksi'); ?>" enctype="multipart/form-data" accept-charset="utf-8">
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="field-4" style="color: red;">Unggah File (.xls/.xlxs)</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" id="field-4" name="file" required>
						</div>
					</div>
					<div class="form-group">
					<div class="col-sm-10">
					<?php echo $this->session->flashdata('msg'); echo "<br>" ?>
					<button type="submit" class="btn btn-info">Upload File</button>
					</div>
					</div>
					</form>			
				</div>
			</div>	
			<!-- Batas Body Page-->

			<footer class="main-footer sticky footer-type-1">
	
			<div class="footer-inner">
			
				<!-- Add your copyright text here -->
				<div class="footer-text">
					&copy; 2014 
					<strong>Xenon</strong> 
					theme by <a href="http://laborator.co" target="_blank">Laborator</a>
				</div>
				
				
				<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
				<div class="go-up">
				
					<a href="#" rel="go-top">
						<i class="fa-angle-up"></i>
					</a>
					
				</div>
				
			</div>
			
		</footer>
		</div>
		</div>
					
<?php
	$this->load->view("fragment/foot");
?>