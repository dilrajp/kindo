<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- foot -->


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">

	
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>


	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" ></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>" defer>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>" defer></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>






<style type="text/css">
  
.editOption{
    width: 90%;
    height: 26px;
    position: relative;
    top: -29px;
    background: #E0F5FF;;
    border: 0;
    padding-left: 5px;
}
</style>
</head>
<body class="page-body">
					
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Undangan Rapat
					<?php 
					/*$total  = total_rapat($this->session->id_dosen);
					$atas  = rapat_hadir($this->session->id_dosen);
					echo round($total / $atas,2)." = ";
					echo $total." / ";
					echo $atas." [] ";
					$row = $this->yeah->all_select('id_dosen','t_dosen','id_dosen')->result();
		
					foreach ($row as $x ) {	
					echo "<br>";		
					$total2  = total_rapat($x->id_dosen);
					$atas2  = rapat_hadir($x->id_dosen);
					$nilai = round($atas2 / $total2,2)*100;
					echo $x->id_dosen;

					echo $atas2;
					echo $total2."   ";
					echo $nilai;
					echo "<br>";
					}
					echo $atas2;
					echo $total2; */
					/*$q = $this->db->query("SELECT id_dosen FROM t_undangan JOIN t_kehadiran using(id_undangan) WHERE t_kehadiran.id_undangan ='2' AND tipe_p = '1'")->row_array();
					echo $q['id_dosen'];
					$id_dosen = getiddosen_rapat(47);
					//echo 'xx'.$id_dosen.'<br>';
					$tot = $this->yeah->TotalKoorRapat($id_dosen,37);
					echo $tot;*/
					?>
					</h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen
						<?php 
						
						?></p>
				</div>
				
					<div class="breadcrumb-env">
					
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Rapat</a>
					</li>
					</ol>					
					</div>
					
			</div>

					
					<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li class="active">
							<a href="#home-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs">Rapat Prodi</span>
							</a>
						</li>
						<li>
							<a href="#profile-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Rapat Koordinasi </span>
							</a>
						</li>
						
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="home-3">
							
							<div>
								
								<div class="panel panel-default">
								<a href="javascript:;" 
								onclick="jQuery('#modal-1').modal('show', {backdrop: 'fade'});" 
								class="btn btn-red btn-icon btn-icon-standalone">
									<i class="fa-pencil"></i>
									<span>Tambah Undangan</span>
								</a>

							<div class="panel-body">
								

								
								<table class="table table-striped table-bordered" id="example-3">
										<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									$("#example-3").dataTable().yadcf([
										{column_number : 1, filter_type: 'text'},
										{column_number : 2, filter_type: 'text'},
										{column_number : 3},
										
									]);
								});
								</script>
									<thead>
										<tr class="replace-inputs">
											
											<th>No</th>
											<th>Agenda</th>
											<th>Ruangan</th>
											<th>Tanggal</th>
											<th> Aksi </th>
										</tr>
									</thead>
									 
									<tbody>
										<?php 
								        $i = 1;
								        foreach ($rows2 as $row) {
								        ?>
										<tr>
											
											<td><?php 
											echo $i++;
											 ?></td>
											<td><?php echo $row->agenda; ?></td>
											<td class="center"><?php echo $row->ruangan; ?></td>
											<td class="center"><?php 
												echo date('Y-m-d H:i:s', strtotime($row->tanggal_rapat)); ?></td>
											<td class="center" >
										
												<a href="<?php echo base_url() ?>kaprodi/edit_rapat/<?php echo $row->id_undangan;?>"  class="btn btn-secondary  fa-refresh icon-left"> </a>
												
											<?php if($row->id_matakuliah == NULL){
											?> <button onClick="CheckDelete(<?= $row->id_undangan; ?>);" class="btn btn-danger fa-trash icon-left"></button>
											<?php }else { ?>
											<button onClick="CheckDelete2(<?= $row->id_undangan; ?>,<?= $row->id_matakuliah; ?>);" class="btn btn-danger fa-trash icon-left"></button>
											<?php } ?>
												
												
												<a href="<?php echo base_url() ?>kaprodi/detail_rapat/<?php echo $row->id_undangan; ?>" class="btn btn-info  fa-search icon-left"> </a>
													
												</a>
											
											</td>
										</tr>
										<?php } ?>
									</tbody>	
										
									
								</table>
							</div>
							</div>
								
								
							</div>
							
						</div>
						<div class="tab-pane" id="profile-3">
		
							
						<div class="panel panel-default">
							<div class="panel-heading">
									
								<div class="panel-options">
									<a href="#" data-toggle="panel">
										<span class="collapse-icon">&ndash;</span>
										<span class="expand-icon">+</span>
									</a>
								</div>
								</div>
								<div class="panel-body">

								<table class="table table-striped table-bordered" id="example-4">
										<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									$("#example-4").dataTable().yadcf([
										{column_number : 0,  filter_type: 'text'},
										{column_number : 1, filter_type: 'text'},
										{column_number : 2, filter_type: 'text'},
										{column_number : 3, filter_type: 'text'},
										
									]);
								});
								</script>
									<thead>
										<tr class="replace-inputs">
											
											<th style="min-width: 20%">Mata Kuliah</th>
											<th style="min-width: 15%">Agenda</th>
											<th style="min-width: 15%">Ruangan</th>
											<th style="min-width: 10%">Tanggal</th>
											<th width="25%"> Aksi </th>
										</tr>
									</thead>
									 
									<tbody>
										<?php 
								        
								        foreach ($rows1 as $row) {
								        ?>
										<tr>
											
											<td style="min-width: 20%"><?php 
											if($row->id_matakuliah == NULL){
												echo " - ";
											}else {
											echo ucwords(strtolower(nama_matkul($row->id_matakuliah))); } ?></td>
											<td style="min-width: 15%"><?php echo $row->agenda; ?></td>
											<td class="center" style="min-width: 15%"><?php echo $row->ruangan; ?></td>
											<td class="center" style="min-width: 10%"><?php 
												echo date('Y-m-d H:i:s', strtotime($row->tanggal_rapat)); ?></td>
											<td width="25%">
										
												<a href="<?php echo base_url() ?>kaprodi/edit_rapat/<?php echo $row->id_undangan;?>"  class="btn btn-secondary  fa-refresh icon-left"> </a>
												
											<?php if($row->id_matakuliah == NULL){
											?> <button onClick="CheckDelete(<?= $row->id_undangan; ?>);" class="btn btn-danger fa-trash icon-left"></button>
											<?php }else { ?>
											<button onClick="CheckDelete2(<?= $row->id_undangan; ?>,<?= $row->id_matakuliah; ?>);" class="btn btn-danger fa-trash icon-left"></button>
											<?php } ?>
												
												
												<a href="<?php echo base_url() ?>kaprodi/detail_rapat/<?php echo $row->id_undangan; ?>" class="btn btn-info  fa-search icon-left"> </a>
													
												</a>
											
											</td>
										</tr>
										<?php } ?>
									</tbody>	
										
									
								</table>			
								</div>
							</div>	
						</div>
						
					</div>
					
					
				</div>
			</div>
		
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
	
		</div>
		
			
	
		
		
	</div>
	<div class="page-loading-overlay">
	<div class="loader-2"></div>
	</div>
</body>
</html>
<script>
function validateForm() {
    var x = document.forms["myForm"]["tanggal"].value;
    if (x == "") {
        /*alertify.error('Tanggal Harus Diisi !');*/
        alertify.set('notifier','position', 'top-right');
 		alertify.success('Tanggal Harus Diisi !' );
        return false;
    }
    var y = document.forms["myForm"]["agenda"].value;
    if (y == "") {
        /*alertify.error('Tanggal Harus Diisi !');*/
        alertify.set('notifier','position', 'top-right');
 		alertify.success('Agenda Harus diisikan !' );
        return false;
    }
}
</script>

	
<div class="modal fade" tabindex="-1" role="dialog" id="modal-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Undangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'Kaprodi/add_rapat'; ?>" onsubmit = "return validateForm()" name="myForm">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Agenda</label>
									
									<div class="col-sm-10">
										<input name="agenda" type="text" class="form-control" id="field-1" >
									</div>
								</div>
							
									<div class="form-group">
									<label class="col-sm-2 control-label">Tipe</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Prodi" disabled="" required="required">
									<input type="hidden" name="radio" value="2" name="radio">
									<input type="hidden" name="radio" value="1" name="pengirim">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Ruangan</label>
									<div class="col-sm-10">
									<div id="billdesc">
									    <select id="test" name="ruangan" class="form-control" required>
									      <option class="non" value="R. Pimpinan">R. Pimpinan</option>
									      <option class="non" value="R. Multimedia 1">R. Multimedia 1</option>
									      <option class="non" value="R. Multimedia 2">R. Multimedia 2</option>
									      <option class="editable" value="Lainnya">Lainnya</option>
									    </select>
									    <input class="editOption" style="display:none;" placeholder="Text"></input>
									</div>
								</div>
								</div>
								<div class="form-group">
								<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
								<div class="col-sm-10">
								<div class="input-append date form_datetime">
							    <input name="tanggal"  class="form control"  type="text"  id="datetimepicker" readonly >
							    <span class="add-on"><i class="icon-th"></i></span>
								</div>
							</div>
						</div>
				     </div>
      	<div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-info">Tambah</button>
		</div>

		</form>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		var date = new Date();
		$('#datetimepicker').datetimepicker({
		todayBtn : 1,
		autoclose : 1,
		todayHighlight : 1,
		forceParse : 0
		});
		$('#datetimepicker').datetimepicker('setStartDate', date);
		
	});
 

function ConfirmMsg()
{
alertify.confirm("This is a confirm dialog.",
  function(){
    alertify.success('Ok');
    return true;
  },
  function(){
    alertify.error('Cancel');
  });
}

/*$(document).ready(function(){
    $('.confirm').on('click', function(event){
        // add me, stop the default link from clicking
        event.preventDefault();

        alertify.confirm("Message", function (e) {
          if (e) {
              // use window.location and go to a->href
          } else {            
            return false; 
          }        
      });     
    });
});*/

function CheckDelete(id) {
  alertify.confirm("Confirmation Message","Data Undangan akan dihapus ?",
    function(input) {
      if (input) {
		alertify.success('Data Berhasil Dihapus');       
        window.location.href = "<?php echo base_url(); ?>kaprodi/del_rapat/"+id;
      } else {
        alertify.error('Cancel');
      }
    }, function(){alertify.error('Cancel');});
}

function CheckDelete2(id,di) {
  alertify.confirm("Confirmation Message","Data Undangan akan dihapus ?",
    function(input) {
      if (input) {
       	 alertify.success('Cancel');
        window.location.href = "<?php echo base_url(); ?>kaprodi/del_rapat_koor/"+id+"/"+di;
      } else {
        alertify.error('Cancel');
      }
    }, function(){ alertify.error('Cancel')});
}


function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }

</script>  

<script type="text/javascript">
var initialText = $('.editable').val();
$('.editOption').val(initialText);

$('#test').change(function(){
var selected = $('option:selected', this).attr('class');
var optionText = $('.editable').text();

if(selected == "editable"){
  $('.editOption').show();
  
  $('.editOption').keyup(function(){
      var editText = $('.editOption').val();
      $('.editable').val(editText);
      $('.editable').html(editText);
  });

}else{
  $('.editOption').hide();
}
});
    
</script>



