<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
					
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Form Ubah Undangan Rapat</h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen</p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1">
									<li>
							<a href="#"><i class="fa-home"></i>Home</a>
						</li>
								<li >
						
										<a href="<?php echo base_url().'kaprodi/list_rapat';?>">Rapat</a>
								</li>
						<li class="active">
						
										<a href="#">Edit Undangan</a>
								</li>
								
				</div>
					
			</div>
		
		

	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Form Edit Undangan</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					<?php foreach($rows as $row){ ?>
					<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/proses_edit_rapat'; ?>">
						<input type="hidden" name="id" value="<?php echo $row->id_undangan ?>">
					
					
									<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Agenda</label>
									
									<div class="col-sm-10">
										<input name="agenda" type="text" value="<?php echo $row->agenda ?>" class="form-control" id="field-1" >
									</div>
								</div>
								<!--<div class="form-group">
									<label class="col-sm-2 control-label">Tipe</label>
									<div class="col-sm-10">
									<label class="radio-inline">
									<input type="radio" name="radio" value="1"  <?php if ($row->tipe == 1) 
									  echo 'checked="checked"'; ?>">
											Koordinasi MK
									</label>
									<label class="radio-inline">
									<input type="radio" name="radio" value="2" <?php if ($row->tipe == 2) 
									  echo 'checked="checked"' ?>">
											Prodi
									</label>
								</div> 						
								</div> -->
									<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tipe</label>
									
									<div class="col-sm-10">
										<input type="text" class="form-control" value="<?php echo namarapat($row->tipe) ?>" disabled="" required="required">
										<input type="hidden" name="radio" value="2" name="radio">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"  for="field-1">Ruangan</label>
									<div class="col-sm-10">
									<input name="ruangan" type="text"  value="<?php echo $row->ruangan ?>" class="form-control" id="field-3">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
									<div class="col-sm-10">
								<div class="input-append date form_datetime">
							    <input name="tanggal"  class="form control"  value="<?php echo $row->tanggal_rapat ?>" type="text"  readonly>
							    <span class="add-on"><i class="icon-th"></i></span>
								</div>
							</div>
						</div>
					<a type="button" class="btn btn-white" data-dismiss="modal" onclick="history.back(-1)">Tutup</a>
					<button type="submit" class="btn btn-info">Simpan</button>	
					</form>
					<?php } ?>
						
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
	
		
		
	</div>


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
     <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.uk.js')?>"></script>
	 

	<?php
	$this->load->view("fragment/foot");
	?>

	<script type="text/javascript">
		 $(document).ready(function() {
        // Untuk sunting
        $('#edit-data').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)

            // Isi nilai pada field
            modal.find('#tipe').attr("value",div.data('tipe'));
            modal.find('#agenda').attr("value",div.data('agenda'));
            modal.find('#ruangan').html(div.data('ruangan'));
            modal.find('#tanggal').attr("value",div.data('tanggal'));
        });
    });
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii:ss"
    });

function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
</script>  