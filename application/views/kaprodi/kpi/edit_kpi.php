<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_kaprodi');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					 <?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Edit KPI</h1>
					<p class="description">Form Edit Data Key Performance Indicator</p>
				</div>
				
					<div class="breadcrumb-env ">
						<ol class="breadcrumb bc-1 ">
						<li>
							<a href="#"><i class="fa-home"></i>Home</a>
						</li>
						<li >
							<a href="<?php echo base_url().'kaprodi/ListIndikator';?>">KPI</a>
						</li>
						<li class="active">
							<a href="#">Edit KPI</a>
						</li>
				</div>
					
			</div>
	
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data KPI</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

					<div class="panel-body">
					<form role="form" class="form-inline" method="POST" action="<?php echo base_url().'kaprodi/proses_kpi' ?>">	

					<?php 
					        
					 foreach ($kpi as $row) {
					 ?>	

					 	<input type="hidden" name="id" value="<?php echo $row->id_kpi ?>">
						<div class="form-group">
									<label class="control-label"><b>Judul Indikator KPI </b></label>
								</div>
								
								<div class="form-group">
									
									<input type="text" class="form-control" name="indikator" size="50" value="<?php echo $row->indikator; ?>">
								</div>
								
								<div class="form-group">
									<button class="btn btn-secondary btn-single">Simpan</button>
								</div>
								
					<?php } ?>
					</form>
					</div>
			</div>

			<div class="row">
			<?php  foreach ($kpi as $rows) {
				if($rows->id_kpi == '6' || $rows->id_kpi == '7' || $rows->id_kpi == '8' || $rows->id_kpi == '9' || $rows->id_kpi == '10' || $rows->id_kpi == '11' || $rows->id_kpi == '15' ){ }else{
				?>

			<a href="javascript:;" onclick="jQuery('#add-penilaian').modal('show', {backdrop: 'fade'});" class="btn btn-red btn-icon btn-icon-standalone" ><i class="fa-pencil"></i><span>Tambah Kriteria Penilaian</span>
			</a>
			<?php } } ?>
			<div class="clearfix"></div>
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Penilaian</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
					
				<div class="panel-body">
			<?php  foreach ($kpi as $rows) {?>
			<!-- kpi jumlah kehadiran rapat prodi & mk -->
			<?php if($rows->id_kpi == '2' || $rows->id_kpi == '3'){ ?>
					Sesuai Prosentase Kehadiran (Keterlambatan lebih dari 30 Menit tanpa Konfirmasi  dianggap tidak hadir).<br>
					Nilai = 0-100
			<?php } ?>
			<!-- kpi jumlah bimbingan -->
				<?php if($rows->id_kpi == '1'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Kondisi</th>
								<th>Nilai</th>						
								<th> Aksi </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo "> ".$row->kondisi." Mahasiswa"; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									<button onClick="CheckDelete('<?= $row->id_kategori ?>','<?=$row->id_kpi  ?>')" class="btn btn-danger fa-trash icon-left"> </button>
							</td>
							</tr>
						<?php }	//end foreach kategori jum bimb ?>
						</tbody>	
					</table>
				<?php }	//end if kpi jum bimb	 ?>

				<!-- kpi rapat koordinasi mk (koor mk) -->
				<?php if($rows->id_kpi == '4'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Kondisi</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo "Melaksanakan Rapat Koordinasi MK Minimal ".$row->kondisi." Kali"; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									<button onClick="CheckDelete('<?= $row->id_kategori ?>','<?=$row->id_kpi  ?>')" class="btn btn-danger fa-trash icon-left"> </button>
							</td>
							</tr>
						<?php }	//end foreach koordinasi mk ?>
						</tbody>	
					</table>
				<?php }	//end if kpi koordinasi mk ?>

				<!-- kpi  kontribusi kelulusan -->
				<?php if($rows->id_kpi == '5'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Kondisi</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo "Prosentase Kontribusi Kelulusan >= ".$row->kondisi." %"; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									<button onClick="CheckDelete('<?= $row->id_kategori ?>','<?=$row->id_kpi  ?>')" class="btn btn-danger fa-trash icon-left"> </button>
							</td>
							</tr>
						<?php }	//end foreach kontribusi kelulusan ?>
						</tbody>	
					</table>
				<?php }	//end if kpi kontribusi kelulusan ?>

				<!-- kpi  rps -->
				<?php if($rows->id_kpi == '6'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end foreach rps ?>
						</tbody>	
					</table>
				<?php }	//end if kpi rpsn ?>

				<!-- kpi  modul -->
				<?php if($rows->id_kpi == '7'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end foreach modul ?>
						</tbody>	
					</table>
				<?php }	//end if modul ?>

				<!-- kpi  soal asessment -->
				<?php if($rows->id_kpi == '8'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end soal ?>
						</tbody>	
					</table>
				<?php }	//end if soal ?>

				<!-- kpi  bahan perkuliahan -->
				<?php if($rows->id_kpi == '9'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end bahan ?>
						</tbody>	
					</table>
				<?php }	//end if bahaan ?>

				<!-- kpi  usulan -->
				<?php if($rows->id_kpi == '10'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end foreach rps ?>
						</tbody>	
					</table>
				<?php }	//end if kpi usulan ?>

				<!-- kpi  nilai asessment-->
				<?php if($rows->id_kpi == '11'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end foreach nilai ?>
						</tbody>	
					</table>
				<?php }	//end if kpi nilai ?>

					<!-- kpi  jumlah menguji-->
				<?php if($rows->id_kpi == '13' || $rows->id_kpi == '12'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Jumlah Menguji (Sebanyak)</th>
								<th>Nilai</th>						
								<th>Action</th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi. ' Kali'; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									<button onClick="CheckDelete('<?= $row->id_kategori ?>','<?=$row->id_kpi  ?>')" class="btn btn-danger fa-trash icon-left"> </button>
									
							</td>
							</tr>
						<?php }	//end foreach nilai ?>
						</tbody>	
					</table>
				<?php }	//end if kpi jml menguji ?>
				<!-- perwalian -->
				<?php if($rows->id_kpi == '14'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Jumlah Perwalian per Semester</th>
								<th>Nilai</th>						
								<th>Action</th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td>>= <?php echo $row->kondisi. ' Kali'; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									<button onClick="CheckDelete('<?= $row->id_kategori ?>','<?=$row->id_kpi  ?>')" class="btn btn-danger fa-trash icon-left"> </button>
									
							</td>
							</tr>
						<?php }	//end foreach nilai ?>
						</tbody>	
					</table>
				<?php }	//end if kpi jml menguji ?>

				<!-- kpi lks -->
				<?php if($rows->id_kpi == '15'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Deadline</th>
								<th>Nilai</th>						
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->nilai; ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kategori; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary fa-refresh icon-left"> </a>
									
							</td>
							</tr>
						<?php }	//end foreach lks ?>
						</tbody>	
					</table>
				<?php }	//end if kpi lks ?>

								<!-- kpi edfom -->
				<?php if($rows->id_kpi == '16'){ ?>
					Sesuai Prosentase Nilai EDOM.<br>
					Nilai = 0-100
				<?php }	//end if kpi edfom ?>

				<?php if($rows->id_kpi == '17'){ ?>
					Sesuai Prosentase Nilai BAP.<br>
					Nilai = 0-100
				<?php }	//end if kpi edfom ?>


				<?php }	//end foreach kpi ?>
				</div>
			</div>
			</div>
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>

	

	<?php
	$this->load->view("fragment/foot");
	?>

<!-- modal tambah nilai -->
<?php foreach($kpi as $row){ //start loop?>
<?php if($row->id_kpi == '1'){ ?>

<div class="modal fade" tabindex="-1" role="dialog" id="add-penilaian">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/add_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id_kpi">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Kondisi Jumlah Mahasiswa (>) </label>
									
									<div class="col-sm-8"">

										<input name="kondisi" class="form-control" type="number" min='1' max='100' id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100'  class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Tambah Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- end if kpi jumlah bimbingan -->
<?php } else if($row->id_kpi == '4'){ ?>

<div class="modal fade custom-width" tabindex="-1" role="dialog" id="add-penilaian">
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/add_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id_kpi">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Kondisi Jumlah Minimal Rapat Koordinasi MK (>=)</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" class="form-control" type="number" min='1' max='100' id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100'  class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Tambah Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>
<?php }else if($row->id_kpi == '5'){ ?>

<div class="modal fade custom-width" tabindex="-1" role="dialog" id="add-penilaian">
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/add_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id_kpi">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Prosentase (%)</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" class="form-control" type="number" min='1' max='100' id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100'  class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Tambah Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- end if kpi 5 -->
<?php }else if($row->id_kpi == '12' || $row->id_kpi == '13'){ ?>

<div class="modal fade custom-width" tabindex="-1" role="dialog" id="add-penilaian">
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/add_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id_kpi">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Jumlah Menguji</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" class="form-control" type="number" min='1' max='100' id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100'  class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Tambah Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>
<?php }else if($row->id_kpi == '14' ){ ?>

<div class="modal fade custom-width" tabindex="-1" role="dialog" id="add-penilaian">
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/add_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id_kpi">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Jumlah Perwalian per Semester</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" class="form-control" type="number" min='1' max='100' id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100'  class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Tambah Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>
<?php } //end if kpi 5?>
<?php } //end foreach ?>

<!-- end modal tambah -->

<!-- modal edit nilai -->

<?php foreach($kpi as $row){ //start loop?>
<?php if($row->id_kpi == '1'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade " tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document"  >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Kondisi Jumlah Mahasiswa (>) </label>
									
									<div class="col-sm-8"">

										<input name="kondisi" value="<?php echo $rows->kondisi ?>" type="number" min='1' max='100' class="form-control" id="field-3" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>
<?php } //end foreach kategori kpi1 ?>
<!-- end if jum bimb -->
<?php } else  if($row->id_kpi == '4'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Kondisi Jumlah Minimal Rapat Koordinasi MK (>=) </label>
									
									<div class="col-sm-8"">

										<input name="kondisi" value="<?php echo $rows->kondisi ?>" type="number" min='1' max='100' class="form-control" id="field-3" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- end if jumlah bikin rapat koor (dk) -->
<?php } ?>
<?php } else  if($row->id_kpi == '5'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Prosentase (%)</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" value="<?php echo $rows->kondisi ?>" type="number" min='1' max='100' class="form-control" id="field-3" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- end if kk -->
<?php } ?>
<?php } elseif($row->id_kpi == '6'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="kondisi"  class="form control input-append date form_datetime"  type="text"  value="<?php echo $rows->kondisi ?>" id="datetimepicker" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>

<?php } ?>

<!-- end if RPS -->


<?php } elseif($row->id_kpi == '7'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="kondisi"  class="form control input-append date form_datetime"  type="text"  value="<?php echo $rows->kondisi ?>" id="datetimepicker" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>

<?php } ?>

<!-- end if RPS -->


<?php } elseif($row->id_kpi == '8'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="kondisi"  class="form control input-append date form_datetime"  type="text"  value="<?php echo $rows->kondisi ?>" id="datetimepicker" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>


<?php } ?>

<!-- end if RPS -->

<?php } elseif($row->id_kpi == '9'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="kondisi"  class="form control input-append date form_datetime"  type="text"  value="<?php echo $rows->kondisi ?>" id="datetimepicker" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>


<?php } ?>

<!-- end if RPS -->

<?php } elseif($row->id_kpi == '10'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="kondisi"  class="form control input-append date form_datetime"  type="text"  value="<?php echo $rows->kondisi ?>" id="datetimepicker" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>


<?php } ?>

<!-- end if RPS -->

<?php } else  if($row->id_kpi == '11' || $row->id_kpi == '15'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="kondisi"  class="form control input-append date form_datetime"  type="text"  value="<?php echo $rows->kondisi ?>" id="datetimepicker" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- jumlah menguji -->
<?php } ?>
<?php } else  if($row->id_kpi == '12' || $row->id_kpi == '13'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Jumlah Menguji</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" value="<?php echo $rows->kondisi ?>" type="number" min='1' max='100' class="form-control" id="field-3" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- end if jumlah menguji -->
<?php } ?>
<!-- perwalian -->
<?php } else  if($row->id_kpi == '14' ){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_kategori; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'kaprodi/edit_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_kategori; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Jumlah Perwalian per Semester</label>
									
									<div class="col-sm-8"">

										<input name="kondisi" value="<?php echo $rows->kondisi ?>" type="number" min='1' max='100' class="form-control" id="field-3" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Bobot Nilai</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai ?>" class="form-control" id="field-3" required="required">
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

		</form>
    </div>
  </div>
</div>
<!-- end if perwalian -->
<?php } ?>
<?php } ?>


<?php } //end foreach kpi?>

<!-- end modal edit -->

<script type="text/javascript">
		$(document).ready(function(){
		var date = new Date();
		date.setDate(date.getDate());

		$('#datetimepicker').datetimepicker({
			weekStart  : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});

		$('#datetimepicker').datetimepicker('setStartDate', date);
		/*$('#datetimepicker').datetimepicker('autoclose', 1);
		$('#datetimepicker').datetimepicker('todayBtn', 1);
		$('#datetimepicker').datetimepicker('minView', 2);
		$('#datetimepicker').datetimepicker('maxView', 1);
		$('#datetimepicker').datetimepicker('startView', 2);*/

	});
function CheckDelete(id,di) {
	console.log(id);
	console.log(di);
	 alertify.confirm("Confirmation Message","Data Penilaian akan dihapus ?",
    function(input) {
      if (input) {
		alertify.success('Data Berhasil Dihapus');       
        window.location.href = "<?php echo base_url(); ?>kaprodi/del_nilai/"+id+"/"+di;
      } else {
        alertify.error('Cancel');
      }
    }, function(){alertify.error('Cancel');});
}
id = "<?php echo $id ?>";
	console.log(id);
</script>







<?php if($this->session->flashdata('msg') == 'ok') { ?>	
<script type="text/javascript">
	 alertify.success('Success!');
	 //window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>";
</script>
<?php } ?>
<?php if($this->session->flashdata('msg') == 'not ok') { ?>	
<script type="text/javascript">
	alertify.alert('Error', 'Data Nilai/Kondisi Tidak Boleh Sama');
	id = "<?php echo $id ?>";
	console.log('dildildil');
	//window.history.pushState('data to be passed', 'Edit KPI', '<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>');
	//window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/"+id;
</script>
<?php  } ?>
<?php if($this->session->flashdata('msg') == 'not ok nilai') { ?>	
<script type="text/javascript">
	alertify.alert('Error', 'Data Nilai Tidak Boleh Sama');
	id = "<?php echo $id ?>";
	console.log('asdasdasdas');
	//window.history.pushState('data to be passed', 'Edit KPI', '<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>');
	//window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/"+id;
</script>
<?php  } ?>
<?php if($this->session->flashdata('msg') == 'not ok deadline') { ?>	
<script type="text/javascript">
	alertify.alert('Error', 'Tanggal Deadline Tidak Boleh Sama');
	id = "<?php echo $id ?>";
	console.log(id);
	//window.history.pushState('data to be passed', 'Edit KPI', '<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>');
	//window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/"+id;
</script>
<?php  } ?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
