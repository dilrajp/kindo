<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>

	
	<script>
		var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
	</script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/knockout-3.1.0.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo base_url('assets/js/chart.js')?>"></script> 

	<script src="<?php echo base_url('assets/js/xenon-widgets.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/regions/jquery-jvectormap-world-mill-en.js') ?>"></script>
</head>
<body class="page-body">
<body class="page-body">
	<?php
	$this->load->view('fragment/sidebar_kaprodi');
	?>
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
					
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>

					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			
			</script>


			
			<!-- Xenon Todo List -->
			<div class="row">
			<div class="col-md-12">
				<div class="chart-item-bg">
						<div class="chart-label">
							<div id="network-mbs-packets" class="h1 text-secondary text-bold" data-count="this" data-from="0.00" data-to="<?php echo $this->db->query("select * from t_kpi where status = 'ON'")->num_rows()?>"  data-duration="4">0.00%</div>
							<span class="text-medium text-muted text-upper">Indikator Penilaian Bidang Pengajaran</span>
							
							<p class="text-medium" style="width: 50%; margin-top: 10px">						
								<a href="<?php echo base_url('kaprodi/ListIndikator'); ?>" class="btn btn-secondary btn-icon btn-icon-standalone btn-icon-standalone-right">				<i class="fa-search"></i>
									<span>View KPI</span>
								</a>
							</p>
						</div>
						<div id="other-stats" style="min-height: 183px">
							<div id="cpu-usage-gauge" style="width: 370px; height: 140px; position: absolute; right: 20px; top: 20px"></div>
						</div>
				</div>
				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-danger" data-count=".num" data-from="0" data-to="<?php echo trps();?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
							<?php  
								$persen = trps();
								if ($persen > 0) {
									echo round($persen*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Document RPS</span>
						</div>
					</div>
					
				</div>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-purple" data-count=".num" data-from="0" data-to="<?php echo tmodul();?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
							<?php  
								$persenmodul = tmodul();
								if ($persenmodul > 0) {
									echo round($persenmodul*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Modul Praktikum</span>
						</div>
					</div>
					
				</div>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-warning" data-count=".num" data-from="0" data-to="<?php echo tsoal(); ?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
								<?php  
								$persensoal = tsoal();
								if ($persensoal > 0) {
									echo round($persensoal*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Soal Asessment</span>
						</div>
					</div>
					
				</div>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-succses" data-count=".num" data-from="0" data-to="<?php echo tbahan(); ?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
								<?php  
								$persenbahan = tbahan();
								if ($persenbahan > 0) {
									echo round($persenbahan*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Bahan Perkuliahan</span>
						</div>
					</div>
					
				</div>

			</div>
			</div>
			</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>
	


	<!-- Bottom Scripts -->
</body>
</html>