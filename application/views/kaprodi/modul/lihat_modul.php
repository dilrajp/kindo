<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_kaprodi");
?>

<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_kaprodi'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Modul Praktikum</h1>
					<p class="description">Daftar Modul Praktikum</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('kaprodi');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Modul Praktikum</strong>
						</li>
						</ol>
				</div>		

			</div>
			
			<!-- Body Page-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Modul</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

				<div class="panel-body">

					<table class="table table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								<th style="width: 10px;">No</th>
								<th style="width: 100px;">Matakuliah</th>
								<th style="width: 100px;">Tanggal</th>
								<th style="width: 50px;">Status</th>
								<th style="width: 50px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody id = "Modul">
							
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->


<!-- Bottom Scripts -->
<footer class="main-footer sticky footer-type-1">
	
	<div class="footer-inner">
	
		<!-- Add your copyright text here -->
		<div class="footer-text">
			&copy; 2014 
			<strong>Xenon</strong> 
			theme by <a href="http://laborator.co" target="_blank">Laborator</a>
		</div>
		
		
		<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
		<div class="go-up">
		
			<a href="#" rel="go-top">
				<i class="fa-angle-up"></i>
			</a>
			
		</div>
		
	</div>
	
</footer>
</div>
</div>

<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>

<?php
$this->load->view("fragment/foot");
?>

<script type="text/javascript">
	function Approval(id) {
 	alertify.confirm("Confirmation Message","Yakin Approval Modul ?",
    function(input) {
      if (input) {
        alertify.success('Approval Modul');
        window.location.href = "<?= base_url() ?>kaprodi/updateStatusModul/"+id;
      } else {
        alertify.error('Cancel');
      }
   	}, function(){  alertify.error('Cancel !');});
 	}	

 	function Notice(id){
 		alertify.prompt("Confirmation Message","Beri Tanggapan Revisi!!!", "Mohon Diperbaiki",
    	function(evt,value) {
    	var input = decodeURI(value);
        alertify.success('Status: Revisi');
         $.ajax({
		  type: "POST",
		  url: "<?= base_url() ?>kaprodi/revisi_modul/",
		  data: {identitas:id,keterangan:input},
		  success:function(data){
		  	window.location.href="<?php echo base_url('kaprodi/lihat_modul'); ?>"
		  }
		});
    }, function(){  alertify.error('Cancel !');});
 	}
</script>

<!-- get data modul using ajax -->
<script type="text/javascript">
	
	viewModul();

	function viewModul(){
	
		var text = "";
		  $.ajax({
	        url: '<?php echo base_url('kaprodi/view_modul'); ?>',
	        dataType:'json',
	        type: "post",
	        success: function(data){
	        	var no = 1;
	        	for (var i = 0; i < data.jumlah; i++) {
	        		if (data.results[i].status_modul == 'WAITING') {
						lihat =  "label label-warning";
					} else if (data.results[i].status_modul == 'REVISI') {
						lihat = "label label-danger";
					} else if (data.results[i].status_modul == 'APPROVE') {
						lihat = "label label-secondary";
					}
	        		text += '<tr>\
	        				<td>'+no+'</td>\
	        				<td>'+data.results[i].nama_matakuliah+'</td>\
	        				<td>'+data.results[i].date+'</td>\
	        				<td><label class="'+lihat+'">'+data.results[i].status_modul+'</td>\
	        				<td>\
	        					<button onClick="Approval('+data.results[i].id_modul+');" class="col-xs-3 btn btn-secondary btn-lg fa-check icon-left"></button>\
									<a href="<?php echo base_url('kaprodi/download_rps/'); ?>'+data.results[i].id_modul+'" class="col-xs-3 btn btn-info btn-lg fa-download"></a>\
									<button onClick="Notice('+data.results[i].id_modul+');" class="col-xs-3 btn btn-danger btn-lg fa-tag icon-left"></button>\
	        				</td>\
	        				</tr>';
	          		$('#Modul').html(text);
	          		no++;
	        	}
	           //console.log(data);
	        }
	      });
	}
	setInterval(function(){ viewModul(); }, 1000);
</script>