<?php 
$this->load->view("fragment/head");
?>
   <link rel="stylesheet" href="<?php echo base_url('assets/css/eventCalendar.css')?>">
   <link rel="stylesheet" href="<?php echo base_url('assets/css/eventCalendar_theme_responsive.css')?>">

<body class="page-body">
				<?php
				$this->load->view('fragment/sidebar_koormk');
				?>
			
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php //$this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				<ul class="user-info-menu left-links list-inline list-unstyled">
				
					
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
					
						</ul>
					</li>
					
				</ul>

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>

			<div class="row">
				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-danger" data-count=".num" data-from="0" data-to="<?php rps($this->session->id_dosen); ?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
								<?php  
								$persen = rps($this->session->id_dosen);
								if ($persen > 0) {
									echo round(rps($this->session->id_dosen)*100,2);
								}else{
									echo "0";
								}
								?>%
							</strong>
							<span>Document RPS</span>
						</div>
					</div>
					
				</div>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-purple" data-count=".num" data-from="0" data-to="<?php echo modul($this->session->id_dosen); ?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
							<?php  
								$persenmodul = modul($this->session->id_dosen);
								if ($persenmodul > 0) {
									echo round(modul($this->session->id_dosen)*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Modul Praktikum</span>
						</div>
					</div>
					
				</div>
				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-warning" data-count=".num" data-from="0" data-to="<?php echo soal($this->session->id_dosen); ?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
								<?php  
								$persensoal = soal($this->session->id_dosen);
								if ($persensoal > 0) {
									echo round(soal($this->session->id_dosen)*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Soal Asessment</span>
						</div>
					</div>
					
				</div>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-vertical-counter xe-vertical-counter-succses" data-count=".num" data-from="0" data-to="<?php echo  bahan($this->session->id_dosen); ?>" data-decimal="," data-suffix="%" data-duration="3">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						
						<div class="xe-label">
							<strong class="num">
								<?php  
								$persenbahan = bahan($this->session->id_dosen);
								if ($persenbahan > 0) {
									echo round(bahan($this->session->id_dosen)*100,2);
								}else{
									echo "0";
								}?>%
							</strong>
							<span>Bahan Perkuliahan</span>
						</div>
					</div>
					
				</div>

			</div>
			<div class="panel panel-color panel-info collapsed"><!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Progres Nilai Asessment dan Usulan Soal</strong></h3>
							
							<div class="panel-options">
								
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
								
							</div>
						</div>
						
					<div class="panel-body">
						<div class="panel-body panel-border">
						
							<div class="row">
								<div class="col-sm-12">
								
									<!-- Table Model 2 -->
									
								
									<table class="table table-model-2 table-hover">
										<thead>
											<tr>												
												<th>No</th>
												<th>Nama</th>
												<th>Matakuliah</th>
												<th>Progress Nilai</th>
												<th>Progress Usulan</th>
											</tr>
										</thead>
										
										<tbody>
										<?php $no = 1; foreach ($progres1 as $row) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row->nama; ?></td>
											<td><?php echo $row->nama_matakuliah; ?></td>
											<td>
												<?php 
													if($row->status_nilai){ ?>
													<i class="fa-check" style="color: green;"></i>	
													<?php } else { ?>
														<i class="fa-times" style="color: red;"></i>
												<?php	}
												?>
											</td>
											<td>
												<?php 
													if($row->status_usulan){ ?>
													<i class="fa-check" style="color: green;"></i>	
													<?php } else { ?>
														<i class="fa-times" style="color: red;"></i>
												<?php	}
												?>
											</td>
										</tr>
										<?php $no ++; } ?>
									</tbody>
									</table>
								
								</div>
							</div>
						
						</div>
			
						</div>
					</div>
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
		
		</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>
	



<!-- Bottom Scripts -->
<?php
$this->load->view("fragment/foot");
?>

<script type="text/javascript">
	//toast untuk notif modul
 	<?php foreach ($rows4 as $x): ?>

 		<?php 
 			if ($x->status_modul=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.warning("Anda memiliki revisi modul.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

 	//toast untuk notif revisi rps
	 	<?php foreach ($rows5 as $x): ?>

	 		<?php 
	 			if ($x->status_rps=='REVISI') {?>

	 				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "default",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					};
					
					toastr.success("Anda memiliki revisi RPS.<br> Segera Tindak Lanjuti!!!", null, opts);
	 			<?php } ?>
	 	<?php endforeach ?>

	 		//toast untuk notif revisi soal
	 	<?php foreach ($rows6 as $x): ?>

	 		<?php 
	 			if ($x->status_soal=='REVISI') {?>

	 				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "default",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					};
					
					toastr.error("Anda memiliki revisi soal.<br> Segera Tindak Lanjuti!!!", null, opts);
	 			<?php } ?>
	 	<?php endforeach ?>

	 	//toast notif revisi bahan
	<?php foreach ($rows7 as $x): ?>

		<?php 
			if ($x->status_bahan=='REVISI') {?>

				var opts = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "default",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
			};
			
			toastr.info("Anda memiliki revisi bahan.<br> Segera Tindak Lanjuti!!!", null, opts);
			<?php } ?>
	<?php endforeach ?>
</script>