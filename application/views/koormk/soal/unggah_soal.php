<?php 
$this->load->view("fragment/head");
?>
										
<?php
$this->load->view('fragment/sidebar_koormk');
?>	
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Soal Asessment</h1>
					<p class="description">Unggah Soal Asessment</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('koor_mk');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Soal Asessment</strong>
						</li>
						</ol>
				</div>		

			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			
			<!-- Body Page-->
			<div class="panel panel-default"> 
   				<div class="panel-heading">
   				 <div class="panel-title"><h3>Unggah Soal Asessment</h3>
   				 	 <p class="description" style="font-size: 13px; color: red;">Batas Unggah
	   				 	<?php foreach ($kondisi as $row): ?>
	   				 		<?php echo $row->kondisi; ?>		
	   				 	<?php endforeach ?>	
	   				 </p>
   				</div> 
   				</div> 
   				<div class="panel-body"> 
   					<form enctype="multipart/form-data" action="<?php echo base_url('koor_mk/unggah_soal_aksi'); ?>" role="form" id="form1" method="post" class="validate" novalidate="novalidate"> 
   						<div class="form-group">
   							<label class="control-label">Matakuliah</label><br>
   							<select name="id_matakuliah" id="id_matakuliah" style="width: 300px;" required>
   								<option value="">--Pilih Matakuliah--</option>
   								<?php foreach ($matkul as $row): ?>
   									<option value="<?php echo $row->id_matakuliah; ?>"><?php echo $row->nama_matakuliah; ?></option>
   								<?php endforeach ?>
   							</select>
   						</div>
   						<div class="form-group">
						<label class="control-label">Upload Bukti Penyerahan Soal</label>
						<p class="description" style="font-size: 13px; color: red;">*Format Foto jpg,jpeg/png</p>
						  	<input type="file" class="form-control" name="userfile1" required="jpg,jpeg,png">
				  		</div>
   						<div class="form-group">
						<label class="control-label">Upload Berkas</label>
						<p class="description" style="font-size: 13px; color: red;">*Format File doc,pptx,pdf dan xlsx</p>
						  	<input type="file" class="form-control" required="" name="userfile2">
				  		</div>
   						<div class="form-group"> 
   						 	<button type="submit" value="Tambah" class="btn btn-success">Save</button> 
   						 	<button type="reset" class="btn btn-white">Reset</button>
   						</div> 
   					</form> 
   				</div>
   			</div>
			<!-- Batas Body Page-->

		</div>

	</div>
			
<?php
	$this->load->view("fragment/foot");
?>

<script type="text/javascript">
 	<?php foreach ($rows4 as $x): ?>

 		<?php 
 			if ($x->status_modul=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.warning("Anda memiliki revisi modul.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>
 	
 	//toast untuk notif revisi rps
 	<?php foreach ($rows5 as $x): ?>

 		<?php 
 			if ($x->status_rps=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.success("Anda memiliki revisi RPS.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

	  //toast untuk notif revisi soal
		<?php foreach ($rows6 as $x): ?>

			<?php 
				if ($x->status_soal=='REVISI') {?>

					var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.error("Anda memiliki revisi soal.<br> Segera Tindak Lanjuti!!!", null, opts);
				<?php } ?>
		<?php endforeach ?>

		//toast notif revisi bahan
		<?php foreach ($rows7 as $x): ?>

			<?php 
				if ($x->status_bahan=='REVISI') {?>

					var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.info("Anda memiliki revisi bahan.<br> Segera Tindak Lanjuti!!!", null, opts);
				<?php } ?>
		<?php endforeach ?>
</script>
<?php if ($this->session->flashdata('msg') == 'gagal'){ ?>
	<script type="text/javascript">
	$(document).ready(function(){
		var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-full-width",
						"onclick": null,
						"showDuration": "1000",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("Kesalahan dalam proses unggah, mohon cek format dokumen dan ukuran file", opts);
	});
	</script>
<?php } ?>

<?php if ($this->session->flashdata('msg') == 'done'){ ?>
	<script type="text/javascript">
	$(document).ready(function(){
		var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-full-width",
						"onclick": null,
						"showDuration": "1000",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("Sudah Melakukan Unggah Berkas Sebelumnya", opts);
	});
	</script>
<?php } ?>
		