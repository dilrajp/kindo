
<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_koormk");
?>
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Bahan Perkuliahan</h1>
					<p class="description">Daftar Bahan Perkuliahan</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('koor_mk');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Bahan Perkuliahan</strong>
						</li>
						</ol>
				</div>		

			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Bahan Perkuliahan</h3>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>

					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 1},
						]);
					});
					</script>

					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr style="">
								<th style="width: 10px;">No.</th>
								<th style="width: 100px;">Matakuliah</th>
								<th style="width: 50px;">Tanggal</th>
								<th style="width: 50px;">Status</th>
								<th style="width: 10px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody>
							<?php if($rows){$no=1; foreach($rows as $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->nama_matakuliah; ?></td>
								<td><?php echo $row->date; ?></td>
								<td>
									<label  style="color: #ffffff;" class="
									<?php 
										if ($row->status_bahan=='WAITING') {
											echo  "label label-warning";
										}elseif($row->status_bahan=='REVISI'){
											echo "label label-danger";
										}elseif($row->status_bahan=='APPROVE'){
											echo "label label-secondary";
										}
									 ?>
									"><?php echo $row->status_bahan;?>
									</label>
								</td>
								<td>
									<a href="<?php echo base_url('koor_mk/download_bahan/'.$row->fileName); ?>" class="col-xs-3 btn btn-info btn-lg fa-download"></a>
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_bahan; ?>').modal('show', {backdrop: 'fade'});" class="col-xs-3 btn btn-warning btn-lg fa-check-square-o"></a>
									<button onClick="Delete(<?= $row->id_bahan; ?>);" class="col-xs-3 btn btn-danger btn-lg fa-trash icon-left"></button>
								</td>
							</tr>
							<?php  $no++; } } ?>
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->
		</div>

	</div>
	<!-- Bottom Scripts -->
	<?php if($rows){ foreach($rows as $row){ ?>
	<div  class="modal fade" id="modal-<?php echo $row->id_bahan; ?>">
		<div class="modal-dialog">
			<div class="modal-content">
				<form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo base_url('koor_mk/revisiBahan/'.$row->id_bahan); ?>">
					<input type="hidden" name="id_matakuliah" value="<?php echo $row->id_matakuliah; ?>">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Revisi RPS</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="field-1">Daftar Revisi</label>
							<div class="col-sm-8">
								<textarea style="height: 150px;" name="keterangan" class="form-control" id="field-2" disabled="">
									<?php echo $row->keterangan; ?>
								</textarea>
							</div>
						</div>
   						<div class="form-group">
							<label class="col-sm-3 control-label" for="field-1">Upload Berkas</label>
						  	<input type="file" name="userfile">
				  		</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-info">Save</button>
							<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php } } ?>
	<!-- Batas modal -->
<?php
$this->load->view("fragment/foot");
?>

<script type="text/javascript">
	function Delete(id) {
	 	alertify.confirm("Confirmation Message","Yakin Hapus Bahan Perkuliahan ?",
	    function(input) {
	      if (input) {
	        alertify.success('Delete Bahan Perkuliahan');
	        window.location.href = "<?= base_url() ?>koor_mk/hapus_bahan/"+id;
	      } else {
	        alertify.error('Cancel');
	      }
	   	}, function(){  alertify.error('Cancel !');});
	 }

	//toast notif revisi bahan
	<?php foreach ($rows as $x): ?>

		<?php 
			if ($x->status_bahan=='REVISI') {?>

				var opts = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "default",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
			};
			
			toastr.info("Anda memiliki revisi bahan.<br> Segera Tindak Lanjuti!!!", null, opts);
			<?php } ?>
	<?php endforeach ?>

	//toast notif revisi modul
	<?php foreach ($rows4 as $x): ?>

		<?php 
			if ($x->status_modul=='REVISI') {?>

				var opts = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "default",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
			};
			
			toastr.warning("Anda memiliki revisi modul.<br> Segera Tindak Lanjuti!!!", null, opts);
			<?php } ?>
	<?php endforeach ?>

	//toast untuk notif revisi rps
	 	<?php foreach ($rows5 as $x): ?>

	 		<?php 
	 			if ($x->status_rps=='REVISI') {?>

	 				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "default",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					};
					
					toastr.success("Anda memiliki revisi RPS.<br> Segera Tindak Lanjuti!!!", null, opts);
	 			<?php } ?>
	 	<?php endforeach ?>

	 	//toast untuk notif revisi soal
	 	<?php foreach ($rows6 as $x): ?>

	 		<?php 
	 			if ($x->status_soal=='REVISI') {?>

	 				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "default",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					};
					
					toastr.error("Anda memiliki revisi soal.<br> Segera Tindak Lanjuti!!!", null, opts);
	 			<?php } ?>
	 	<?php endforeach ?>
</script>
	