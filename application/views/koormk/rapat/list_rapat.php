<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- foot -->


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">

	
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>


	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" ></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>" defer>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>" defer></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>
</head>	
<body class="page-body">
<?php 
$this->load->view("fragment/sidebar_koormk");
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
					
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Undangan Rapat</h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen</p>
				</div>
				
					<div class="breadcrumb-env">
					
					<ol class="breadcrumb bc-1">
							<li>
							<a href="koor_mk"><i class="fa-home"></i>Home</a>
							</li>
							<li class="active">
							<a href="list_rapat">Rapat</a>
							</li>
					</ol>
						
								
				</div>
					
			</div>
			<a href="javascript:;" 
			onclick="jQuery('#modal-1').modal('show', {backdrop: 'fade'});" 
			class="btn btn-red btn-icon btn-icon-standalone">
				<i class="fa-pencil"></i>
				<span>Tambah Undangan</span>
			</a>
			<?php
			?>
		

	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Undangan Rapat Koordinasi Mata Kuliah</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
					
					<table class="table table-striped table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								
								<th>Mata Kuliah</th>
								<th>Agenda</th>
								<th>Ruangan</th>
								<th>Tanggal</th>
								<th> Aksi </th>

							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        
					        foreach ($rows as $row) {
					        ?>
							<tr>
								
								<td><?php echo  ucwords(strtolower(nama_matkul($row->id_matakuliah))); ?>	</td>
								<td><?php echo $row->agenda; ?></td>
								<td class="center"><?php echo $row->ruangan; ?></td>
								<td class="center"><?php 
									echo date('Y-m-d H:i:s', strtotime($row->tanggal_rapat)); ?></td>
								<td>
							
									<a href="<?php echo base_url() ?>koor_mk/edit_rapat/<?php echo $row->id_undangan;?>"  class="btn btn-secondary fa-refresh icon-left"> </a>									
									<button onClick="CheckDelete(<?= $row->id_undangan; ?>,<?= $row->id_matakuliah; ?>);" class="btn btn-danger fa-trash icon-left">
									</button>									
									<a href="<?php echo base_url() ?>koor_mk/detail_rapat/<?php echo $row->id_undangan; ?>" class="btn btn-info fa-search icon-left"> </a>
										
									</a>
								
								</td>
							</tr>
							<?php } ?>
						</tbody>	
							
					</table>
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
	
		</div>
		
			
	
		
		
	</div>
<div class="page-loading-overlay">
<div class="loader-2"></div>
</div>	
</body>
</html>
	<!-- <script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
						
							{column_number : 0, filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							
						]);
					});
					</script> -->
	<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
	</script>


<div class="modal fade" tabindex="-1" role="dialog" id="modal-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Undangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_mk/add_rapat'; ?>">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Agenda</label>
									
									<div class="col-sm-10">
										<input name="agenda" type="text" class="form-control" id="field-1" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tipe</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Koordinasi Mata Kuliah" disabled="">
									<input type="hidden" name="radio" value="1"/ name="radio">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Ruangan</label>
									
									<div class="col-sm-10">
										<input name="ruangan" type="text" class="form-control" id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
									<div class="col-sm-10">
								<div class="input-append date form_datetime">
							    <input name="tanggal"  class="form control"  type="text" id='datetimepicker' readonly required="required">
							    <span class="add-on"><i class="icon-th"></i></span>
								</div>
							</div>
						</div>

						<div class="form-group">
									<label class="col-sm-2 control-label">Peserta</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-1").select2({
												placeholder: 'Daftar Mata Kuliah yang tersedia...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select  id="s2example-1" required name="mk">
										<option></option>
										<optgroup label="Daftar Matakuliah">
										<?php foreach ($nama as $row) {		?>	
											<option value="<?php echo $row->id_matakuliah ?>" ><?php echo $row->nama_matakuliah ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								
						
				
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

					</form>
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$('#datetimepicker').datetimepicker({
		todayBtn : 1,
		autoclose : 1,
		todayHighlight : 1,
		forceParse : 0
		});
		var date = new Date();
		date.setDate(date.getDate());
		var date2 = new Date();
		date2.setDate(date.getDate()+30); 
		$('#datetimepicker').datetimepicker('setStartDate', date);
		$('#datetimepicker').datetimepicker('setEndDate', date2);
	
	});
</script>

	<script type="text/javascript">
		
function ConfirmMsg()
{
alertify.confirm("This is a confirm dialog.",
  function(){
    alertify.success('Ok');
    return true;
  },
  function(){
    alertify.error('Cancel');
  });
}

$(document).ready(function(){
    $('.confirm').on('click', function(event){
        // add me, stop the default link from clicking
        event.preventDefault();

        alertify.confirm("Message", function (e) {
          if (e) {
              // use window.location and go to a->href
          } else {            
            return false; 
          }        
      });     
    });
});

function CheckDelete(id,di) {
  alertify.confirm("Konfirmasi Aksi","Data Undangan akan dihapus ?",
    function(input) {
      if (input) {
       	 alertify.success('Cancel');
        window.location.href = "<?php echo base_url(); ?>koor_mk/del_rapat/"+id+"/"+di;
      } else {
        alertify.error('Cancel');
      }
    }, function(){ alertify.error('Cancel')});
}

function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }

    
</script>  



<script type="text/javascript">
//toast untuk notif revisi modul
 	<?php foreach ($rows4 as $x): ?>

 		<?php 
 			if ($x->status_modul=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.warning("Anda memiliki revisi modul.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

 	//toast untuk notif revisi rps
 	<?php foreach ($rows5 as $x): ?>

 		<?php 
 			if ($x->status_rps=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.success("Anda memiliki revisi RPS.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

 	 //toast untuk notif revisi soal
	<?php foreach ($rows6 as $x): ?>

		<?php 
			if ($x->status_soal=='REVISI') {?>

				var opts = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "default",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
			};
			
			toastr.error("Anda memiliki revisi soal.<br> Segera Tindak Lanjuti!!!", null, opts);
			<?php } ?>
	<?php endforeach ?>

	//toast notif revisi bahan
	<?php foreach ($rows7 as $x): ?>

		<?php 
			if ($x->status_bahan=='REVISI') {?>

				var opts = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "default",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
			};
			
			toastr.info("Anda memiliki revisi bahan.<br> Segera Tindak Lanjuti!!!", null, opts);
			<?php } ?>
	<?php endforeach ?>
</script>

