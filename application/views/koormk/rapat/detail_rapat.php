<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>"> 
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- foot -->


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">

	
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>


	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" ></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>" defer>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>" defer></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>


	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>">
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>
</head>

<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koormk');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Detail Undangan Rapat</h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen</p>
				</div>
				
					<div class="breadcrumb-env">
					
					<ol class="breadcrumb bc-1">
					<li>
					<a href="dosen"><i class="fa-home"></i>Home</a>
					</li>
					<li >
					<a href="list_rapat">Rapat</a>
					</li>
					<li class="active">
					<a href="#">Detail Undangan</a>
					</li>
					</ol>
								
				</div>
					
			</div>
	
	<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Detail Undangan</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

					<div class="panel-body">
					<form role="form" class="form-horizontal">	
					<?php 
					        
					 foreach ($rows as $row) {
					 ?>	
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Rapat	</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo namarapat($row->tipe); ?>" disabled="">
							</div>
							<label class="col-sm-2 control-label"><b>Agenda</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo $row->agenda; ?>" disabled="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Tanggal</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php 
									echo date('Y-m-d H:i:s', strtotime($row->tanggal_rapat)); ?>" disabled="">
							</div>
							<label class="col-sm-2 control-label"><b>Ruangan</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo $row->ruangan; ?>" disabled="">
							</div>
						</div>
							
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Bukti Rapat</b></label>
							<div class="col-sm-4">						
							<img  style="width: 50%;display: block;   margin-left: auto;  margin-right: auto" id="image_from_url">
								
							</div>
						</div>
					<?php } ?>

					</form>
					</div>
			</div>
			<!-- Custom column filtering -->
		
	
					
				<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Unggah Bukti Rapat</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{


							var i = 1,
								$example_dropzone_filetable = $("#example-dropzone-filetable"),
								example_dropzone = $("#advancedDropzone").dropzone({
								url: '<?php echo base_url('koor_mk/add_bukti/'.$this->uri->segment(3)); ?>',
								acceptedFiles: '.jpeg,.jpg,.png',
								
								// Events
								addedfile: function(file)
								{
									if(i == 1)
									{
										$example_dropzone_filetable.find('tbody').html('');
									}
									
									var size = parseInt(file.size/1024, 10);
									size = size < 1024 ? (size + " KB") : (parseInt(size/1024, 10) + " MB");
									
									var	$el = $('<tr>\
													<td class="text-center">'+(i++)+'</td>\
													<td>'+file.name+'</td>\
													<td><div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div></td>\
													<td>'+size+'</td>\
													<td>Uploading...</td>\
												</tr>');
									
									$example_dropzone_filetable.find('tbody').append($el);
									file.fileEntryTd = $el;
									file.progressBar = $el.find('.progress-bar');
								},
								
								uploadprogress: function(file, progress, bytesSent)
								{
									file.progressBar.width(progress + '%');
								},
								
								success: function(file)
								{
									file.fileEntryTd.find('td:last').html('<span class="text-success">Uploaded</span>');
									file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success');
								},
								
								error: function(file)
								{
									file.fileEntryTd.find('td:last').html('<span class="text-danger">Failed</span>');
									file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
								}


							});
							
							$("#advancedDropzone").css({
								minHeight: 200
							});
							
						});
					</script>
					
					<br />
					<div class="row">
						<div class="col-sm-3 text-center">
						
							<div id="advancedDropzone" class="droppable-area">
								Drop Files Here
							</div>
							
						</div>
						<div class="col-sm-9">
							
							<table class="table table-bordered table-striped" id="example-dropzone-filetable">
								<thead>
									<tr>
										<th width="10%" class="text-center">No.</th>
										<th width="40%">Nama File</th>
										<th width="20%">Progres Unggah</th>
										<th>Size</th>
										<th width="15%">Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="5">Upload Bukti Rapat 1 File Saja ( Bertipe.jpg/.jpeg )</td>
									</tr>
								</tbody>
							</table>
							
						</div>
					</div>
					
				</div>
						
			</div>
				
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">List Peserta</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

				<div class="panel-body">
					<table class="table table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Nama</th>
								<th>Status</th>						
								<th>Keterangan</th>						
								<th> Aksi </th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        $id=1;
					        foreach ($res as $row) {
					        
					        ?>

							<tr >
								
								<td><?php echo $id++ ?>	</td>
								<td><?php echo getnamabyid($row->id_dosen); 	?></td>
								<?php 
								if($row->status == "TERKIRIM") {
								  echo "<td class='center' bgcolor='#ddd'><font color='#d5080f'>";
								 } else if($row->status == "ALPHA") {
								 	  echo "<td class='center' bgcolor='#d5080f'><font color='#fff'>";
								 } else {
								  echo  "<td class='center'>";
								 } ?>
									<?php echo $row->status; ?></td>
								<td><?php echo $row->keterangan	?></td>
								
							<td class="center">
									<button onClick="ChangeHadir(<?= $row->id_undangan; ?>, <?= $row->id_kehadiran; ?>, <?= $row->id_dosen; ?> );" class="btn btn-secondary fa-check icon-left"> </button>
									<button onClick="ChangeIzin(<?= $row->id_undangan; ?>, <?= $row->id_kehadiran; ?>, <?= $row->id_dosen; ?> );" class="btn btn-warning fa-tag"> </button>
									<button onClick="ChangeAlpha(<?= $row->id_undangan; ?>, <?= $row->id_kehadiran; ?>, <?= $row->id_dosen; ?>);" class="btn btn-danger fa-times icon-left"> </button>

																	
								
								</td>
							</tr>
							<?php } ?>
						</tbody>	
							
					
					</table>
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
	
		
		
	</div>
</body>
</html>

	<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 0, filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2},
							{column_number : 3, filter_type: 'text'},
							

						]);
					});
					</script>


<script>

    function ChangeHadir(id,di,da) {
 	alertify.prompt("Confirmation Message","Merubah status ke Hadir ?", "Informasi Hadir",
    function(evt,value) {
    	var input = decodeURI(value);
    	console.log (decodeURI(value));
    	alertify.success('Status: Hadir');
        window.location.href = "<?= base_url() ?>koor_mk/rapat_hadir/"+id+"/"+input+"/"+di+"/"+da;
             
    }, function(){  alertify.error('Gagal !');});
	}

    function ChangeAlpha(id,di,da) {
     alertify.prompt("Confirmation Message","Merubah status ke Alpha ?", "Informasi Alpha",
    function(evt,value) {
    	var input = decodeURI(value);
    	console.log (decodeURI(value));
    	alertify.success('Status: Alpha');
        window.location.href = "<?= base_url() ?>koor_mk/rapat_alpha/"+id+"/"+input+"/"+di+"/"+da;
             
    }, function(){  alertify.error('Gagal !');});
	}

	function ChangeIzin(id,di,da) {
    alertify.prompt("Confirmation Message","Merubah status ke Izin ?", "Informasi Izin",
    function(evt,value) {
    	var input = decodeURI(value);
    	console.log (decodeURI(value));
    	alertify.success('Status: Izin');
        window.location.href = "<?= base_url() ?>koor_mk/rapat_izin/"+id+"/"+input+"/"+di+"/"+da;
             
    }, function(){  alertify.error('Gagal !');});
 	
	}
</script>  

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 35%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Bukti Rapat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						<img  style="width: 50%;display: block;   margin-left: auto;  margin-right: auto" id="image_from_url">
		      					
					
							
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					
				</div>
				
					
    </div>
  </div>
</div>
<div id="myModal" class="modal">
  
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<style>

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#image_from_url:hover {cursor: zoom-in;}
#img01:hover {cursor: zoom-out;}

/* The Modal (background) */


/* Modal Content (image) */
.modal-content {

    margin: auto;
    display: block;
    
    
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {  

    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}



@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}



/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
        height: 100%;
    }
}
</style>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('image_from_url');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
img01.onclick = function() { 
    modal.style.display = "none";
}
</script>
<script type="text/javascript">
$(document).ready(function(){
v_gambar();
setInterval(v_gambar,1000);
    console.log(`%c ________________________________________
< dildildildildildil its dilraj code  >
 ----------------------------------------
        \\   ^__^
         \\  (oo)\\_______
            (__)\\       )\\/\\
                ||----w |
                ||     ||`, "font-family:monospace")
//v_gambar();
		//GET UPDATE
		 function v_gambar(){
		 	var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
		 	var pathArray = window.location.pathname.split( '/' );
		 	//console.log(newURL);
            var id = pathArray[4];
            //console.log(id);
       
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('koor_mk/view_gambar')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(gambar){
                		
                    	//$('#ModalaEdit').modal('show');
                    	if(data.gambar == 'x'){

                    	}else{
            			$( "#image_from_url" ).attr('src',data.gambar );
            			};
            			
            		
            		});
                }
            });
            return false;
        };

		
    });		
 </script>
