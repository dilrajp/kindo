<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>"> 
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- foot -->


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">

	
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>


	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" ></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>" defer>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>" defer></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>



</head>
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koormk');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Edit Undangan Rapat</h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen</p>
				</div>
				
					<div class="breadcrumb-env">
					
					<ol class="breadcrumb bc-1">
					<li>
					<a href="dosen"><i class="fa-home"></i>Home</a>
					</li>
					<li >
					<a href="list_rapat">Rapat</a>
					</li>
					<li class="active">
					<a href="#">Edit Undangan</a>
					</li>
					</ol>
								
				</div>
					
			</div>
		
		

	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Form Edit Undangan</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					<?php foreach($rows as $row){ ?>
					<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_mk/proses_edit_rapat'; ?>">
						<input type="hidden" name="id" value="<?php echo $row->id_undangan ?>">
					
					
									<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Agenda</label>
									
									<div class="col-sm-10">
										<input name="agenda" type="text" value="<?php echo $row->agenda ?>" class="form-control" id="field-1" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tipe</label>
									
									<div class="col-sm-10">
										<input type="text" class="form-control" value="<?php echo namarapat($row->tipe) ?>" disabled="" required="required">
										<input type="hidden" name="radio" value="1" name="radio">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"  for="field-1">Ruangan</label>
									
									<div class="col-sm-10">
										<input name="ruangan" type="text"  value="<?php echo $row->ruangan ?>" class="form-control" id="field-3" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
									<div class="col-sm-10">
								
							    <input name="tanggal"  class="form control"  id="datetimepicker" value="<?php echo $row->tanggal_rapat ?>" type="text"  readonly>
							    <span class="add-on"><i class="icon-th"></i></span>
								
							</div>
						</div>
					<a class="btn btn-white" href="<?php echo base_url().'koor_mk/list_rapat';?>">Tutup</a>
					<button type="submit" class="btn btn-info">Simpan</button>	
					</form>
					<?php } ?>
						
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
		</div>
		
			
	
		
		
	</div>

<div class="page-loading-overlay">
<div class="loader-2"></div>
</div>
</body>
</html>
	<script type="text/javascript">
		 $(document).ready(function() {
        // Untuk sunting
        $('#edit-data').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)

            // Isi nilai pada field
            modal.find('#tipe').attr("value",div.data('tipe'));
            modal.find('#agenda').attr("value",div.data('agenda'));
            modal.find('#ruangan').html(div.data('ruangan'));
            modal.find('#tanggal').attr("value",div.data('tanggal'));
        });

        var date = new Date();
		date.setDate(date.getDate());

		$('#datetimepicker').datetimepicker({
			weekStart  : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 1,
			forceParse : 0
		});
		$('#datetimepicker').datetimepicker('setStartDate', date);
    });


function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
</script>  
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
