
<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_koormk");
?>
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Nilai Asessment</h3>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>

					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 1},
						]);
					});
					</script>
					
					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr style="">
								<th style="width: 10px;">#</th>
								<th style="width: 100px;">Matakuliah</th>
								<th style="width: 100px;">Dosen</th>
								<th style="width: 100px;">Asessment</th>
								<th style="width: 100px;">Tanggal</th>
								<th style="width: 50px;">Keterangan</th>
								<th style="width: 100px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody id="nilai">
							
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->
			

		</div>

	</div>
	<!-- Bottom Scripts -->
<?php
$this->load->view("fragment/foot");
?>

<script type="text/javascript">
 	<?php foreach ($rows4 as $x): ?>

 		<?php 
 			if ($x->status_modul=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.warning("Anda memiliki revisi modul.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

 	//toast untuk notif revisi rps
 	<?php foreach ($rows5 as $x): ?>

 		<?php 
 			if ($x->status_rps=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.success("Anda memiliki revisi RPS.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

	 //toast untuk notif revisi soal
		<?php foreach ($rows6 as $x): ?>

			<?php 
				if ($x->status_soal=='REVISI') {?>

					var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.error("Anda memiliki revisi soal.<br> Segera Tindak Lanjuti!!!", null, opts);
				<?php } ?>
		<?php endforeach ?>

		//toast notif revisi bahan
		<?php foreach ($rows7 as $x): ?>

			<?php 
				if ($x->status_bahan=='REVISI') {?>

					var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.info("Anda memiliki revisi bahan.<br> Segera Tindak Lanjuti!!!", null, opts);
				<?php } ?>
		<?php endforeach ?>                                                                                                       
</script>
	
<script type="text/javascript">
	
	viewNilai();

	function viewNilai(){
		
		var text = "";
		  $.ajax({
	        url: '<?php echo base_url('koor_mk/view_nilai'); ?>',
	        dataType:'json',
	        type: "post",
	        success: function(data){
	        	var no = 1;
	        	for (var i = 0; i < data.jumlah; i++) {
	        		if (data.results[i].status_nilai == 'APPROVE') {
	        			if (data.results[i].status_nilai == 'WAITING') {
						lihat =  "label label-warning";
						} else if (data.results[i].status_nilai == 'REVISI') {
							lihat = "label label-danger";
						} else if (data.results[i].status_nilai == 'APPROVE') {
							lihat = "label label-secondary";
						}
		        		text += '<tr>\
		        				<td>'+no+'</td>\
		        				<td>'+data.results[i].nama_matakuliah+'</td>\
		        				<td>'+data.results[i].kode_dosen+'</td>\
		        				<td>'+data.results[i].asessment+'</td>\
		        				<td>'+data.results[i].date+'</td>\
		        				<td><label class="'+lihat+'">'+data.results[i].status_nilai+'</td>\
		        				<td>\
		        					<p>Selesai</p>\
		        				</td>\
		        				</tr>';
		          		$('#nilai').html(text);
		          		no++;
	        		}else{
	        			if (data.results[i].status_nilai == 'WAITING') {
						lihat =  "label label-warning";
						} else if (data.results[i].status_nilai == 'REVISI') {
							lihat = "label label-danger";
						} else if (data.results[i].status_nilai == 'APPROVE') {
							lihat = "label label-secondary";
						}
		        		text += '<tr>\
		        				<td>'+no+'</td>\
		        				<td>'+data.results[i].nama_matakuliah+'</td>\
		        				<td>'+data.results[i].kode_dosen+'</td>\
		        				<td>'+data.results[i].asessment+'</td>\
		        				<td>'+data.results[i].date+'</td>\
		        				<td><label class="'+lihat+'">'+data.results[i].status_nilai+'</td>\
		        				<td>\
		        					<button onClick="Approval('+data.results[i].id_nilaiasessment+','+data.results[i].id_dosen+');" class="btn btn-sm btn-secondary  fa-check"></button>\
										<a href="<?php echo base_url('koor_mk/download_nilai/'); ?>'+data.results[i].id_nilaiasessment+'" class="btn  btn-sm btn-info  fa-download"></a>\
										<button onClick="Notice('+data.results[i].id_nilaiasessment+');" class="btn btn-sm btn-danger  fa-times"></button>\
		        				</td>\
		        				</tr>';
		          		$('#nilai').html(text);
		          		no++;
	        		}
	        	}
	           //console.log(data);
	        }
	      });
	}
	setInterval(function(){ viewNilai(); }, 1000);
</script>		

<script type="text/javascript">
	function Approval(id,iddosen) {
 	alertify.confirm("Confirmation Message","Yakin Approval Nilai ?",
    function(input) {
      if (input) {
        alertify.success('Approval Nilai');
        window.location.href = "<?= base_url() ?>koor_mk/updateStatusNilai/"+id+"/"+iddosen;
      } else {
        alertify.error('Cancel');
      }
   	}, function(){  alertify.error('Cancel !');});
 	}

 	function Notice(id){
 		alertify.prompt("Confirmation Message","Beri Tanggapan Revisi!!!", "Mohon Diperbaiki",
    	function(evt,value) {
    	var input = decodeURI(value);
        alertify.success('Status : Revisi');
        $.ajax({
		  type: "POST",
		  url: "<?= base_url() ?>koor_mk/revisi_nilai/",
		  data: {identitas:id,keterangan:input},
		  success:function(data){
		  	window.location.href="<?php echo base_url('koor_mk/lihat_nilai'); ?>"
		  }
		});
    }, function(){  alertify.error('Cancel !');});
 	}	
</script>