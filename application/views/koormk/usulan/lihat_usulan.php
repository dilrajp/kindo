<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_koormk");
?>
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Usulan Soal Asessment</h1>
					<p class="description">Daftar Usulan Soal Asessment</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('koor_mk');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Usulan Soal Asessment</strong>
						</li>
						</ol>
				</div>		
				
			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Usulan Soal Asessment</h3>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>

					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr style="">
								<th style="width: 10px;">#</th>
								<th style="width: 100px;">Matakuliah</th>
								<th style="width: 100px;">Dosen</th>
								<th style="width: 100px;">Tanggal</th>
								<th style="width: 100px;">Assessment</th>
								<th style="width: 50px;">Keterangan</th>
								<th style="width: 100px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody id="usulan">
							
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->
		

		</div>

	</div>
	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>

	<script type="text/javascript">
 	<?php foreach ($rows4 as $x): ?>

 		<?php 
 			if ($x->status_modul=='REVISI') {?>

 				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.warning("Anda memiliki revisi modul.<br> Segera Tindak Lanjuti!!!", null, opts);
 			<?php } ?>
 	<?php endforeach ?>

 	//toast untuk notif revisi rps
	 	<?php foreach ($rows5 as $x): ?>

	 		<?php 
	 			if ($x->status_rps=='REVISI') {?>

	 				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": "toast-bottom-right",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "default",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					};
					
					toastr.success("Anda memiliki revisi RPS.<br> Segera Tindak Lanjuti!!!", null, opts);
	 			<?php } ?>
	 	<?php endforeach ?>

	 	 //toast untuk notif revisi soal
		<?php foreach ($rows6 as $x): ?>

			<?php 
				if ($x->status_soal=='REVISI') {?>

					var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.error("Anda memiliki revisi soal.<br> Segera Tindak Lanjuti!!!", null, opts);
				<?php } ?>
		<?php endforeach ?>
		
		<?php foreach ($rows7 as $x): ?>

			<?php 
				if ($x->status_bahan=='REVISI') {?>

					var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "default",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
				
				toastr.info("Anda memiliki revisi bahan.<br> Segera Tindak Lanjuti!!!", null, opts);
				<?php } ?>
		<?php endforeach ?>                  
</script>


<script type="text/javascript">
	
	viewUsulan();

	function viewUsulan(){
		
		var text = "";
		  $.ajax({
	        url: '<?php echo base_url('koor_mk/view_usulan'); ?>',
	        dataType:'json',
	        type: "post",
	        success: function(data){
	        	var no = 1;
	        	for (var i = 0; i < data.jumlah; i++) {
	        		if (data.results[i].status_usulan == 'APPROVE') {
	        			if (data.results[i].status_usulan == 'WAITING') {
						lihat =  "label label-warning";
						} else if (data.results[i].status_usulan == 'REVISI') {
							lihat = "label label-danger";
						} else if (data.results[i].status_usulan == 'APPROVE') {
							lihat = "label label-secondary";
						}
		        		text += '<tr>\
	        				<td>'+no+'</td>\
	        				<td>'+data.results[i].nama_matakuliah+'</td>\
	        				<td>'+data.results[i].kode_dosen+'</td>\
	        				<td>'+data.results[i].date+'</td>\
	        				<td>'+data.results[i].asessment+'</td>\
	        				<td><label class="'+lihat+'">'+data.results[i].status_usulan+'</td>\
	        				<td>\
								<p>Selesai</p>\
	        				</td>\
	        				</tr>';
		          		$('#usulan').html(text);
		          		no++;
	        		}else{
	        			if (data.results[i].status_usulan == 'WAITING') {
						lihat =  "label label-warning";
						} else if (data.results[i].status_usulan == 'REVISI') {
							lihat = "label label-danger";
						} else if (data.results[i].status_usulan == 'APPROVE') {
							lihat = "label label-secondary";
						}
		        		text += '<tr>\
	        				<td>'+no+'</td>\
	        				<td>'+data.results[i].nama_matakuliah+'</td>\
	        				<td>'+data.results[i].kode_dosen+'</td>\
	        				<td>'+data.results[i].date+'</td>\
	        				<td>'+data.results[i].asessment+'</td>\
	        				<td><label class="'+lihat+'">'+data.results[i].status_usulan+'</td>\
	        				<td>\
								<button onClick="Approval('+data.results[i].id_usulan+','+data.results[i].id_dosen+');" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>\
									<a href="<?php echo base_url('koor_mk/download_usulan/'); ?>'+data.results[i].id_usulan+'" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>\
									<button onClick="Notice('+data.results[i].id_usulan+');" class="btn btn-danger btn-sm"><i class="fa fa-tag icon-left"></i></button>\
	        				</td>\
	        				</tr>';
		          		$('#usulan').html(text);
		          		no++;
	        		}
	        		
	        	}
	           //console.log(data);
	        }
	      });
	}
	setInterval(function(){ viewUsulan(); }, 1000);
</script>	

<script type="text/javascript">
	function Approval(id,iddosen) {
 	alertify.confirm("Confirmation Message","Yakin Approval Usulan ?",
    function(input) {
      if (input) {
        alertify.success('Approval Usulan');
        window.location.href = "<?= base_url() ?>koor_mk/updateStatusUsulan/"+id+"/"+iddosen;
      } else {
        alertify.error('Cancel');
      }
   	}, function(){  alertify.error('Cancel !');});
 	}

 	function Notice(id){
 		alertify.prompt("Confirmation Message","Beri Tanggapan Revisi!!!", "Mohon Diperbaiki",
    	function(evt,value) {
    	var input = decodeURI(value);
        alertify.success('Status : Revisi');
        $.ajax({
		  type: "POST",
		  url: "<?= base_url() ?>koor_mk/revisi_usulan/",
		  data: {identitas:id,keterangan:input},
		  success:function(data){
		  	window.location.href="<?php echo base_url('koor_mk/lihat_usulan'); ?>"
		  }
		});
    }, function(){  alertify.error('Cancel !');});
 	}	
</script>