<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>">
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>"></script>

</head>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koormk');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Key Performance Indicator</h1>
					<p class="description">Kelola Key Performance Indicator (KPI)</p>
				</div>
				
				<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>

					<li class="active">
					<a href="forms-native.html">Indikator</a>
					</li>
					</ol>
				</div>
					
			</div>
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">List KPI</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					<table class="table table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Indikator</th>
								<th width="8%">Status</th>
								<th width="17%">Action</th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        $no = 1;
					        foreach ($rows as $row) {
					        ?>
							<tr>
								<td><?php echo $no++;?>	</td>
								<td><?php echo $row->indikator;?>	</td>
								<?php
								if($row->status == 'ON'){ ?>
								 <td bgcolor="#00b19d" align="center" style="color:white; font-weight: bold;"> <?php echo $row->status; ?> </td>
								 <?php } else { ?>
								 <td bgcolor="#a00a0a" align="center" style="color:white; font-weight: bold;"><?php echo $row->status; ?> </td>
								<?php } ?>
								 </td>
								
								<td>
									<a href="<?= base_url().'koor_mk/edit_kpiusulan/'.$row->id_kpi; ?>"  style="margin-left:5px;" class="btn btn-secondary btn-sm fa-edit icon-left"> </a>
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kpi; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-info btn-sm fa-search icon-left"> </a>
									
								</td>
							</tr>
							<?php } ?>
						</tbody>	
							
						
					</table>
					
				</div>
			</div>
	
			<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 0,filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2},
							
						]);
					});
			</script>
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>



<div class="page-loading-overlay">
<div class="loader-2"></div>
</div>

</body>
</html>
<!-- kpi jumlah bimbingan -->

		<div class="modal fade custom-width" id="modal-10">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows10 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros10 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai_usulan ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->tgl_deadlineusulan,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="modal fade custom-width" id="modal-11">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows11 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros11 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai_assessment ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->tgl_deadlinenilai,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>