<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koormk');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					 <?php $this->load->view('fragment/notif_koormk'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Edit KPI</h1>
					<p class="description">Form Edit Data Key Performance Indicator</p>
				</div>
				
					<div class="breadcrumb-env ">
						<ol class="breadcrumb bc-1 ">
						<li>
							<a href="#"><i class="fa-home"></i>Home</a>
						</li>
						<li >
							<a href="<?php echo base_url().'koormk/ListIndikator';?>">KPI</a>
						</li>
						<li class="active">	
							<a href="#">Edit KPI</a>
						</li>
				</div>
					
			</div>
	
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data KPI</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

					<div class="panel-body">
					<?php 
					        
					 foreach ($kpi as $row) {
					 ?>	

					 	<input type="hidden" name="id" value="<?php echo $row->id_kpi ?>">
						<div class="form-group">
									<label class="control-label"><b>Judul Indikator KPI </b></label>
								</div>
								
								<div class="form-group">
									
									<input type="text" disabled="" class="form-control" name="indikator" size="50" value="<?php echo $row->indikator; ?>">
								</div>
								
					<?php } ?>
					</div>
			</div>

			<div class="row">
			<?php  foreach ($kpi as $rows) { ?>
			<?php if ($rows->id_kpi == '10'){ ?>
				<a href="javascript:;" onclick="jQuery('#add-penilaian').modal('show', {backdrop: 'fade'});" class="btn btn-red btn-icon btn-icon-standalone" ><i class="fa-pencil"></i><span>Tambah Usulan</span>
				</a>
			<?php }else{ ?>
				<a href="javascript:;" onclick="jQuery('#add-nilai').modal('show', {backdrop: 'fade'});" class="btn btn-red btn-icon btn-icon-standalone" ><i class="fa-pencil"></i><span>Tambah Nilai</span></a>
			<?php } ?>
			<?php } ?>
			<div class="clearfix"></div>
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Penilaian</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
					
				<div class="panel-body">
			<?php  foreach ($kpi as $rows) {?>
			<!-- kpi jumlah kehadiran rapat prodi & mk -->


				<!-- kpi  usulan -->
				<?php if($rows->id_kpi == '10'){ ?>
				 <table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Matakuliah</th>
								<th>Deadline</th>
								<th>Nilai</th>		
								<th>Jenis Nilai</th>				
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->nama_matakuliah; ?></td>
								<td><?php echo $row->tgl_deadlineusulan; ?></td>
								<td><?php echo $row->nilai_usulan; ?></td>
								<td><?php if ($row->jenis_nilai == 'asessment1') {
									echo "Asessment 1";
								}elseif($row->jenis_nilai == 'asessment2'){
									echo "Asessment 2";
								}else{
									echo "Asessment 3";
								}?>
								</td>
							<td class="center">
								<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_deadlineusulan; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary btn-sm el-arrows-cw icon-left"> </a>
							</td>
							</tr>
						<?php }	//end foreach rps ?>
						</tbody>	
					</table>
				<?php }else{	//end if kpi usulan ?>
					<table class="table table-bordered">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Matakuliah</th>
								<th>Deadline</th>
								<th>Nilai</th>	
								<th>Jenis Nilai</th>					
								<th> Action </th>
							</tr>
						</thead>
						 
						<tbody>
						<?php 
						$id = 1;
						foreach ($penilaian2 as $row) {   ?>
							<tr>

								<td><?php echo $id++ ?>	</td>
								<td><?php echo $row->nama_matakuliah; ?></td>
								<td><?php echo $row->tgl_deadlinenilai; ?></td>
								<td><?php echo $row->nilai_assessment; ?></td>
								<td><?php if ($row->jenis_nilai == 'asessment1') {
									echo "Asessment 1";
								}elseif($row->jenis_nilai == 'asessment2'){
									echo "Asessment 2";
								}else{
									echo "Asessment 3";
								}?>
								</td>
							<td class="center">
								<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_deadlinenilai; ?>').modal('show', {backdrop: 'fade'});" " class="btn btn-secondary btn-sm el-arrows-cw icon-left"> </a>
							</td>
							</tr>
						<?php }	//end foreach rps ?>
						</tbody>	
					</table>
				<?php }	//end kpi nilai ?>
				<?php }	//end foreach kpi ?>
				</div>
			</div>
			</div>
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>

	

	<?php
	$this->load->view("fragment/foot");
	?>


<?php foreach($kpi as $row){ //start loop?>
<?php if($row->id_kpi == '10'){ ?>
<?php foreach($penilaian as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_deadlineusulan; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_mk/edit_nilai'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_deadlineusulan; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline </label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php //echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									 <input name="tgl_deadlineusulan"  class="form control input-append date form_datetime datetimepickerrrrrr"   type="date"  value="<?php echo $rows->tgl_deadlineusulan ?>" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Nilai Kinerja</label>
									<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai_usulan ?>" class="form-control" id="field-3"  disabled>
							</div>
						</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Save Changes</button>

				</div>

		</form>
    </div>
  </div>
</div>


<?php } } ?>

<?php if($row->id_kpi == '11'){ ?>
<?php foreach($penilaian2 as $rows) { ?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="modal-<?php echo $rows->id_deadlinenilai; ?>">
  <div class="modal-dialog" role="document" style="width: 55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit KPI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_mk/edit_nilai2'; ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $rows->id_deadlinenilai; ?>" name="id">
										<input type="hidden" value="<?php echo $row->id_kpi; ?>" name="id2">
									<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $row->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline </label>
									
									<div class="col-sm-8"">

									<!-- <input name="kondisi" value="<?php //echo $rows->kondisi ?>" class="form-control" id="field-3" > -->
									  <input name="tgl_deadlinenilai"  class="form control input-append date form_datetime datetimepickerrrrrr"   type="date"  value="<?php echo $rows->tgl_deadlinenilai ?>" readonly  data-date-format="yyyy-mm-dd">
									</div>
								</div>
								<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">Nilai Kinerja</label>
								<div class="col-sm-8"">
									<input name="nilai" type="number" min='0' max='100' value="<?php echo $rows->nilai_assessment ?>" class="form-control" id="field-3" readonly>
								</div>
								</div>
				 		    </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Save Changes</button>

				</div>

		</form>
    </div>
  </div>
</div>


<?php } } ?>

<?php } //end foreach kpi?>

<!-- end modal edit -->
<!-- modal usulan -->
<?php foreach($kpi as $r){ //start loop?>
<div class="modal fade custom-width" tabindex="-1" role="dialog" id="add-penilaian">
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Usulan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url('koor_mk/tambah_deadline_usulan'); ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $r->id_kpi; ?>" name="id_kpi">
										<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $r->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline </label>
									<div class="col-sm-8">
										<input name="tgl_deadlineusulan" required="required" class="form control input-append date form_datetime datetimepickerrrrrr"  type="text" data-date-format="yyyy-mm-dd" readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Nilai Kinerja</label>
									<div class="col-sm-8">
									<input name="nilai_usulan" type="number" min='0' max='100' value="100" class="form-control" required="required" readonly="">
									</div>
								</div>
								<div class="form-group">
	   							<label class="col-sm-3 control-label" for="field-1">Pilih Asessment</label>
	   							<div class="col-sm-8">
	   							<select name="jenis_nilai" class="form-control" required>
	   								<option value="">--Pilih Asessment--</option>
	   								<option value="asessment1">Asessment 1</option>
	   								<option value="asessment2">Asessment 2</option>
	   								<option value="asessment3">Asessment 3</option>
	   							</select>
	   							</div>
   								</div>
								<div class="form-group">
	   							<label class="col-sm-3 control-label" for="field-1">Pilih Matakuliah</label>
	   							<div class="col-sm-8">
	   							<select name="id_matakuliah" id="id_matakuliah" class="form-control" required>
	   								<option value="">--Pilih Matakuliah--</option>
	   								<?php foreach ($matkul as $row): ?>
	   								<option value="<?php echo $row->id_matakuliah; ?>"><?php echo $row->nama_matakuliah; ?></option>
	   								<?php endforeach ?> 
	   							</select>
	   							</div>
   								</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Add Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>

<div class="modal fade custom-width" tabindex="-1" role="dialog" id="add-nilai">
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Nilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url('koor_mk/tambah_deadline_nilai'); ?>">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Indikator KPI</label>
									<div class="col-sm-8">
										<input type="hidden" value="<?php echo $r->id_kpi; ?>" name="id_kpi">
										<input name="indikator" type="text" class="form-control" id="field-1" value="<?php echo $r->indikator; ?>" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Deadline</label>
									<div class="col-sm-8">
										<input name="tgl_deadlinenilai" required="required" class="form control input-append date form_datetime datetimepickerrrrrr"  type="text"   data-date-format="yyyy-mm-dd" readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="field-1">Nilai Kinerja</label>
									<div class="col-sm-8">
									<input name="nilai_assessment" type="number" min='0' max='100' value="100" class="form-control" required="required" readonly="">
									</div>
								</div>
								<div class="form-group">
	   							<label class="col-sm-3 control-label" for="field-1">Pilih Asessment</label>
	   							<div class="col-sm-8">
	   							<select name="jenis_nilai" class="form-control" required>
	   								<option value="">--Pilih Asessment--</option>
	   								<option value="asessment1">Asessment 1</option>
	   								<option value="asessment2">Asessment 2</option>
	   								<option value="asessment3">Asessment 3</option>
	   							</select>
	   							</div>
   								</div>
								<div class="form-group">
	   							<label class="col-sm-3 control-label" for="field-1">Pilih Matakuliah</label>
	   							<div class="col-sm-8">
	   							<select name="id_matakuliah" id="id_matakuliah" class="form-control" required>
	   								<option value="">--Pilih Matakuliah--</option>
	   								<?php foreach ($matkul as $row): ?>
	   								<option value="<?php echo $row->id_matakuliah; ?>"><?php echo $row->nama_matakuliah; ?></option>
	   								<?php endforeach ?> 
	   							</select>
	   							</div>
   								</div>
				     </div>

			      	<div class="modal-footer">
			        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Add Kriteria</button>

				</div>

		</form>
    </div>
  </div>
</div>
<?php } ?>
<!-- modal nilai-->

<script type="text/javascript">
		$(document).ready(function(){
		var date = new Date();
		date.setDate(date.getDate());

		$('#datetimepicker').datetimepicker({
			weekStart  : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 1,
			forceParse : 0
		});


		$('#datetimepicker3').datetimepicker({
			weekStart  : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});

		$('.datetimepickerrrrrr').datetimepicker({
			weekStart  : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});

		$('#datetimepicker').datetimepicker('setStartDate', date);
		$('#datetimepicker2').datetimepicker('setStartDate', date);
		$('#datetimepicker3').datetimepicker('setStartDate', date);
		$('.datetimepickerrrrrr').datetimepicker('setStartDate', date);
		
	});
</script>







<?php if($this->session->flashdata('msg') == 'ok') { ?>	
<script type="text/javascript">
	 alertify.success('Success!');
</script>
<?php } ?>
<?php if($this->session->flashdata('msg') == 'not ok') { ?>	
<script type="text/javascript">
	alertify.alert('Error', 'Data Nilai/Kondisi Tidak Boleh Sama');
	id = "<?php echo $id ?>";
	console.log('dildildil');
	//window.history.pushState('data to be passed', 'Edit KPI', '<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>');
	//window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/"+id;
</script>
<?php  } ?>
<?php if($this->session->flashdata('msg') == 'not ok nilai') { ?>	
<script type="text/javascript">
	alertify.alert('Error', 'Data Nilai Tidak Boleh Sama');
	id = "<?php echo $id ?>";
	console.log('asdasdasdas');
	//window.history.pushState('data to be passed', 'Edit KPI', '<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>');
	//window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/"+id;
</script>
<?php  } ?>
<?php if($this->session->flashdata('msg') == 'not ok deadline') { ?>	
<script type="text/javascript">
	alertify.alert('Error', 'Tanggal Deadline Tidak Boleh Sama');
	id = "<?php echo $id ?>";
	console.log(id);
	//window.history.pushState('data to be passed', 'Edit KPI', '<?php echo base_url();?>kaprodi/edit_kpi/<?php echo $id ?>');
	//window.location.href = "<?php echo base_url();?>kaprodi/edit_kpi/"+id;
</script>
<?php  } ?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
