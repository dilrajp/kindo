
			
<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_admin");
?>

<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Dosen D3 Manajemen Informatika</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a href="#" data-toggle="remove">
							&times;
						</a>
					</div>
				</div>
				<div class="panel-body">
					
					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 6},
							{column_number : 1, filter_type: 'text'},
							
						]);
					});
					</script>
					
					<table class="table table-striped table-bordered" id="example-3">
						<thead>
							<tr>
								<th style="width: 70px;">No.</th>
								<th style="width: 100px;">Kode Dosen</th>
								<th style="width: 130px;">NIP</th>
								<th style="width: 150px;">Nama Dosen</th>
								<th style="width: 150px;">Email Dosen</th>
								<th style="width: 125px;">No Telepon</th>
								<th style="width: 80px;">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php if($rows2){$no=1; foreach($rows2 as $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo htmlspecialchars(strtoupper($row->kode_dosen)); ?></td>
								<td><?php echo $row->nip; ?></td>
								<td><?php echo htmlspecialchars(ucwords($row->nama)); ?></td>
								<td><?php echo $row->email_dosen; ?></td>
								<td><?php echo $row->no_telp; ?></td>
								<td><?php echo $row->status; ?></td>
							</tr>
							<?php  $no++; } } ?>
						</tbody>
					</table>
					
				</div>
			</div>
						
			<!-- Batas Body Page-->
			
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2014 
						<strong>Xenon</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>
	



	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>