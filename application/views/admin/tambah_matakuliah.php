<?php 
$this->load->view("fragment/head");
?>
										
<?php
$this->load->view('fragment/sidebar_admin');
?>	
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					
						</ul>
					</li>
					
				</ul>
			

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

			<!-- Body Page-->
			<div class="panel panel-default"> 
   				<div class="panel-heading">
   				 <div class="panel-title"><h3>Tambah Matakuliah</h3></div> 
   				</div> 
   				<div class="panel-body"> 
   					<form action="<?php echo base_url('admin/tambah_aksi'); ?>" role="form" id="form1" method="post" class="validate" novalidate="novalidate">
   						<div class="form-group"> 
   							<label class="control-label">Kode Mata Kuliah</label>
   							<input type="text" class="form-control" name="kode_matakuliah" data-validate="required" data-message-required="required" placeholder="Kode Mata Kuliah Field">
   						</div> 
   						<div class="form-group"> 
   							<label class="control-label">Nama Mata Kuliah</label> 
   							<input type="text" class="form-control" name="nama_matakuliah" data-validate="required" data-message-required="required"  placeholder="Nama Mata Kuliah Field"> 
   						</div>
   						<div class="form-group"> 
   							<label class="control-label">SKS</label>
   							<input type="text" class="form-control" name="sks_matakuliah" data-message-required="required" data-validate="required,number,maxlength[4]" placeholder="SKS Field"> 
   						</div> 
   						<div class="form-group"> 
   							<label class="control-label">Status Mata Kuliah</label><br>
   							<input type="radio" name="status_wajib" value="Wajib Prodi"> Wajib Prodi
  							<input type="radio" name="status_wajib" value="Tidak Wajib Prodi">Tidak Wajib Prodi 
   						</div> 
   						<div class="form-group">
   							<label class="control-label">Jenis Mata Kuliah</label><br>
   							<select name="tipe_matakuliah">
   								<option value="pilih">--Pilih Tingkat--</option>
   								<option value="Tingkat 1">Tingkat 1</option>
   								<option value="Tingkat 2">Tingkat 2</option>
   								<option value="Tingkat 3">Tingkat 3</option>
   							</select>
   						</div>
   						<div class="form-group"> 
   							<label class="control-label">Status Aktif</label><br> 
   							<input type="radio" name="status_aktif" value="Ya"> Ya
  							<input type="radio" name="status_aktif" value="Tidak">Tidak
   						</div> 
   						 <div class="form-group"> 
   						 	<button type="submit" value="Tambah" class="btn btn-success">Save</button> 
   						 	<button type="reset" class="btn btn-white">Reset</button>
   						 </div> 
   					</form> 
   				</div>
   			</div>
			<!-- Batas Body Page-->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2014 
						<strong>Xenon</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>


	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>