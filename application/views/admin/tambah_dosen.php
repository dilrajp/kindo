<?php 
$this->load->view("fragment/head");
?>
										
<?php
$this->load->view('fragment/sidebar_admin');
?>	
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					
						</ul>
					</li>
					
				</ul>

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>

		

			<!-- Body Page-->
			<div class="panel panel-default"> 
   				<div class="panel-heading">
   				 <div class="panel-title"><h3>Tambah Dosen</h3></div> 
   				</div> 
   				<div class="panel-body"> 
   					<form enctype="multipart/form-data" action="<?php echo base_url('admin/tambah_dosen_aksi'); ?>" role="form" id="form1" method="post" class="validate" novalidate="novalidate"> 
   						<div class="form-group"> 
   							<label class="control-label">Kode Dosen</label>
   							<input type="text" class="form-control" name="kode_dosen" data-validate="required" data-message-required="required">
   						</div> 
   						<div class="form-group"> 
   							<label class="control-label">NIP</label>
   							<input type="text" class="form-control" name="nip" data-validate="number,required" data-message-required="required">
   						</div> 
   						<div class="form-group"> 
   							<label class="control-label">Nama Dosen</label> 
   							<input type="text" class="form-control" name="nama_dosen" data-validate="required"> 
   						</div>
   						<div class="form-group"> 
   							<label class="control-label">Email Dosen</label>
   							<input type="email" class="form-control" name="email_dosen" data-validate="required" data-message-required="required" > 
   						</div>
   						<div class="form-group"> 
   							<label class="control-label">Password</label>
   							<input type="password" class="form-control" name="password" data-validate="required" data-message-required="required"> 
   						</div>
   						<div class="form-group"> 
   							<label class="control-label">No Telepon</label>
   							<input type="text" class="form-control" name="no_telp" data-validate="number,maxlength[12]" data-message-required="required"> 
   						</div>
   						<div class="form-group"> 
   							<label class="control-label">Status</label><br> 
   							<input type="radio" name="status" value="Ya"> Ya
  							<input type="radio" name="status" value="Tidak">Tidak
   						</div>  
   						<div class="form-group">
   							<label class="control-label">Jabatan</label><br>
   							<select class= "form-control" name="level">
   								<option value="pilih">--Pilih Jabatan--</option>
   								<option value="1">Dosen</option>
   								<option value="2">Koormk</option>
   								<option value="3">Koorpa</option>
   								<option value="4">Kaprodi</option>
   								<option value="5">Admin</option>
   							</select>
   						</div>
   						<div class="form-group">
						<label class="control-label">Foto</label>
						  	<input type="file" class="form-control" name="userfile">
				  		</div>
   						<div class="form-group"> 
   						 	<button type="submit" value="Tambah" class="btn btn-success">Validate</button> 
   						 	<button type="reset" class="btn btn-white">Reset</button>
   						</div> 
   					</form> 
   				</div>
   			</div>
			<!-- Batas Body Page-->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2014 
						<strong>Xenon</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>


	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>