
<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_admin");
?>

<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>

			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Kelola Data Dosen</h3>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-1").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>

					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable().yadcf([
								{column_number : 6},
								{column_number : 6, filter_type: 'text'},
								{column_number : 1, filter_type: 'text'},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
							]);
						});
					</script>
					
					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="width: 50px;">No.</th>
								<th style="width: 130px;">Kode Matakuliah</th>
								<th style="width: 130px;">Nama Matakuliah</th>
								<th style="width: 100px;">SKS</th>
								<th style="width: 150px;">Status Matakuliah</th>
								<th style="width: 125px;">Jenis Matakuliah</th>
								<th style="width: 100px;">Status Aktif</th>
								<th style="width: 130px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody>
							<?php if($rows){$no=1; foreach($rows as $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo htmlspecialchars(strtoupper($row->kode_matakuliah)); ?></td>
								<td><?php echo htmlspecialchars(ucwords($row->nama_matakuliah)); ?></td>
								<td><?php echo $row->sks_matakuliah; ?></td>
								<td><?php echo $row->status_wajib; ?></td>
								<td><?php echo $row->tipe_matakuliah; ?></td>
								<td><?php echo $row->status_aktif; ?></td>
								<td>
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_matakuliah; ?>').modal('show', {backdrop: 'fade'});" class="btn btn-icon btn-info"><i class="fa-edit"></i></a>
									<a href="<?php echo base_url('admin/hapus_matakuliah/'.$row->id_matakuliah); ?>" onclick="return confirm('Anda yakin akan menghapus data mata kuliah?')" class="btn btn-icon btn-red"><i class="fa-remove"></i></a>
								</td>
							</tr>
							<?php  $no++; } } ?>
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->


<!-- Bottom Scripts -->
<?php
$this->load->view("fragment/foot");
?>