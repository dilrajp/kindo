
<?php 
$this->load->view("fragment/head");
?>
										
<?php
$this->load->view('fragment/sidebar_admin');
?>	
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Kelola Data Dosen</h3>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-1").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>

					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable().yadcf([
								{column_number : 6},
								{column_number : 6, filter_type: 'text'},
								{column_number : 1, filter_type: 'text'},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
								{column_number : 6},
							]);
						});
					</script>
					
					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="width: 50px;">No.</th>
								<th style="width: 100px;">Kode Dosen</th>
								<th style="width: 130px;">NIP</th>
								<th style="width: 150px;">Nama Dosen</th>
								<th style="width: 150px;">Email Dosen</th>
								<th style="width: 125px;">No Telepon</th>
								<th style="width: 80px;">Status</th>
								<th style="width: 130px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody>
							<?php if($rows){$no=1; foreach($rows as $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo htmlspecialchars(strtoupper($row->kode_dosen)); ?></td>
								<td><?php echo $row->nip; ?></td>
								<td><?php echo htmlspecialchars(ucwords($row->nama)); ?></td>
								<td><?php echo $row->email_dosen; ?></td>
								<td><?php echo $row->no_telp; ?></td>
								<td><?php echo $row->status; ?></td>
								<td>
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_dosen; ?>').modal('show', {backdrop: 'fade'});" class="btn btn-icon btn-info"><i class="fa-edit"></i></a>
									<a href="<?php echo base_url('admin/hapus_dosen/'.$row->id_dosen); ?>" onclick="return confirm('Anda yakin akan menghapus data dosen?')" class="btn btn-icon btn-red"><i class="fa-remove"></i></a>
								</td>
							</tr>
							<?php  $no++; } } ?>
						</tbody>
					</table>
				

				</div>
			</div>

			<!-- Batas Body Page-->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2014 
						<strong>Xenon</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>

		</div>
		
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>

	<?php if($rows){ foreach($rows as $row){ ?>
		<div class="modal fade" id="modal-<?php echo $row->id_dosen; ?>">
			<div class="modal-dialog">
				<div class="modal-content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo base_url('admin/update_dosen'); ?>">

						<input type="hidden" name="id_dosen" value="<?php echo $row->id_dosen; ?>">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Edit Data Dosen</h4>
						</div>
						
						<div class="modal-body">
	
							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">Kode Dosen</label>
								
								<div class="col-sm-8">
									<input type="text" name="kode_dosen" class="form-control" id="field-2" placeholder="Ketikkan Kode Dosen" required minlength="3" maxlength="10" value="<?php echo $row->kode_dosen; ?>">
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">NIP</label>
								
								<div class="col-sm-8">
									<input type="text" name="nip" class="form-control" id="field-2" placeholder="NIP" required minlength="4" maxlength="10" value="<?php echo $row->nip; ?>">
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-3">Nama Dosen</label>
								
								<div class="col-sm-8">
									<input type="text" name="nama_dosen" class="form-control" id="field-3" placeholder="Ketikkan Nama Dosen" required maxlength="150" value="<?php echo $row->nama; ?>">
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-4">Email Dosen</label>
								
								<div class="col-sm-8">
									<input type="email" name="email_dosen" class="form-control" id="field-4" placeholder="Ketikan Email" required  value="<?php echo $row->email_dosen; ?>">
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-4">No Telepon</label>
								
								<div class="col-sm-8">
									<input type="number" name="no_telp" class="form-control" id="field-4" placeholder="Ketikan No Telepon" required minlength="10" maxlength="12" value="<?php echo $row->no_telp; ?>">
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-5">Status</label>
								<div class="col-sm-8">
									<select class="form-control" name="status" required>
										<option value="">-Pilih Status-</option>
										<option value="Ya" <?php if($row->status == 'Ya'){ echo 'selected'; } ?>>Ya</option>
										<option value="Tidak" <?php if($row->status == 'Tidak'){ echo 'selected'; } ?>>Tidak</option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-secondary">Save changes</button>
						</div>

					</form>

				</div>
			</div>
		</div>
	<?php } } ?>

	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>