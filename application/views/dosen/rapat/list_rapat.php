<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>" defer> 
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>" defer>
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>" defer></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" defer></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>

</head>
	<script type="text/javascript">
	function ChangeHadir(id) {
 	alertify.confirm("Konfirmasi","Merubah status ke Hadir ?",
    function(input) {
      if (input) {
        alertify.success('Data berhasil dirubah');
        window.location.href = "<?= base_url() ?>dosen/rapat_hadir/"+id;
      } else {
        alertify.error('Cancel');
      }
   	}, function(){  alertify.error('Dibatalkan !');});
 	}	

    function ChangeAlpha(id) {
    alertify.confirm("Konfirmasi","Merubah status ke Alpha ?",
    function(input) {
      if (input) {
        alertify.success('Data berhasil dirubah');
        window.location.href = "<?= base_url() ?>dosen/rapat_alpha/"+id;
      } else {
        alertify.error('Cancel');
      }
    }, function(){  alertify.error('Dibatalkan !');});
 	
	}

	function ChangeIzin(id) {
    alertify.prompt("Pesan Izin","Merubah status ke Izin ?", "Informasi Izin",
    function(evt,value) {
    	var input = decodeURI(value);
    	console.log (decodeURI(value));
        alertify.success('Status: Izin');
        window.location.href = "<?= base_url() ?>dosen/rapat_izin/"+id+"/"+input;
      
    }, function(){  alertify.error('Dibatalkan !');});
 	
	}

	function ConfirmYes()
    {
      var x = confirm('Konfirmasi','Mengubah status ke HADIR ?');
      if (x)
          return true;
      else
        return false;
    }	
	function ConfirmDelete()
    {
      var x = confirm('Konfirmasi','Mengubah status ke ALPHA ?');
      if (x)
          return true;
      else
        return false;
    }
</script>  
<body class="page-body">
	
						
				<?php
				$this->load->view('fragment/sidebar_dosen');
				
				?>
			
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Agenda Program Studi <?= nama_tahun(gettahun_aktif()) ?></h1>
					<p class="description">Pengelolaan Undangan Rapat Dosen</p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1">
									<li>
							<a href="dosen"><i class="fa-home"></i>Home</a>
						</li>
								<li class="active">
						
										<a href="forms-native.html">Rapat</a>
								</li>
						
								
				</div>
					
			</div>
		
		

	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Undangan Rapat</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
				<style type="text/css">
				.table-fit {
				  width: 1%;
				  white-space: nowrap;
				}
				.table td.fit, 
				.table th.fit {
			    white-space: nowrap;
			    width: 1%;
					}
				</style>
					<div class="table-responsive">

					<table class="table table-bordered  table-responsive" id="example-3">
						<thead>
							<tr class="replace-inputs">
								
								<th>Rapat&nbsp;</th>
								<th >Mata Kuliah &nbsp;</th>
								<th >Agenda&nbsp;</th>
								<th >Ruangan</th>
								<th >Tanggal</th>
								<th >Jam</th>
								<th >Status&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th > Aksi </th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        
					        foreach ($rows as $row) {
					        ?>
							<tr>
								
								<td style="min-width:12%"><?php echo namarapat($row->tipe); ?>	</td>
								<td style="min-width:10%"><?php echo nama_matkul($row->id_matakuliah); ?></td>
								<td style="min-width:13%"><?php echo $row->agenda; ?></td>
								<td class="center"  style="min-width:10%"><?php echo $row->ruangan; ?></td>
								<td class="center" style="min-width:10%"><?php 
									echo tanggal_indo($row->tanggal_rapat, true);// date('l, d F Y', strtotime($row->tanggal_rapat)); ?></td>
								<td style="min-width:10%"><?php 
									echo date('H:i:s', strtotime($row->tanggal_rapat)); ?>
										<?php $today = date("Y-m-d H:i:s");
$date = $row->tanggal_rapat; ?>

									</td>
								<?php 
								if($row->status == "TERKIRIM") {
								  echo "<td class='center' bgcolor='#ddd' style='min-width:20%'><font color='#d5080f' >";
								 } else if($row->status == "ALPHA") {
								 	  echo "<td class='center' bgcolor='#d5080f' style='min-width:20%'><font color='#fff'>";
								 } else {
								  echo  "<td class='center' style='min-width:20%'>";
								 } ?>
								 <?php if($row->keterangan == NULL || $row->keterangan == ' ') { ?>
								<?php echo $row->status; ?></td>
								 	<?php }else {?>
								<?php echo $row->status.'<br> ('. $row->keterangan.')'; ?></td>
								<?php } ?>
								<?php if ($date > $today) {  ?>
								<td class="center" >
								<div class="col-md-12 "> 
								 <div class="row">
   									<div class="col-md-4">
									<button onClick="ChangeHadir(<?= $row->id_undangan; ?>, <?= $row->id_kehadiran; ?> );" class=" btn btn-secondary  fa-check" data-toggle="tooltip" title="HADIR"> </button>
									   </div><div class="col-md-4">
									<button onClick="ChangeIzin(<?= $row->id_undangan; ?>);" class="btn btn-warning  fa-tag" data-toggle="tooltip" title="IZIN" > </button>
									 </div><div class="col-md-4">
									<button onClick="ChangeAlpha(<?= $row->id_undangan; ?>, <?= $row->id_kehadiran; ?> );" class="btn btn-danger  fa-times icon-left"  data-toggle="tooltip" title="SAKIT"> </button>
									 </div>
									 </div>
									</div>
								</td>
							<?php } else {?>
							<td >	<div class="col-md-12 "><b>Kegiatan Sudah Selesai</b></font>
							<?php } ?>
							<?php } ?>
							</tr>
							
						</tbody>	
							
						
					</table>
					</div>
				</div>
			</div>

			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
	
		
		
	</div>

	<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 0},
							{column_number : 1},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							{column_number : 4, filter_type: 'text'},
							{column_number : 5, filter_type: 'text'},
							{column_number : 6}
							
						]);
					});
					</script>

	<!-- Bottom Scripts -->
<div class="page-loading-overlay">
				<div class="loader-2"></div>
			</div>
</body>
</html>
