<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	

	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->



	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>">
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>

<style>
/* unvisited link */
a:link.asd{
    color: #135584;
    font-weight: bold;
}

/* visited link */
a:visited.asd {
    color: green;
}

/* mouse over link */
a:hover.asd{
    color: #00b19d;
}

/* selected link */
a:active.asd{
    color: blue;
}
</style>

</head>

<body class="page-body">
<?php
$this->load->view('fragment/sidebar_dosen');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Indikator Penilaian Kinerja Pengajaran</h1>
					<p class="description">Key Performance Indicator (KPI)</p>
				</div>
				
				<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Indikator</a>
					</li>
					</ol>
				</div>		

			</div>
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">List KPI</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-1").dataTable({
							aLengthMenu: [
								[25, 50, 100, -1], [ 25, 50, 100, "All"]
							]
						});
					});
					</script>
		
				<div class="panel-body">
					<table class="table table-bordered" id="example-1">
						<thead>
							<tr class="replace-inputs">
								
								<th>No</th>
								<th>Indikator Penilaian Bidang Pengajaran</th>
								<th width="8%">Status</th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        $no = 1;
					        foreach ($rows as $row) {
					        ?>
							<tr>
								<td><?php echo $no++;?>	</td>
								<td>
								<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_kpi; ?>').modal('show', {backdrop: 'fade'});" " class="asd"><?php echo $row->indikator;?></a>

								</td>
								<?php
								if($row->status == 'ON'){ ?>
								 <td bgcolor="#00b19d" align="center" style="color:white; font-weight: bold;"> <?php echo $row->status; ?> </td>
								 <?php } else { ?>
								 <td bgcolor="#a00a0a" align="center" style="color:white; font-weight: bold;"><?php echo $row->status; ?> </td>
								<?php } ?>
								 </td>
							</tr>
							<?php } ?>
						</tbody>	
							
						
					</table>
					
				</div>
			</div>
	
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>


<div class="page-loading-overlay">
<div class="loader-2"></div>
</div>

</body>
</html>
<!-- kpi jumlah bimbingan -->
	<div class="modal fade custom-width" id="modal-1">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows1 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros1 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Kondisi" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="> <?= $row->kondisi ?> Mahasiswa"  
									<input type="text" class="form-control tooltip-blue" placeholder="> <?= $row->kondisi ?> Kali"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Jumlah Bimbingan  " readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- kpi kehadiran rapat & undangan prodi -->
	<div class="modal fade custom-width" id="modal-2">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows2 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<div class="col-xs-12">	
					Sesuai Prosentase Kehadiran (Keterlambatan lebih dari 30 Menit tanpa Konfirmasi  dianggap tidak hadir).<br>
					Nilai = 0-100
				</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- kpi kehadiran rapat koordinasi mk dosen -->
	<div class="modal fade custom-width" id="modal-3">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows3 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<div class="col-xs-12">	
					Sesuai Prosentase Kehadiran (Keterlambatan lebih dari 30 Menit tanpa Konfirmasi  dianggap tidak hadir).<br>
					Nilai = 0-100
				</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- kpi kontribusi kelulusan -->
	<div class="modal fade custom-width" id="modal-5">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows5 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros5 as $row){ 		 ?>
							<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Kondisi " />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder=">=  <?= $row->kondisi ?> %"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Prosentase Kontribusi Kelulusan " readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- kpi koordinasi mk koordinator dosen-->
	<div class="modal fade custom-width" id="modal-4">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows4 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros4 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Kondisi" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="> <?= $row->kondisi ?> Kali"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Melaksanakan Rapat Koordinasi MK (Minimal) " readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

<!-- INDIKATOR JML MENGUJI  -->
	<div class="modal fade custom-width" id="modal-12">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows12 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros12 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Kondisi" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="> <?= $row->kondisi ?> Kali"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Jumlah Menguji DE" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

		<div class="modal fade custom-width" id="modal-13">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows13 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros13 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Kondisi" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="> <?= $row->kondisi ?> Kali"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Jumlah Menguji Sidang" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>


<div class="modal fade custom-width" id="modal-14">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows14 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros14 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Kondisi" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder=">= <?= $row->kondisi ?> Kali"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Melaksanan Perwalian (Tatap Muka) Minimal <?= $row->kondisi ?> Kali Per Semester" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>


<div class="modal fade custom-width" id="modal-15">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows15 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros15 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Melaporkan Bukti LKS" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>


<div class="modal fade custom-width" id="modal-6">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows6 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros6 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="modal fade custom-width" id="modal-7">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows7 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros7 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="modal fade custom-width" id="modal-8">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows8 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros8 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

		<div class="modal fade custom-width" id="modal-9">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows9 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros9 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>


		<div class="modal fade custom-width" id="modal-10">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows10 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros10 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>


		<div class="modal fade custom-width" id="modal-11">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows11 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<?php 		foreach($ros11 as $row){ 		 ?>
				<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control" placeholder="Nilai" />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control" placeholder="<?= $row->nilai ?>"  readonly/>
								</div>
									
								<div class="col-xs-3">
									<input type="text" style="border-color:white;font-weight:bold;" class="form-control " placeholder="Deadline Tanggal"  />
								</div>
									
								<div class="col-xs-3">
									<input type="text" class="form-control tooltip-blue" placeholder="<?= tanggal_indo($row->kondisi,true) ?>"  data-toggle="tooltip" data-placement="top" title="" data-original-title="" readonly/>
								</div>
								<br>
				<?php }	 ?>	
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

		<!-- kpi edom-->
	<div class="modal fade custom-width" id="modal-16">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows16 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<div class="col-xs-12">	
					Sesuai Presentase Nilai EDOM.<br>
					Nilai = 0-100 %
				</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>

		<!-- kpi edom-->
	<div class="modal fade custom-width" id="modal-17">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
			<?php 
			foreach($rows17 as $row) {
			?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Informasi KPI</h4>
				</div>
				<form role="form" class="form-horizontal">				
				<div class="modal-body">
				<div class="form-group">
				<label class="col-sm-2 control-label">Indikator</label>
				<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="linecons-key"></i>
							</span>
							<input type="email" class="form-control" placeholder="<?= $row->indikator ?>" readonly>
						</div>
					</div>
				</div>
			
				<div class="form-group-separator"></div>
				<div class="form-group-separator"></div>			
				<div class="row col-margin">
								<div class="col-xs-12">	
									<b>Penilaian</b>
								</div>
				<div class="col-xs-12">	
					Sesuai Presentase Nilai BAP.<br>
					Nilai = 0-100 %
				</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				</div>
				</form>	
				<?php } ?>
			</div>
		</div>
	</div>