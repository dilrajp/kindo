<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>

	
	<script>
		var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
	</script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/knockout-3.1.0.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo base_url('assets/js/chart.js')?>"></script> 

	<script src="<?php echo base_url('assets/js/xenon-widgets.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/regions/jquery-jvectormap-world-mill-en.js') ?>"></script>
</head>
<body class="page-body">
<body class="page-body">
		<?php 
	$iddos=  $this->session->id_dosen;
	$tahun = gettahun_aktif();
	$jml_de = $this->db->query("
		SELECT * FROM `t_jadwal_de`  where (t_jadwal_de.puj1 = '$iddos' or t_jadwal_de.puj2 = '$iddos') and tahun = '$tahun' ORDER BY `t_jadwal_de`.`id_jadwal_de`  DESC LIMIT 2")->result();
	$jml_sidang = $this->db->query(" 
		SELECT * FROM `t_jadwal_sidang`  where (t_jadwal_sidang.puj1 = '$iddos' or t_jadwal_sidang.puj2 = '$iddos') and tahun = '$tahun' ORDER BY `t_jadwal_sidang`.`id_jadwal_sidang`  DESC  LIMIT 2")->result();
	$i = 1;
	?>
	<?php
	$this->load->view('fragment/sidebar_dosen');
	?>
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
					
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>

					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});

				$("#cpu-usage-gauge").dxCircularGauge({
						scale: {
							startValue: 0,
							endValue: 17,
							majorTick: {
								tickInterval: 6
							}
						},
						rangeContainer: {
							palette: 'pastel',
							width: 3,
							ranges: [
								{ startValue: 0, endValue: 6, color: "#d5080f" },
								{ startValue: 6, endValue: 12, color: "#40bbea" },
								{ startValue: 12, endValue: 17, color: "#68b828" },
							],
						},
						value: <?php echo $this->db->query("select * from t_kpi where status = 'ON'")->num_rows(); ?>,
						valueIndicator: {
							offset: 17,
							color: '#68b828',
							type: 'rectangleNeedle',
							spindleSize: 12
						}
					});
			});

				function between(randNumMin, randNumMax)
				{
					var randInt = Math.floor((Math.random() * ((randNumMax + 1) - randNumMin)) + randNumMin);
					
					return randInt;
				}
			</script>


			
			<!-- Xenon Todo List -->
			<div class="row">
			<div class="col-md-12">
				<div class="chart-item-bg">
						<div class="chart-label">
							<div id="network-mbs-packets" class="h1 text-secondary text-bold" data-count="this" data-from="0.00" data-to="<?php echo $this->db->query("select * from t_kpi where status = 'ON'")->num_rows()?>"  data-duration="4">0.00%</div>
							<span class="text-medium text-muted text-upper">Indikator Penilaian Bidang Pengajaran</span>
							
							<p class="text-medium" style="width: 50%; margin-top: 10px">						
								<a href="<?php echo base_url('dosen/ListIndikator'); ?>" class="btn btn-secondary btn-icon btn-icon-standalone btn-icon-standalone-right">				<i class="fa-search"></i>
									<span>View KPI</span>
								</a>
							</p>
						</div>
						<div id="other-stats" style="min-height: 183px">
							<div id="cpu-usage-gauge" style="width: 370px; height: 140px; position: absolute; right: 20px; top: 20px"></div>
						</div>
					</div>
			</div>
				<div class="col-md-6">
					
					<div class="xe-widget xe-todo-list xe-todo-list-turquoise">
						<div class="xe-header">
							<div class="xe-icon">
								<i class="fa-file-text-o"></i>
							</div>
							<div class="xe-label">
								<span>To Do List</span>
								<strong>Indikator [KPI]	 </strong>
							</div>
						</div>
						<div class="xe-body">
							
							<ul class="list-unstyled">
							<?php 
							foreach($ind->result() as $d){ ?>
							
							<?php if($d->id_kpi == '14' || $d->id_kpi == '15'){
								if(cek_nilai($d->id_kpi) == 'yy'){
							?>
							<?php if($d->id_kpi == '15'){ ?>
							<li >
							<label>
							<input type="checkbox" class="cbr cbr-turquoise" disabled/>
							<span><?php echo $d->indikator.'  <font style="font-weight:bold;color:yellow";
							>[ '.tanggal_indo(tanggalmati($d->id_kpi),true).' ]</font>' ?></span>
							</label>
							</li>
							<?php }else if($d->id_kpi =='14'){?>
							<li >
							<label>
							<input type="checkbox" class="cbr cbr-turquoise" disabled />
							<span><?php echo $d->indikator.'  <font style="font-weight:bold;color:yellow";
							>[<font style="font-weight:bold;color:white";> '.total_perwalian($this->session->id_dosen,getsemester_aktif()).' </font>/ '.total_kpi_perwalian($d->id_kpi).' Kali ] </font>' ?></span>
								</label>
								</li>
							<?php } ?>

							<?php } else { ?>
							
							<?php if($d->id_kpi == '15'){ ?>
							<li  class="done">
							<label >
							<input type="checkbox" class="cbr cbr-turquoise" disabled checked />
							<span><?php echo $d->indikator.'  <font style="font-weight:bold;color:yellow";
							>[ '.tanggal_indo(tanggalmati($d->id_kpi),true).' ]</font>' ?></span>
							</label>
							</li>
							<?php }else if($d->id_kpi =='14'){?>
							<li class="done" >
							<label >
							<input type="checkbox" class="cbr cbr-turquoise" disabled checked/>
							<span><?php echo $d->indikator.'  <font style="font-weight:bold;color:yellow";
							>[<font style="font-weight:bold;color:white";> '.total_perwalian($this->session->id_dosen,getsemester_aktif()).' </font>/ '.total_kpi_perwalian($d->id_kpi).' Kali ] </font>' ?></span>
							</label>
							</li>
										<?php } ?>
							 <?php }} ?>
														
							<?php } ?>	
							</ul>
							
						</div>
						<div class="xe-footer">
						</div>
					</div>
					
				</div>
			<div class="col-sm-6">
					
					<div class="xe-widget xe-counter xe-counter-turquoise">
						<div class="xe-icon">
							<i class="linecons-doc"></i>
						</div>
						<div class="xe-label">
							<strong class="num">Mata Kuliah Aktif</strong>
							<span style="color:#000000b8;font-weight: bold;">
							<?php 
							$iddos = $this->session->id_dosen;
							$sem = getsemester_aktif();
							$daftar_matkul = $this->db->query("SELECT * FROM `t_pengajaran` join t_matakuliah using(id_matakuliah)
								join t_semester using(id_semester) where id_dosen= '$iddos' and t_matakuliah.id_semester = '$sem'");
							foreach($daftar_matkul->result() as $rows){
							echo '<font color="#68b828" face="Verdana">'.$rows->nama_matakuliah.'</font> | ';
							}
							?>	

							</span>
						</div>
						
				</div>
			</div>
				<div class="col-sm-3">
				<div class="xe-widget xe-progress-counter xe-progress-counter-red" data-count=".num" data-from="0" data-to="<?php echo rapat_hadirrrr($this->session->id_dosen,2); ?>" data-duration="3">
						
						<div class="xe-background">
							<i class="linecons-calendar"></i>
						</div>
						
						<div class="xe-upper">
							<div class="xe-icon">
								<i class="linecons-calendar"></i>
							</div>
							<div class="xe-label">
								<span>Rapat Prodi</span>
								<strong class="num">0</strong>
							</div>
						</div>
						
						<div class="xe-progress">
							<span class="xe-progress-fill" data-fill-from="0" data-fill-to="<?php echo total_rapat($this->session->id_dosen,2)*6; ?>" data-fill-unit="%" data-fill-property="width" data-fill-duration="2" data-fill-easing="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo 'Total Rapat '.total_rapat($this->session->id_dosen,2) ?>"></span>
						</div>
						
						<div class="xe-lower">
							<span>Pencapaian Kinerja</span>
							<strong><?php 
							$tot_ra = total_rapat($this->session->id_dosen,2);
							$tot_ada = rapat_hadirrrr($this->session->id_dosen,2);
							if($tot_ada > 0 && $tot_ra > 0) 
							 {echo round(rapat_hadirrrr($this->session->id_dosen,2)/total_rapat($this->session->id_dosen,2)*100,2); }
							else{
								echo '0';
							} ?> % </strong>
						</div>
						
					</div>
					
				
				</div>		
			
				<div class="col-sm-3">
				<div class="xe-widget xe-progress-counter xe-progress-counter-red" data-count=".num" data-from="0" data-to="<?php echo rapat_hadirrrr($this->session->id_dosen,1); ?>" data-duration="3">
						
						<div class="xe-background">
							<i class="linecons-calendar"></i>
						</div>
						
						<div class="xe-upper">
							<div class="xe-icon">
								<i class="linecons-calendar"></i>
							</div>
							<div class="xe-label">
								<span>Rapat MK</span>
								<strong class="num">0</strong>
							</div>
						</div>
						
						<div class="xe-progress">
							<span class="xe-progress-fill" data-fill-from="0" data-fill-to="<?php echo total_rapat($this->session->id_dosen,1)*6; ?>" data-fill-unit="%" data-fill-property="width" data-fill-duration="2" data-fill-easing="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo 'Total Rapat '.total_rapat($this->session->id_dosen,1) ?>"></span>
						</div>
						
						<div class="xe-lower">
							<span>Pencapaian Kinerja</span>
							<strong><?php
							$tot_ra = total_rapat($this->session->id_dosen,1);
							$tot_ada = rapat_hadirrrr($this->session->id_dosen,1);
							if($tot_ada > 0 && $tot_ra > 0) 
							 {echo round(rapat_hadirrrr($this->session->id_dosen,1)/total_rapat($this->session->id_dosen,1)*100,2); }
							else{
								echo '0';
							}

							 ?> % </strong>
						</div>
						
					</div>
					
				
				</div>		

			

			<div class="col-md-6">
					
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="<?php echo total_bimb_all($this->session->id_dosen); ?>" data-suffix=" Mahasiswa" data-duration="2">
						<div class="xe-upper">
							
							<div class="xe-icon">
								<i class="linecons-graduation-cap"></i>
							</div>
							<div class="xe-label">
								<strong class="num">0.0%</strong>
								<span>Tahun <?php echo nama_tahun(gettahun_aktif()); ?></span>
							</div>
							
						</div>
						<div class="xe-lower">
							<div class="border"></div>
							
							<span>Data Proyek Akhir</span>
							<br>
							<div style="font-weight: bold;" >
							<strong>Total Mahasiswa Proyek Akhir <?php echo total_bimb_all($this->session->id_dosen); ?></strong>
							<strong>Tahun <?php echo nama_tahun(gettahun_aktif()).' = '; echo total_bimb_tahun($this->session->id_dosen); ?></strong>
							<strong class="col-sm-3"><?php
							$tot_bim = total_bimb($this->session->id_dosen);
							$tot_tahap = total_bimb_tahap($this->session->id_dosen,3);
							if($tot_bim > 0 && $tot_tahap > 0)
							 {echo round(total_bimb_tahap($this->session->id_dosen,3)/total_bimb($this->session->id_dosen),2)*100;}
							else{
								echo '0';
							}

							  ?> % Input Theta</strong>
							<strong class="col-sm-3"><?php 
							$tot_bim = total_bimb($this->session->id_dosen);
							$tot_tahap = total_bimb_tahap($this->session->id_dosen,6);
							if($tot_bim > 0 && $tot_tahap > 0)
							 {echo round(total_bimb_tahap($this->session->id_dosen,6)/total_bimb($this->session->id_dosen),2)*100;}
							else{
								echo '0';
							} ?> % Sidang</strong>
							<strong class="col-sm-3"><?php 
							$tot_bim = total_bimb($this->session->id_dosen);
							$tot_tahap = total_bimb_tahap($this->session->id_dosen,7);
							if($tot_bim > 0 && $tot_tahap > 0)
							 {echo round(total_bimb_tahap($this->session->id_dosen,7)/total_bimb($this->session->id_dosen),2)*100;}
							else{
								echo '0';
							} ?> % Yudisium</strong>
							</div>
							<br>
						</div>
					</div>
				
			</div>
			<?php
					$tot_bim = total_bimb_all($this->session->id_dosen);
					$tot_tahap1 = total_bimb_tahap($this->session->id_dosen,1);	
					if($tot_bim > 0 && $tot_tahap1 > 0)
						 {
						 $hasil_prade = round(($tot_tahap1/$tot_bim)*100,2);
						 }
						else{
							 $hasil_prade = 0;
						}
			 ?>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-counter xe-counter-green" data-count=".num" data-from="0" data-to="<?php echo $hasil_prade; ?>" data-suffix="%" data-duration="2" data-easing="false">
						<div class="xe-icon">
							<i class="linecons-user"></i>
						</div>
						<div class="xe-label">
							<strong class="num">0.0%</strong>
							<span style="color:#000000b8;font-weight: bold;">Jumlah Pra-DE</span>
						</div>
						
				</div>
				
				</div>		
			<div class="col-sm-3">
				<?php
					$tot_tahap2 = total_bimb_tahap($this->session->id_dosen,2);	
					if($tot_bim > 0 && $tot_tahap2 > 0)
						 {
						 $hasil_de = round(($tot_tahap2/$tot_bim)*100,2);
						 }
						else{
						$hasil_de = 0;
						}
			 	?>					
					<div class="xe-widget xe-counter xe-counter-pink" data-count=".num" data-from="0" data-to="<?php echo $hasil_de; ?>" data-suffix="%" data-duration="2" data-easing="false">
						<div class="xe-icon">
							<i class="linecons-user"></i>
						</div>
						<div class="xe-label">
							<strong class="num">0.0%</strong>
							<span style="color:#000000b8;font-weight: bold;">Jumlah DE</span>
						</div>
						
				</div>
				
				</div>			
			<div class="col-sm-3">
				<?php
					$tot_tahap4 = total_bimb_tahap($this->session->id_dosen,4);	
					if($tot_bim > 0 && $tot_tahap4 > 0)
						 {
						 $hasil_sk = round(($tot_tahap4/$tot_bim)*100,2);
						 }
						else{
						$hasil_sk = 0;
						}
			 		?>
				<div class="xe-widget xe-counter xe-counter-turquoise" data-count=".num" data-from="0" data-to="<?php echo $hasil_sk; ?>" data-suffix="%" data-duration="2" data-easing="false">
						<div class="xe-icon">
							<i class="linecons-user"></i>
						</div>
						<div class="xe-label">
							<strong class="num">0.0%</strong>
							<span style="color:#000000b8;font-weight: bold;">Jumlah Pengajuan SK</span>
						</div>

				</div>
				
				</div>
			

				<?php
					$tot_tahap5 = total_bimb_tahap($this->session->id_dosen,5);	
					if($tot_bim > 0 && $tot_tahap5 > 0)
						 {
						 $hasil_yud = round(($tot_tahap5/$tot_bim)*100,2);
						 }
						else{
							 $hasil_yud = 0;
						}
				 ?>

				<div class="col-sm-3">
					
					<div class="xe-widget xe-counter xe-counter-info" data-count=".num" data-from="0" data-to="<?php echo $hasil_yud; ?>" data-suffix="%" data-duration="2" data-easing="false">
						<div class="xe-icon">
							<i class="linecons-user"></i>
						</div>
						<div class="xe-label">
							<strong class="num">0.0%</strong>
							<span style="color:#000000b8;font-weight: bold;">Jumlah Pra-Sidang</span>
						</div>

				</div>
				
				</div>
			</div>
				<div class="panel panel-color panel-info collapsed"><!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Jadwal Menguji</strong></h3>
							
							<div class="panel-options">
								
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
								
							</div>
						</div>
						
						<div class="panel-body">
						<div class="panel-body panel-border">
						
							<div class="row">
								<div class="col-sm-12">
								
									<!-- Table Model 2 -->
									
								
									<table class="table table-model-2 table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										
										<tbody>
											<?php foreach($jml_de as $val) { ?>
											<tr>
												<td><?php echo $i++; ?></td>
												<td><?php echo 'DE' ?></td>
												<td><?php echo convertmhs($val->id_mhs,'nama_mhs')?></td>
												<td><?php echo $val->tanggal ?></td>
											</tr>
											<?php } ?>

											<?php foreach ($jml_sidang as $val) { ?>
											<tr>
												<td><?php echo $i++; ?></td>
												<td><?php echo 'Sidang' ?></td>
												<td><?php echo convertmhs($val->id_mhs,'nama_mhs')?></td>
												<td><?php echo tanggal_indo($val->tanggal,true) ?></td>
											</tr>
											<?php } ?>
											
										</tbody>
									</table>
								
								</div>
							</div>
						
						</div>
			
						</div>
					</div>
				
						
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			
		</div>
		
			
		<!-- start: Chat Section -->
		
		<!-- end: Chat Section -->
		
	</div>
	


	<!-- Bottom Scripts -->
</body>
</html>