<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	

	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->



	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>">
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>

	

</head>

<body class="page-body">
<?php
$this->load->view('fragment/sidebar_dosen');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Detail Pembimbing</h1>
					<p class="description">Data Bimbingan <?php echo $this->session->nama; ?></p>
				</div>
				
					<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Dosen Pembimbing</a>
					</li>
					</ol>
					</div>
					
			</div>
				


	<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Detail Dosen Pembimbing</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

					<div class="panel-body">
					<form role="form" class="form-horizontal">	
					<?php 
					        
					 foreach ($rows as $row) {
					 ?>	
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Nama Dosen</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo $row->nama; ?>" disabled="">
							</div>
							<label class="col-sm-2 control-label"><b>NIP</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo $row->nip; ?>" disabled="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Total Pembimbing 1</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php 
									echo total_pembimbing1_v2($row->id_dosen);; ?>" disabled="">
							</div>
							<label class="col-sm-2 control-label"><b>Total Pembimbing 2</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo total_pembimbing2_v2($row->id_dosen); ?>" disabled="">
							</div>
						</div>
					
					<?php } ?>
					</form>
					</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">List Mahasiswa Proyek Akhir</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				
				<div class="panel-body">
		
				
				<table class="table table-striped table-bordered" id="mydata">
						<thead>
							<tr class="replace-inputs">
								<th width="13%">NIM</th>
								<th width="15%">Nama</th>
								<th width="15%" >Judul PA</th>
								<th width="12%">Tahun</th>
								<th width="10%">Grup</th>
								<th width="10%">Angkatan</th>
								<th width="10%"> Tahap </th>
								<th width="15%"> Action </th>
							</tr>
						</thead>
						<tbody id="show_data">
							
						</tbody>
				</table>
				
				</div>
			</div> 
	
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>

	





	<?php
	
	if($this->session->flashdata('msg') == 'ok')
	{	?>
		<script type="text/javascript">
		alertify.success('Success!');
	</script>
	<?php } ?>

<!-- Edit -->

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 75%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Mahasiswa Proyek Akhir</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						 <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'dosen/edit_mhs'; ?>">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM</label>
									
									<div class="col-sm-10">
										<input name="nim" type="text" class="form-control" id="nim_mhs" disabled>
										<input type="hidden" name="id" id="id_mhs">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama </label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama_mhs" disabled>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul</label>
									
									<div class="col-sm-10">
										<textarea name="judul" rows="3" class="form-control" id="judul_pa" disabled></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Tahap 
										</label>
									
									<div class="col-sm-10">	
								
										<select class="form-control js-example-basic-single" id="tahap"  required name="tahap">
										<optgroup label="Daftar Tahap Proyek Akhir">
										<option value="1" >Pra-Desk Evaluation</option>
										<option value="2" >Desk Evaluation</option>
										<option value="3" >Input Theta</option>
										<option value="4" >Pengajuan SK</option>
										<option value="5" >Pra-Sidang</option>
										<option value="6" >Sidang</option>
									
										</optgroup>
										</select>
									</div>	
								</div>
						
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info" >Simpan</button>

				</div>
			</form>		
					
    </div>
  </div>
</div>

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaView">
  <div class="modal-dialog" role="document" style="width: 75%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Mahasiswa Proyek Akhir</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
			<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/edit_mhs'; ?>">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim" type="text" class="form-control" id="nim_mhs" disabled>
										<input type="hidden" name="id" id="id_mhs">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama_mhs" disabled>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<textarea name="judul" rows="3" class="form-control" id="judul_pa" disabled></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Grup</label>
									
									<div class="col-sm-10">
										<input name="grup" type="text" class="form-control" id="grup" disabled>
									</div>
								</div>
							
							<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 1</label>
									
									<div class="col-sm-10">	
									<input name="pb1" type="text" class="form-control" id="pb1" disabled>
									</div>	
								</div>
						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 2</label>
									
									<div class="col-sm-10">	
									<input name="pb2" type="text" class="form-control" id="pb2" disabled>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahun</label>
									
									<div class="col-sm-10">	
									<input name="tahun" type="text" class="form-control" id="tahun" disabled>
									</div>	
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Tahap</label>
									
									<div class="col-sm-10">	
								
										<select class="form-control .js-example-disabled-multi" id="tahapp"  disabled name="tahap">
										<optgroup label="Daftar Tahap Proyek Akhir">
										<option value="1" >Pra-Desk Evaluation</option>
										<option value="2" >Desk Evaluation</option>
										<option value="3" >Input Theta</option>
										<option value="4" >Pengajuan SK</option>
										<option value="5" >Pra-Sidang</option>
										<option value="6" >Sidang</option>
										</optgroup>
										</select>
									</div>	
								</div>
						
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				</div>
			</form>		
					
    </div>
  </div>
</div>

	<div class="page-loading-overlay">
	<div class="loader-2"></div>
	</div>
<script type="text/javascript">
								$(document).ready(function() {
							  $(".js-example-disabled-multi");
							  $('.js-example-basic-single').select2();
									});
									</script>

<script type="text/javascript">
$(document).ready(function(){
    tampil_data_barang();   //pemanggilan fungsi tampil barang.
	
	$("#mydata").dataTable().yadcf([
						
							
							{column_number : 0, filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							{column_number : 4, filter_type: 'text' },
							{column_number : 5, },
							{column_number : 6, },
							
						]);
				

    function tampil_data_barang(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo base_url()?>dosen/data_mhs',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                	var tahap = ' ';
                	if(data[i].tahap == 2){
                		tahap = 'DE';
                	}else if(data[i].tahap == 3){
                		tahap = 'Input Theta';
                	}else if(data[i].tahap == 5){
                		tahap = 'Pra-Sidang';
                	}else if(data[i].tahap == 6){
                		tahap = 'Sidang';
                	}else if(data[i].tahap == 7){
                		tahap = 'Yudisium';
                	}else if(data[i].tahap == 1){
                		tahap = 'Pra-DE';
                	}else if(data[i].tahap == 4){
                		tahap = 'Pengajuan SK';
                	};


                	var grup = ' '
                	if(data[i].grup == null){
                		grup = ' ';
                	}else{
                		grup = data[i].grup;
                	}
                	
                    html += '<tr>'+
                            '<td>'+data[i].nim_mhs+'</td>'+
                            '<td>'+data[i].nama_mhs+'</td>'+
                            '<td>'+data[i].judul_pa+'</td>'+
                            '<td>'+data[i].tahunajaran+'</td>'+
                            '<td>'+ grup+'</td>'+
                            '<td>'+ data[i].angkatan+'</td>'+
                            '<td >'+tahap+'</td>'+
                            '<td style="text-align:right;">'+
                            '<a href="javascript:;" class="btn btn-secondary fa-refresh icon-left item_edit" data="'+data[i].id_mhs+'"></a>'+
                            '<a href="javascript:;" class="btn btn-info fa-search icon-left item_detail" data="'+data[i].id_mhs+'"></a>'+
                            '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }


		//GET UPDATE
		$('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('dosen/edit_mhss')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(nim_mhs,nama_mhs,judul_pa,tahunajaran,id_doping1,id_doping2,grup,tahap,id_mhs){
                    	$('#ModalaEdit').modal('show');
            			$('[name="nim"]').val(data.nim_mhs);
            			$('[name="id"]').val(data.id_mhs);
            			$('[name="nama"]').val(data.nama_mhs);
            			$('[name="judul"]').val(data.judul_pa);
            			$('[name="grup"]').val(data.grup);
            			$('#s2example-1').val(data.id_doping1).trigger('change');
            			$('#s2example-2').val(data.id_doping2).trigger('change');
            			$('#s2example-3').val(data.id_tahunajaran).trigger('change');
            			$('#tahap').val(data.tahap).trigger('change');

            		});
                }
            });
            return false;
        });

		//GET UPDATE
		$('#show_data').on('click','.item_detail',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('dosen/edit_mhss')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(nim_mhs,nama_mhs,judul_pa,tahun,dop1,dop2grup,tahap,id_mhs){
                		var pb1 = "<?php echo getnamabyid("data.id_doping1"); ?>";
                		console.log(pb1);
                    	$('#ModalaView').modal('show');
            			$('[name="nim"]').val(data.nim_mhs);
            			$('[name="id"]').val(data.id_mhs);
            			$('[name="nama"]').val(data.nama_mhs);
            			$('[name="judul"]').val(data.judul_pa);
            			$('[name="grup"]').val(data.grup);
            			$('[name="pb1"]').val(data.dop1);
            			$('[name="pb2"]').val(data.dop2);
            			$('[name="tahun"]').val(data.tahun);
            			$('#s2example-30').val(data.id_tahunajaran).trigger('change');
            			$('#tahapp').val(data.tahap).trigger('change');
            		});
                }
            });
            return false;
        });

    });		
 </script>
<?php if($this->session->flashdata('msg') == 'gagal') { ?>	
<script type="text/javascript">
	$(document).ready(function(){
var opts = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-full-width",
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "0",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

toastr.error("Data tahap mahasiswa tidak boleh mundur", "Hmm.. Operasi Gagal", opts);
});
</script>	
<?php  } ?>
<?php if($this->session->flashdata('msg') == 'ok') { ?>	
<script type="text/javascript">
	$(document).ready(function(){
var opts = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-bottom-right",
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "0",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

toastr.success("Data berhasil diubah", "Operasi Sukses", opts);
});
</script>	
<?php  } ?>