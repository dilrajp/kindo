<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	

	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->



	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>" defer> 
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>" defer>
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>" defer></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" defer></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>

	

</head>

<body class="page-body">
<?php
$this->load->view('fragment/sidebar_dosen');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Jadwal Menguji</h1>
					<p class="description">Data Menguji <?php echo $this->session->nama." ".nama_tahun(gettahun_aktif()); ?></p>
				</div>
				
					<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Jadwal Menguji</a>
					</li>
					</ol>
					</div>
					
			</div>
				

			<div class="tabs-vertical-env">
					
						<ul class="nav tabs-vertical">
							<li class="active"><a href="#v-home" data-toggle="tab">Desk Evaluation</a></li>
							<li><a href="#v-profile" data-toggle="tab">Sidang</a></li>
							
						</ul>
						
						<div class="tab-content">
							<div class="tab-pane active" id="v-home">

								<div class="title-env">
									<p class="description">Desk Evaluation <?php echo $this->session->nama." ".nama_tahun(gettahun_aktif()); ?></p>
								</div>
								<hr>
								<style type="text/css">
								.table td.fit, 
								.table th.fit {
								    white-space: nowrap;
								    width: 1%;
								}
								.table-fit{
								  width: 1%;
									  white-space: nowrap;
									}
							</style>
								
									<table class="table table-striped table-bordered" id="example-3">
										<thead>
											<tr class="replace-inputs">
												
												<th class="fit">NIM &nbsp;&nbsp;&nbsp;</th>
												<th class="fit">Nama &nbsp;&nbsp;&nbsp;</th>
												<th class="fit">Periode&nbsp;&nbsp;</th>
												<th class="fit">Tanggal&nbsp;&nbsp;</th>
												<th class="fit">Penguji 1&nbsp;&nbsp;</th>
												<th class="fit">Penguji 2&nbsp;&nbsp;</th>
												
											</tr>
										</thead>
										<tbody>
										<?php foreach ($jadwal_de as $rows1){ ?>
										<tr >
										
												<td ><?php echo convertmhs($rows1->id_mhs,'nim_mhs') ?></td>
												<td  ><?php echo convertmhs($rows1->id_mhs,'nama_mhs') ?></td>
												<td><?php echo $rows1->periode ?></td>
												<td><?php echo $rows1->tanggal ?></td>
												<?php if($rows1->PUJ1 == $this->session->id_dosen) {
												  echo "<td class='center'><font color='#38bc48' style='font-weight:bold'>";
												 } else {
												  echo  "<td class='center'>";
												 } ?>
													<?php echo getkode($rows1->PUJ1); ?>
												</td>
												<?php if($rows1->PUJ2 == $this->session->id_dosen) {
												  echo "<td class='center'><font color='#38bc48' style='font-weight:bold'>";
												 } else {
												  echo  "<td class='center'>";
												 } ?>
													<?php echo getkode($rows1->PUJ2); ?>
												</td>
																
												
										</tr>
										<?php } ?>
																
										</tbody>
									
								</table>
							</div>
							<div class="tab-pane " id="v-profile">
								<div class="title-env">
									<p class="description">Sidang Akademik <?php echo $this->session->nama." ".nama_tahun(gettahun_aktif()); ?></p>
								</div>
								<hr>
								<style type="text/css">
								.table td.fit, 
								.table th.fit {
								    white-space: nowrap;
								    width: 1%;
								}
								.table-fit{
								  width: 1%;
									  white-space: nowrap;
									}
								</style>

								<table class="table table-striped table-bordered" id="example-4">
										<thead>
											<tr class="replace-inputs">
												
												<th class="fit" >NIM &nbsp;&nbsp;</th>
												<th class="fit">Nama &nbsp;&nbsp;</th>
												<th class="fit">Tanggal&nbsp;&nbsp;</th>
												<th class="fit">Waktu&nbsp;&nbsp;</th>
												<th class="fit">Penguji 1&nbsp;&nbsp;</th>
												<th class="fit">Penguji 2&nbsp;&nbsp;</th>
												
											</tr>
										</thead>
										<tbody>
										<?php foreach ($jadwal_sidang as $rows2){ ?>
										<tr >
										
												<td  width="15%"><?php echo convertmhs($rows2->id_mhs,'nim_mhs') ?></td>
												<td  width="20%"><?php echo convertmhs($rows2->id_mhs,'nama_mhs') ?></td>
												<td width="20%"><?php echo tanggal_indo($rows2->tanggal,true) ?></td>
												<td width="15%"><?php echo $rows2->waktu ?></td>
												<?php if($rows2->PUJ1 == $this->session->id_dosen) {
												  echo "<td class='center'><font color='#38bc48' style='font-weight:bold' width='15%''>";
												 } else {
												  echo  "<td class='center' width='15%'>";
												 } ?>
													<?php echo getkode($rows2->PUJ1); ?>
												</td>
												<?php if($rows2->PUJ2 == $this->session->id_dosen) {
												  echo "<td class='center'><font color='#38bc48' style='font-weight:bold' width='15%'>";
												 } else {
												  echo  "<td class='center' width='15%'>";
												 } ?>
													<?php echo getkode($rows2->PUJ2); ?>
												</td>
																
												
										</tr>
										<?php } ?>
																
										</tbody>
									
								</table>
							</div>
						
						</div>
						
					</div>	
			
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>

	
<div class="page-loading-overlay">
				<div class="loader-2"></div>
			</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
	//view_data();
	$("#example-3").dataTable().yadcf([
						
						
							{column_number : 0, filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2},
							{column_number : 3},
							{column_number : 4, filter_type: 'text'}, 
							{column_number : 5, filter_type: 'text'}
						
							
	]);
		$("#example-4").dataTable().yadcf([
						
						
							{column_number : 0, filter_type: 'text'},
							{column_number : 1, filter_type: 'text'},
							{column_number : 2},
							{column_number : 3},
							{column_number : 4, filter_type: 'text'}, 
							{column_number : 5, filter_type: 'text'}
						
							
	]);
	function view_data(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo base_url()?>koor_pa/data_jadwal_de',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                	var x = i+1;
                           	
                    html += '<tr>'+
                    		
                            '<td>'+data[i].nim_mhs+'</td>'+
                            '<td>'+data[i].nama_mhs+'</td>'+
                            '<td>'+data[i].periode+'</td>'+
                            '<td>'+data[i].tanggal+'</td>'+
                            '<td>'+data[i].kd1+'</td>'+
                            '<td>'+data[i].kd2+'</td>'+
                            
                           
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }
	});
</script>
<script type="text/javascript">
	var styles = [
    'background: linear-gradient(#D33106, #571402)'
    , 'border: 1px solid #3E0E02'
    , 'color: white'
    , 'display: block'
    , 'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)'
    , 'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset'
    , 'line-height: 40px'
    , 'text-align: center'
    , 'font-weight: bold'
].join(';');

console.log('%c yohohoho its Dilraj', styles);
</script>

