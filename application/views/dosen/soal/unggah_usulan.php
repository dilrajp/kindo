<?php 
$this->load->view("fragment/head");
?>
										
<?php
$this->load->view('fragment/sidebar_dosen');
?>	
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Usulan Soal Assessment</h1>
					<p class="description">Unggah Usulan Soal</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('dosen');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Nilai Asessment</strong>
						</li>
						</ol>
				</div>		
				
			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>

			<!-- Body Page-->
			<div class="panel panel-default"> 
   				<div class="panel-heading">
   				<div class="panel-title"><h3>Unggah Usulan Soal</h3>
   			
   				 	<p class="description" style="font-size: 13px; color: red;">Batas Unggah<br><br>
	   				 	<?php foreach ($kondisi as $row): ?>
	   				 		<?php echo nama_matkul($row->id_matakuliah).' =>  '.tanggal_indo($row->tgl_deadlineusulan,true).'=>'.jenis_nilai($row->id_deadlineusulan).'<br>'; ?>
	   				 	<?php endforeach ?>	
	   				</p>
   				</div> 
   				</div> 
   				<div class="panel-body"> 
   					<form enctype="multipart/form-data" action="<?php echo base_url('dosen/unggah_usulan_aksi'); ?>" role="form" id="form1" method="post" class="validate" novalidate="novalidate">
   						<div class="form-group">
   							<label class="control-label">Matakuliah</label><br>
   							<select name="id_matakuliah" id="id_matakuliah" style="width: 300px;" required>
   								<option value="">--Pilih Matakuliah--</option>
   									<?php foreach ($matkul as $row): ?>
   										<option value="<?php echo $row->id_matakuliah; ?>"><?php echo $row->nama_matakuliah; ?></option>
   									<?php endforeach ?> 
   							</select>
   						</div>
   						<div class="form-group">
   							<label class="control-label">Asessment Ke-</label><br>
   							<select name="asessment" id="id_assessment" style="width: 300px;" required>
   								<option value="">-Pilih Assessment-</option>
   								
   							</select>
   						</div>
   						<div class="form-group">
						<label class="control-label">Upload Berkas</label>
						<p class="description" style="font-size: 13px; color: red;">*Format File doc,pptx,pdf dan xlsx</p>
						  	<input type="file" class="form-control" name="userfile" required="required">
				  		</div>
   						<div class="form-group"> 
   						 	<button type="submit" value="Tambah" class="btn btn-success">Save</button> 
   						 	<button type="reset" class="btn btn-white">Reset</button>
   						</div> 
   					</form> 
   				</div>
   			</div>
			<!-- Batas Body Page-->
		

		</div>

	</div>
<?php
	$this->load->view("fragment/foot");
?>
<?php if ($this->session->flashdata('msg') == 'gagal'){ ?>
	<script type="text/javascript">
	$(document).ready(function(){
		var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-full-width",
						"onclick": null,
						"showDuration": "1000",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("Kesalahan dalam proses unggah, mohon cek format dokumen dan ukuran file", opts);
	});
	</script>
<?php } ?>

<script type="text/javascript">
	 $(document).ready(function(){
	 	$("#id_assessment").html('<option value="">-Pilih Dulu Matakuliah-</option>');
	 });
	 $("#id_matakuliah").change(function(){  
            var matakuliahID = $(this).val();
    		if(matakuliahID){
                /*dropdown post *///  
                $.ajax({  
                    url:"<?php echo base_url('dosen/getassessment2'); ?>",  
                    data: {id: $(this).val()},
                    type: "POST",  
                    success:function(data){  
                    	$("#id_assessment").html(data);
                	}
            	});
            } else {
	            $('#id_assessment').html('<option value="">-Pilih Dulu Matakuliah-</option>');
	        }
    	});

</script>

