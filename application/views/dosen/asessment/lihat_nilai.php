
<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_dosen");
?>
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Nilai Asessment</h1>
					<p class="description">Daftar Nilai Asessment</p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li >
						<a href="<?php echo base_url('dosen');?>"><i class="fa-home"></i>
						Home</a>
						</li>
						<li class="active">
							<strong>Nilai Asessment</strong>
						</li>
						</ol>
				</div>		

			</div>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Nilai Asessment</h3>
				</div>

				<div class="panel-body">
					
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#example-3").dataTable({
								aLengthMenu: [
									[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
								]
							});
						});
					</script>

					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 1},
						]);
					});
					</script>

					<table id="example-3" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr style="">
								<th style="width: 10px;">No.</th>
								<th style="width: 100px;">Matakuliah</th>
								<th style="width: 80px;">Asessment</th>
								<th style="width: 80px;">Tanggal</th>
								<th style="width: 80px;">Status</th>
								<th style="width: 10px;">Aksi</th>
							</tr>
						</thead>
					
						<tbody>
							<?php if($rows){$no=1; foreach($rows as $row){ ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->nama_matakuliah; ?></td>
								<td><?php echo $row->asessment; ?></td>
								<td><?php echo $row->date; ?></td>
								<td>
									<label  style="color: #ffffff;" class="
									<?php 
										if ($row->status_nilai=='WAITING') {
											echo  "label label-warning";
										}elseif($row->status_nilai=='REVISI'){
											echo "label label-danger";
										}elseif($row->status_nilai=='APPROVE'){
											echo "label label-secondary";
										}
									 ?>
									"><?php echo $row->status_nilai;?>
									</label>
								</td>
								<td align="center">
									<a href="<?php echo base_url('dosen/download_nilai/'.$row->fileName); ?>" class="btn btn-icon btn-info"><i class="fa-download"></i></a>
									<button onClick="Delete(<?= $row->id_nilaiasessment; ?>);" class="btn btn-icon btn-red"><i class="fa-trash"></i></button>
								</td>
							</tr>
							<?php  $no++; } } ?>
						</tbody>
					</table>
				

				</div>
			</div>
			<!-- Batas Body Page-->
	


		</div>

	</div>
	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>

	<script type="text/javascript">
		function Delete(id) {
	 	alertify.confirm("Confirmation Message","Yakin Hapus?",
	    function(input) {
	      if (input) {
	        alertify.success('Delete Nilai Asessment');
	        window.location.href = "<?= base_url() ?>dosen/hapus_nilai/"+id;
	      } else {
	        alertify.error('Cancel');
	      }
	   	}, function(){  alertify.error('Cancel !');});
	 	}
	</script>