<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>" defer> 
	<link rel="stylesheet" href="<?php echo base_url('assets/js/dropzone/css/dropzone.css'); ?>" defer>
	<script src="<?php echo base_url('assets/js/dropzone/dropzone.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>" defer></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" defer></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>

<style>

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#image_from_url:hover {cursor: zoom-in;}
#img01:hover {cursor: zoom-out;}

/* The Modal (background) */


/* Modal Content (image) */

.modall-content {

    margin: auto;
    display: block;
    
    
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modall-content, #caption {  

    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}



@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}



/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
        height: 100%;
    }
}
</style>
</head>

<body class="page-body">
	
						
				<?php
				$this->load->view('fragment/sidebar_dosen');
				
				?>
			


		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_dosen'); ?>
					
						</ul>
					</li>
					
				</ul>
				

				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Data Perwalian</h1>
					<p class="description">Pengelolaan Data Perwalian Semester <?php echo nama_semester(getsemester_aktif()).' '.nama_tahun(gettahun_aktif())?></p>
				</div>
				
					<div class="breadcrumb-env">
					
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Perwalian</a>
					</li>
					</ol>					
					</div>
					
			</div>
			
			<button href="javascript:;" class="btn btn-red btn-icon btn-icon-standalone" 	
			onclick="jQuery('#modal-1').modal('show', {backdrop: 'fade'});" >
							<i class="fa-pencil"></i>
							<span>Tambah Perwalian</span>
							</button>
	
			<!-- Custom column filtering -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Unggahan Perwalian <?php echo $this->session->flashdata('msg'); ?></h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
				<table class="table table-bordered" id="example-3">
						<thead>
						<tr class="replace-inputs">
								
								<th width="17.5%">Kelas</th>
								<th width="17.5%">File</th>
								<th width="20%">Tanggal</th>
								<th width="10%">Status</th>
								<th width="17.5%"> Aksi </th>
							</tr>
						</thead>
						<tbody id="show_data">
							<?php foreach($rows as $row){ ?>
						<tr>
							<td width="17.5%"><?= $row->kelas ?></td>
							<td style="text-align: center"> 
							<button href="javascript:;" class="btn btn-sm btn-blue btn-icon btn-icon-standalone item_edit gambar" data="<?php echo $row->id_perwalian; ?>" >
							<i class="fa-eye"></i>
							<span>Lihat File</span>
							</button>
								<!-- <img src="<?php echo base_url('uploadfile/perwalian/').$row->file_perwalian ?>" style="width: 50%;display: block;   margin-left: auto;  margin-right: auto" id="image_from_url"> -->
							</td>
							<td><?= tanggal_indo($row->tanggal,true) ?></td>
							<?php if($row->status == 'waiting'){ ?>
							<td width="10%"><div class="label label-info"><?= strtoupper($row->status) ?></div></td>
							<?php }else if($row->status == 'approved'){ ?>
							<td width="8%"><div class="label label-secondary"><?= strtoupper($row->status) ?></div></td>
							<?php }else{ ?>
							<td width="8%"><div class="label label-danger popover-info"  data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $row->keterangan ?>" data-original-title="Pesan Revisi"><?= strtoupper($row->status) ?></div></td>
							<?php } ?>
							<td width="17.5%" style="text-align: center">
								<a href="javascript:;" class="btn btn-secondary fa-refresh icon-left item_edit" data="<?php echo $row->id_perwalian; ?>"></a>
								 <button onClick="CheckDelete(<?= $row->id_perwalian ?>,<?= $row->id_dosen ?>);" class="btn btn-danger fa-trash icon-left"></button>
							</td>
						</tr>
							<?php } ?>
						</tbody>
					</table>		
						
					
				</div>
			</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>

	</div>

	<script type="text/javascript">
					
		jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							{column_number : 0},
						
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
						
							
							
						]);
					});
	</script>

	<!-- Bottom Scripts -->
<div class="page-loading-overlay">
				<div class="loader-2"></div>
			</div>
</body>
</html>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Perwalian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'dosen/add_perwalian'; ?>"  enctype="multipart/form-data">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Kelas</label>
									<div class="col-sm-10">
									<input name="kelas" type="text" class="form-control" id="field-1">
									</div>
								</div>
							
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">File(.jpg/.jpeg)</label>
									<div class="col-sm-10">
									<input type="file"  class="form-control" name="berkas" required />
									<p>Bukti Perwalian dapat berupa scan berita acara/kegiatan perwalian, dsb</p>
									</div>
								</div>
								
								

			
		</div>
		
      	<div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-info">Simpan</button>
		</div>
	</div>
	</form>	
    </div>
  </div>
</div>

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 35%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">File Perwalian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						<img  style="width: 50%;display: block;   margin-left: auto;  margin-right: auto" id="image_from_url">
		      					
					
							
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					
				</div>
				
					
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="EditMo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ubah Perwalian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'dosen/proses_edit_perwalian'; ?>"  enctype="multipart/form-data">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Kelas</label>
									<div class="col-sm-10">
									<input name="kelass" type="text" class="form-control" id="field-1" disabled>
									<input name="id_perwalian" type="hidden" class="form-control" id="field-1" >
									<input name="pile" type="hidden" class="form-control" id="field-1" >
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									<div class="col-sm-10">
									<input name="tanggal" type="text" class="form-control" id="field-1" disabled>
									</div>
								</div>
							
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">File(.jpg/.jpeg)</label>
									<div class="col-sm-10">
									<input type="file"  class="form-control" name="berkas" required />
									</div>
								</div>
								
								

			
		</div>
		
      	<div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-info">Simpan</button>
		</div>
	</div>
	</form>	
    </div>
  </div>
</div>



<script type="text/javascript">
	function CheckDelete(id,di) {
	  alertify.confirm("Konfirmasi Aksi","Data Perwalian akan dihapus ?",
	    function(input) {
	      if (input) {
			alertify.success('Data Berhasil Dihapus');       
	        window.location.href = "<?php echo base_url(); ?>dosen/del_perwalian/"+id+"/"+di;
	      } else {
	        alertify.error('Cancel');
	      }
	    }, function(){alertify.error('Cancel');});
	}
</script>

<script type="text/javascript">

$(document).ready(function(){
		 
	

		//GET UPDATE
		$(".gambar").click(function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('dosen/file_perwalian')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(gambar){
                		
                    	$('#ModalaEdit').modal('show');
                    	if(data.gambar == 'x'){

                    	}else{
            			$( "#image_from_url" ).attr('src',data.gbr );
            			};
            			
            		
            		});
                }
            });
            return false;
        });

        $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('dosen/edit_perwalian')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(kelas,tanggal,file_perwalian,id){
                    	$('#EditMo').modal('show');
            			$('[name="kelass"]').val(data.kelas);
            			$('[name="tanggal"]').val(data.tanggal);
            			$('[name="pile"]').val(data.file_perwalian);
            			$('[name="id_perwalian"]').val(data.id);
            			

            		});
                }
            });
            return false;
        });


		var modal = document.getElementById('myModal');

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img = document.getElementById('image_from_url');
		var modalImg = document.getElementById("img01");
		var captionText = document.getElementById("caption");
		img.onclick = function(){
		    modal.style.display = "block";
		    modalImg.src = this.src;
		    captionText.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks on <span> (x), close the modal
		img01.onclick = function() { 
		    modal.style.display = "none";
		}	

    });		
 </script>


<?php if($this->session->flashdata('msg') == 'gagal') { ?>	
<script type="text/javascript">
	$(document).ready(function(){
var opts = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-full-width",
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "0",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

toastr.error("File Data Perwalian harus bertipe JPG/JPEG/PNG dan Tidak lebih dari 10 Mb", "Hmm.. File Unggahan Anda Bermasalah", opts);
});
</script>	
<?php  } ?>

<div id="myModal" class="modal">
  
  <img class="modall-content" id="img01">
  <div id="caption"></div>
</div>




