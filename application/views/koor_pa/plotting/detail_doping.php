<?php 
$this->load->view("fragment/head");
?>
	<script>
		var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
	</script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/knockout-3.1.0.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo base_url('assets/js/chart.js')?>"></script> 

	<script src="<?php echo base_url('assets/js/xenon-widgets.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/regions/jquery-jvectormap-world-mill-en.js') ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Detail Pembimbing</h1>
					<p class="description">Data Bimbingan</p>
				</div>
				
					<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li>
					<a href="view_doping">Dosen Pembimbing</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Detail </a>
					</li>
					</ol>
					</div>
					
			</div>
				

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Detail Dosen Pembimbing</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

					<div class="panel-body">
					<form role="form" class="form-horizontal">	
					<?php 
					        
					 foreach ($rows as $row) {
					 ?>	
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Nama Dosen</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo $row->nama; ?>" disabled="">
							</div>
							<label class="col-sm-2 control-label"><b>NIP</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo $row->nip; ?>" disabled="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Total Pembimbing 1</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php 
									echo total_pembimbing1_v2($row->id_dosen);; ?>" disabled="">
							</div>
							<label class="col-sm-2 control-label"><b>Total Pembimbing 2</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php echo total_pembimbing2_v2($row->id_dosen); ?>" disabled="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><b>Status</b></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php 
									if(getstatus($row->id_dosen) == 'AKTIF') {
									echo "Aktif";
									}else{
									echo "Non-aktif";
									} ?>" disabled="">
							</div>
						</div>
					<?php } ?>
					</form>
					</div>
			</div>
		</div>
			<div class="col-md-12">
			<div class="panel panel-default collapsed">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Mahasiswa Bimbingan </h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
		
				<div class="panel-body">
					
					
					
					<table class="table table-bordered" id="example-3">
						<thead>
							<tr class="replace-inputs">
								
								<th style="min-width: 15px">#</th>
								<th >NIM</th>
								<th >Nama</th>
								<th width="30%">Judul</th>		
								<th >Tahap</th>		
								<th >Tahun</th>		
								<th >Aksi </th>
							</tr>
						</thead>
						 
						<tbody>
							<?php 
					        $id=1;
					        foreach ($rows2 as $row) {
					       	
			                	
			                	$tahap = ' ';
			                	if($row->tahap == 2){
			                		$tahap = 'DE';
			                	}else if($row->tahap == 3){
			                		$tahap = 'Input Theta';
			                	}else if($row->tahap == 5){
			                		$tahap = 'Pra-Sidang';
			                	}else if($row->tahap == 6){
			                		$tahap = 'Sidang';
			                	}else if($row->tahap == 7){
			                		$tahap = 'Yudisium';
			                	}else if($row->tahap == 1){
			                		$tahap = 'Pra-DE';
			                	}else if($row->tahap == 4){
			                		$tahap = 'Pengajuan SK';
			                	};
					        ?>

							<tr >
								
								<td  style="min-width: 15px"><?php echo $id++ ?>	</td>
								<td><?php echo $row->nim_mhs; ?></td>
								<td><?php echo $row->nama_mhs ?></td>
								<td width="30%"><?php echo $row->judul_pa ?></td>
								<td><?php echo $tahap ?></td>
								<td><?php echo nama_tahun($row->id_tahunajaran) ?></td>
								
							<td class="center">
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $row->id_mhs; ?>').modal('show', {backdrop: 'fade'});" 
									class="btn btn-secondary fa-search icon-left"> </a>
										
									</a>

									
								
								</td>
							</tr>
	
							<?php } ?>
						</tbody>	
							
					
					</table>
					
				</div>
			</div>
		</div>
			<div class="col-md-12">
					
					<!-- Collapsed panel -->
					<div class="panel panel-default "><!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">Persebaran Mahasiswa PA Bimbingan <?php echo getnamabyid($id_dosen);  ?></h3>
							
							<div class="panel-options">
								
								<a href="#" data-toggle="panel">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
								
								<a href="#" data-toggle="remove">
									&times;
								</a>
							</div>
						</div>
						
						<div class="panel-body">
							
									<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									var dataSource = [
										{region: "Asia", val: 4119626293},
										{region: "Africa", val: 1012956064},
										{region: "Northern America", val: 344124520},
										{region: "Latin America and the Caribbean", val: 590946440},
										{region: "Europe", val: 727082222},
										{region: "Oceania", val: 35104756}
									], timer;

												var products = [
								 <?php 
								 $id = $id_dosen;
									$data_pa = $this->db->query("SELECT count(*) as ct, tahap FROM `t_mhs_pa` where tahap != 7  and (id_doping1 = '$id' or id_doping2 = '$id') GROUP BY `tahap` ")->result();
									$i = 1;
									foreach($data_pa as $r){
										if($i < count($data_pa)){
											if($r->tahap == '1'){
											echo "{ region: 'Pra-DE', val:".$r->ct."},";
											$i++;	
											}else if($r->tahap == '2'){
											echo "{ region: 'DE', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '3'){
											echo "{ region: 'Input Theta', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '4'){
											echo "{ region: 'Pengajuan SK', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '5'){
											echo "{ region: 'Pra-Sidang', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '6'){
											echo "{ region: 'Sidang', val:".$r->ct."},";	
											$i++;
											}
										}else{
											if($r->tahap == '1'){
											echo "{ region: 'Pra-DE', val:".$r->ct."}";	
											}else if($r->tahap == '2'){
											echo "{ region: 'DE', val:".$r->ct."}";
											}else if($r->tahap == '3'){
											echo "{ region: 'Input Theta', val:".$r->ct."}";
											}else if($r->tahap == '4'){
											echo "{ region: 'Pengajuan SK', val:".$r->ct."}";
											}else if($r->tahap == '5'){
											echo "{ region: 'Pra-Sidang', val:".$r->ct."}";
											}else if($r->tahap == '6'){
											echo "{ region: 'Sidang', val:".$r->ct."}";	
											}
										}
									}

									?>

									];
									
									$("#bar-10").dxPieChart({
										dataSource: products,
										title: "Persebaran Mahasiswa Proyek Akhir",
										tooltip: {
											enabled: true,
										  	format:"fixedPoint",
											customizeText: function() { 
												return this.argumentText + "<br/>" + this.valueText;
											}
										},
										size: {
											height: 420
										},
										pointClick: function(point) {
											point.showTooltip();
											clearTimeout(timer);
											timer = setTimeout(function() { point.hideTooltip(); }, 2000);
											$("select option:contains(" + point.argument + ")").prop("selected", true);
										},
										legend: {
											visible: true
										},  
										series: [{
											type: "doughnut",
											argumentField: "region"
										}],
										palette: xenonPalette
									});
									
								});
							</script>
							<div id="bar-10" style="height: 450px; width: 100%;"></div>
			
						</div>
					</div>
					
				</div>
	
			</div>
	</div>
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
		</div>
		
	</div>

	<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-3").dataTable().yadcf([
							
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							{column_number : 4},
							{column_number : 5, filter_type: 'text'},
							
							
							
							
						]);
					});
					</script>




	<?php
	$this->load->view("fragment/foot");
	?>



	<?php if($rows){ foreach($rows2 as $row){ ?>
		<div class="modal fade custom-width" id="modal-<?php echo $row->id_mhs; ?>">
			<div class="modal-dialog"  style="width: 60%;">
				<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">View Data Mahasiswa</h4>
						</div>
						
						<div class="modal-body">
	
							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">Nama</label>
								
								<div class="col-sm-8">
									<label class="form-control" id="field-2" ><?php echo $row->nama_mhs; ?> </label>
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">NIP</label>
								
								<div class="col-sm-8">
									<label class="form-control" id="field-2" ><?php echo $row->nim_mhs; ?> </label>
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">NIP</label>
								
								<div class="col-sm-8">
									<label class="form-control" id="field-2" ><?php echo $row->angkatan; ?> </label>
								</div>

							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-3">Judul Proyek Akhir</label>
								
								<div class="col-sm-8">
								<label class="form-control" id="field-2" style="height:30%;width:100%"><?php echo $row->judul_pa; ?> </label>
								</div>

							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-4">Grup:</label>
								<div class="col-sm-8">
								<label class="form-control"  style="width: 8%;"><?= $row->grup;; ?></label>
								</div>

							</div>
							
						
							<div class="form-group" style="margin-bottom: 40px">
								<label class="col-sm-3 control-label" >Pembimbing 1:</label>
								<label class="col-sm-2 control-label" style="width: 0%;margin-right: 80px" ><b><?php echo getkode($row->id_doping1); ?></b></label>
								<label class="col-sm-3 control-label" >Pembimbing 2:</label>
								<label class="col-sm-2 control-label" style="width: 0%" ><b><?= getkode($row->id_doping2); ?></b></label>
								<br>
								
							
							</div>

								<Br><br><br><Br><br><Br><br><Br>
					
						
						<div class="form-group">
							
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><b class="col-sm-3 control-label" >Progress</b><br><br>
								<div class="col-md-12">
									
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" style="width: 10%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pra- Desk Evaluation">
										<span class="sr-only">15% Complete (success)</span>
										</div>
										<?php if ($row->tahap == 2) { ?>
										<div class="progress-bar progress-bar-success" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Desk Evaluation">
											<span class="sr-only">15% Complete (success)</span>
										</div>
										<?php }else if ($row->tahap == 3) { ?>
										<div class="progress-bar progress-bar-success" style="width: 10%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Desk Evaluation">
										<span class="sr-only">15% Complete (success)</span>
										</div>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 10%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Input Theta">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<?php } else if ($row->tahap == 4) { ?>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 20%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Input Theta">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<div class="progress-bar progress-bar-danger" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<?php } else if ($row->tahap == 5){ ?>
										<div class="progress-bar progress-bar-success" style="width: 10%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Desk Evaluation">
										<span class="sr-only">15% Complete (success)</span>
										</div>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 10%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Input Theta">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<div class="progress-bar progress-bar-danger" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<div class="progress-bar progress tooltip-secondary" style="width: 20%" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pra-Sidang">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<?php } else if ($row->tahap == 6){ ?>
										<div class="progress-bar progress-bar-success" style="width: 10%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Desk Evaluation">
										<span class="sr-only">15% Complete (success)</span>
										</div>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 10%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Input Theta">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<div class="progress-bar progress-bar-danger" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<div class="progress-bar progress tooltip-secondary" style="width: 20%" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pra-Sidang">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<div class="progress-bar progress-bar-info " style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sidang"	>
											<span class="sr-only">20% Complete (info)</span>
										</div>
										<?php } else if ($row->tahap == 7){ ?>
										<div class="progress-bar progress-bar-success" style="width: 10%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Yudisium"	>
											<span class="sr-only">100% Complete (info)</span>
										</div>
										<div class="progress-bar progress-bar-success tooltip-secondary" style="width: 10%"  data-toggle="tooltip" data-placement="bottom" data-original-title="Yudisium"	>
											<span class="sr-only">100% Complete (info)</span>
										</div>
										<div class="progress-bar progress-bar-success" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Yudisium"	>
											<span class="sr-only">100% Complete (info)</span>
										</div>
										<div class="progress-bar progress-bar-success" style="width: 20%" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Yudisium"	>
											<span class="sr-only">100% Complete (info)</span>
										</div>
										<div class="progress-bar progress-bar-success " style="width: 30%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Yudisium"	>
											<span class="sr-only">100% Complete (info)</span>
										</div>
										<?php } ?>
									</div>
									
								</div>
								
							</div>	
					</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	<?php } } ?>