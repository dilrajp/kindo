<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Plotting Pembimbing</h1>
					<p class="description">Plotting Data Proyek Akhir (Mahasiswa dan Dosen Pembimbing)</p>
				</div>
				
				<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Plotting</a>
					</li>
					</ol>
				</div>
					
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Template File Excel<br></h3><br>
					<?php
						/*
					foreach($rows as $row){
						$total = total_bimb($row->id_dosen);
						
						$kategori = $this->yeah->getById('id_kpi','1','t_kategori')->result();
						foreach($kategori as $res){

						//echo $x->nama_dosen." ".$total." ".$res->kondisi;
						//echo "<br>";
							$total = total_bimb($row->id_dosen);
				if($total >= $res->kondisi){
					$hasil = array(
						'nilai_jml_bimb' => $res->nilai
					);
					$nil = $res->nilai;
					echo "<font color='red'>nilai 1".$row->nama_dosen." ".$total." ".$res->kondisi." ".$nil."</font>";
					echo "<br>";
					//$this->yeah->update('id_dosen',$row->id_dosen,'t_nilai',$nil);
					
					break;
				}else{
					$hasil = array(
						'nilai_jml_bimb' => 80
					);
					$nil = 0;
					echo $row->nama_dosen." ".$total." ".$res->kondisi." ".$nil;
					echo "<br>";
					//$this->yeah->update('id_dosen',$row->id_dosen,'t_nilai',$nil);
				}
						}
						echo "<br>";

					};
				
*/
					?>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<h4 style="color:grey"> Download Template Plotting Pembimbing (Excel) untuk diunggah kedalam Aplikasi </h4>
					<a href="<?php echo base_url('koor_pa/download_template_pa'); ?>" class="btn btn-success btn-icon btn-icon-standalone">
					<i class="fa-download"></i><span>Unduh Template</span>
					</a>
		
				</div>
			</div>	


			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Unggah File</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form role="form" class="form-horizontal" method="post" action="<?php echo base_url('koor_pa/createdata'); ?>" enctype="multipart/form-data" accept-charset="utf-8">
					<div class="form-group-separator"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="field-4">File Plotting (.xls/.xlxs)</label>
						<div class="col-sm-10">
						<p>File yang diunggah akan menganti data-data didalam aplikasi apabila ada data yang sama</p>
						<input type="file" class="form-control" id="field-4" name="file" required>
						</div>
					</div>
					<div class="form-group">
					<div class="col-sm-10">
					<?php //echo $this->session->flashdata('msg'); echo "<br>" ?>
					<button type="submit" class="btn btn-info  btn-icon btn-icon-standalone"><i class="fa-upload"></i><span>Unggah File</span></button>
					</div>
					</div>
					</form>			
				</div>
			</div>	

			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>



	<?php
	$this->load->view("fragment/foot");
	?>


	<?php if($this->session->flashdata('msg') == 'Ada kesalahan dalam proses upload') { ?>	
	<script type="text/javascript">
	$(document).ready(function(){
	var opts = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-full-width",
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "0",
		"extendedTimeOut": "0",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};

	toastr.error("File gagal diunggah, Pastikan header/data/format sesuai template dan Ukuran file tidak lebih dari 100 mb", "Hmm.. File Unggahan Anda Bermasalah", opts);
	});
	</script>
	<?php } ?>
	<?php if($this->session->flashdata('msg') == 'ok') { ?>	
	<script type="text/javascript">
		$(document).ready(function(){
var opts = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-bottom-right",
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "0",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

toastr.success("Data berhasil diunggah", "Operasi Sukses", opts);
	});
	</script>
	<?php  } ?>
	