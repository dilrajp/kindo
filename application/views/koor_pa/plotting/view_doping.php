<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Daftar Dosen Pembimbing</h1>
					<p class="description">Halaman data dosen pembimbing</p>
				</div>
				
				
					
					<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>
					<li class="active">
					<a href="forms-native.html">Dosen Pembimbing</a>
					</li>
					</ol>
					</div>
												
				
					
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Dosen Pembimbing</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
						
					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-1").dataTable({
							aLengthMenu: [
								[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
							]
						});
					});
					</script>
					
					<table class="table table-striped table-bordered" id="example-1">
						<thead>
							<tr class="replace-inputs">
								<th>No</th>
								<th>NIP</th>
								<th>Nama </th>
								<th>Pembimbing 1</th>
								<th>Pembimbing 2</th>
								
								
								<th> Action </th>
							</tr>
						</thead>
						 <?php  ?>
						<tbody>
							<?php 
							$no = 0;
					        foreach($rows as $x ) {
					      	$no++;
					        ?>
							<tr>
								
								<td><?php echo $no;?></td>
								<td><?php echo $x->nip;?></td>
								<td><?php echo $x->nama;?></td>
								<td class="center"><?php echo total_pembimbing1_v2($x->id_dosen);?></td>
								<td class="center"><?php echo total_pembimbing2_v2($x->id_dosen);?></td>
								<td>
								<a href="<?php echo base_url() ?>koor_pa/detail_doping/<?php echo $x->id_dosen;?>" class="btn btn-info fa-search icon-left"> </a>
									</a>
								</td>
							</tr>
						<?php } ?>
				
						</tbody>	
							
					</table>
					
				
				</div>
			</div>	


			
	
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>

	
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
     <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.uk.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>


	<?php
	$this->load->view("fragment/foot");
	?>

