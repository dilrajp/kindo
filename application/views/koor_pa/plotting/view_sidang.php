
<?php 
$this->load->view("fragment/head");
?>

<?php 
$this->load->view("fragment/sidebar_koorpa");
?>
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<script>
			jQuery(document).ready(function($)
			{
				$('a[href="#layout-variants"]').on('click', function(ev)
				{
					ev.preventDefault();
					
					var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};
					
					TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
						{
							$(window).scrollTop(win.top);
						}
					});
				});
			});
			</script>
			<!-- Body Page-->
			
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Informasi Sidang</h1>
					<p class="description"></p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li class="active">
						<i class="fa-home"></i>
						<strong>Sidang</strong>
						</li>
						</ol>
				</div>
			</div>

			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li class="active">
							<a href="#home-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs">Jadwal DE</span>
							</a>
						</li>
						<li>
							<a href="#profile-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Upload Plotting</span>
							</a>
						</li>
						
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="home-3">
							
							<div>
								
								<p>Carriage quitting securing be appetite it declared. High eyes kept so busy feel call in. Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment. Passage weather as up am exposed. And natural related man subject. Eagerness get situation his was delighted. </p>
					
								<p>Fulfilled direction use continual set him propriety continued. Saw met applauded favourite deficient engrossed concealed and her. Concluded boy perpetual old supposing. Farther related bed and passage comfort civilly. Dashwoods see frankness objection abilities the. As hastened oh produced prospect formerly up am. Placing forming nay looking old married few has. Margaret disposed add screened rendered six say his striking confined. </p>
								
							</div>
							
						</div>
						<div class="tab-pane" id="profile-3">
							
							<div class="panel-body">
							<h4 style="color:grey"> Download Template Plotting Pembimbing (Excel) untuk diunggah kedalam Aplikasi </h4>
							<a href="<?php echo base_url('koor_pa/download_template'); ?>" class="btn btn-success btn-icon btn-icon-standalone">
							<i class="el-download-alt"></i><span>Unduh Template</span>
							</a>
							</div>
							
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Upload File Excel</h3>
									
								<div class="panel-options">
									<a href="#" data-toggle="panel">
										<span class="collapse-icon">&ndash;</span>
										<span class="expand-icon">+</span>
									</a>
								</div>
								</div>
								<div class="panel-body">
									<form role="form" class="form-horizontal" method="post" action="<?php echo base_url('koor_pa/createdata_de'); ?>" enctype="multipart/form-data" accept-charset="utf-8">
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="field-4">Unggah File Plotting (.xls/.xlxs)</label>
										<div class="col-sm-10">
										<input type="file" class="form-control" id="field-4" name="file" required>
										</div>
									</div>
									<div class="form-group">
									<div class="col-sm-10">
									<?php echo $this->session->flashdata('msg'); echo "<br>" ?>
									<button type="submit" class="btn btn-info">Upload File</button>
									</div>
									</div>
									</form>			
								</div>
							</div>	
						</div>
						
					</div>
					
					
				</div>
			</div>
	
				
			<!-- Batas Body Page-->

	<!-- Bottom Scripts -->
	<?php
	$this->load->view("fragment/foot");
	?>