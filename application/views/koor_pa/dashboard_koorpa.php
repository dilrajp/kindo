<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/alertify.min.js')?>"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>

	
	<script>
		var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
	</script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/knockout-3.1.0.js')?>"></script>
	<script src="<?php echo base_url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo base_url('assets/js/chart.js')?>"></script> 

	<script src="<?php echo base_url('assets/js/xenon-widgets.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jvectormap/regions/jquery-jvectormap-world-mill-en.js') ?>"></script>
</head>
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
	
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php //$this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
<!-- 			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Dashboard Koordinator Proyek Akhir</h1>
					<p class="description"></p>
				</div>
				
				<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li class="active">
						<i class="fa-home"></i>
						<strong>Home</strong>
						</li>
						</ol>
				</div>
			</div> -->

			<div class="row">
				<div class="col-sm-12">
				
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"></h3>
							
						</div>
						<div class="panel-body">	
						 <div class="col-md-6">
				            <div class="card">
				              <h3 class="card-title">Kontribusi Kelulusan</h3>
				              <?php if($tahun->num_rows()) { ?>
				              <div class="embed-responsive embed-responsive-16by9">
				                <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
				              </div>
				              <?php }else{ ?>
				       		   <img src="<?php echo base_url('assets/images/gambar/logo.png');?>" alt="" width="80" />
				              <?php } ?>
				            </div>
				          </div>
						

					<div class="col-md-6">
							
					<div class="xe-widget xe-counter-block xe-counter-block-red" data-count=".num" data-count=".num" data-from="0" data-to="<?php print_r( $this->db->query('SELECT count(*) as jumlah FROM `t_mhs_pa` where tahap != 7')->row()->jumlah);?>" data-duration="2" data-easing="true">
						<div class="xe-upper">
							
							<div class="xe-icon">
								<i class="linecons-user"></i>
							</div>
							<div class="xe-label">
								<strong class="num"><?php print_r( $this->db->query('SELECT count(*) as jumlah FROM `t_mhs_pa` where tahap != 7')->row()->jumlah);?></strong>
								<span style="font-size:11px;font-weight: bold">Mahasiswa Proyek Akhir <?php echo nama_tahun(gettahun_aktif()) ?></span>
							</div>
							
						</div>
						<div class="xe-lower">
							
							
						</div>
					</div>

					<div class="xe-widget xe-counter-block xe-counter-block-green" data-count=".num" data-from="0"  data-to="<?php $sem = gettahun_aktif(); print_r( $this->db->query("SELECT count(*) as jumlah FROM `t_jadwal_de` where tahun = '$sem' ")->row()->jumlah);?>" data-duration="3">
						<div class="xe-upper">
							
							<div class="xe-icon">
								<i class="linecons-calendar"></i>
							</div>
							<div class="xe-label">
								<strong class="num"><?php $sem = gettahun_aktif();  print_r( $this->db->query("SELECT count(*) as jumlah FROM `t_jadwal_de` where tahun = '$sem' ")->row()->jumlah);?></strong>
								<span style="font-size:11px;font-weight: bold">Jumlah Desk Evaluation <?php echo nama_tahun(gettahun_aktif()) ?></span>
							</div>
							<div class="xe-lower">
							<div class="border"></div>
							
							
						</div>
						</div>
						<div class="xe-lower">
							
							
						</div>
					</div>

				
					<div class="xe-widget xe-counter-block xe-counter-block-blue" data-count=".num" data-from="0" data-to= "<?php $sem = gettahun_aktif();  print_r( $this->db->query("SELECT count(*) as jumlah FROM `t_jadwal_sidang` where tahun = '$sem' ")->row()->jumlah);?> "data-duration="3">
						<div class="xe-upper">
							
							<div class="xe-icon">
								<i class="linecons-note"></i>
							</div>
							<div class="xe-label">
								<strong class="num"><?php $sem = gettahun_aktif();  print_r( $this->db->query("SELECT count(*) as jumlah FROM `t_jadwal_sidang` where tahun = '$sem'  ")->row()->jumlah);?></strong>
								<span style="font-size:11px;font-weight: bold">Jumlah Sidang <?php echo nama_tahun(gettahun_aktif()) ?></span>
							</div>
							
						</div>
						<div class="xe-lower">
							
							
						</div>
					</div>
					
					</div>
				 <div class="col-md-12">
				<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									var dataSource = [
										{region: "Asia", val: 4119626293},
										{region: "Africa", val: 1012956064},
										{region: "Northern America", val: 344124520},
										{region: "Latin America and the Caribbean", val: 590946440},
										{region: "Europe", val: 727082222},
										{region: "Oceania", val: 35104756}
									], timer;

												var products = [
								 <?php 
									$data_pa = $this->db->query("SELECT count(*) as ct, tahap FROM `t_mhs_pa` where tahap != 7 GROUP BY `tahap` ")->result();
									$i = 1;
									foreach($data_pa as $r){
										if($i < count($data_pa)){
											if($r->tahap == '1'){
											echo "{ region: 'Pra-DE', val:".$r->ct."},";
											$i++;	
											}else if($r->tahap == '2'){
											echo "{ region: 'DE', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '3'){
											echo "{ region: 'Input Theta', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '4'){
											echo "{ region: 'Pengajuan SK', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '5'){
											echo "{ region: 'Pra-Sidang', val:".$r->ct."},";
											$i++;
											}else if($r->tahap == '6'){
											echo "{ region: 'Sidang', val:".$r->ct."},";	
											$i++;
											}
										}else{
											if($r->tahap == '1'){
											echo "{ region: 'Pra-DE', val:".$r->ct."}";	
											}else if($r->tahap == '2'){
											echo "{ region: 'DE', val:".$r->ct."}";
											}else if($r->tahap == '3'){
											echo "{ region: 'Input Theta', val:".$r->ct."}";
											}else if($r->tahap == '4'){
											echo "{ region: 'Pengajuan SK', val:".$r->ct."}";
											}else if($r->tahap == '5'){
											echo "{ region: 'Pra-Sidang', val:".$r->ct."}";
											}else if($r->tahap == '6'){
											echo "{ region: 'Sidang', val:".$r->ct."}";	
											}
										}
									}

									?>

									];
									
									$("#bar-10").dxPieChart({
										dataSource: products,
										title: "Persebaran Mahasiswa Proyek Akhir",
										tooltip: {
											enabled: true,
										  	format:"fixedPoint",
											customizeText: function() { 
												return this.argumentText + "<br/>" + this.valueText;
											}
										},
										size: {
											height: 420
										},
										pointClick: function(point) {
											point.showTooltip();
											clearTimeout(timer);
											timer = setTimeout(function() { point.hideTooltip(); }, 2000);
											$("select option:contains(" + point.argument + ")").prop("selected", true);
										},
										legend: {
											visible: true
										},  
										series: [{
											type: "doughnut",
											argumentField: "region"
										}],
										palette: xenonPalette
									});
									
								});
							</script>
							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
						
				</div>
			</div>
		</div>

		
	
		
		<script type="text/javascript">
		
		var data = {
      	labels: [<?php $i =1;foreach($tahun->result() as $t){ 
							if($i < count($tahun->result())  ){
							echo '"'.$t->tahunajaran.'",';
							}else{
							echo '"'.$t->tahunajaran.'"';	
							}
							$i++;
							} ?>],
      	datasets: [
      	
      		{
      			label: "My Second dataset",
      			fillColor: "rgba(151,187,205,0.2)",
      			strokeColor: "rgba(151,187,205,1)",
      			pointColor: "rgba(151,187,205,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(151,187,205,1)",
      			data: [<?php $i =1;
				foreach($jumlah as $t){ 
					if($i < count($jumlah)  ){
					echo $t->tot.',';
					}else{
					echo $t->tot;	
					}
				$i++;
				} ?>]
      		}
      	]
      };


	 	var ctxl = $("#lineChartDemo").get(0).getContext("2d");
	    var lineChart = new Chart(ctxl).Line(data);
		</script>	
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
	
		</div>
		
	</div>
</body>
</html>

