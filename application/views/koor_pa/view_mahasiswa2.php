<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Daftar Mahasiswa</h1>
					<p class="description">Halaman data mahasiswa proyek akhir</p>
				</div>
				
				
					
					<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>

					<li class="active">
					<a href="forms-native.html">Mahasiswa</a>
					</li>
					</ol>
					</div>
												
				
					
			</div>
				<a href="javascript:;"onclick="jQuery('#add').modal('show', {backdrop: 'fade'});" 			class="btn btn-red btn-icon btn-icon-standalone">
				<i class="fa-pencil"></i>
				<span>Tambah Mahasiswa</span>
			</a>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Mahasiswa Proyek Akhir </h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

				<div class="panel-body" style="display:table-cell;">
		
				
				<table class="table table-striped" id="mydata">
						<thead>
							<tr class="replace-inputs">
								<th width="10%">No</th>
								<th width="10%">NIM</th>
								<th width="10%">Nama</th>
								<th width="20%">Judul PA</th>
								<th width="10%">Tahun</th>
								<th width="10%">Grup</th>
								<th width="10%"> Angkatan </th>
								<th width="10%"> Tahap </th>
								<th width="15%"> Aksi </th>
							</tr>
						</thead>
						<tbody id="show_data">
							
						</tbody>
				</table>
				
				</div>
			</div>	


			
	
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
		</div>
		
	</div>

	
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.uk.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>


	<?php
	$this->load->view("fragment/foot");
		if($alert == 'fail edit')
	{	?>
	<script type="text/javascript">
		alertify
  		.alert("Error DR4<br> Dosen Pembimbing 1 tidak boleh sama dengan Dosen Pembimbing 2", function(){
   		
  		});
	</script>
<?php }else	if($alert == 'fail')
	{	?>
	<script type="text/javascript">
		alertify
  		.alert("Error DR4<br> Dosen Pembimbing 1 tidak boleh sama dengan Dosen Pembimbing 2", function(){
   		
  		});
	</script>
<?php } else if($this->session->flashdata('msg') == 'Sukses ...!!') { ?>
<script type="text/javascript">
	  alertify.success('Operation Success');
</script>
<?php }	?>


<script type="text/javascript">
	
function CheckDelete(id) {
  alertify.confirm("Konfirmasi Aksi","Data Mahasiswa PA akan dihapus ?",
    function(input) {
      if (input) {
		alertify.success('Data Berhasil Dihapus');       
        window.location.href = "<?php echo base_url(); ?>koor_pa/del_mhs/"+id;
      } else {
        alertify.error('Cancel');
      }
    }, function(){alertify.error('Cancel');});
}

</script>	
<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="add">
  <div class="modal-dialog" role="document" style="width: 60%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Mahasiswa Proyek Akhir</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/add_mhs'; ?>">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim-add" type="number" class="form-control" id="field-1" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama-add" required>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Angkatan</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="angkatan-add" required>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<input name="judul-add" type="text" class="form-control" id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Grup</label>
									
									<div class="col-sm-10">
										<input name="grup-add" type="number" class="form-control" id="field-3" >
									</div>
								</div>
								
								

								<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 1</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#doping1").select2({
												placeholder: 'Daftar Dosen Pembimbing...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="doping1" required name="p1">
										<option></option>
										<optgroup label="Daftar Dosen Pembimbing">
										<?php foreach ($nama as $row) {		?>	
										<option value="<?php echo $row->id_dosen ?>" ><?php echo $row->kode_dosen." ".$row->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 2</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#doping2").select2({
												placeholder: 'Daftar Dosen Pembimbing...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="doping2" required name="p2">
										<option></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row) {		?>	
											<option value="<?php echo $row->id_dosen ?>" ><?php echo $row->kode_dosen." ".$row->nama ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahun</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#tahunnn").select2({
												placeholder: 'Daftar Tahun Ajaran...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="tahunnn" required name="p3">
										<option></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($tahun as $row) {		?>	
											<option value="<?php echo $row->id_tahunajaran ?>" ><?php echo nama_tahun($row->id_tahunajaran); ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Tahap</label>
									
									<div class="col-sm-10">	
								
										<select class="form-control js-example-basic-single" id="tahappp"  required name="tahap">
										<optgroup label="Daftar Tahap Proyek Akhir">
										<option value="1" >Pra-Desk Evaluation</option>
										<option value="2" >Desk Evaluation</option>
										<option value="3" >Input Theta</option>
										<option value="4" >Pengajuan SK</option>
										<option value="5" >Pra-Sidang</option>
										<option value="6" >Sidang</option>
										<option value="7" >Yudisium</option>
									
									
										</optgroup>
										</select>
									</div>	
								</div>
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info">Simpan</button>

				</div>

					</form>
    </div>
  </div>
</div>


<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaView">
  <div class="modal-dialog" role="document" style="width: 60%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Mahasiswa Proyek Akhir</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
			<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/edit_mhs'; ?>">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim" type="text" class="form-control" id="nim_mhs" disabled>
										<input type="hidden" name="id" id="id_mhs">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama_mhs" disabled>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<input name="judul" type="text" class="form-control" id="judul_pa" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Grup</label>
									
									<div class="col-sm-10">
										<input name="grup" type="text" class="form-control" id="grup" disabled>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Angkatan</label>
									
									<div class="col-sm-10">
										<input name="angkatan" type="text" class="form-control" id="angkatan" disabled>
									</div>
								</div>
							
							<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 1</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
									$("#s2example-10").select2({
												placeholder: 'Daftar Pembimbing yang tersedia...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
									</script>
										<select class="form-control" id="s2example-10" disabled name="p1">
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 2</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										$("#s2example-20").select2({
												placeholder: 'Daftar Mata Kuliah yang tersedia...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
									</script>
										<select class="form-control" id="s2example-20" disabled name="p2">
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row) {		?>	
											<option value="<?php echo $row->id_dosen ?>" ><?php echo $row->kode_dosen." ".$row->nama ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahun</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-30-disabled").prop("disabled", true);
											
										});
									</script>
										<select class="form-control" id="s2example-30-disabled" disabled name="p3">
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($tahun as $row) {		?>	
											<option value="<?php echo $row->id_tahunajaran ?>" ><?php echo nama_tahun($row->id_tahunajaran); ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Tahap</label>
									
									<div class="col-sm-10">	
								
										<select class="form-control js-example-basic-single-disabled" id="tahapp"  disabled name="tahap">
										<optgroup label="Daftar Tahap Proyek Akhir">
										<option value="1" >Pra-Desk Evaluation</option>
										<option value="2" >Desk Evaluation</option>
										<option value="3" >Input Theta</option>
										<option value="4" >Pengajuan SK</option>
										<option value="5" >Pra-Sidang</option>
										<option value="6" >Sidang</option>
										<option value="7" >Yudisium</option>
									
										</optgroup>
										</select>
									</div>	
								</div>
						
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				</div>
			</form>		
					
    </div>
  </div>
</div>


<!-- Edit -->

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 60%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Form Edit Mahasiswa Proyek Akhir</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						 <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/edit_mhs'; ?>">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim" type="text" class="form-control" id="nim_mhs" required>
										<input type="hidden" name="id" id="id_mhs">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama_mhs" required>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<input name="judul" type="text" class="form-control" id="judul_pa" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Grup</label>
									
									<div class="col-sm-10">
										<input name="grup" type="number" class="form-control" id="grup" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Angkatan</label>
									
									<div class="col-sm-10">
										<input name="angkatan" type="text" class="form-control" id="angkatan" >
									</div>
								</div>
								

							<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 1</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-1").select2({
												placeholder: 'Daftar Mata Kuliah yang tersedia...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="s2example-1" required name="p1">
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 2</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-2").select2({
												placeholder: 'Daftar Dosen Pembimbing...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="s2example-2" required name="p2">
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row) {		?>	
											<option value="<?php echo $row->id_dosen ?>" ><?php echo $row->kode_dosen." ".$row->nama ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahun</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-3").select2({
												placeholder: 'Daftar Tahun Ajaran...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="s2example-3" required name="p3">
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($tahun as $row) {		?>	
											<option value="<?php echo $row->id_tahunajaran ?>" ><?php echo nama_tahun($row->id_tahunajaran); ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Tahap</label>
									
									<div class="col-sm-10">	
								
										<select class="form-control js-example-basic-single" id="tahap"  required name="tahap">
										<optgroup label="Daftar Tahap Proyek Akhir">
										<option value="1" >Pra-Desk Evaluation</option>
										<option value="2" >Desk Evaluation</option>
										<option value="3" >Input Theta</option>
										<option value="4" >Pengajuan SK</option>
										<option value="5" >Pra-Sidang</option>
										<option value="6" >Sidang</option>
										<option value="7" >Yudisium</option>
									
										</optgroup>
										</select>
									</div>	
								</div>
						
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info" >Simpan</button>

				</div>
			</form>		
					
    </div>
  </div>
</div>
<script type="text/javascript">
									$(document).ready(function() {
									    $('.js-example-basic-single').select2();
									});
									</script>
<script type="text/javascript">
$(document).ready(function(){
    tampil_data_barang();   //pemanggilan fungsi tampil barang.
	
	$("#mydata").dataTable().yadcf([
						
							
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							{column_number : 5,},
							{column_number : 4, },
							{column_number : 7, },
							{column_number : 6, },
							
						]);
				

    function tampil_data_barang(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo base_url()?>koor_pa/data_mhs',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                	var x = i+1;
                	var tahap = ' ';
                	if(data[i].tahap == 2){
                		tahap = 'DE';
                	}else if(data[i].tahap == 3){
                		tahap = 'Input Theta';
                	}else if(data[i].tahap == 5){
                		tahap = 'Pra-Sidang';
                	}else if(data[i].tahap == 6){
                		tahap = 'Sidang';
                	}else if(data[i].tahap == 7){
                		tahap = 'Yudisium';
                	}else if(data[i].tahap == 1){
                		tahap = 'Pra-DE';
                	}else if(data[i].tahap == 4){
                		tahap = 'Pengajuan SK';
                	};

                	var grup = ' '
                	if(data[i].grup == null){
                		grup = ' ';
                	}else{
                		grup = data[i].grup;
                	}
                	
                    html += '<tr>'+
                    		'<td>'+x+'</td>'+
                            '<td>'+data[i].nim_mhs+'</td>'+
                            '<td>'+data[i].nama_mhs+'</td>'+
                            '<td>'+data[i].judul_pa+'</td>'+
                            '<td>'+data[i].tahunajaran+'</td>'+
                            '<td>'+ grup+'</td>'+
                            '<td>'+ data[i].angkatan+'</td>'+
                            '<td >'+tahap+'</td>'+
                            '<td style="text-align:right;">'+
                            '<a href="javascript:;" style="vertical-align:center; " class="btn btn-secondary btn-block fa-refresh item_edit" data="'+data[i].id_mhs+'"></a>'+
                            '<a href="javascript:;" style="vertical-align:center; " class="btn btn-info btn-block fa-search  item_detail" data="'+data[i].id_mhs+'"></a>'+
                            '<button onClick="CheckDelete(<?= "'+ data[i].id_mhs+'"?>);" style="vertical-align:center; " class="btn btn-danger btn-block fa-trash " data="'+data[i].id_mhs+'"></button>'+
                            '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }


		//GET UPDATE
		$('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('koor_pa/edit_mhss')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(nim_mhs,nama_mhs,judul_pa,ang,tahunajaran,id_doping1,id_doping2,grup,tahap,id_mhs){
                    	$('#ModalaEdit').modal('show');
            			$('[name="nim"]').val(data.nim_mhs);
            			$('[name="id"]').val(data.id_mhs);
            			$('[name="nama"]').val(data.nama_mhs);
            			$('[name="judul"]').val(data.judul_pa);
            			$('[name="grup"]').val(data.grup);
            			$('[name="angkatan"]').val(data.ang);
            			$('#s2example-1').val(data.id_doping1).trigger('change');
            			$('#s2example-2').val(data.id_doping2).trigger('change');
            			$('#s2example-3').val(data.id_tahunajaran).trigger('change');
            			$('#tahap').val(data.tahap).trigger('change');

            		});
                }
            });
            return false;
        });

		//GET UPDATE
		$('#show_data').on('click','.item_detail',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('koor_pa/edit_mhss')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(nim_mhs,nama_mhs, ang,judul_pa,tahunajaran,id_doping1,id_doping2,grup,tahap,id_mhs){
                    	$('#ModalaView').modal('show');
            			$('[name="nim"]').val(data.nim_mhs);
            			$('[name="id"]').val(data.id_mhs);
            			$('[name="nama"]').val(data.nama_mhs);
            			$('[name="judul"]').val(data.judul_pa);
            			$('[name="grup"]').val(data.grup);
            			$('[name="angkatan"]').val(data.ang);
            			$('#s2example-10').val(data.id_doping1).trigger('change');
            			$('#s2example-20').val(data.id_doping2).trigger('change');
            			$('#s2example-30').val(data.id_tahunajaran).trigger('change');
            			$('#tahapp').val(data.tahap).trigger('change');
            		});
                }
            });
            return false;
        });
        		

    });		
 </script>
