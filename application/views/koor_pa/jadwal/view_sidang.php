<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/gambar/logo2.png'); ?>"> 
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/linecons/css/linecons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/alertify.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/alertify/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" />
	<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/elusive/css/elusive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">
	


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">

	<!-- foot -->


	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css'); ?>">

	
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>" defer></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>" defer></script>


	<script src="<?php echo base_url('assets/js/alertify.min.js')?>" ></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>" defer></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>" defer></script>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>" defer>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>" defer></script>


	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>
	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>

</head>


<body class="page-body">
<!-- Top Scripts -->
<div class="page-loading-overlay">
<div class="loader-2"></div>
</div>
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php //$this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile'); ?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Informasi Sidang</h1>
					<p class="description"></p>
				</div>
				
				<div class="breadcrumb-env">
					<ol class="breadcrumb bc-1">
					<li>
					<a href="index"><i class="fa-home"></i>Home</a>
					</li>

					<li class="active">
					<a href="forms-native.html">Sidang</a>
					</li>
					</ol>
				</div>
			</div>

		
				
			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li class="active">
							<a href="#home-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs">Jadwal Sidang</span>
							</a>
						</li>
						<li>
							<a href="#profile-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Upload Plotting</span>
							</a>
						</li>
						
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="home-3">
							
							<div>
								
								<div class="panel panel-default">
								<a href="javascript:;"onclick="jQuery('#add').modal('show', {backdrop: 'fade'});" 	 class="btn btn-secondary btn-icon btn-icon-standalone">
									<i class="fa-edit"></i>
									<span>Tambah Jadwal</span>
								</a>
								<div class="panel-body">
									
									<script type="text/javascript">
									
									</script>
									<style type="text/css">
										.table-fit {
									  width: 1%;
									  white-space: nowrap;
									}
									</style>
									<div class="table-responsive">
									<table class="table table-striped table-bordered " id="example-3">
										<thead>
											<tr class="replace-inputs">
												<th>#&nbsp;</th>
												<th>NIM&nbsp; </th>
												<th>Nama&nbsp;</th>
												<th>Tanggal&nbsp;&nbsp;&nbsp;</th>
												<th>Waktu&nbsp;&nbsp;&nbsp;</th>
												<th>Penguji 1&nbsp;&nbsp;&nbsp;</th>
												<th>Penguji 2&nbsp;&nbsp;&nbsp;</th>
												<th>Ruangan&nbsp;&nbsp;&nbsp;</th>
												<th>Aksi&nbsp;&nbsp;&nbsp;</th>
											</tr>
										</thead>
										<tbody id="show_data">
										
							
										</tbody>
									
									</table>
									</div>
								</div>
							</div>
								
							</div>
							
						</div>
						<div class="tab-pane" id="profile-3">
							
							<div class="panel-body">
							<h4 style="color:grey"> Download Template Jadwal Sidang (Excel) untuk diunggah kedalam Aplikasi </h4>
							<a href="<?php echo base_url('koor_pa/download_template_sidang'); ?>" class="btn btn-success btn-icon btn-icon-standalone">
							<i class="fa-download"></i><span>Unduh Template</span>
							</a>
							</div>
							
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Upload File Excel</h3>
									
								<div class="panel-options">
									<a href="#" data-toggle="panel">
										<span class="collapse-icon">&ndash;</span>
										<span class="expand-icon">+</span>
									</a>
								</div>
								</div>
								<div class="panel-body">
									<form role="form" class="form-horizontal" method="post" action="<?php echo base_url('koor_pa/createdata_sidang'); ?>" enctype="multipart/form-data" accept-charset="utf-8">
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="field-4">Unggah File Jadwal (.xls/.xlxs)</label>
										<div class="col-sm-10">
										<input type="file" class="form-control" id="field-4" name="file" required>
										</div>
									</div>
									<div class="form-group">
									<div class="col-sm-10">
									<?php echo $this->session->flashdata('msg'); echo "<br>" ?>
									<button type="submit" class="btn btn-info btn-icon-standalone"><i class="fa-upload"></i><span>Unggah File</span></button>
									</div>
									</div>
									</form>			
								</div>
							</div>	
						</div>
						
					</div>
					
					
				</div>
			</div>
	
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	</div>

<!-- add -->
<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="add">
  <div class="modal-dialog" role="document" style="width: 60%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Jadwal Sidang </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						 <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/proses_add_sidang'; ?>">
							
								<div class="form-group">
									<label class="col-sm-2 control-label">NIM Mahasiswa</label>
								<div class="col-sm-10">
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#list-nim-sidang").select2({
												placeholder: 'Daftar Mahasiswa...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
									<select class=""  id="list-nim-sidang" name="id_mhs" required>
									<option></option>
									<optgroup label="Daftar Mahasiswa...">
									  <?php foreach ($listsidang as $row) {		?>	
										<option value="<?php echo $row->ini ?>" ><?php echo $row->nim_mhs." ".$row->nama_mhs  ?></option>
										<?php } ?>
									</select>								
								</div>
								</div>
							
								<hr>
								<div class="form-group">
								<label class="col-sm-2 control-label">Penguji 1</label>
									<div class="col-sm-10">	
										<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#penguji1").select2({
												placeholder: 'Daftar Dosen Penguji...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="" id="penguji1" name="puj1" required>
											<option></option>
										<optgroup label="Daftar Dosen Penguji...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
								</div>
								</div>

								<div class="form-group">
								<label class="col-sm-2 control-label">Penguji 2</label>
									<div class="col-sm-10">	
										<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#penguji2").select2({
												placeholder: 'Daftar Dosen Penguji...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
									
										<select class="" id="penguji2" name="puj2" required>
											<option></option>
										<optgroup label="Daftar Dosen Penguji...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
								</div>
								</div>

							
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
									<div class="col-sm-10">
										
										<input name="tanggal"  class="form control input-append date form_datetime"  type="text"  id="datetimepickerr" readonly  data-date-format="yyyy-mm-dd" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Waktu</label>
									
									<div class="col-sm-10">
										
										<input type="text" class="form-control" id="waktu" name="waktu" required/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Ruangan</label>
									
									<div class="col-sm-10">
										
										<input type="text" class="form-control" id="ruangan" name="ruangan" required/>
									</div>
								</div>
						
						
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info" >Simpan</button>

				</div>
			</form>		
					
    </div>
  </div>
</div>

<!-- edit -->
<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="ModalaEdit">
  <div class="modal-dialog" role="document" style="width: 60%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Jadwal Sidang </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		
						 <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/proses_edit_sidang'; ?>">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim" type="text" class="form-control" id="nim_mhs" disabled>
										<input type="hidden" name="id" id="id_jadwal_de" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" id="nama_mhs" disabled>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<input name="judul" type="text" class="form-control" id="judul_pa" disabled>
									</div>
								</div>
						
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Angkatan</label>
									
									<div class="col-sm-10">
										<input name="angkatan" type="text" class="form-control" id="angkatan" disabled>
									</div>
								</div>
								<div class= "form-group">
									<div class="col-xs-1">
									
								</div>
									
								<div class="col-xs-2">
									<label class="control-label" for="field-1" >Pembimbing 1</label>
								</div>
									
								<div class="col-xs-2"">
									<input type="text" class="form-control" placeholder=".col-xs-3" id="doping1" disabled/>
								</div>
									
								<div class="col-xs-1">
								</div>
									
								<div class="col-xs-1">
								</div>
									
								<div class="col-xs-2">
								<label class="control-label" for="field-1" >Pembimbing 2</label>
								</div>
									
								<div class="col-xs-2"">
								<input type="text" class="form-control" placeholder=".col-xs-3" id="doping2" disabled/>
								</div>
									
								<div class="col-xs-1">
								</div>
									
								<div class="col-xs-1">
								</div>
									
								<div class="col-xs-1">
								</div>
									
								<div class="col-xs-1">
								</div>
									
								<div class="col-xs-1">
								</div>
								</div>
								
								<hr>
									
								<div class="form-group">
								<label class="col-sm-2 control-label">Penguji 1</label>
									<div class="col-sm-10">	
										<select class="js-example-basic-single" id="daftar-puj1" name="puj1" required> 
										<optgroup label="Daftar Dosen Penguji...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
								</div>
								</div>
								<input type="hidden" id="puj1-lama" name="puj1-lama">
								<input type="hidden" id="puj2-lama" name="puj2-lama">

								<div class="form-group">
								<label class="col-sm-2 control-label">Penguji 2</label>
									<div class="col-sm-10">	
										<select class="js-example-basic-single" id="daftar-puj2" name="puj2" required>
										<optgroup label="Daftar Dosen Penguji...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama  ?></option>
										<?php } ?>
										</optgroup>
									</select>
								</div>
								</div>						

								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Tanggal</label>
									
									<div class="col-sm-10">
										
										<input name="tanggal"  class="form control input-append date form_datetime"  type="text"  id="datetimepicker" readonly  data-date-format="yyyy-mm-dd" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Waktu</label>
									
									<div class="col-sm-10">
										
										<input type="text" class="form-control" id="waktu" name="waktu" required/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Ruangan</label>
									
									<div class="col-sm-10">
										
										<input type="text" class="form-control" id="ruangan" name="ruangan" required/>
									</div>
								</div>
						
						
						
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-info" >Simpan</button>

				</div>
			</form>		
					
    </div>
  </div>
</div>	
<!-- end edit -->
<script type="text/javascript">
$(document).ready(function(){
		var date = new Date();
		date.setDate(date.getDate());

		$('#datetimepicker').datetimepicker({
			weekStart  : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 3	,
			minView : 2,
			forceParse : 0
		});

		$('#datetimepicker').datetimepicker('setStartDate', date);

		$('#datetimepickerr').datetimepicker({
		weekStart  : 1,
		todayBtn : 1,
		autoclose : 1,
		todayHighlight : 1,
		startView : 3	,
		minView : 2,
		forceParse : 0
		});

		$('#datetimepickerr').datetimepicker('setStartDate', date);
    view_data();   
	$("#example-3").dataTable().yadcf([
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3},
							{column_number : 4},
							{column_number : 5},
							{column_number : 6},
							{column_number : 7},
							
	]);
				

    function view_data(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo base_url()?>koor_pa/data_jadwal_sidang',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                	var x = i+1;
                           	
                    html += '<tr>'+
                    		'<td>'+x+'</td>'+
                            '<td>'+data[i].nim_mhs+'</td>'+
                            '<td>'+data[i].nama_mhs+'</td>'+
                            '<td>'+data[i].tanggal+'</td>'+
                            '<td>'+data[i].waktu+'</td>'+
                            '<td>'+data[i].kd1+'</td>'+
                            '<td>'+data[i].kd2+'</td>'+
                            '<td>'+data[i].ruangan+'</td>'+
                            '<td style="text-align:right;">'+
                            '<a href="javascript:;" class="btn btn-secondary fa-refresh icon-left item_edit" data="'+data[i].id_jadwal_sidang+'"></a>'+
                            '<button onClick="CheckDelete(<?= "'+ data[i].id_jadwal_sidang+'"?>);" class="btn btn-danger fa-trash icon-left" data="'+data[i].id_jadwal_sidang+'"></button>'+
                            '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });

    }


		//GET UPDATE
		$('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('koor_pa/edit_jadwal_sidang')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                	$.each(data,function(nim_mhs,nama_mhs,judul_pa,ang,tahunajaran,id_doping1,id_doping2,grup,tahap,id_mhs,puj1,puj2,periode,tanggal,id_jadwal_sidang,waktu,ruangan){
                    	$('#ModalaEdit').modal('show');
            			$('[name="nim"]').val(data.nim_mhs);
            			$('[name="id"]').val(data.id_jadwal_de);
            			$('[name="nama"]').val(data.nama_mhs);
            			$('[name="judul"]').val(data.judul_pa);
            			$('[name="angkatan"]').val(data.ang);
            			$('[name="tanggal"]').val(data.tanggal);
            			$('[name="waktu"]').val(data.waktu);
            			$('[name="ruangan"]').val(data.ruangan);
            			$('[name="periode"]').val(data.periode);
            			$('[name="puj1-lama"]').val(data.puj1);
            			$('[name="puj2-lama"]').val(data.puj2);
            			$('#doping1').val(data.id_doping1).trigger('change');
            			$('#doping2').val(data.id_doping2).trigger('change');
            			$('#daftar-puj1').val(data.puj1).trigger('change');
            			$('#daftar-puj2').val(data.puj2).trigger('change');

            		});
                }
            });
            return false;
        }); 


 	$('.js-example-basic-single').select2();
        		

    });	

    function CheckDelete(id) {
  alertify.confirm("Konfirmasi Aksi","Data Mahasiswa PA akan dihapus ?",
    function(input) {
      if (input) {
		alertify.success('Data Berhasil Dihapus');       
        window.location.href = "<?php echo base_url(); ?>koor_pa/del_jadwal/Sidang/t_jadwal_sidang//"+id;
      } else {
        alertify.error('Cancel');
      }
    }, function(){alertify.error('Cancel');});
}	
 </script>

	

<?php
		if($this->session->flashdata('msg') == 'puj1=puj2')
	{	?>
	<script type="text/javascript">
			alertify
  		.alert("Err Or","Data tidak tersimpan<br> Dosen Penguji 1 tidak boleh sama dengan Dosen Penguji 2" ,function(){
   		
  		});
	</script>
<?php }	?>
<?php if($this->session->flashdata('msg') == 'ok') { ?>	
<script type="text/javascript">
$(document).ready(function(){
var opts = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-bottom-right",
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "0",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

toastr.success("Data berhasil diunggah", "Operasi Sukses", opts);
	});
</script>
<?php } ?>
</body>
</html>


<?php if($this->session->flashdata('msg') == 'Ada kesalahan dalam proses upload') { ?>	
<script type="text/javascript">
	$(document).ready(function(){
	var opts = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-full-width",
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "0",
		"extendedTimeOut": "0",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};

	toastr.error("File gagal diunggah, Pastikan header/data/format sesuai template dan Ukuran file tidak lebih dari 100 mb", "Hmm.. File Unggahan Anda Bermasalah", opts);
	});
</script>
<?php } ?>


</body>
</html>