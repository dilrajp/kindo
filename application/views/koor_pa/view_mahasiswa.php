<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">
<body class="page-body">
<?php
$this->load->view('fragment/sidebar_koorpa');
?>
		
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
					<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
				
					<?php $this->load->view('fragment/change_login'); ?>
					<?php $this->load->view('fragment/notif_koorpa'); ?>
					
						</ul>
					</li>
					
				</ul>
				
						</ul>
					</li>
					
				</ul>
				
				<ul class="user-info-menu right-links list-inline list-unstyled">									
					<?php  $this->load->view('fragment/user_profile');?>
				</ul>
				
			</nav>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Daftar Mahasiswa</h1>
					<p class="description">Halaman data mahasiswa proyek akhir</p>
				</div>
				
				
					
					<div class="breadcrumb-env">
						<ol class="breadcrumb auto-hidden">
						<li>
							<a href="<?php echo base_url('koor_pa');?>"><i class="fa-home"></i>Home</a>
						</li>
						<li class="active">
							<strong>Mahasiswa Proyek Akhir</strong>
						</li>
						</ol>
					</div>
												
				
					
			</div>
				<a href="javascript:;"onclick="jQuery('#mod-1').modal('show', {backdrop: 'fade'});" 			class="btn btn-red btn-icon btn-icon-standalone">
				<i class="fa-pencil"></i>
				<span>Add Mahasiswa</span>
			</a>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data Mahasiswa Proyek Akhir</h3>
					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>

				<div class="panel-body">
						
				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-1").dataTable().yadcf([
						
							
							{column_number : 1, filter_type: 'text'},
							{column_number : 2, filter_type: 'text'},
							{column_number : 3, filter_type: 'text'},
							{column_number : 5, filter_type: 'text' },
							{column_number : 4, },
							{column_number : 6, },
							
						]);
					});
					</script>
				
					
					<table class="table table-striped table-bordered" id="example-1">
						<thead>
							<tr class="replace-inputs">
								<th width="7%">No</th>
								<th>NIM</th>
								<th width="15%">Nama</th>
								<th width="30%">Judul PA</th>
								<th>Tahun</th>
								<th width="8%">Grup</th>
								<th> Tahap </th>
								<th> Action </th>
							</tr>
						</thead>
						 <?php  ?>
						<tbody>
							<?php 
							$no = 0;
					        foreach($rows as $x ) {
					      	$no++;
					        ?>
							<tr>
								
								<td width="7%"><?php echo $no;?></td>
								<td><?php echo $x->nim_mhs;?></td>
								<td><?php echo $x->nama_mhs;?></td>
								<td><?php echo $x->judul_pa;?></td>
								<td><?php echo nama_tahun($x->id_tahunajaran);?></td>
								<td width="8%" align="center"><?php echo $x->grup;?></td>
								<td><?php 
								if($x->tahap == 1){
									echo "DE";
								}else if($x->tahap == 2){
									echo "Pengajuan SK";
								}else if($x->tahap == 3){
									echo "Sidang";
								}else if($x->tahap == 4){
									echo "Revisi";
								}else if($x->tahap == 5){
									echo "Yudisium";
								}

								;?></td>
								<td>
									<a href="javascript:;" onclick="jQuery('#modl-<?php echo $x->id_mhs; ?>').modal('show', {backdrop: 'fade'});" 
									class="btn btn-secondary btn-sm el-arrows-cw icon-left"> </a>
									<a href="javascript:;" onclick="jQuery('#modal-<?php echo $x->id_mhs; ?>').modal('show', {backdrop: 'fade'});" 
									class="btn btn-info btn-sm el-search icon-left"> </a>
									<button onClick="CheckDelete(<?= $x->id_mhs; ?>);" class="btn btn-danger btn-sm el-trash icon-left">	</button>
										
								</td>
							</tr>
						<?php } ?>
				
						</tbody>	
							
					</table>
					
				
				</div>
			</div>	


			
	
			
			
			<!-- Table exporting -->
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2018
						<strong>Manajemen Informatika</strong> 
						theme by <a href="http://laborator.co" target="_blank">Laborator</a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
	</div>

	
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.css')?>">
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js');?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
     <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.js')?>"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker/bootstrap-datetimepicker.uk.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2-bootstrap.css')?>"">
	<link rel="stylesheet" href="<?php echo base_url('assets/js/multiselect/css/multi-select.css')?>""> 
	<script src="<?php echo base_url('assets/js/select2/select2.min.js')?>""></script>


	<?php
	$this->load->view("fragment/foot");
	
	if($alert == 'fail')
	{	?>
		<script type="text/javascript">
		alertify
  		.alert("Error DR4<br> Dosen Pembimbing 1 tidak boleh sama dengan Dosen Pembimbing 2", function(){
   		
  		});
	</script>
<?php } else if($this->session->flashdata('msg') == 'Sukses ...!!') { ?>
<script type="text/javascript">
	  alertify.success('Data Berhasil Ditambahkan');
</script>
<?php }	?>


<script type="text/javascript">
	
function CheckDelete(id) {
  alertify.confirm("Confirmation Message","Data Mahasiswa PA akan dihapus ?",
    function(input) {
      if (input) {
		alertify.success('Data Berhasil Dihapus');       
        window.location.href = "<?php echo base_url(); ?>koor_pa/del_mhs/"+id;
      } else {
        alertify.error('Cancel');
      }
    }, function(){alertify.error('Cancel');});
}

</script>	

<div class="modal fade  custom-width" tabindex="-1" role="dialog" id="mod-1">
  <div class="modal-dialog" role="document" style="width: 60%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Undangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       		<form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/add_mhs'; ?>">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim" type="text" class="form-control" id="field-1" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" required>
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<input name="judul" type="text" class="form-control" id="field-3" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Grup</label>
									
									<div class="col-sm-10">
										<input name="grup" type="text" class="form-control" id="field-3" required="required">
									</div>
								</div>
								
								

						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 1</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-1").select2({
												placeholder: 'Daftar Mata Kuliah yang tersedia...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="s2example-1" required name="p1">
										<option></option>
										<optgroup label="Daftar Matakuliah">
										<?php foreach ($nama as $row) {		?>	
										<option value="<?php echo $row->id_dosen ?>" ><?php echo $row->kode_dosen." ".$row->nama_dosen  ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 2</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-2").select2({
												placeholder: 'Daftar Dosen Pembimbing...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="s2example-2" required name="p2">
										<option></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row) {		?>	
											<option value="<?php echo $row->id_dosen ?>" ><?php echo $row->kode_dosen." ".$row->nama_dosen ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahun</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-3").select2({
												placeholder: 'Daftar Tahun Ajaran...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control" id="s2example-3" required name="p3">
										<option></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($tahun as $row) {		?>	
											<option value="<?php echo $row->id_tahunajaran ?>" ><?php echo nama_tahun($row->id_tahunajaran); ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
		      	</div>
    	  		<div class="modal-footer">
    			    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Save changes</button>

				</div>

					</form>
    </div>
  </div>
</div>


	<?php if($rows){ foreach($rows as $row){ ?>
		<div class="modal fade custom-width" id="modal-<?php echo $row->id_mhs; ?>">
			<div class="modal-dialog"  style="width: 60%;">
				<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">View Data Mahasiswa</h4>
						</div>
						
						<div class="modal-body">
	
							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">Nama</label>
								
								<div class="col-sm-8">
									<label class="form-control" id="field-2" ><?php echo $row->nama_mhs; ?> </label>
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-1">NIP</label>
								
								<div class="col-sm-8">
									<label class="form-control" id="field-2" ><?php echo $row->nim_mhs; ?> </label>
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-3">Judul Proyek Akhir</label>
								
								<div class="col-sm-8">
								<label class="form-control" id="field-2" style="height:30%;width:100%"><?php echo $row->judul_pa; ?> </label>
								</div>

							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="field-4">Grup:</label>
								<div class="col-sm-8">
								<label class="form-control"  style="width: 8%;"><?= $row->grup;; ?></label>
								</div>

							</div>
							
						
							<div class="form-group" style="margin-bottom: 40px">
								<label class="col-sm-3 control-label" >Pembimbing 1:</label>
								<label class="col-sm-2 control-label" style="width: 0%;margin-right: 80px" ><b><?php echo getkode($row->id_doping1); ?></b></label>
								<label class="col-sm-3 control-label" >Pembimbing 2:</label>
								<label class="col-sm-2 control-label" style="width: 0%" ><b><?= getkode($row->id_doping2); ?></b></label>
								<br>
								
							
							</div>

								<Br><br><br><Br><br><Br>
					
						
						<div class="form-group">
							
								
								&nbsp;&nbsp;&nbsp;&nbsp;<b>Progress</b><br>
								<div class="col-md-12">
									
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Desk Evaluation">
											<span class="sr-only">15% Complete (success)</span>
										</div>
										<?php if ($row->tahap == 2) { ?>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 20%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<?php } else if ($row->tahap == 3) { ?>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 20%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<div class="progress-bar progress-bar-danger" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sidang">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<?php } else if ($row->tahap == 4){ ?>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 20%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<div class="progress-bar progress-bar-danger" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sidang">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<div class="progress-bar progress tooltip-secondary" style="width: 20%" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Revisi">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<?php } else if ($row->tahap == 5){ ?>
										<div class="progress-bar progress-bar-warning tooltip-secondary" style="width: 20%"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pengajuan SK">
											<span class="sr-only">10% Complete (warning)</span>
										</div>
										<div class="progress-bar progress-bar-danger" style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sidang">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<div class="progress-bar progress tooltip-secondary" style="width: 20%" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Revisi">
											<span class="sr-only">18% Complete (danger)</span>
										</div>
										<div class="progress-bar progress-bar-info " style="width: 20%" data-toggle="tooltip" data-placement="top" title="" data-original-title="Yudisium"	>
											<span class="sr-only">25% Complete (info)</span>
										</div>
										<?php } ?>
									</div>
									
								</div>
								
							</div>	
					</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	<?php } } ?>
<script type="text/javascript">
									$(document).ready(function() {
									    $('.js-example-basic-single').select2();
									});
									</script>
		<?php if($rows){ foreach($rows as $row){ ?>
		<div class="modal fade custom-width" id="modl-<?php echo $row->id_mhs; ?>">
			<div class="modal-dialog"  style="width: 60%;">
				<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">View Data Mahasiswa</h4>
						</div>
						
						<div class="modal-body">
	
							    <form role="form" method="POST" class="form-horizontal" action="<?php echo base_url(). 'koor_pa/edit_mhs'; ?>">
							    	<input type="hidden" name="id" value="<?php echo $row->id_mhs ?>">
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">NIM Mahasiswa</label>
									
									<div class="col-sm-10">
										<input name="nim" value="<?php echo $row->nim_mhs; ?>" type="text" class="form-control" id="field-1" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama Mahasiswa</label>
									<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" required value="<?php echo $row->nama_mhs; ?>">
								
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Judul PA</label>
									
									<div class="col-sm-10">
										<input name="judul" type="text" class="form-control" id="field-3" required="required" value="<?php echo $row->judul_pa; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="field-1">Grup</label>
									
									<div class="col-sm-10">
										<input name="grup" type="text" class="form-control" id="field-3" value="<?php echo $row->grup; ?>">
									</div>
								</div>
								
								

						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 1</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-10").select2({
												placeholder: 'Daftar Mata Kuliah yang tersedia...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control js-example-basic-single" required name="p1">
										<option value="<?php echo $row->id_doping1; ?>"><?php echo getnamabyid($row->id_doping1); ?></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row2) {		?>	
										<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama_dosen  ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
						<div class="form-group">
									<label class="col-sm-2 control-label">Pembimbing 2</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-20").select2({
												placeholder: 'Daftar Dosen Pembimbing...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control js-example-basic-single"  required name="p2">
										<option value="<?php echo $row->id_doping2; ?>"><?php echo getnamabyid($row->id_doping2); ?></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($nama as $row2) {		?>	
											<option value="<?php echo $row2->id_dosen ?>" ><?php echo $row2->kode_dosen." ".$row2->nama_dosen ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahun</label>
									
									<div class="col-sm-10">	
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-30").select2({
												placeholder: 'Daftar Tahun Ajaran...',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<select class="form-control js-example-basic-single" required name="p3">
										<option value="<?php echo $row->id_tahunajaran; ?>"><?php echo  nama_tahun($row->id_tahunajaran);?></option>
										<optgroup label="Daftar Dosen Pembimbing...">
										<?php foreach ($tahun as $row2) {		?>	
											<option value="<?php echo $row2->id_tahunajaran ?>" ><?php echo nama_tahun($row2->id_tahunajaran); ?></option>
										<?php } ?>
										</optgroup>
									</select>
									</div>	
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tahap</label>
									
									<div class="col-sm-10">	
								
										<select class="form-control js-example-basic-single"  required name="tahap">
										<option value="<?php echo $row->tahap; ?>"><?php echo  nama_tahap($row->tahap);?></option>
										<optgroup label="Daftar Tahap Proyek Akhir">
										<option value="1" >Desk Evaluation</option>
										<option value="2" >Pengajuan SK</option>
										<option value="3" >Sidang</option>
										<option value="4" >Revisi</option>
										<option value="5" >Yudisium</option>
									
										</optgroup>
										</select>
									</div>	
								</div>
								
						
					</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-white" >Save Data</button>
					</form>
					</div>
				</div>
			</div>
		</div>
	<?php } } ?>