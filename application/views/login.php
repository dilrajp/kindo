<?php 
$this->load->view("fragment/head");
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/login/css/style.css') ?>">


  <div id="particles-js"></div>
<body class="page-body login-page login-light">

	

	<br><br><Br><br><br><Br><br>
	<div class="login-container">
	
		<div class="row">
		
			<div class="col-sm-6">
			
				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						// Reveal Login form
						setTimeout(function(){ $(".fade-in-effect").addClass('in'); }, 1);
						
						
						// Validation and Ajax action
						$("form#login").validate({
							rules: {
								username: {
									required: true
								},
								
								passwd: {
									required: true
								}
							},
							
							messages: {
								username: {
									required: 'Please enter your username.'
								},
								
								passwd: {
									required: 'Please enter your password.'
								}
							},
							
							// Form Processing via AJAX
							submitHandler: function(form)
							{
								show_loading_bar(70); // Fill progress bar to 70% (just a given value)
								
								var opts = {
									"closeButton": true,
									"debug": false,
									"positionClass": "toast-top-full-width",
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"timeOut": "5000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut"
								};
									
								$.ajax({

									url: "<?php echo site_url('login/check')?>",
									method: 'POST',
									dataType: 'json',
									data: {
										do_login: true,
										username: $(form).find('#username').val(),
										passwd: $(form).find('#passwd').val(),
									},
									success: function(resp)
									{
										show_loading_bar({
											delay: .5,
											pct: 100,
											finish: function(){
												
												// Redirect after successful login page (when progress bar reaches 100%)
												if(resp.accessGranted)
												{
													 
													if(resp.level == '1'){	
													window.location = 'dosen' 
													} else if(resp.level == '2'){
														window.location = 'dosen'
													}else if(resp.level == '3'){
														window.location = 'dosen'
													}else if(resp.level == '4'){
														window.location = 'dosen'
													}else if(resp.level == '5'){
														window.location = 'dosen'
													}												
													
												}
																						}
										});
										
																			
										// Remove any alert
										$(".errors-container .alert").slideUp('fast');
										
										
										// Show errors
										if(resp.accessGranted == false)
										{
											$(".errors-container").html('<div class="alert alert-danger">\
												<button type="button" class="close" data-dismiss="alert">\
													<span aria-hidden="true">&times;</span>\
													<span class="sr-only">Close</span>\
												</button>\
												' + resp.errors + '\
											</div>');
											
											
											$(".errors-container .alert").hide().slideDown();
											$(form).find('#passwd').select();
										}
																		}
								});
								
							}
						});
						
						// Set Form focus
						$("form#login .form-group:has(.form-control):first .form-control").focus();
					});
				</script>
				
				<!-- Errors container -->
				<div class="errors-container">
				
									
				</div>
				
				<!-- Add class "fade-in-effect" for login form effect -->
				<form method="post" role="form" id="login" class="login-form fade-in-effect"
				action="<?php echo $action ?>">
					
					<div class="login-header">
						
							<img src="<?php echo base_url('assets/images/gambar/logo.png');?>" alt="" width="80" />
							<span></span>
					
						
						<font color='black'><p>Aplikasi Pengelolaan Pencapaian Kinerja Dosen</p>
						<b><p>Semester Aktif: <?php echo nama_semester(getsemester_aktif()).' '.nama_tahun(gettahun_aktif());   ?></font></b>

					</div>
	
					
					<div class="form-group">
						<label class="control-label" for="username">Username</label>
						<input type="text" class="form-control" name="username" id="username" autocomplete="off" />
					</div>
					
					<div class="form-group">
						<label class="control-label" for="passwd">Password</label>
						<input type="password" class="form-control" name="passwd" id="passwd" autocomplete="off" />
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary  btn-block text-left">
							<i class="fa-lock"></i>
							Log In
						</button>
					</div>
					
				<!-- 	<div class="login-footer">
						<a href="#">Forgot your password?</a>
						
						<div class="info-links">
							<a href="#">ToS</a> -
							<a href="#">Privacy Policy</a>
						</div>
						
					</div>
					 -->
				</form>
				
			
				
			</div>
			
		</div>
		
	</div>
</div>

	<script src="<?php echo base_url('assets/js/xenon-custom.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js')?>"></script> 
	<script src="<?php echo base_url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo base_url('assets/js/xenon-toggles.js')?>"></script>
	<script src="<?php echo base_url('assets/js/datatables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.min.js')?>"></script>

	<script src="<?php echo base_url('assets/js/toastr/toastr.min.js')?>"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url('assets/js/xenon-custom.js')?>"></script>
	<script src='https://cldup.com/S6Ptkwu_qA.js'></script>
<script  src="<?php echo base_url('assets/login/js/index.js'); ?>"></script>
	
<?php $this->load->view("fragment/footer");?>
</body>

</html>