<?php

function getnama($id)
{
    $ci=& get_instance();
    $q = $ci->db->query("select nama from t_dosen where nip='$id'")->row_array();
    return $q['nama'];
}




function cek_nilai($id){
 	$ci=& get_instance();
	$dosen = $ci->session->id_dosen;
	$kolom = '';
	if($id == '14'){
		$kolom = 'nilai_perwalian as xyz';
	}
	if($id == '15'){
		$kolom = 'nilai_lks as xyz';
	}
    $ci->db->select($kolom);
    $ci->db->from('t_nilai');
    $ci->db->where('id_dosen',$ci->session->id_dosen);
    $q = $ci->db->get()->row_array();
 	 if(isset($q['xyz'])){
    	return 'xx';
    }else{
    	return 'yy';
    }
    //return print_r($q);
}


function total_kpi_perwalian($id){
	$ci=& get_instance();
    $q = $ci->db->query("SELECT kondisi FROM `t_kategori` WHERE  id_kpi = '$id' ORDER BY kondisi DESC LIMIT 1")->row_array();
    return $q['kondisi'];
}
function tanggalmati($id){
	$ci=& get_instance();
    $q = $ci->db->query("SELECT kondisi as 'tanggal' FROM `t_kategori` WHERE  id_kpi = '$id'")->row_array();
    return $q['tanggal'];
}

function getprodi($id)
{
	$ci =& get_instance();
	$q = $ci->db->query("select prodi from t_dosen where id_dosen='$id'")->row_array();
    return $q['prodi'];
}

function cek_kpi($id){
	$ci=& get_instance();
	$q = $ci->db->query("SELECT * from t_kpi where status='ON' and id_kpi ='$id'");
	if($q->num_rows() > 0){
	return true;	
	};
	return false;
}



function nama_tahap($id){
	if($id == 1){
		return "Desk Evaluation";
	}else if($id == 2){
		return "input Theta";
	}else if($id == 3){
		return "Pra-Sidang";
	}else if($id == 4){
		return "Revisi";
	}else if($id == 5){
		return "Yudisium";
	}
}


function total_bimb($id){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total1' from t_dosen join t_mhs_pa
					 on(id_doping1 = id_dosen) where id_doping1 = '$id' ")->row_array();
	$qu = $ci->db->query("SELECT COUNT(nim_mhs) as 'total2' from t_dosen join t_mhs_pa
					 on(id_doping2 = id_dosen) where id_doping2 = '$id' ")->row_array();
	$total = $q['total1'] + $qu['total2'];
	return $total;
}

function total_bimb_tahun($id){
	$ci =& get_instance();
	$tahun = gettahun_aktif();
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total1' from t_dosen join t_mhs_pa
					 on(id_doping1 = id_dosen) where id_doping1 = '$id' and id_tahunajaran ='$tahun'")->row_array();
	$qu = $ci->db->query("SELECT COUNT(nim_mhs) as 'total2' from t_dosen join t_mhs_pa
					 on(id_doping2 = id_dosen) where id_doping2 = '$id' and id_tahunajaran ='$tahun'")->row_array();
	$total = $q['total1'] + $qu['total2'];
	return $total;
}

function total_bimb_all($id){
	$ci =& get_instance();
	$tahap = 7;
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total1' from t_dosen join t_mhs_pa
					 on(id_doping1 = id_dosen) where id_doping1 = '$id' and tahap != '7'")->row_array();
	$qu = $ci->db->query("SELECT COUNT(nim_mhs) as 'total2' from t_dosen join t_mhs_pa
					 on(id_doping2 = id_dosen) where id_doping2 = '$id' and tahap != '7'")->row_array();
	$total = $q['total1'] + $qu['total2'];
	return $total;
}

function total_bimb_tahap($id,$tahap){
	$ci =& get_instance();
	$tahap = $tahap;
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total1' from t_dosen join t_mhs_pa
					 on(id_doping1 = id_dosen) where id_doping1 = '$id' and tahap = '$tahap'")->row_array();
	$qu = $ci->db->query("SELECT COUNT(nim_mhs) as 'total2' from t_dosen join t_mhs_pa
					 on(id_doping2 = id_dosen) where id_doping2 = '$id' and tahap = '$tahap'")->row_array();
	$total = $q['total1'] + $qu['total2'];
	return $total;
} 

function total_menguji($id,$tabel){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT COUNT(id_mhs) as 'total1' from t_dosen 
						join $tabel on(PUJ1 = id_dosen)
						where PUJ1 = '$id' ")->row_array();
	$qu = $ci->db->query("SELECT COUNT(id_mhs) as 'total2' from t_dosen 
						join $tabel on(PUJ2 = id_dosen)
						where PUJ2 = '$id' ")->row_array();
	$total = $q['total1'] + $qu['total2'];
	return $total;
}

function total_pembimbing1($id){
	$ci =& get_instance();
	$tahun = gettahun_aktif();
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total1' from t_dosen join t_mhs_pa
					 on(id_doping1 = id_dosen) where id_doping1 = '$id' and id_tahunajaran = '$tahun' ")->row_array();
	return $q['total1'];
	}
function total_pembimbing2($id){
	$ci =& get_instance();
	$tahun = gettahun_aktif();
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total2' from t_dosen join t_mhs_pa
					 on(id_doping2 = id_dosen) where id_doping2 = '$id' and id_tahunajaran = '$tahun' ")->row_array();
	return $q['total2'];
	}

function total_pembimbing1_v2($id){
	$ci =& get_instance();
	$tahun = gettahun_aktif();
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total1' from t_dosen join t_mhs_pa
					 on(id_doping1 = id_dosen) where id_doping1 = '$id' and tahap != 7")->row_array();
	return $q['total1'];
	}
function total_pembimbing2_v2($id){
	$ci =& get_instance();
	$tahun = gettahun_aktif();
	$q = $ci->db->query("SELECT COUNT(nim_mhs) as 'total2' from t_dosen join t_mhs_pa
					 on(id_doping2 = id_dosen) where id_doping2 = '$id' and tahap != 7")->row_array();
	return $q['total2'];
	}


function convertkode($kd){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_dosen from t_dosen where kode_dosen = '$kd' ")->row_array();
	return $q['id_dosen'];
}
function checkcode($kd){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_dosen from t_dosen where kode_dosen = '$kd' ")->num_rows();
	if ($q > 0) {
		return true;
	}else{
		return false;
	}	
}

function convertidmhs($nim){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_mhs FROM t_mhs_pa WHERE nim_mhs = '$nim'")->row_array();
	return $q['id_mhs'];
}

function getangkatan($nim){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT angkatan FROM t_mhs_pa WHERE id_mhs = '$nim'")->row_array();
	return $q['angkatan'];
}

function getp1($nim){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_doping1 FROM t_mhs_pa WHERE id_mhs = '$nim'")->row_array();
	return $q['id_doping1'];
}
function getp2($nim){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_doping2 FROM t_mhs_pa WHERE id_mhs = '$nim'")->row_array();
	return $q['id_doping2'];
}

function convertmhs($nim,$row){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT $row FROM t_mhs_pa WHERE id_mhs = '$nim'")->row_array();
	return $q[$row];
}


function getkode($id){

	$ci =& get_instance();
	$q = $ci->db->query("SELECT kode_dosen from t_dosen where id_dosen = '$id' ")->row_array();
	return $q['kode_dosen'];
}
function getstatus($id){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT status from t_dosen where id_dosen = '$id' ")->row_array();
	return $q['status'];
}
function getnamabyid($id){
	$ci=& get_instance();
    $q = $ci->db->query("select nama from t_dosen where id_dosen='$id'")->row_array();
    return $q['nama'];
}

function getnipbyid($id){
	$ci=& get_instance();
    $q = $ci->db->query("select nip from t_dosen where id_dosen='$id'")->row_array();
    return $q['nip'];
}
function convertnip($id){
	$ci=& get_instance();
    $q = $ci->db->query("select id_dosen from t_dosen where id_dosen='$id'")->row_array();
    return $q['id_dosen'];	
}	

function getiddosen($nama){
	$ci=& get_instance();
    $q = $ci->db->query("select id_dosen from t_dosen where nama ='$nama'")->row_array();
    return $q['id_dosen'];	
}

function getiddosenkode($nama){
	$ci=& get_instance();
    $q = $ci->db->query("select id_dosen from t_dosen where kode_dosen='$nama'")->row_array();
    return $q['id_dosen'];	
}


function getfoto($id)
{
    $ci=& get_instance();
    $q = $ci->db->query("select picture from t_dosen where id_dosen='$id'")->row_array();
    return $q['picture'];
}
function namarapat($id){
	if($id == '1'){
		$nama = "Koordinasi MK";
	}else {
		$nama = "Prodi";
	}
	return $nama;
}
function get_level($id){
	if($id == '1'){
		$nama = "Dosen";
	}else if($id == '2'){
		$nama = "Koor MK";
	}else if($id == '3') {
		$nama = "Koor PA";
	}elseif($id='4'){
		$nama = "Ka. Prodi";
	}else{
		$nama = "Admin";
	}
	return $nama;
}

function getsemester_aktif(){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_semester from t_semester where status='Aktif'")->row_array();
	return $q['id_semester'];
}
function gettahun_aktif(){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT t_tahunajaran.id_tahunajaran from t_tahunajaran join t_semester on(t_semester.id_tahunajaran = t_tahunajaran.id_tahunajaran) where status = 'Aktif' ")->row_array();
	return $q['id_tahunajaran'];
}

function nama_semester($id){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT semester from t_semester where id_semester='$id'")->row_array();
	return $q['semester'];
}
function nama_tahun($id){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT tahunajaran from t_tahunajaran where id_tahunajaran='$id'")->row_array();
	return $q['tahunajaran'];
}
function nama_dosen(){
	$ci=& get_instance();
    $q = $ci->db->query("select nama from t_dosen ")->result();
    return $q;
}

function nama_matkul($id){
	$ci=& get_instance();
    $q = $ci->db->query("SELECT nama_matakuliah from t_matakuliah where id_matakuliah='$id' ")->row_array();
    return $q['nama_matakuliah'];
}
function jenis_nilai($id){
	$ci=& get_instance();
    $q = $ci->db->query("SELECT jenis_nilai from t_deadlineusulan where id_deadlineusulan='$id' ")->row_array();
    return $q['jenis_nilai'];
}
function jenis_nilai2($id){
	$ci=& get_instance();
    $q = $ci->db->query("SELECT jenis_nilai from t_deadlinenilai where id_deadlinenilai='$id' ")->row_array();
    return $q['jenis_nilai'];
}
function id_dosen(){
	$ci=& get_instance();
    $q = $ci->db->query("select id_dosen from t_dosen ")->result();
    return $q;
}
function rps($id){
	$ci=& get_instance();
	$q = $ci->db->query("SELECT * from t_rps join t_matakuliah ON t_rps.id_matakuliah = t_matakuliah.id_matakuliah join t_pengajaran ON t_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah join t_dosen ON t_pengajaran.id_dosen = t_dosen.id_dosen where t_pengajaran.status_koordinator ='y' and  t_dosen.id_dosen = $id")->num_rows();
	if($q > 0){
	return true;	
	};
	return false;
	return $q;
}
function modul($id){
	$ci=& get_instance();
	$q = $ci->db->query("SELECT * from t_modul join t_matakuliah ON t_modul.id_matakuliah = t_matakuliah.id_matakuliah join t_pengajaran ON t_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah join t_dosen ON t_pengajaran.id_dosen = t_dosen.id_dosen where t_pengajaran.status_koordinator ='y' and  t_dosen.id_dosen = $id")->num_rows();
	if($q > 0){
	return true;	
	};
	return false;
	return $q;
}
function bahan($id){
	$ci=& get_instance();
	$q = $ci->db->query("SELECT * from t_bahanperkuliahan join t_matakuliah ON t_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah join t_pengajaran ON t_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah join t_dosen ON t_pengajaran.id_dosen = t_dosen.id_dosen where t_pengajaran.status_koordinator ='y' and  t_dosen.id_dosen = $id")->num_rows();
	if($q > 0){
	return true;	
	};
	return false;
	return $q;
}
function soal($id){
	$ci=& get_instance();
	$q = $ci->db->query("SELECT * from t_soal join t_matakuliah ON t_soal.id_matakuliah = t_matakuliah.id_matakuliah join t_pengajaran ON t_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah join t_dosen ON t_pengajaran.id_dosen = t_dosen.id_dosen where t_pengajaran.status_koordinator ='y' and  t_dosen.id_dosen = $id")->num_rows();
	if($q > 0){
	return true;	
	};
	return false;
	return $q;
}
function trps(){
	$ci = get_instance();
	$q = $ci->db->query("select * from t_rps")->num_rows();
	if($q > 0){
	return 100;	
	};
	return 0;
	return $q;
}
function tmodul(){
	$ci = get_instance();
	$q = $ci->db->query("select * from t_modul")->num_rows();
	if($q > 0){
	return 100;	
	};
	return 0;
	return $q;
}
function tsoal(){
	$ci = get_instance();
	$q = $ci->db->query("select * from t_soal")->num_rows();
	if($q > 0){
	return 100;	
	};
	return 0;
	return $q;
}
function tbahan(){
	$ci = get_instance();
	$q = $ci->db->query("select * from t_bahanperkuliahan")->num_rows();
	if($q > 0){
	return 100;	
	};
	return 0;
	return $q;
}


// function rapat_notif_dosen($id){
// 	$ci=& get_instance();
// 	$q = $ci->db->query("select id_dosen from t_kehadiran where id_dosen ='$id' AND status = 'UNREAD'");
// 	if($q->num_rows() > 0){
// 		return 1;
// 	}else {
// 		return 0;
// 	}
// }
// function total_notif_modul($id){
// 	$ci=& get_instance();
// 	$q = $ci->db->query("select id_statusmodul from t_statusmodul where id_dosen ='$id' AND status = 'SETUJU'");
// 	if($q->num_rows() > 0){
// 		return 1;
// 	}else {
// 		return 0;
// 	}
// }
// function total_notif_soal($id){
// 	$ci=& get_instance();
// 	$q = $ci->db->query("select id_statussoal from t_statussoal where id_dosen ='$id' AND status = 'SETUJU'");
// 	if($q->num_rows() > 0){
// 		return 1;
// 	}else {
// 		return 0;
// 	}
// }
// function total_notif_rps($id){
// 	$ci=& get_instance();
// 	$q = $ci->db->query("select id_statusrps from t_statusrps where id_dosen ='$id' AND status = 'SETUJU'");
// 	if($q->num_rows() > 0){
// 		return 1;
// 	}else {
// 		return 0;
// 	}
// }
// function total_notif_bahan($id){
// 	$ci=& get_instance();
// 	$q = $ci->db->query("select id_statusbahan from t_statusbahan where id_dosen ='$id' AND status = 'SETUJU'");
// 	if($q->num_rows() > 0){
// 		return 1;
// 	}else {
// 		return 0;
// 	}
// }

// function total_notif($x,$y,$z,$v,$w){
// 	$ci=& get_instance();
// 	$total2 = $x+$y+$z+$v+$w;
// 	return $total2;
// }



//notif koordinator matakuliah

// function total_notif_koor($id){
// 	$ci=& get_instance();
// 	$a = $ci->db->query("select id_statususulan from t_statususulan where id_dosen ='$id' AND status = 'UNREAD'")->num_rows();
// 	$b = $ci->db->query("select id_statusnilaiasessment from t_statusnilaiasessment where id_dosen ='$id' AND status = 'UNREAD'")->num_rows();
// 	$total = $a+$b;
// 	return $total;
// }


function tanggal_indo($tanggal, $cetak_hari = false)
{
	$tanggal = date('Y-m-d',strtotime($tanggal));
	$hari = array ( 1 =>    'Senin',
				'Selasa',
				'Rabu',
				'Kamis',
				'Jumat',
				'Sabtu',
				'Minggu'
			);
			
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	
	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}


?>