<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('all_dosen'))
{
function all_dosen($order,$table,$select){
	$ci=& get_instance();
    $ci->db->order_by($order);
    $ci->db->where('status','AKTIF');
    $ci->db->select($select);
    $ci->db->from($table);
    return $ci->db->get();

}
}

if ( ! function_exists('check_kpi'))
{
function check_kpi($id){
	$ci=& get_instance();
	$q = $ci->db->query("SELECT * from t_kpi where status='ON' and id_kpi ='$id'");
	if($q->num_rows() > 0){
	return true;	
	};
	return false;
}
}
if ( ! function_exists('getKPI'))
{
  function getKPI($x){
	 $ci=& get_instance();
     $ci->db->select('*')
                ->order_by('nilai','DESC')
                ->from('t_kpi')
                ->join('t_kategori','t_kpi.id_kpi = t_kategori.id_kpi')
                ->where('t_kategori.id_kpi',$x);
     return $ci->db->get();
}
}


if ( ! function_exists('total_rapat'))
{
function total_rapat($id,$tipe){
	$ci=& get_instance();
	$sem = getsemester_aktif();
    $q = $ci->db->query("SELECT count(*) as 'total' FROM `t_kehadiran` join `t_undangan` using(id_undangan) where id_dosen = '$id' and tipe ='$tipe' and id_semester ='$sem'")->row_array();
    return $q['total'];

}
}
if ( ! function_exists('rapat_hadirrrr'))
{
function rapat_hadirrrr($id,$tipe){
	$ci=& get_instance();
	$sem = getsemester_aktif();
    $q = $ci->db->query("SELECT count(*) as 'absen' FROM `t_kehadiran` join `t_undangan` using(id_undangan) where id_dosen = '$id' and (status = 'HADIR' or status = 'IZIN') and tipe ='$tipe' and id_semester ='$sem'")->row_array();
    return $q['absen'];

}
}

if ( ! function_exists('update_nilai'))
{
function update_nilai($data,$id,$where,$sem){
	$ci =&get_instance();
    $ci->db->where('id_dosen',$id);
    $ci->db->where('id_semester',$sem);
    $cek = $ci->db->get('t_nilai');
    if($cek->num_rows() > 0){

    $q1 = $ci->db->query("UPDATE t_nilai SET $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem'");
    }else {
    $q2 = $ci->db->query("INSERT INTO `t_nilai` ( `id_dosen`, `$where`, `id_semester`) VALUES ('$id', '$data', '$sem')");
    }
    return true;
}
}



if ( ! function_exists('update_nilai_koor'))
{
 function update_nilai_koor($data,$id,$where,$sem,$idmatakuliah){
 	$ci =& get_instance();
    $ci->db->where('id_dosen',$id);
    $ci->db->where('id_matakuliah',$idmatakuliah);
    $ci->db->where('id_semester',$sem);
    $cek = $ci->db->get('t_nilai_koor');
    if($cek->num_rows() > 0){

    $q1 = $ci->db->query("UPDATE t_nilai_koor set $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem' and `id_matakuliah` = '$idmatakuliah';");
    }else {
    $q2 = $ci->db->query("INSERT INTO `t_nilai_koor` ( `id_dosen`, `$where`, `id_semester`, `id_matakuliah`) VALUES ('$id', '$data', '$sem', '$idmatakuliah');");
    
    }
    return true;
    }
}


if ( ! function_exists('hitungnilai_rapat_koor_dosen'))
{
	  function hitungnilai_rapat_koor_dosen($iddosen){
		$ci =& get_instance();
		if(check_kpi('3')){

		$total2  = total_rapat($iddosen,1);
		$atas2  = rapat_hadirrrr($iddosen,1);

		if($total2 > 0 && $atas2 > 0){
		$nilai = round(($atas2 / $total2)*100,2);
		update_nilai($nilai,$iddosen,'nilai_rapat_koor',getsemester_aktif());
		} else {
		update_nilai('0',$iddosen,'nilai_rapat_koor',getsemester_aktif());
		
		}
	}
	}
}


if ( ! function_exists('hitung_nilai_rapat_dosen'))
{
	  function hitungnilai_rapat_dosen($iddosen){

		$ci =& get_instance();
		if(check_kpi('2')){
		
		$total2  = total_rapat($iddosen,2);
		$atas2  = rapat_hadirrrr($iddosen,2);

		if($total2 > 0 && $atas2 > 0){
		$nilai = round(($atas2 / $total2)*100,2);
		update_nilai($nilai,$iddosen,'nilai_rapat_prodi',getsemester_aktif());
		} else {
		update_nilai(0,$iddosen,'nilai_rapat_prodi',getsemester_aktif());
		}	
	}
	}
}



if ( ! function_exists('TotalKoorRapat'))
{

      function TotalKoorRapat($id,$matkul){
      $ci =& get_instance();
       $ci->db->select('*')
                ->from('t_undangan')
                ->join('t_kehadiran','t_kehadiran.id_undangan = t_undangan.id_undangan')
                ->where('tipe','1')
                ->where('tipe_p','1')
                ->where('id_dosen',$id)
                ->where('id_matakuliah',$matkul);
        return $ci->db->get()->num_rows();
    }
}

if ( ! function_exists('getiddosen_rapat'))
{
	function getiddosen_rapat($id){
		$ci =& get_instance();
		$q = $ci->db->query("SELECT id_dosen FROM t_undangan JOIN t_kehadiran using(id_undangan) WHERE t_kehadiran.id_undangan ='$id' AND tipe_p = '1'")->row_array();
		return $q['id_dosen'];
	}
}
if ( ! function_exists('hitung_nilai_koor'))
{
	  function hitung_nilai_koor($matkul,$iddosen){
		$tot = TotalKoorRapat($iddosen,$matkul);
		if(check_kpi('4')){
			$kategori = getKPI('4')->result();
			foreach($kategori as $res){
				if($tot >= $res->kondisi){
				update_nilai_koor($res->nilai,$iddosen,'nilai_koor_rapat',getsemester_aktif(),$matkul);
				break;
				}else{
				update_nilai_koor('0',$iddosen,'nilai_koor_rapat',getsemester_aktif(),$matkul);
				}
			}
			
		}

	}
}
if (!function_exists('total_perwalian'))
{
function total_perwalian($id,$sem){
	$ci=& get_instance();
    $q = $ci->db->query("SELECT count(*) as 'total' FROM `t_perwalian` where id_dosen = '$id' and id_semester ='$sem' and status='approved'")->row_array();
    return $q['total'];
}
}


if (!function_exists('getiddosen_perwalian'))
{
function getiddosen_perwalian($id){
	$ci =& get_instance();
	$q = $ci->db->query("SELECT id_dosen FROM t_perwalian WHERE id_perwalian ='$id'")->row_array();
	return $q['id_dosen'];
}
}
if (!function_exists('hitungnilai_perwalian'))
{

 function hitungnilai_perwalian($id){
	$ci =& get_instance();
	if(check_kpi('14')){

		$kategori = getKPI('14')->result();
		$total_perwalian = total_perwalian($id,getsemester_aktif());
		foreach($kategori as $res){
			if($total_perwalian >= $res->kondisi){
			
				$nil = $res->nilai;
				update_nilai($nil,$id,'nilai_perwalian',getsemester_aktif());
				break;
			}else{
				$nil = 0;
				update_nilai($nil,$id,'nilai_perwalian',getsemester_aktif());
			}
		}
		//$this->session->set_flashdata('msg','Sukses ...!!');
	}
}
}


if (!function_exists('TglLKS'))
{
      function TglLKS($id){
     	 $ci =& get_instance();
         $ci->db->select('tgl_lks,id_dosen');
         $ci->db->where('id_lks',$id);
         $q = $ci->db->get('t_lks')->result();
         return $q;
    }

}

if (!function_exists('hitungnilai_lks'))
{
 function hitungnilai_lks($id){
	$ci =& get_instance();
	if(check_kpi('15')){

		$kategori =  getKPI('15')->result();
		$tgl_upload = TglLKS($id);
			foreach($tgl_upload as $res2){
			foreach($kategori as $res){
				$now = strtotime($res->kondisi); // tanggal deadline

				$your_date = strtotime($res2->tgl_lks);  //tgl upload
				$datediff = $now - $your_date;

				$selisih =  round($datediff / (60 * 60 * 24));
				//echo $selisih.'<br>';

				$nilai = $res->nilai;

				if($selisih < 0){
				$nilai = $res->nilai - (10*$selisih*-1);
				if($nilai <= 60){
				$nilai = 60;
				}
						
				}

				}
			   	$where = 'nilai_lks'; 
				$idd = $res2->id_dosen;
				$idsem  = getsemester_aktif();
				update_nilai($nilai,$idd,$where,$idsem);
			}
		}
	}
}

if (!function_exists('hitungnilai_lks_dosen'))
{
function hitungnilai_lks_dosen($iddosen){
	$ci =& get_instance();
	$sem = getsemester_aktif();
	$queri_bro = $ci->db->query("SELECT id_lks,status FROM t_lks WHERE id_dosen = '$iddosen' and id_semester='$sem' ORDER BY tgl_lks DESC");
	if($queri_bro->num_rows() > 0){
		foreach($queri_bro->result() as $rows){
			if($rows->status == 'approved'){
				hitungnilai_lks($rows->id_lks);
			}else{
				update_nilai(0,$iddosen,'nilai_lks',$sem);	
			}
			}
	}else{
		update_nilai(0,$iddosen,'nilai_lks',$sem);	
	}
}


}