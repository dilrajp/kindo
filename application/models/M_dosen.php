<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dosen extends CI_Model {
    
   
    public function all($id,$table){
        $this->db->order_by($id);
        return $this->db->get($table);

    }

    public function all_rapat($id,$where,$join,$table1,$table2){

    $this->db->select('*')
             ->from($table1)
             ->join($table2, $join)
             ->where($where,$id)
             ->where('id_semester',getsemester_aktif());
    return $this->db->get();

    }


   public function getListKPI(){

    $this->db->select('*')
             ->from('t_kpi')
             ->join('t_prodi','t_kpi.prodi = t_prodi.id_prodi')
             ->where('t_kpi.prodi',getprodi($this->session->id_dosen))
             ->where('t_kpi.status','ON');
    return $this->db->get();

    }

     function update($data, $table,$id,$where){
        $this->db->where($where,$id);
        $this->db->update($table, $data);

    }



    
    public function update_rapat($update,$where,$where2){
        $data = array(
                'status' => $update,
                'keterangan' => ' '
        );

    	$this->db->where('id_undangan', $where);
    	$this->db->where('id_dosen', $where2);
    	$this->db->update('t_kehadiran',$data);
        $q = $this->db->select('tipe')->where('id_undangan',$where)->get('t_undangan')->row('tipe');
        if($q == '1'){
        hitungnilai_rapat_koor_dosen($where2);
        } else if($q == '2'){
        hitungnilai_rapat_dosen($where2);
        }

    }

    public function update_rapat_izin($id,$ket){
        $data = array(
                'status' => 'IZIN',
                'keterangan' => $ket 
            );
              $this->db->where('id_undangan',$id)
                       ->where('id_dosen',$this->session->id_dosen)
                 ->update('t_kehadiran',$data);

        $q = $this->db->select('tipe')->where('id_undangan',$id)->get('t_undangan')->row('tipe');
        if($q == '1'){
        hitungnilai_rapat_koor_dosen($this->session->id_dosen);
        } else if($q == '2'){
        hitungnilai_rapat_dosen($this->session->id_dosen);
        }

    }
    
    public function getById($where,$id,$table){
    	 $this->db->where($where,$id);
         return $this->db->get($table);
    }

    public function getKategori($where,$x,$table){
     $this->db->where($where,$x);
     $this->db->order_by('nilai','DESC');
     return $this->db->get($table);
    }

    public function getmodul($id_dosen){
        $this->db->select('*');
        $this->db->from('t_modul');
        $this->db->join('t_matakuliah', 't_modul.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusmodul', 't_dosen.id_dosen = t_statusmodul.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_statusmodul.status','READ');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function getStatusModul($id_dosen){
        $this->db->select('*');
        $this->db->from('t_statusmodul');
        $this->db->where('status','SETUJU');
        $this->db->where('id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function update_data_modul($id,$data){
        $this->db->where('id_statusmodul', $id);
        $this->db->update('t_statusmodul', $data);
    }
    public function getAllModul($id_dosen){
        $this->db->select('t_modul.id_modul');
        $this->db->from('t_modul');
        $this->db->join('t_matakuliah', 't_modul.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusmodul', 't_dosen.id_dosen = t_statusmodul.id_dosen');
        $this->db->where('t_statusmodul.status','SETUJU');
        $this->db->where('t_pengajaran.id_dosen',$id_dosen);
        return $this->db->get()->result();
    }
    public function updateAllModul($id_dosen,$id_modul,$data){
        $this->db->where('id_dosen', $id_dosen);
        $this->db->where_in('id_modul', $id_modul);
        $this->db->update('t_statusmodul',$data);
    }
    public function getrps($id_dosen){
        $this->db->select('*');
        $this->db->from('t_rps');
        $this->db->join('t_matakuliah', 't_rps.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusrps', 't_dosen.id_dosen = t_statusrps.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_statusrps.status','READ');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function getStatusRps($id_dosen){
        $this->db->select('*');
        $this->db->from('t_statusrps');
        $this->db->where('status','SETUJU');
        $this->db->where('id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function update_data_rps($id,$data){
        $this->db->where('id_statusrps', $id);
        $this->db->update('t_statusrps', $data);
    }
    public function getAllRps($id_dosen){
        $this->db->select('t_rps.id_rps');
        $this->db->from('t_rps');
        $this->db->join('t_matakuliah', 't_rps.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusrps', 't_dosen.id_dosen = t_statusrps.id_dosen');
        $this->db->where('t_statusrps.status', 'SETUJU');
        $this->db->where('t_pengajaran.id_dosen',$id_dosen);
        return $this->db->get()->result();
    }
    public function updateAllRps($id_dosen,$id_rps,$data){
        $this->db->where('id_dosen', $id_dosen);
        $this->db->where_in('id_rps', $id_rps);
        $this->db->update('t_statusrps',$data);
    }
    public function getsoal($id_dosen){
        $this->db->select('*');
        $this->db->from('t_soal');
        $this->db->join('t_matakuliah', 't_soal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statussoal', 't_dosen.id_dosen = t_statussoal.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_statussoal.status', 'READ');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
     public function getStatusSoal($id_dosen){
        $this->db->select('*');
        $this->db->from('t_statussoal');
        $this->db->where('status','SETUJU');
        $this->db->where('id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function update_data_soal($id,$data){
        $this->db->where('id_statussoal', $id);
        $this->db->update('t_statussoal', $data);
    }
    public function getAllSoal($id_dosen){
        $this->db->select('t_soal.id_soal');
        $this->db->from('t_soal');
        $this->db->join('t_matakuliah', 't_soal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statussoal', 't_dosen.id_dosen = t_statussoal.id_dosen');
        $this->db->where('t_statussoal.status', 'SETUJU');
        $this->db->where('t_pengajaran.id_dosen',$id_dosen);
        return $this->db->get()->result();
    }
    public function updateAllSoal($id_dosen,$id_soal,$data){
        $this->db->where('id_dosen', $id_dosen);
        $this->db->where_in('id_soal', $id_soal);
        $this->db->update('t_statussoal',$data);
    }
    public function getbahan($id_dosen){
        $this->db->select('*');
        $this->db->from('t_bahanperkuliahan');
        $this->db->join('t_matakuliah', 't_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusbahan', 't_dosen.id_dosen = t_statusbahan.id_dosen');
        $this->db->where('t_statusbahan.status', 'READ');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function getStatusBahan($id_dosen){
        $this->db->select('*');
        $this->db->from('t_statusbahan');
        $this->db->where('status','SETUJU');
        $this->db->where('id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function update_data_bahan($id,$data){
        $this->db->where('id_statusbahan', $id);
        $this->db->update('t_statusbahan', $data);
    }
    public function getAllBahan($id_dosen){
        $this->db->select('t_bahanperkuliahan.id_bahan');
        $this->db->from('t_bahanperkuliahan');
        $this->db->join('t_matakuliah', 't_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusbahan', 't_dosen.id_dosen = t_statusbahan.id_dosen');
        $this->db->where('t_statusbahan.status', 'SETUJU');
        $this->db->where('t_pengajaran.id_dosen',$id_dosen);
        return $this->db->get()->result();
    }
    public function updateAllBahan($id_dosen,$id_bahan,$data){
        $this->db->where('id_dosen', $id_dosen);
        $this->db->where_in('id_bahan', $id_bahan);
        $this->db->update('t_statusbahan',$data);
    }
    // public function listtahunajaran(){
    //     $this->db->select('*');  
    //     $this->db->from('t_tahunajaran');
    //     $this->db->order_by('tahunajaran', 'desc');  
    //     return $this->db->get();
    // }
    //  public function getselectedsemester($id_tahunajaran = string){
    //     $this->db->select('*');
    //     $this->db->from('t_semester');
    //     $this->db->join('t_tahunajaran', 't_semester.id_tahunajaran = t_tahunajaran.id_tahunajaran');
    //     $this->db->where('t_semester.id_tahunajaran', $id_tahunajaran);
    //     $this->db->order_by('t_tahunajaran.tahunajaran', 'asc');
    //     $this->db->order_by('t_semester.semester', 'asc');
    //     return $this->db->get()->result();
    // }
    // public function getselectedmatakuliah($id_semester, $id_dosen){
    //     $this->db->select('*');
    //     $this->db->from('t_matakuliah');
    //     $this->db->join('t_semester', 't_matakuliah.id_semester = t_semester.id_semester');
    //     $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
    //     $this->db->where('t_semester.id_semester', $id_semester);
    //     $this->db->where('t_pengajaran.status_koordinator', 'n');
    //     $this->db->where('t_pengajaran.id_dosen', $id_dosen);
    //     return $this->db->get()->result();
    // }
    public function getselectedmatakuliah($id_dosen){
        $this->db->select('*');
        $this->db->from('t_matakuliah');
        $this->db->join('t_semester', 't_matakuliah.id_semester = t_semester.id_semester');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_semester.status', 'Aktif');
        $this->db->where('t_pengajaran.status_koordinator', 'n');
        $this->db->where('t_pengajaran.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function getselectedmatakuliah2($id_dosen){
        $this->db->select('*');
        $this->db->from('t_matakuliah');
        $this->db->join('t_semester', 't_matakuliah.id_semester = t_semester.id_semester');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_semester.status', 'Aktif');
        $this->db->where('t_pengajaran.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    // public function getusulan2($id_dosen){
    //     $this->db->select('*');
    //     $this->db->from('t_usulansoal');
    //     $this->db->join('t_matakuliah', 't_usulansoal.id_matakuliah = t_matakuliah.id_matakuliah');
    //     $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
    //     $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
    //     $this->db->where('t_pengajaran.status_koordinator','n');
    //     $this->db->where('t_dosen.id_dosen', $id_dosen);
    //     return $this->db->get()->result();
    // }
    public function getusulan($id_usulan){
        $this->db->select('*');
        $this->db->from('t_usulansoal');
        $this->db->join('t_matakuliah', 't_usulansoal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->where_in('t_usulansoal.id_usulan', $id_usulan);
        return $this->db->get()->result();
    }
    function getidusulan(){
       $this->db->select('id_usulan');
       $this->db->from('t_usulansoal');
       $this->db->join('t_dosen', 't_usulansoal.id_dosen = t_dosen.id_dosen');
       $this->db->join('t_pengajaran', 't_dosen.id_dosen = t_pengajaran.id_dosen');
       $this->db->where('t_pengajaran.status_koordinator', 'n');
       $this->db->where('t_dosen.id_dosen', $this->session->userdata('id_dosen'));
       return $this->db->get()->result();
    }
    public function input_data_usulan($data,$table){
        $this->db->insert($table,$data);
    }
    public function update_data_usulan($id_usulan,$data){
        $this->db->where('id_usulan', $id_usulan);
        $this->db->update('t_usulansoal', $data);
    }
    public function update_status_usulan($id_usulan,$data){
        $this->db->where('id_usulan', $id_usulan);
        $this->db->update('t_statususulan', $data);
    }
    public function update_status_RPS($id,$data){
        $this->db->where('id_usulan', $id);
        $this->db->update('t_statususulan', $data);
    }
    public function hapus_data_usulan($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function tampungStatus($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_pengajaran');
        $this->db->where('id_matakuliah', $id_matakuliah);
        $this->db->where('status_koordinator', 'y');
        return $this->db->get()->result();
    }
    public function input_status_usulan($data,$table){
        $this->db->insert($table,$data);
    }
    public function last_datausulan($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_usulan');
       return  $this->db->get($table,1);
    }

    //unggah nilai asessment
     public function getnilaiasessment($id_nilaiasessment){
        $this->db->select('*');
        $this->db->from('t_nilaiasessment');
        $this->db->join('t_matakuliah', 't_nilaiasessment.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->where_in('t_nilaiasessment.id_nilaiasessment', $id_nilaiasessment);
        return $this->db->get()->result();
    }
    public function input_nilai_asessment($data,$table){
        $this->db->insert($table,$data);
    }
    public function update_data_nilai($id_nilaiasessment,$data){
        $this->db->where('id_nilaiasessment', $id_nilaiasessment);
        $this->db->update('t_nilaiasessment', $data);
    }
    public function update_status_nilai($id_nilaiasessment,$data){
        $this->db->where('id_nilaiasessment', $id_nilaiasessment);
        $this->db->update('t_statusnilaiasessment', $data);
    }
    public function hapus_nilai_asessment($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function input_status_nilai($data,$table){
        $this->db->insert($table,$data);
    }
    public function last_nilaiasessment($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_nilaiasessment');
       return  $this->db->get($table,1);
    }
    function getidnilai(){
       $this->db->select('id_nilaiasessment');
       $this->db->from('t_nilaiasessment');
       $this->db->join('t_dosen', 't_nilaiasessment.id_dosen = t_dosen.id_dosen');
       $this->db->join('t_pengajaran', 't_dosen.id_dosen = t_pengajaran.id_dosen');
       $this->db->where('t_pengajaran.status_koordinator', 'n');
       $this->db->where('t_dosen.id_dosen', $this->session->userdata('id_dosen'));
       return $this->db->get()->result();
    }

    //data pa
        public function data_mhs($id){
        $this->db->select('*')
                ->join('t_tahunajaran','id_tahunajaran')
                ->where('id_doping1',$id)
                ->where('tahap !=',7)
                ->or_where('id_doping2',$id)
                ->from('t_mhs_pa');
        return $this->db->get();
        }

        public function edit_mhs($id){
         $id2 = $this->session->id_dosen;
        $hsl = $this->db->query("SELECT id_mhs,nim_mhs,judul_pa,nama_mhs, grup, tahap,tahunajaran,dos1.nama as nama_dos1 ,dos2.nama as nama_dos2 
    FROM `t_mhs_pa`
    join t_tahunajaran on t_mhs_pa.id_tahunajaran = t_tahunajaran.id_tahunajaran 
    INNER JOIN
    (SELECT id_dosen as id, nama as nama
    FROM t_dosen) dos1 on id_doping1 = dos1.id
    INNER JOIN
    (SELECT id_dosen as id, nama as nama
    FROM t_dosen) dos2 on id_doping2 = dos2.id
    where  id_mhs = '$id' and (id_doping1 = '$id2' or id_doping2 = '$id2')");

        if($hsl->num_rows() > 0){
        foreach ($hsl->result() as $data) {

        $hasil=array(
            'nim_mhs' => $data->nim_mhs,
            'nama_mhs' => $data->nama_mhs,
            'judul_pa' => $data->judul_pa,
            'id_tahunajaran' => $data->tahunajaran,
            'tahun' => $data->tahunajaran,
            'dop1' => $data->nama_dos1,
            'dop2' => $data->nama_dos2,
            'grup' => $data->grup,
            'tahap' => $data->tahap,
            'id_mhs' => $data->id_mhs
            );
        }

        }
        $ros2 = $this->db->query("select tahap from t_mhs_pa where id_mhs = '1'")->row('tahap');
        $this->session->set_flashdata('tahap',$ros2);
        return $hasil;
    }


    //kpi

     public function getKPI($x){
         $this->db->select('*')
                    ->order_by('nilai','DESC')
                    ->from('t_kpi')
                    ->join('t_kategori','t_kpi.id_kpi = t_kategori.id_kpi')
                    ->where('t_kategori.id_kpi',$x);
         return $this->db->get();
    }


    public function TglUpload($where,$x,$table,$join,$on,$join2,$on2,$where2,$x2){
         $this->db->where($where,$x);
         $this->db->join($join,$on);
         $this->db->join($join2,$on2);
         $this->db->where($where2,$x2);
         $q = $this->db->get($table)->result();
         return $q;
    }


    //hitung nilai

    function update_nilai($data,$id,$where,$sem){
        $this->db->where('id_dosen',$id);
        $cek = $this->db->get('t_nilai');
        if($cek->num_rows() > 0){

        $q1 = $this->db->query("UPDATE t_nilai SET $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem'");
        }else {
        $q2 = $this->db->query("INSERT INTO `t_nilai` ( `id_dosen`, `$where`, `id_semester`) VALUES ('$id', '$data', '$sem')");
        }
        return true;
    }
    //untuk ambil tanggal unggah

    // public function getDeadlineUsulan(){
    //     // $this->db->select('*');
    //     // $this->db->from('t_deadlineusulan');
    //     // $this->db->join('t_matakuliah', 't_deadlineusulan.id_matakuliah = t_matakuliah.id_matakuliah');
    //     // $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
    //     // //$this->db->where('t_pengajaran.status_koordinator', 'n');
    //     // $this->db->where('t_matakuliah.id_semester',getsemester_aktif());
    //     // $this->db->where('t_pengajaran.id_dosen', $this->session->userdata('id_dosen'));
    //     $query = "select distinct t_matakuliah.nama_matakuliah, t_matakuliah.id_matakuliah from t_deadlineusulan join t_matakuliah using(id_matakuliah) join t_pengajaran using(id_matakuliah) where t_matakuliah.id_semester = ".getsemester_aktif()." and t_pengajaran.id_dosen = ".$this->session->userdata('id_dosen');
    //     return $this->db->query($query)->result();
    // }
    // public function getDeadlineNilai(){
    //     // $this->db->select('*');
    //     // $this->db->from('t_deadlinenilai');
    //     // $this->db->join('t_matakuliah', 't_deadlinenilai.id_matakuliah = t_matakuliah.id_matakuliah');
    //     // $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
    //     // $this->db->where('t_matakuliah.id_semester',getsemester_aktif());
    //     // //$this->db->where('t_pengajaran.status_koordinator', 'n');
    //     // $this->db->where('t_pengajaran.id_dosen', $this->session->userdata('id_dosen'));
    //     $query = "select distinct t_matakuliah.nama_matakuliah, t_matakuliah.id_matakuliah from t_deadlineusulan join t_matakuliah using(id_matakuliah) join t_pengajaran using(id_matakuliah) where t_matakuliah.id_semester = ".getsemester_aktif()." and t_pengajaran.id_dosen = ".$this->session->userdata('id_dosen');
    //     return $this->db->query($query)->result();
    // }
    public function getassessment($id_matakuliah){
        return $this->db->get_where('t_deadlinenilai', array('id_matakuliah' => $id_matakuliah))->result();
    }
    public function getassessment2($id_matakuliah){
        return $this->db->get_where('t_deadlineusulan', array('id_matakuliah' => $id_matakuliah))->result();
    }
    public function getKondisi(){
        $this->db->select('*');
        $this->db->from('t_deadlineusulan');
        $this->db->join('t_pengajaran','id_matakuliah');
        $this->db->where('id_dosen',$this->session->userdata('id_dosen'));
        return $this->db->get()->result();
    }
    public function getKondisi2(){
        $this->db->select('*');
        $this->db->from('t_deadlinenilai');
        $this->db->join('t_pengajaran','id_matakuliah');
        $this->db->where('id_dosen',$this->session->userdata('id_dosen'));

        return $this->db->get()->result();
    }
    //get notif dosen by ajax
    public function get_data_rps($id_dosen){
        $this->db->select('*');
        $this->db->from('t_rps');
        $this->db->join('t_matakuliah', 't_rps.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusrps', 't_dosen.id_dosen = t_statusrps.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_statusrps.status','SETUJU');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function get_data_modul($id_dosen){
        $this->db->select('*');
        $this->db->from('t_modul');
        $this->db->join('t_matakuliah', 't_modul.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusmodul', 't_dosen.id_dosen = t_statusmodul.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_statusmodul.status','SETUJU');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function get_data_soal($id_dosen){
        $this->db->select('*');
        $this->db->from('t_soal');
        $this->db->join('t_matakuliah', 't_soal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statussoal', 't_dosen.id_dosen = t_statussoal.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_statussoal.status', 'SETUJU');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function get_data_bahan($id_dosen){
        $this->db->select('*');
        $this->db->from('t_bahanperkuliahan');
        $this->db->join('t_matakuliah', 't_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->join('t_statusbahan', 't_dosen.id_dosen = t_statusbahan.id_dosen');
        $this->db->where('t_statusbahan.status', 'SETUJU');
        $this->db->where('t_pengajaran.status_koordinator','n');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function cek_notif_rapat($id){
        $this->db->select('id_dosen,status,id_undangan');
        $this->db->from('t_kehadiran');
        $this->db->where('id_dosen', $id);
        $this->db->where('status', 'TERKIRIM');
        return $this->db->get()->result();
    }
    public function cek_notif_modul($id){
        $this->db->select('id_statusmodul');
        $this->db->from('t_statusmodul');
        $this->db->where('id_dosen', $id);
        $this->db->where('status', 'SETUJU');
        return $this->db->get()->result();
    }
    public function cek_notif_soal($id){
        $this->db->select('id_statussoal');
        $this->db->from('t_statussoal');
        $this->db->where('id_dosen', $id);
        $this->db->where('status', 'SETUJU');
        return $this->db->get()->result();
    }
    public function cek_notif_rps($id){
        $this->db->select('id_statusrps');
        $this->db->from('t_statusrps');
        $this->db->where('id_dosen', $id);
        $this->db->where('status', 'SETUJU');
        return $this->db->get()->result();
    }
    public function cek_notif_bahan($id){
        $this->db->select('id_statusbahan');
        $this->db->from('t_statusbahan');
        $this->db->where('id_dosen', $id);
        $this->db->where('status', 'SETUJU');
        return $this->db->get()->result();
    }

    public function jadwal_menguji($table){
        $this->db->select('*')
                 ->where('PUJ1',$this->session->id_dosen)
                 ->or_where('PUJ2',$this->session->id_dosen)
                 ->from($table);
        return $this->db->get()->result();

    }

    public function gambar_perwalian($id){
        $this->db->select('*');
        $this->db->from('t_perwalian');
        $this->db->where('id_perwalian', $id);

        $hsl = $this->db->get();

        if($hsl->num_rows() > 0){
        foreach ($hsl->result() as $data) {
        if($data->file_perwalian == 'x'){
        $b ="x";
        }else{
        $b = base_url('uploadfile/perwalian/').$data->file_perwalian;
        }
        $hasil=array(
            'gbr' => $b
            );
        }

        }
        return $hasil;

    }

    public function gambar_lks($id){
        $this->db->select('*');
        $this->db->from('t_lks');
        $this->db->where('id_lks', $id);

        $hsl = $this->db->get();

        if($hsl->num_rows() > 0){
        foreach ($hsl->result() as $data) {
        if($data->file_lks == 'x'){
        $b ="x";
        }else{
        $b = base_url('uploadfile/lks/').$data->file_lks;
        }
        $hasil=array(
            'gbr' => $b
            );
        }

        }
        return $hasil;

    }
public function edit_perwalian($id){
    $hsl = $this->db->query("SELECT * FROM t_perwalian WHERE id_perwalian='$id'");
    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {

    $hasil=array(
        'id' => $data->id_perwalian,
        'kelas' => $data->kelas,
        'tanggal' => tanggal_indo($data->tanggal,true),
        'file_perwalian' => $data->file_perwalian,
        );
    }

    }
    return $hasil;
}
public function edit_lks($id){
    $hsl = $this->db->query("SELECT * FROM t_lks WHERE id_lks='$id'");
    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {

    $hasil=array(
        'id' => $data->id_lks,
        'tanggal' => tanggal_indo($data->tgl_lks,true),
        'file_lks' => $data->file_lks,
        );
    }

    }
    return $hasil;
}
}