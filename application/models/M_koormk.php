<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_koormk extends CI_Model {

    public function all($order,$table){
        $this->db->order_by($order);
        return $this->db->get($table);

    }
    public function all_dosen($order,$table,$select){
    $this->db->order_by($order);
    $this->db->where('status','AKTIF');
    $this->db->select($select);
    $this->db->from($table);
    return $this->db->get();

    }
public function gambar_bukti($id){
    $this->db->select('*');
    $this->db->from('t_undangan');
    $this->db->where('id_undangan', $id);

    $hsl = $this->db->get();

    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {
    if($data->bukti == 'x'){
    $b = base_url('assets/images/gambar/logo.PNG');
    }else{
    $b = base_url('uploadfile/rapat/').$data->bukti;
    }
    $hasil=array(
        'gambar' => $b
        );
    }

    }
    return $hasil;

}

public function getbukti($id){
    $q = $this->db->query("SELECT bukti from t_undangan where id_undangan = '$id'")->row_array();
    return $q['bukti'];
}
public function TglUpload($where,$x,$table,$join,$on,$join2,$on2,$where2,$x2){
     $this->db->where($where,$x);
     $this->db->join($join,$on);
     $this->db->join($join2,$on2);
     $this->db->where($where2,$x2);
     $q = $this->db->get($table)->result();
     return $q;
}

    public function all_select($order,$table,$select){
    $this->db->order_by($order);
    $this->db->select($select);
    $this->db->from($table);
    return $this->db->get();

    }
    public function nama_dosen(){
        $this->db->order_by('id_dosen');
        $this->db->select('nama_dosen');
        //return $this->db->get('t_dosen');
        return $this->db->get('t_dosen');
    }
    public function nama_matkul(){
        $this->db->select('*')
            ->join('t_matakuliah','id_matakuliah');
        $this->db->where('t_matakuliah.id_semester',getsemester_aktif())
                ->where('id_dosen',$this->session->id_dosen)
                ->where('status_koordinator','y');
        return $this->db->get('t_pengajaran');
    }
    public function getpesertarapat($id){
       $this->db->select('*')
                ->join('t_matakuliah','id_matakuliah')
                ->where('t_matakuliah.id_semester',getsemester_aktif())
                ->where('id_matakuliah',$id)
                ->from('t_pengajaran');
       return $this->db->get();
    }
    public function getById($where,$x,$table){
    	 $this->db->where($where,$x);
         return $this->db->get($table);

    }
    public function rapat_koormk($where){
        $this->db->select('*')
             ->from('t_kehadiran')
             ->join('t_undangan', 'id_undangan')
             ->where('tipe_p','1')
             ->where('id_dosen',$where)
             ->where('tipe','1');
        return $this->db->get();
    }
    public function all_rapat(){
        $this->db->order_by('id_undangan');
        return $this->db->get('t_undangan');

    }
    public function add($data,$table){
    	$this->db->insert($table,$data);

    }
    public function delete($where,$id,$table){
    	$this->db->where($where,$id);
    	$this->db->delete($table);
    }
    public function last_data($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_undangan');
       return  $this->db->get($table,1);
    }
    public function last_data2($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_modul');
       return  $this->db->get($table,1);
    }
    public function last_datasoal($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_soal');
       return  $this->db->get($table,1);
    }
    public function last_datarps($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_rps');
       return  $this->db->get($table,1);
    }
    public function last_databahan($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_bahan');
       return  $this->db->get($table,1);
    }
    function update($data, $table,$id,$where){
        $this->db->where($where,$id);
        $this->db->update($table, $data);

    }


 
 public function getKPI($x){
     $this->db->select('*')
                ->order_by('nilai','DESC')
                ->from('t_kpi')
                ->join('t_kategori','t_kpi.id_kpi = t_kategori.id_kpi')
                ->where('t_kategori.id_kpi',$x);
     return $this->db->get();
}
    
    //update nilai koor
    function update_nilai_koor($data, $table,$id,$where,$sem,$idmatakuliah){
    $this->db->where('id_dosen',$id);
    $this->db->where('id_matakuliah',$idmatakuliah);
    $this->db->where('id_semester',$sem);
    $cek = $this->db->get('t_nilai_koor');
    if($cek->num_rows() > 0){

    $q1 = $this->db->query("UPDATE t_nilai_koor set $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem' and `id_matakuliah` = '$idmatakuliah'");
    }else {
    $q2 = $this->db->query("INSERT INTO `t_nilai_koor` ( `id_dosen`,  $where , `id_semester`, `id_matakuliah`) VALUES ('$id', '$data', '$sem', '$idmatakuliah');");
    }
    return true;
    }
     
 

    public function update_rapat($field,$update,$table,$id,$where){

        $this->db->set($field, $update);
        $this->db->where($id, $where);
        $this->db->update($table);

    }

   
    public function update_nilai($id,$table,$data,$wow,$sem){
    
    $this->db->where('id_dosen',$id);
    $this->db->where('id_semester',$sem);
    $cek = $this->db->get('t_nilai');
    if($cek->num_rows() > 0){
    $q1 = $this->db->query("UPDATE `t_nilai` SET $wow = '$data' WHERE `t_nilai`.`id_dosen` = '$id' AND `t_nilai`.`id_semester` = '$sem'");
    }else {
    $q2 = $this->db->query("INSERT INTO `t_nilai` ( `id_dosen`, $wow, `id_semester`) VALUES ('$id', '$data', '$sem');");    
    }
    return true;
    }

    function update_nilai2($data,$id,$where,$sem){
        $this->db->where('id_dosen',$id);
        $cek = $this->db->get('t_nilai');
        if($cek->num_rows() > 0){

        $q1 = $this->db->query("UPDATE t_nilai SET $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem'");
        }else {
        $q2 = $this->db->query("INSERT INTO `t_nilai` ( `id_dosen`, `$where`, `id_semester`) VALUES ('$id', '$data', '$sem')");
        }
        return true;
    }

    public function update_status_rapat($id,$ket,$status){
        $data = array(
                'status' => $status,
                'keterangan' => $ket 
                );
        $this->db->where('id_kehadiran',$id)
                 ->update('t_kehadiran',$data);
    }

    //tampung Modul
    public function tampungStatus($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_pengajaran');
        $this->db->where('id_matakuliah', $id_matakuliah);
        $this->db->where('status_koordinator', 'n');
        return $this->db->get()->result();
    }
    public function input_status_modul($data,$table){
        $this->db->insert($table,$data);
    }
    public function update_status_modul($id){
        $data = array(
            'status' => 'READ'
        );
        $this->db->where('id_statusmodul', $id)
                ->update('t_statusmodul', $data);
    }
    public function revisi_status_modul($id,$data){
        $this->db->where('id_modul', $id);
        $this->db->update('t_statusmodul', $data);
    }

    //tampung soal
    public function tampungSoal($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_pengajaran');
        $this->db->where('id_matakuliah', $id_matakuliah);
        $this->db->where('status_koordinator', 'n');
        return $this->db->get()->result();
    }
    public function input_status_soal($data,$table){
        $this->db->insert($table,$data);
    }
    public function update_status_soal($id){
        $data = array(
            'status' => 'READ'
        );
        $this->db->where('id_statusoal', $id)
                ->update('t_statussoal', $data);
    }
     public function revisi_status_soal($id,$data){
        $this->db->where('id_soal', $id);
        $this->db->update('t_statussoal', $data);
    }

    //tampung rps
     public function tampungRps($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_pengajaran');
        $this->db->where('id_matakuliah', $id_matakuliah);
        $this->db->where('status_koordinator', 'n');
        return $this->db->get()->result();
    }
    public function input_status_rps($data,$table){
        $this->db->insert($table,$data);
    }
    public function update_status_rps($id){
        $data = array(
            'status' => 'READ'
        );
        $this->db->where('id_statusrps', $id)
                ->update('t_statusrps', $data);
    }
    public function revisi_status_RPS($id,$data){
        $this->db->where('id_rps', $id);
        $this->db->update('t_statusrps', $data);
    }

    //tampung bahan perkuliahan
    public function tampungBahan($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_pengajaran');
        $this->db->where('id_matakuliah', $id_matakuliah);
        $this->db->where('status_koordinator', 'n');
        return $this->db->get()->result();
    }
    public function input_status_bahan($data,$table){
        $this->db->insert($table,$data);
    }
    public function update_status_bahan($id){
        $data = array(
            'status' => 'READ'
        );
        $this->db->where('id_statusbahan', $id)
                ->update('t_statusbahan', $data);
    }
     public function revisi_status_bahan($id,$data){
        $this->db->where('id_bahan', $id);
        $this->db->update('t_statusbahan', $data);
    }

    public function getselectedmatakuliah($id_dosen){
        $this->db->select('*');
        $this->db->from('t_matakuliah');
        $this->db->join('t_semester', 't_matakuliah.id_semester = t_semester.id_semester');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_semester.status', 'Aktif');
        $this->db->where('t_pengajaran.status_koordinator', 'y');
        $this->db->where('t_pengajaran.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function getselectedmatakuliah2($id_dosen){
        $this->db->select('*');
        $this->db->from('t_matakuliah');
        $this->db->join('t_semester', 't_matakuliah.id_semester = t_semester.id_semester');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_semester.status', 'Aktif');
        $this->db->where('t_pengajaran.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    //get rps
    public function getrps($id_dosen){
        $this->db->select('*');
        $this->db->from('t_rps');
        $this->db->join('t_matakuliah', 't_rps.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','y');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function input_data_rps($data,$table){
        $this->db->insert($table,$data);
    }
    public function hapus_data_rps($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function update_revisi_rps($id_rps,$data){
        $this->db->where('id_rps', $id_rps);
        $this->db->update('t_rps', $data);
    }

    //get modul
    public function getmodul($id_dosen){
        $this->db->select('*');
        $this->db->from('t_modul');
        $this->db->join('t_matakuliah', 't_modul.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','y');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function input_data_modul($data,$table){
        $this->db->insert($table,$data);
    }
    public function hapus_data_modul($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function update_revisi_modul($id_modul,$data){
        $this->db->where('id_modul', $id_modul);
        $this->db->update('t_modul', $data);
    }
    public function getKondisi($id_kpi){
        $this->db->select('kondisi');
        $this->db->from('t_kategori');
        $this->db->where('id_kpi', $id_kpi);
        return $this->db->get()->result();
    }

    //lihat usulan soal
    public function getusulan(){
        $this->db->select('*');
        $this->db->from('t_usulansoal');
        $this->db->join('t_matakuliah', 't_usulansoal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_dosen', 't_usulansoal.id_dosen = t_dosen.id_dosen');
        return $this->db->get()->result();
    }

    public function getStatusUsulan($id_dosen){
        $this->db->select('*');
        $this->db->from('t_statususulan');
        $this->db->join('t_usulansoal', 't_statususulan.id_usulan = t_usulansoal.id_usulan');
        $this->db->join('t_matakuliah', 't_usulansoal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_dosen', 't_usulansoal.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_statususulan.status', 'WAITING');
        $this->db->where('t_statususulan.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function update_data_usulan($id,$data,$data2){
        $this->db->where('id_usulan', $id);
        $this->db->update('t_usulansoal', $data);
        $this->db->update('t_statususulan', $data2);
    }
    public function update_revisi_usulan($id,$keterangan,$data2){
      $data = array(
        'keterangan' => $keterangan,
        'status_usulan' => 'REVISI'
      );
      $data2 = array(
        'status' => 'REVISI'
      );
      $this->db->where('id_usulan', $id);
      $this->db->update('t_usulansoal', $data);
      $this->db->update('t_statususulan', $data2);
    }

    //lihat nilai asessment 
    public function last_nilaiasessment($table,$order){
        $this->db->order_by($order,"DESC");
        $this->db->select('id_nilaiasessment');
        return  $this->db->get($table,1);
    }
    public function getnilaiasessment(){
        $this->db->select('*');
        $this->db->from('t_nilaiasessment');
        $this->db->join('t_matakuliah', 't_nilaiasessment.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_dosen', 't_nilaiasessment.id_dosen = t_dosen.id_dosen');
        return $this->db->get()->result();
    }
    public function getStatusNilai($id_dosen){
        $this->db->select('*');
        $this->db->from('t_statusnilaiasessment');
        $this->db->join('t_nilaiasessment', 't_statusnilaiasessment.id_nilaiasessment = t_nilaiasessment.id_nilaiasessment');
        $this->db->join('t_matakuliah', 't_nilaiasessment.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_dosen', 't_nilaiasessment.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_statusnilaiasessment.status','UNREAD');
        $this->db->where('t_statusnilaiasessment.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function update_data_nilai($id,$data,$data2){
        $this->db->where('id_nilaiasessment', $id);
        $this->db->update('t_nilaiasessment', $data);
        $this->db->update('t_statusnilaiasessment', $data2);
    }
    public function update_revisi_nilai($id,$keterangan,$data2){
      $data = array(
        'keterangan' => $keterangan,
        'status_nilai' => 'REVISI'
      );
      $data2 = array(
        'status' => 'REVISI'
      );
      $this->db->where('id_nilaiasessment', $id);
      $this->db->update('t_nilaiasessment', $data);
      $this->db->update('t_statusnilaiasessment', $data2);
    }

    //soal asessment
    public function getsoal($id_dosen){
        $this->db->select('*');
        $this->db->from('t_soal');
        $this->db->join('t_matakuliah', 't_soal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','y');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function input_data_soal($data,$table){
        $this->db->insert($table,$data);
    }
    public function hapus_data_soal($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function update_revisi_soal($id_soal,$data){
        $this->db->where('id_soal', $id_soal);
        $this->db->update('t_soal', $data);
    }

    //bahan perkuliahan
    public function getbahan($id_dosen){
        $this->db->select('*');
        $this->db->from('t_bahanperkuliahan');
        $this->db->join('t_matakuliah', 't_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->join('t_dosen', 't_pengajaran.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator','y');
        $this->db->where('t_dosen.id_dosen', $id_dosen);
        return $this->db->get()->result();
    }
    public function input_data_bahan($data,$table){
        $this->db->insert($table,$data);
    }
    public function hapus_data_bahan($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function update_revisi_bahan($id_bahan,$data){
        $this->db->where('id_bahan', $id_bahan);
        $this->db->update('t_bahanperkuliahan', $data);
    }

    //get data by ajax
    public function get_data_usulan($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_statususulan');
        $this->db->join('t_usulansoal', 't_statususulan.id_usulan = t_usulansoal.id_usulan');
        $this->db->join('t_matakuliah', 't_usulansoal.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_dosen', 't_usulansoal.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_statususulan.status', 'UNREAD');
        $this->db->where_in('t_usulansoal.id_matakuliah', $id_matakuliah);
        return $this->db->get()->result();
    }
    public function get_data_nilai($id_matakuliah){
        $this->db->select('*');
        $this->db->from('t_statusnilaiasessment');
        $this->db->join('t_nilaiasessment', 't_statusnilaiasessment.id_nilaiasessment = t_nilaiasessment.id_nilaiasessment');
        $this->db->join('t_matakuliah', 't_nilaiasessment.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_dosen', 't_nilaiasessment.id_dosen = t_dosen.id_dosen');
        $this->db->where('t_statusnilaiasessment.status','UNREAD');
        $this->db->where_in('t_nilaiasessment.id_matakuliah', $id_matakuliah);
        return $this->db->get()->result();
    }
    public function cek_notif_usulan($id){
        $this->db->select('id_statususulan');
        $this->db->from('t_statususulan');
        $this->db->where_in('id_dosen', $id);
        $this->db->where('status', 'UNREAD');
        return $this->db->get()->num_rows();
    }
    public function cek_notif_nilai($id){
        $this->db->select('id_statusnilaiasessment');
        $this->db->from('t_statusnilaiasessment');
        $this->db->where_in('id_dosen', $id);
        $this->db->where('status', 'UNREAD');
        return $this->db->get()->num_rows();
    }

    //progres bar
    function progresnilai($id_matakuliah){
        $this->db->select('*, t_statusnilaiasessment.status as status_nilai, t_statususulan.status as status_usulan');
        $this->db->from('t_dosen');
        $this->db->join('t_pengajaran', 't_dosen.id_dosen = t_pengajaran.id_dosen');
        $this->db->join('t_matakuliah', 't_pengajaran.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_statusnilaiasessment', 't_dosen.id_dosen = t_statusnilaiasessment.id_dosen','left');
        $this->db->join('t_statususulan', 't_dosen.id_dosen = t_statususulan.id_dosen','left');
        $this->db->where('t_matakuliah.id_semester',getsemester_aktif());
        $this->db->where('t_pengajaran.status_koordinator', 'n');
        $this->db->where_in('t_pengajaran.id_matakuliah', $id_matakuliah);
        return $this->db->get()->result();
    }
 
    function koormatkul(){
        $this->db->select('t_matakuliah.id_matakuliah');
        $this->db->from('t_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_pengajaran.status_koordinator', 'y');
        $this->db->where('t_pengajaran.id_dosen', $this->session->userdata('id_dosen'));
        return $this->db->get()->result();
    }
    function koordosen($id_matakuliah){
        $this->db->select('t_dosen.id_dosen');
        $this->db->from('t_dosen');
        $this->db->join('t_pengajaran', 't_dosen.id_dosen = t_pengajaran.id_dosen');
        $this->db->where('t_pengajaran.status_koordinator', 'n');
        $this->db->where_in('t_pengajaran.id_matakuliah', $id_matakuliah);
        return $this->db->get()->result();
    }
    //batas progres bar
    //usulankpi
    public function getkpiusulannilai(){
        $this->db->select('*');
        $this->db->from('t_kpi');
        $this->db->where_in('id_kpi', array(10,11));
        return $this->db->get()->result();
    }
    public function getKategori($where,$x,$table){
     $this->db->where($where,$x);
     $this->db->order_by('nilai','DESC');
     return $this->db->get($table);

    }
    public function getDeadlineUsulan(){
        $this->db->select('*');
        $this->db->from('t_deadlineusulan');
        $this->db->join('t_matakuliah', 't_deadlineusulan.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_pengajaran.status_koordinator', 'y');
        $this->db->where('t_pengajaran.id_dosen', $this->session->userdata('id_dosen'));
        return $this->db->get()->result();
    }
    public function getDeadlineNilai(){
        $this->db->select('*');
        $this->db->from('t_deadlinenilai');
        $this->db->join('t_matakuliah', 't_deadlinenilai.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->join('t_pengajaran', 't_matakuliah.id_matakuliah = t_pengajaran.id_matakuliah');
        $this->db->where('t_pengajaran.status_koordinator', 'y');
        $this->db->where('t_pengajaran.id_dosen', $this->session->userdata('id_dosen'));
        return $this->db->get()->result();
    }
    function tambah_deadline_usulan($data, $table){
        $this->db->insert($table,$data);
    }
    function tambah_deadline_nilai($data, $table){
        $this->db->insert($table,$data);
    }
    public function update_deadline_usulan($id_deadlineusulan,$data){
        $this->db->where('id_deadlineusulan', $id_deadlineusulan);
        $this->db->update('t_deadlineusulan', $data);
    }
    public function update_deadline_nilai($id_deadlinenilai,$data){
        $this->db->where('id_deadlinenilai', $id_deadlinenilai);
        $this->db->update('t_deadlinenilai', $data);
    }
    function getIdDosen($id_matakuliah){
        $this->db->select('t_dosen.id_dosen');
        $this->db->from('t_dosen');
        $this->db->join('t_pengajaran', 't_dosen.id_dosen = t_pengajaran.id_dosen');
        $this->db->join('t_matakuliah', 't_pengajaran.id_matakuliah = t_matakuliah.id_matakuliah');
        $this->db->where('t_matakuliah.id_semester',getsemester_aktif());
        $this->db->where('t_pengajaran.status_koordinator', 'n');
        $this->db->where_in('t_pengajaran.id_matakuliah', $id_matakuliah);
        return $this->db->get()->result();
    }
}