<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_koorpa extends CI_Model {

public function all($order,$table){
$this->db->order_by($order);
return $this->db->get($table);

}


public function getById($where,$x,$table){
	 $this->db->where($where,$x);
     return $this->db->get($table);

}
public function add($data,$table){
	$this->db->insert($table,$data);

}
public function getmhsby($id){
	$this->db->select('*')
			->where('id_doping1',$id)
			->or_where('id_doping2',$id)
			->from('t_mhs_pa')
			->join('t_dosen','id_doping1 = id_dosen');
	return $this->db->get();
}

public function all_dosen(){
	$this->db->select('*')
			->where('status','AKTIF')
			->from('t_dosen');
	return $this->db->get();
}

public function data_mhs(){
	$this->db->select('*')
			->join('t_tahunajaran','id_tahunajaran')
			->from('t_mhs_pa');
	return $this->db->get();
}
public function data_jadwal_de(){
	$idtahun =  gettahun_aktif();
	$q = $this->db->query("SELECT t_jadwal_de.*,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_de.puj1) as kd1,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_de.puj2) as kd2,
			nim_mhs,nama_mhs  
			FROM `t_jadwal_de` 
			join t_tahunajaran on(t_tahunajaran.id_tahunajaran = t_jadwal_de.tahun)
			join t_mhs_pa on (t_mhs_pa.id_mhs = t_jadwal_de.id_mhs)
			WHERE tahun = '$idtahun' ");
	return $q;
}

public function edit_jadwal_de($id){
	$idtahun =  gettahun_aktif();
	$hsl = $this->db->query("SELECT *,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_de.puj1) as kd1,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_de.puj2) as kd2,
			(select kode_dosen from t_dosen where id_dosen = t_mhs_pa.id_doping1) as p1,
			(select kode_dosen from t_dosen where id_dosen = t_mhs_pa.id_doping2) as p2,
			nim_mhs,nama_mhs  
			FROM `t_jadwal_de` 
			join t_tahunajaran on(t_tahunajaran.id_tahunajaran = t_jadwal_de.tahun)
			join t_mhs_pa on (t_mhs_pa.id_mhs = t_jadwal_de.id_mhs)
			WHERE tahun = '$idtahun' AND t_jadwal_de.id_jadwal_de = '$id' ");
	if($hsl->num_rows() > 0){
	foreach ($hsl->result() as $data) {

	$hasil = array(
		'id_jadwal_de' => $data->id_jadwal_de,
		'nim_mhs' => $data->nim_mhs,
		'nama_mhs' => $data->nama_mhs,
		'judul_pa' => $data->judul_pa,
		'tahunajaran' => $data->tahunajaran,
		'ang' => $data->angkatan,
		'id_doping1' => $data->p1,
		'id_doping2' => $data->p2,
		'grup' => $data->grup,
		'tahap' => $data->tahap,
		'id_mhs' => $data->id_mhs,
		'puj1' => $data->PUJ1,
		'puj2' => $data->PUJ2,
		'periode' => $data->periode,
		'tanggal' => $data->tanggal,
		);
	}

	}
	return $hasil;
}

public function data_jadwal_sidang(){
	$idtahun =  gettahun_aktif();
	$q = $this->db->query("SELECT t_jadwal_sidang.*,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_sidang.puj1) as kd1,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_sidang.puj2) as kd2,
			nim_mhs,nama_mhs  
			FROM `t_jadwal_sidang` 
			join t_tahunajaran on(t_tahunajaran.id_tahunajaran = t_jadwal_sidang.tahun)
			join t_mhs_pa on (t_mhs_pa.id_mhs = t_jadwal_sidang.id_mhs)
			WHERE tahun = '$idtahun' ");
	return $q;
}

public function edit_jadwal_sidang($id){
	$idtahun =  gettahun_aktif();
	$hsl = $this->db->query("SELECT *,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_sidang.puj1) as kd1,
			(select kode_dosen from t_dosen where id_dosen = t_jadwal_sidang.puj2) as kd2,
			(select kode_dosen from t_dosen where id_dosen = t_mhs_pa.id_doping1) as p1,
			(select kode_dosen from t_dosen where id_dosen = t_mhs_pa.id_doping2) as p2,
			nim_mhs,nama_mhs  
			FROM `t_jadwal_sidang` 
			join t_tahunajaran on(t_tahunajaran.id_tahunajaran = t_jadwal_sidang.tahun)
			join t_mhs_pa on (t_mhs_pa.id_mhs = t_jadwal_sidang.id_mhs)
			WHERE tahun = '$idtahun' AND t_jadwal_sidang.id_jadwal_sidang  = '$id' ");
	if($hsl->num_rows() > 0){
	foreach ($hsl->result() as $data) {

	$hasil = array(
		'id_jadwal_de' => $data->id_jadwal_sidang,
		'nim_mhs' => $data->nim_mhs,
		'nama_mhs' => $data->nama_mhs,
		'judul_pa' => $data->judul_pa,
		'tahunajaran' => $data->tahunajaran,
		'ang' => $data->angkatan,
		'id_doping1' => $data->p1,
		'id_doping2' => $data->p2,
		'grup' => $data->grup,
		'tahap' => $data->tahap,
		'id_mhs' => $data->id_mhs,
		'puj1' => $data->PUJ1,
		'puj2' => $data->PUJ2,
		'tanggal' => $data->tanggal,
		'waktu' => $data->waktu,
		'ruangan' => $data->ruangan
		);
	}

	}
	return $hasil;
}

public function edit_mhs($id){
	$this->db->select('*')
			->join('t_tahunajaran','id_tahunajaran')
			->where('id_mhs',$id)
			->from('t_mhs_pa');
	$hsl = $this->db->query("SELECT * FROM t_mhs_pa JOIN t_tahunajaran using(id_tahunajaran) WHERE id_mhs='$id'");
	if($hsl->num_rows() > 0){
	foreach ($hsl->result() as $data) {

	$hasil=array(
		'nim_mhs' => $data->nim_mhs,
		'nama_mhs' => $data->nama_mhs,
		'judul_pa' => $data->judul_pa,
		'id_tahunajaran' => $data->id_tahunajaran,
		'tahunajaran' => $data->tahunajaran,
		'ang' => $data->angkatan,
		'id_doping1' => $data->id_doping1,
		'id_doping2' => $data->id_doping2,
		'grup' => $data->grup,
		'tahap' => $data->tahap,
		'id_mhs' => $data->id_mhs
		);
	}

	}
	return $hasil;
}
public function update($data, $table,$id,$where){
    $this->db->where($where,$id);
    $this->db->update($table, $data);

}

public function delete($id,$table,$where){
	$this->db->where($where,$id);
	$this->db->delete($table);
}

public function update_nilai($id,$table,$data,$wow,$sem){
    
    $this->db->where('id_dosen',$id);
    $this->db->where('id_semester',$sem);
    $cek = $this->db->get('t_nilai');
    if($cek->num_rows() > 0){
    $q1 = $this->db->query("UPDATE `t_nilai` SET $wow = '$data' WHERE `t_nilai`.`id_dosen` = '$id' AND `t_nilai`.`id_semester` = '$sem'");
    }else {
    $q2 = $this->db->query("INSERT INTO `t_nilai` ( `id_dosen`, $wow, `id_semester`) VALUES ('$id', '$data', '$sem');");    
    }
    return true;
}

public function getKPI($x){
     $this->db->select('*')
                ->order_by('nilai','DESC')
                ->from('t_kpi')
                ->join('t_kategori','t_kpi.id_kpi = t_kategori.id_kpi')
                ->where('t_kategori.id_kpi',$x);
     return $this->db->get();
}


public function KontribusiKelulusan($tahu){
    $this->db->select('*');
    $this->db->from('t_dosen');
    $this->db->join('t_kontribusi','t_dosen.id_dosen = t_kontribusi.id_dosen');
    $this->db->join('t_tahunajaran','t_kontribusi.tahun=t_tahunajaran.id_tahunajaran');
    $this->db->where('tahun',$tahu);
    return $this->db->get();
}
public function EditKontribusi($id){
    $this->db->select('*');
    $this->db->from('t_dosen');
    $this->db->join('t_kontribusi','t_dosen.id_dosen = t_kontribusi.id_dosen');
    $this->db->where('id_kl',$id);
    $hsl = $this->db->get();

    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {
    
    $hasil=array(
        'nip' => $data->nip,
        'id' => $data->id_kl,
        'id_dosen' => $data->id_dosen,
        'nama' => $data->nama,
        'total' => $data->total_mhs,
        'jumlah' => $data->jml_bimbingan
        );
        }
    }
   return $hasil;
}

}