<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Model {
    
//    untuk mengcek jumlah username dan password yang sesuai
    function login($username,$password) {
        $this->db->where('nip', $username);
        $this->db->where('password', $password);
        $query =  $this->db->get('t_dosen');
        return $query->num_rows();
    }
    
//    untuk mengambil data hasil login
    function data_login($username,$password) {
        $this->db->where('nip', $username);
        $this->db->where('password', $password);
        return $this->db->get('t_dosen')->row();
    }

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */