<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {

	private $table = 't_matakuliah';
	private $pk = 'id_matakuliah';

	private $table2 = 't_dosen';
	private $pk2 = 'id_dosen';
	
	public function all(){
		$this->db->order_by($this->pk);
		return $this->db->get($this->table);
	}
	public function all2(){
		$this->db->order_by($this->pk2);
		return $this->db->get($this->table2);
	}
	public function getById($x){
		 $this->db->where('id_matakuliah',$x);
	     return $this->db->get('t_matakuliah');
	}
	public function getById2($x){
		 $this->db->where('id_dosen',$x);
	     return $this->db->get('t_dosen');
	}
	public function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	public function input_data2($data,$table){
		$this->db->insert($table,$data);
	}
	public function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function update_data2($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	public function hapus_data2($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
