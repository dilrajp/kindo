<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kaprodi extends CI_Model {

public function all($order,$table){
    $this->db->order_by($order);
    return $this->db->get($table);

}

public function all_undangan($order,$table,$id){
    $this->db->order_by($order);
    $this->db->where('tipe',$id);
    return $this->db->get($table);

}

public function all_select($order,$table,$select){
    $this->db->order_by($order);
    $this->db->select($select);
    $this->db->from($table);
    return $this->db->get();

}
public function getListKPI(){

    $this->db->select('*')
             ->from('t_kpi')
             ->join('t_prodi','t_kpi.prodi = t_prodi.id_prodi')
             ->where('t_kpi.prodi',getprodi($this->session->id_dosen))
             ->where('t_kpi.status','ON');
    return $this->db->get();

}
public function all_dosen($order,$table,$select){
    $this->db->order_by($order);
    $this->db->where('status','AKTIF');
    $this->db->select($select);
    $this->db->from($table);
    return $this->db->get();

}
public function nama_dosen(){
    $this->db->order_by('id_dosen');
    $this->db->select('nama');
    //return $this->db->get('t_dosen');
    return $this->db->get('t_dosen');
}

public function cek_rapat($id){
    $this->db->select('tipe')
            ->where('id_undangan',$id);
    $q = $this->db->get('t_undangan')->row_array();
    return $q['tipe'];
}

public function getById($where,$x,$table){
	 $this->db->where($where,$x);
     return $this->db->get($table);

}

public function getKategori($where,$x,$table){
     $this->db->where($where,$x);
     $this->db->order_by('nilai','DESC');
     return $this->db->get($table);

}

public function all_rapat(){
    $this->db->order_by('id_undangan');
    return $this->db->get('t_undangan');

}
public function add($data,$table){
	$this->db->insert($table,$data);

}
public function delete($where,$id,$table){
	$this->db->where($where,$id);
	$this->db->delete($table);
}
public function last_data($table,$order){
    $this->db->order_by($order,"DESC");
    $this->db->select('id_undangan');
   return  $this->db->get($table,1);
}
function update($data, $table,$id,$where){
    $this->db->where($where,$id);
    $this->db->update($table, $data);

}

public function update_bukti($x,$id){
    $q = $this->db->query("UPDATE `t_undangan` SET `bukti` = '$x' WHERE `t_undangan`.`id_undangan` = '$id'");
}
public function getKPI($x){
     $this->db->select('*')
                ->order_by('nilai','DESC')
                ->from('t_kpi')
                ->join('t_kategori','t_kpi.id_kpi = t_kategori.id_kpi')
                ->where('t_kategori.id_kpi',$x);
     return $this->db->get();
}
//query nilai
/*function update_nilai($data, $table,$id,$where,$sem){
    $this->db->where('id_dosen',$id);
    $cek = $this->db->get('t_nilai');
    if($cek->num_rows() > 0){

    $q1 = $this->db->query("UPDATE t_nilai SET $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem'");
    }else {
    $q2 = $this->db->query("INSERT INTO `t_nilai` ( `id_dosen`, `$where`, `id_semester`) VALUES ('$id', '$data', '$sem')");
    }
    return true;
}
*/

function update_nilai($data,$id,$where,$sem){
    $this->db->where('id_dosen',$id);
    $this->db->where('id_semester',$sem);
    $cek = $this->db->get('t_nilai');
    if($cek->num_rows() > 0){

    $q1 = $this->db->query("UPDATE t_nilai SET $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem'");
    }else {
    $q2 = $this->db->query("INSERT INTO `t_nilai` ( `id_dosen`, `$where`, `id_semester`) VALUES ('$id', '$data', '$sem')");
    }
    return true;
}

 function update_nilai_koor($data,$id,$where,$sem,$idmatakuliah){
    $this->db->where('id_dosen',$id);
    $this->db->where('id_matakuliah',$idmatakuliah);
    $this->db->where('id_semester',$sem);
    $cek = $this->db->get('t_nilai_koor');
    if($cek->num_rows() > 0){

    $q1 = $this->db->query("UPDATE t_nilai_koor set $where = '$data' WHERE `id_dosen` = '$id' and `id_semester` = '$sem' and `id_matakuliah` = '$idmatakuliah';");
    }else {
    $q2 = $this->db->query("INSERT INTO `t_nilai_koor` ( `id_dosen`, `$where`, `id_semester`, `id_matakuliah`) VALUES ('$id', '$data', '$sem', '$idmatakuliah');");
    
    }
    return true;
    }



public function update_status_rapat($id,$ket,$status){
    $data = array(
            'status' => $status,
            'keterangan' => $ket 
            );
    $this->db->where('id_kehadiran',$id)
             ->update('t_kehadiran',$data);
}

public function update_status_file($id,$ket,$status,$table,$primary){
    $data = array(
            'status' => $status,
            'keterangan' => $ket 
            );
    $this->db->where($primary,$id)
             ->update($table,$data);
}

public function total_rapat($id){
    $this->db->select('count(*) as total')
             ->where('id_dosen',$id);
        return $this->db->get('t_kehadiran');

}

public function rapat_hadir($id){
    $this->db->select('count(*) as absen')
            ->where('id_dosen',$id)
            ->where('status','IZIN');
     return $this->db->get('t_kehadiran');
}

//unggah edom
public function getEdom(){
   $this->db->select('*');
   $this->db->from('t_edom');
   $this->db->join('t_dosen', 't_edom.id_dosen = t_dosen.id_dosen');
   $this->db->join('t_semester', 't_edom.semester = t_semester.id_semester');
   $this->db->where('t_edom.semester', getsemester_aktif());
   return $this->db->get();
}
public function get_nilai_edom($where){
    return $this->db->get_where('t_nilai', $where)->num_rows();
}
public function update_edom($id_dosen,$data){
    $this->db->where('id_dosen', $id_dosen);
    $this->db->where('semester', getsemester_aktif());
    $this->db->update('t_edom', $data);
}
public function update_edom2($id_dosen,$id_semester,$data2){
    $this->db->where('id_dosen', $id_dosen);
    $this->db->where('id_semester', $id_semester);
    $this->db->update('t_nilai', $data2);
}

//unggah bap
public function getBap(){
   $this->db->select('*');
   $this->db->from('t_bap');
   $this->db->join('t_dosen', 't_bap.id_dosen = t_dosen.id_dosen');
   $this->db->join('t_semester', 't_bap.semester = t_semester.id_semester');
   $this->db->where('t_bap.semester', getsemester_aktif());
   return $this->db->get();
}
public function get_nilai_bap($where){
    return $this->db->get_where('t_nilai', $where)->num_rows();
}
public function update_bap($id_dosen,$data){
    $this->db->where('id_dosen', $id_dosen);
    $this->db->where('semester', getsemester_aktif());
    $this->db->update('t_bap', $data);
}
public function update_bap2($id_dosen,$id_semester,$data2){
    $this->db->where('id_dosen', $id_dosen);
    $this->db->where('id_semester', $id_semester);
    $this->db->update('t_nilai', $data2);
}

public function getbukti($id){
    $q = $this->db->query("SELECT bukti from t_undangan where id_undangan = '$id'")->row_array();
    return $q['bukti'];
}

//query ajax hasil penialian
public function gambar_bukti($id){
     $this->db->select('*');
    $this->db->from('t_undangan');
    $this->db->where('id_undangan', $id);

    $hsl = $this->db->get();

    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {
    if($data->bukti == 'x'){
    $b = base_url('assets/images/gambar/logo.PNG');
    }else{
    $b = base_url('uploadfile/rapat/').$data->bukti;
    }
    $hasil = array(
        'gambar' => $b
        );
    }

    }
    return $hasil;

}


public function NilaiRapat(){
    $this->db->select('*');
    $this->db->from('t_dosen');
    $this->db->order_by('t_nilai.id_nilai');
    $this->db->join('t_nilai','t_dosen.id_dosen = t_nilai.id_dosen');
    return $this->db->get();

}

public function EditNilaiRapat($id){
    $this->db->select('*');
    $this->db->from('t_dosen');
    $this->db->join('t_nilai','t_dosen.id_dosen = t_nilai.id_dosen');
    $this->db->where('t_nilai.id_nilai',$id);
    $this->db->where('t_nilai.id_semester', getsemester_aktif());

    $hsl = $this->db->get();

    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {

    $hasil=array(
        'nip' => $data->nip,
        'id' => $data->id_nilai,
        'nama' => $data->nama,
        'nilai_prodi' => $data->nilai_rapat_prodi,
        'nilai_koor' => $data->nilai_rapat_koor,
        'nilai_prodi2' => $data->nilai_rapat_prodi2,
        'nilai_koor2' => $data->nilai_rapat_koor2,
        );
    }

    }
    return $hasil;
}
//kontribusi kelulusan
public function export_kontribusi(){
    $q = $this->db->query("SELECT kode_dosen, id_dosen, (SELECT count(id_doping1) FROM t_mhs_pa WHERE id_doping1 = t_dosen.id_dosen)+(SELECT count(id_doping2) FROM t_mhs_pa WHERE id_doping2 = t_dosen.id_dosen) AS jumlah FROM t_dosen WHERE status = 'AKTIF' ");
    return $q;
}
public function export_kontribusi2($id){
    $tahun = gettahun_aktif();
    $q = $this->db->query("SELECT COUNT(*) as 'jml' FROM `t_mhs_pa`
                           WHERE tahap = '7' 
                           and (id_doping1 = '$id' or id_doping2 = '$id') 
                           and id_tahunajaran = '$tahun'");
    return $q;
}

/*
public function KontribusiKelulusan($tahu){
    $this->db->select('*');
    $this->db->from('t_dosen');
    $this->db->join('t_kontribusi','t_dosen.id_dosen = t_kontribusi.id_dosen');
    $this->db->join('t_tahunajaran','t_kontribusi.tahun=t_tahunajaran.id_tahunajaran');
    $this->db->where('tahun',$tahu);
    return $this->db->get();
}*/

/*public function EditKontribusi($id){
    $this->db->select('*');
    $this->db->from('t_dosen');
    $this->db->join('t_kontribusi','t_dosen.id_dosen = t_kontribusi.id_dosen');
    $this->db->where('id_kl',$id);
    $hsl = $this->db->get();

    if($hsl->num_rows() > 0){
    foreach ($hsl->result() as $data) {
    
    $hasil=array(
        'nip' => $data->nip,
        'id' => $data->id_kl,
        'id_dosen' => $data->id_dosen,
        'nama' => $data->nama,
        'total' => $data->total_mhs,
        'jumlah' => $data->jml_bimbingan
        );
        }
    }
   return $hasil;
}*/

//Bagian Farizt
//get rps
public function getrps(){
    $this->db->select('*');
    $this->db->from('t_rps');
    $this->db->join('t_matakuliah', 't_rps.id_matakuliah = t_matakuliah.id_matakuliah');
    return $this->db->get()->result();
}
public function getStatusRps(){
    $this->db->select('*');
    $this->db->from('t_statusrps');
    $this->db->where('status','UNREAD');
    return $this->db->get()->result();
}
public function update_data_rps($id,$data,$data2){
    $this->db->where('id_rps', $id);
    $this->db->update('t_rps', $data);
    $this->db->update('t_statusrps', $data2);
}
public function update_revisi_rps($id,$keterangan,$data2){
  $data = array(
    'keterangan' => $keterangan,
    'status_rps' => 'REVISI'
  );
  $data2 = array(
    'status' => 'REVISI'
  );
  $this->db->where('id_rps', $id);
  $this->db->update('t_rps', $data);
  $this->db->update('t_statusrps', $data2);
}


public function TglUpload($where,$x,$table,$join,$on,$join2,$on2,$where2,$x2){
     $this->db->where($where,$x);
     $this->db->join($join,$on);
     $this->db->join($join2,$on2);
     $this->db->where($where2,$x2);
     $q = $this->db->get($table)->result();
     return $q;
}
//get modul
public function getmodul(){
    $this->db->select('*');
    $this->db->from('t_modul');
    $this->db->join('t_matakuliah', 't_modul.id_matakuliah = t_matakuliah.id_matakuliah');
    return $this->db->get()->result();
}
public function getStatusModul(){
    $this->db->select('*');
    $this->db->from('t_statusmodul');
    $this->db->where('status','UNREAD');
    return $this->db->get()->result();
}
public function update_data_modul($id,$data,$data2){
    $this->db->where('id_modul', $id);
    $this->db->update('t_modul', $data);
    $this->db->update('t_statusmodul', $data2);
}
public function update_revisi_modul($id,$keterangan,$data2){
  $data = array(
    'keterangan' => $keterangan,
    'status_modul' => 'REVISI'
  );
  $data2 = array(
    'status' => 'REVISI'
  );
  $this->db->where('id_modul', $id);
  $this->db->update('t_modul', $data);
  $this->db->update('t_statusmodul', $data2);
}

//get soal
public function getsoal(){
    $this->db->select('*');
    $this->db->from('t_soal');
    $this->db->join('t_matakuliah', 't_soal.id_matakuliah = t_matakuliah.id_matakuliah');
    return $this->db->get()->result();
}
public function getStatusSoal(){
    $this->db->select('*');
    $this->db->from('t_statussoal');
    $this->db->where('status','UNREAD');
    return $this->db->get()->result();
}
public function update_data_soal($id,$data,$data2){
    $this->db->where('id_soal', $id);
    $this->db->update('t_soal', $data);
    $this->db->update('t_statussoal', $data2);
}
public function update_revisi_soal($id,$keterangan,$data2){
  $data = array(
    'keterangan' => $keterangan,
    'status_soal' => 'REVISI'
  );
  $data2 = array(
    'status' => 'REVISI'
  );
  $this->db->where('id_soal', $id);
  $this->db->update('t_soal', $data);
  $this->db->update('t_statussoal', $data2);
}

//get bahan
public function getbahan(){
    $this->db->select('*');
    $this->db->from('t_bahanperkuliahan');
    $this->db->join('t_matakuliah', 't_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah');
    return $this->db->get()->result();
}
public function getStatusBahan(){
    $this->db->select('*');
    $this->db->from('t_statusbahan');
    $this->db->where('status','UNREAD');
    return $this->db->get()->result();
}
public function update_data_bahan($id,$data,$data2){
    $this->db->where('id_bahan', $id);
    $this->db->update('t_bahanperkuliahan', $data);
    $this->db->update('t_statusbahan', $data2);
}
public function update_revisi_bahan($id,$keterangan,$data2){
  $data = array(
    'keterangan' => $keterangan,
    'status_bahan' => 'REVISI'
  );
  $data2 = array(
    'status' => 'REVISI'
  );
  $this->db->where('id_bahan', $id);
  $this->db->update('t_bahanperkuliahan', $data);
  $this->db->update('t_statusbahan', $data2);
}

//ajax get notif kaprodi
public function get_data_rps(){
    $this->db->select('*');
    $this->db->from('t_rps');
    $this->db->join('t_matakuliah', 't_rps.id_matakuliah = t_matakuliah.id_matakuliah');
    $this->db->join('t_statusrps', 't_rps.id_rps = t_statusrps.id_rps');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function get_data_modul(){
    $this->db->select('*');
    $this->db->from('t_modul');
    $this->db->join('t_matakuliah', 't_modul.id_matakuliah = t_matakuliah.id_matakuliah');
    $this->db->join('t_statusmodul', 't_modul.id_modul = t_statusmodul.id_modul');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function get_data_perwalian(){
    $this->db->select('*');
    $this->db->from('t_perwalian');
    $this->db->where('status', 'waiting');
    return $this->db->get()->result();
}
public function get_data_lks(){
    $this->db->select('*');
    $this->db->from('t_lks');
    $this->db->where('status', 'waiting');
    return $this->db->get()->result();
}
public function get_data_soal(){
    $this->db->select('*');
    $this->db->from('t_soal');
    $this->db->join('t_matakuliah', 't_soal.id_matakuliah = t_matakuliah.id_matakuliah');
    $this->db->join('t_statussoal', 't_soal.id_soal = t_statussoal.id_soal');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function get_data_bahan(){
    $this->db->select('*');
    $this->db->from('t_bahanperkuliahan');
    $this->db->join('t_matakuliah', 't_bahanperkuliahan.id_matakuliah = t_matakuliah.id_matakuliah');
    $this->db->join('t_statusbahan', 't_bahanperkuliahan.id_bahan = t_statusbahan.id_bahan');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function cek_notif_modul(){
    $this->db->select('id_statusmodul');
    $this->db->from('t_statusmodul');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function cek_notif_perwalian(){
    $this->db->select('id_perwalian');
    $this->db->from('t_perwalian');
    $this->db->where('status', 'waiting');
    return $this->db->get()->result();
}
public function cek_notif_rps(){
    $this->db->select('id_statusrps');
    $this->db->from('t_statusrps');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function cek_notif_soal(){
    $this->db->select('id_statussoal');
    $this->db->from('t_statussoal');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}
public function cek_notif_bahan(){
    $this->db->select('id_statusbahan');
    $this->db->from('t_statusbahan');
    $this->db->where('status', 'UNREAD');
    return $this->db->get()->result();
}

function getAngkatan(){
    $this->db->select('*');
    $this->db->from('t_angkatan');
    return $this->db->get()->result();
}
public function update_angkatan($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
}
public function hapus_angkatan($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
}
}
