-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18 Jun 2018 pada 13.05
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thespora_kindo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_angkatan`
--

CREATE TABLE `t_angkatan` (
  `id_angkatan` int(11) NOT NULL,
  `angkatan` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `id_prodi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bahanperkuliahan`
--

CREATE TABLE `t_bahanperkuliahan` (
  `id_bahan` int(11) NOT NULL,
  `fileName` text NOT NULL,
  `date` date NOT NULL,
  `status_bahan` varchar(150) NOT NULL,
  `keterangan` text,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_bahanperkuliahan`
--

INSERT INTO `t_bahanperkuliahan` (`id_bahan`, `fileName`, `date`, `status_bahan`, `keterangan`, `id_matakuliah`) VALUES
(1, '001-DONA2.docx', '2018-06-03', 'APPROVE', NULL, 83);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bap`
--

CREATE TABLE `t_bap` (
  `id_bap` int(11) NOT NULL,
  `rfid` int(12) NOT NULL,
  `total` int(12) NOT NULL,
  `non_rfid` int(12) NOT NULL,
  `belum_submit` int(12) NOT NULL,
  `sudah_submit` int(12) NOT NULL,
  `pres_waktubap` double NOT NULL,
  `pres_kehadiran` double NOT NULL,
  `pres_rfid` double NOT NULL,
  `pres_inputbap` double NOT NULL,
  `kinerja_dosen` double NOT NULL,
  `semester` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bobot`
--

CREATE TABLE `t_bobot` (
  `id_bobot` bigint(20) NOT NULL,
  `komponen_bobot` varchar(50) NOT NULL,
  `batasan_awal` double NOT NULL,
  `batasan_akhir` double NOT NULL,
  `bobot_nilai` double NOT NULL,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_deadlinenilai`
--

CREATE TABLE `t_deadlinenilai` (
  `id_deadlinenilai` int(11) NOT NULL,
  `tgl_deadlinenilai` date NOT NULL,
  `jenis_nilai` varchar(150) NOT NULL,
  `nilai_assessment` double NOT NULL,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_deadlinenilai`
--

INSERT INTO `t_deadlinenilai` (`id_deadlinenilai`, `tgl_deadlinenilai`, `jenis_nilai`, `nilai_assessment`, `id_matakuliah`) VALUES
(1, '2018-06-03', 'asessment1', 100, 83),
(2, '2018-06-03', 'asessment2', 100, 83),
(3, '2018-06-03', 'asessment3', 100, 83),
(4, '2018-06-11', 'asessment1', 100, 88),
(5, '2018-06-13', 'asessment2', 100, 88),
(6, '2018-06-15', 'asessment3', 100, 88);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_deadlineusulan`
--

CREATE TABLE `t_deadlineusulan` (
  `id_deadlineusulan` int(11) NOT NULL,
  `tgl_deadlineusulan` date NOT NULL,
  `nilai_usulan` double NOT NULL,
  `jenis_nilai` varchar(150) NOT NULL,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_deadlineusulan`
--

INSERT INTO `t_deadlineusulan` (`id_deadlineusulan`, `tgl_deadlineusulan`, `nilai_usulan`, `jenis_nilai`, `id_matakuliah`) VALUES
(3, '2018-06-08', 100, 'asessment1', 88);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_dosen`
--

CREATE TABLE `t_dosen` (
  `id_dosen` int(11) NOT NULL,
  `kode_dosen` varchar(3) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email_dosen` varchar(100) NOT NULL,
  `nip` varchar(8) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `status` varchar(15) NOT NULL,
  `level` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jabatan_akademik` varchar(255) NOT NULL,
  `prodi` int(11) NOT NULL,
  `status_kaprodi` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_dosen`
--

INSERT INTO `t_dosen` (`id_dosen`, `kode_dosen`, `nama`, `email_dosen`, `nip`, `no_telp`, `picture`, `status`, `level`, `password`, `jabatan_akademik`, `prodi`, `status_kaprodi`) VALUES
(1, 'WDM', 'Wardani Muhamad, S.T., M.T.', 'wdm@tass.telkomuniversity.ac.id', '07810043', '08211122332', 'wardani.jpg', 'NON', 4, '47dda45f3448ece19caa4d72be8c1579', 'Lektor ', 1, 'n'),
(2, 'ADY', 'Ady Purna Kurniawan, S.T., M.T.', 'ady@tass.telkomuniversity.ac.id', '14880002', '087333283737', 'ady.jpg', 'AKTIF', 1, 'ee5d3ccd60fb6a846a73b4f7d4033c72', 'Asisten Ahli ', 1, 'n'),
(3, 'AUP', 'Agus Pratondo, S.T., M.T., Ph.D.', 'agus@tass.telkomuniversity.ac.id', '09770043', '08211122334', 'agus.jpg', 'AKTIF', 1, 'd0afc39a1d10ca6080f64cf59dea441a', 'Lektor ', 1, 'n'),
(4, 'ARS', 'Aris Hermansyah Suryadi, S.S.', 'aris@tass.telkomuniversity.ac.id', '10850029', '08722134225', 'aris.jpg', 'AKTIF', 1, '56b2e9b58105f6f3400133e1345ad045', 'NJFA ', 1, 'n'),
(5, 'BYU', 'Bayu Rima Aditya, S.T., M.T.', 'bayu@tass.telkomuniversity.ac.id', '14870095', '08211122342', 'bayu.png', 'NON', 1, '6107472036969c5150a4917b4c4a3d6d', 'Asisten Ahli ', 1, 'n'),
(6, 'DAN', 'Dahliar Ananda, S.T., M.T.', 'dan@tass.telkomuniverity.ac.id', '09820030', '082218323185', 'dahliar.jpg', 'AKTIF', 5, '8dd3d04e57ef030e3f694ffb56569512', 'Asisten Ahli ', 1, 'n'),
(7, 'DRW', 'Dedy Rahman Wijaya, S.T., M.T.', 'drw@tass.telkomuniversity.ac.id', '07840011', '08722134234', 'dedy.jpg', 'NON', 1, '2dc9d35e9c6a0a22c2a41d656543528a', 'Asisten Ahli ', 1, 'n'),
(8, 'EWD', 'Eka Widhi Yunarso, S.T., MM.T.', 'ewd@tass.telkomuniversity.ac.id', '10810024', '08211122335', 'eka.jpg', 'AKTIF', 1, '45256b153944636592e44571413e776b', 'Asisten Ahli ', 1, 'n'),
(9, 'ELT', 'Elis Hernawati, S.T., M.Kom.', 'elis@tass.telkomuniversity.ac.id', '14750035', '08722134222', 'elis.jpg', 'AKTIF', 1, '303090c0eb7eead2b2d10b81f08c6962', 'Asisten Ahli ', 1, 'n'),
(10, 'ELR', 'Ir. Ely Rosely, M.B.S.', 'ely@tass.telkomuniversity.ac.id', '15640027', '08211122331', 'ely.jpg', 'AKTIF', 1, '1d899ad869a968a501f946910a40bb0b', 'Asisten Ahli ', 1, 'n'),
(11, 'FRA', 'Ferra Arik Tridalestari, S.T., M.T.', 'ferra@tass.telkomuniversity.ac.id', '14860006', '08722134277', 'ferra.jpg', 'AKTIF', 1, 'b2b8845dc63d992e5d6c8d4e38eb2297', 'Asisten Ahli ', 1, 'n'),
(12, 'GTR', 'Guntur Prabawa Kusuma, S.T., M.T.', 'guntur@tass.telkomuniversity.ac.id', '08810079', '081776327363', 'guntur.jpg', 'AKTIF', 2, '2701c0b7d9e65ae1dc712a738710f712', 'Asisten Ahli ', 1, 'n'),
(13, 'HNP', 'Hanung Nindito Prasetyo, S.Si., M.T.', 'hanungnp@tass.telkomuniversity.ac.id', '14790020', '081223333111', 'hanung.jpg', 'AKTIF', 1, '041079d7a6b22db64139af2628bd18e7', 'Asisten Ahli ', 1, 'n'),
(14, 'HRO', 'Heru Nugroho, S.Si., M.T.', 'heru@tass.telkomuniversity.ac.id', '11810053', '081223333112', 'heru.jpg', 'AKTIF', 4, '577dde8705a1beec1a97fb1e9bf34416', 'Lektor ', 1, 'y'),
(15, 'INE', 'Inne Gartina Husein, S.Kom., M.T.', 'ine@tass.telkomuniversity.ac.id', '12730018', '081223333777', 'inne.jpg', 'NON', 1, 'a352f3f41496b527cc58aeb4640fdbae', 'Asisten Ahli ', 1, 'n'),
(16, 'MBS', 'Muhammad Barja Sanjaya, S.T., M.T.', 'mbs@tass.telkomuniversity.ac.id', '14850096', '082112233134', 'barja.jpg', 'AKTIF', 1, '860882e6f48d904a1339720a14b11717', 'Asisten Ahli ', 1, 'n'),
(17, 'MQA', 'Mutia Qana’a, S.Psi., M.Psi', 'mutia@tass.telkomuniversity.ac.id', '13870082', '082222333444', 'mutia.jpg', 'AKTIF', 1, '5bfbd92e6637fae84fb91601600ccc14', 'NJFA ', 1, 'n'),
(18, 'PTI', 'Patrick Adolf Telnoni, S.T., M.T.', 'patrick@tass.telkomuniversity.ac.id', '15890013', '087221342112', 'patrick.jpg', 'AKTIF', 2, 'be79d63706de84df7ed4a7174d7d1db9', 'Asisten Ahli ', 1, 'n'),
(19, 'PRM', 'RA. Paramita Mayadewi, S.Kom, M.T.', 'paramita@tass.telkomuniversity.ac.id', '12700043', '081223333115', 'paramita.jpg', 'AKTIF', 1, '5677ce4e87280a20278e821087e018e6', 'Asisten Ahli ', 1, 'n'),
(20, 'RBD', 'Reza Budiawan, S.T., M.T.', 'rbd@tass.telkomuniversity.ac.id', '14880064', '087221342119', 'reza.jpg', 'AKTIF', 1, '2cab336221f2dd8e85fd192f90953269', 'Asisten Ahli ', 1, 'n'),
(21, 'RHN', 'Robbi Hendriyanto, S.T., M.T.', 'robbi@tass.telkomuniversity.ac.id', '13850086', '087777222111', 'robbi.jpg', 'AKTIF', 1, 'a0cb220b0c00b82ee3602a0fe78d94cb', 'NJFA ', 1, 'n'),
(22, 'SDW', 'Sari Dewi Budiwati, S.T., M.T.', 'saridewi@tass.telkomuniversity.ac.id', '07820036', '087221342312', 'saridewi.jpg', 'NON', 2, '0a5fe9f1a17cff511f3e53cb42565a0f', 'Asisten Ahli ', 1, 'n'),
(23, 'SKS', 'Siska Komala Sari, S.T., M.T.', 'siska@tass.telkomuniversity.ac.id', '07810044', '08722134212', 'siska.jpeg', 'AKTIF', 1, '293bb3ffdfe890f3528424b6018e8bc7', 'Asisten Ahli ', 1, 'n'),
(24, 'SYN', 'Suryatiningsih, S.T., M.T.', 'oca@tass.telkomuniversity.ac.id', '07800068', '081111222333', 'oca.jpg', 'AKTIF', 1, '393d54029a19941fa0b1c7ac50364daf', 'Asisten Ahli ', 1, 'n'),
(25, 'TFN', 'Toufan Diansyah Tambunan, S.T., M.T.', 'toufan@tass.telkomuniversity.ac.id', '15850031', '087222111777', 'toufan.png', 'AKTIF', 1, '6172e5a9ca7b7471210bf9f2b775cb57', 'Asisten Ahli ', 1, 'n'),
(26, 'WHY', 'Wahyu Hidayat, S.T., M.T.', 'wahyu@tass.telkomuniversity.ac.id', '14850015', '081777748213', 'wahyu.jpg', 'AKTIF', 3, '267440931c48c18de561567e2a3b51bd', 'Asisten Ahli ', 1, 'n'),
(27, 'WIU', 'Wawa Wikusna, S.T., M.Kom.', 'wawa@tass.telkomuniversity.ac.id', '14740031', '087222111345', 'wawa.jpg', 'AKTIF', 3, '81d525cf8fdb2953061532e4897f3c91', 'Lektor ', 1, 'n'),
(28, 'YNG', 'Yuningsih, S.S.', 'yuningsih@tass.telkomuniversity.ac.id', '10830051', '083212345765', 'yuyun.jpg', 'AKTIF', 1, 'ff5d3587eb3a38415498637ad161c068', 'NJFA ', 1, 'n'),
(29, 'PRA', 'Pramuko Aji, S.T., M.T.', 'pram@tass.telkomuniversity.ac.id', '14800022', '08212345234', 'pramuko.png', 'AKTIF', 1, 'df18b9dda1de2fff1af09884f0c87bc5', 'NJFA', 1, 'n'),
(30, 'PWW', 'Dr. Pikir Wisnu, Wijayanto, M. Hum', 'pikirwisnu@tass.telkomuniversity.ac.id', '10800029', '081321395140', 'user.png', 'AKTIF', 1, '0eca806dc586b69d47fd18ad0296ebb0', 'Lektor', 1, 'n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_edom`
--

CREATE TABLE `t_edom` (
  `id_edom` int(11) NOT NULL,
  `jml_kelas` int(12) NOT NULL,
  `jml_mahasiswa` int(12) NOT NULL,
  `presentase` double NOT NULL,
  `semester` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_edom2`
--

CREATE TABLE `t_edom2` (
  `id_edom` bigint(20) NOT NULL,
  `kategori` varchar(150) NOT NULL,
  `pertanyaan` text NOT NULL,
  `tingkat_kepuasan1` int(2) NOT NULL,
  `tingkat_kepuasan2` int(2) NOT NULL,
  `tingkat_kepuasan3` int(2) NOT NULL,
  `tingkat_kepuasan4` int(2) NOT NULL,
  `ratarata` double NOT NULL,
  `persentase` double NOT NULL,
  `nama_kelas` varchar(15) NOT NULL,
  `id_portofolio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_jadwal_de`
--

CREATE TABLE `t_jadwal_de` (
  `id_jadwal_de` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `PUJ1` int(11) NOT NULL,
  `PUJ2` int(11) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_jadwal_de`
--

INSERT INTO `t_jadwal_de` (`id_jadwal_de`, `id_mhs`, `periode`, `tanggal`, `PUJ1`, `PUJ2`, `tahun`) VALUES
(1, 83, 'Apr-18', '43135', 6, 16, 3),
(2, 84, 'Apr-18', '43136', 6, 16, 3),
(3, 82, 'Apr-18', '43137', 6, 16, 3),
(4, 85, 'Apr-18', '43138', 27, 29, 3),
(5, 1, 'Apr-18', '43139', 13, 11, 3),
(6, 8, 'Apr-18', '43140', 9, 20, 3),
(7, 9, 'Apr-18', '43141', 14, 23, 3),
(8, 10, 'Apr-18', '43142', 26, 4, 3),
(9, 49, 'Apr-18', '43143', 26, 4, 3),
(10, 47, 'Apr-18', '43144', 28, 3, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_jadwal_sidang`
--

CREATE TABLE `t_jadwal_sidang` (
  `id_jadwal_sidang` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` varchar(20) NOT NULL,
  `PUJ1` int(11) NOT NULL,
  `PUJ2` int(11) NOT NULL,
  `ruangan` varchar(20) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_jadwal_sidang`
--

INSERT INTO `t_jadwal_sidang` (`id_jadwal_sidang`, `id_mhs`, `tanggal`, `waktu`, `PUJ1`, `PUJ2`, `ruangan`, `tahun`) VALUES
(1, 83, '2018-05-12', '08:00 - 10:00', 6, 16, 'A7', 3),
(2, 84, '2018-05-13', '08:00 - 10:01', 6, 16, 'A7', 3),
(3, 85, '2018-05-15', '08:00 - 10:03', 27, 29, 'A7', 3),
(4, 1, '2018-05-16', '08:00 - 10:04', 13, 11, 'A7', 3),
(5, 8, '2018-05-17', '08:00 - 10:05', 9, 20, 'A7', 3),
(6, 9, '2018-05-18', '08:00 - 10:06', 14, 23, 'A7', 3),
(7, 10, '2018-05-19', '08:00 - 10:07', 26, 4, 'A7', 3),
(8, 49, '2018-05-20', '08:00 - 10:08', 26, 4, 'A7', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kategori`
--

CREATE TABLE `t_kategori` (
  `id_kategori` int(22) NOT NULL,
  `kondisi` varchar(255) NOT NULL,
  `nilai` double NOT NULL,
  `id_kpi` int(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_kategori`
--

INSERT INTO `t_kategori` (`id_kategori`, `kondisi`, `nilai`, `id_kpi`) VALUES
(5, '2', 90, 4),
(39, '3', 100, 4),
(41, '1', 80, 4),
(42, '1', 80, 1),
(43, '5', 90, 1),
(44, '8', 95, 1),
(45, '100', 100, 5),
(46, '90', 90, 5),
(47, '80', 80, 5),
(48, '70', 70, 5),
(50, '2018-05-25', 100, 6),
(51, '2018-03-01', 100, 7),
(52, '2018-02-28', 100, 8),
(53, '2018-02-27', 100, 9),
(54, '2018-03-06', 100, 10),
(55, '2018-04-06', 100, 11),
(56, '2', 80, 12),
(57, '3', 90, 12),
(58, '2', 80, 13),
(59, '3', 10, 13),
(60, '1', 50, 12),
(61, '4', 70, 13),
(62, '2', 100, 14),
(63, '1', 90, 14),
(64, '2018-04-18', 100, 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kehadiran`
--

CREATE TABLE `t_kehadiran` (
  `id_kehadiran` int(11) NOT NULL,
  `id_dosen` int(12) NOT NULL,
  `status` varchar(30) NOT NULL,
  `tipe_p` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `id_undangan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_kehadiran`
--

INSERT INTO `t_kehadiran` (`id_kehadiran`, `id_dosen`, `status`, `tipe_p`, `keterangan`, `id_undangan`) VALUES
(1, 2, 'TERKIRIM', 2, ' ', 1),
(2, 3, 'TERKIRIM', 2, ' ', 1),
(3, 4, 'TERKIRIM', 2, ' ', 1),
(4, 6, 'TERKIRIM', 2, ' ', 1),
(5, 8, 'TERKIRIM', 2, ' ', 1),
(6, 9, 'TERKIRIM', 2, ' ', 1),
(7, 10, 'TERKIRIM', 2, ' ', 1),
(8, 11, 'TERKIRIM', 2, ' ', 1),
(9, 12, 'TERKIRIM', 2, ' ', 1),
(10, 13, 'TERKIRIM', 2, ' ', 1),
(11, 14, 'HADIR', 1, ' ', 1),
(12, 16, 'TERKIRIM', 2, ' ', 1),
(13, 17, 'TERKIRIM', 2, ' ', 1),
(14, 18, 'TERKIRIM', 2, ' ', 1),
(15, 19, 'TERKIRIM', 2, ' ', 1),
(16, 20, 'TERKIRIM', 2, ' ', 1),
(17, 21, 'TERKIRIM', 2, ' ', 1),
(18, 23, 'TERKIRIM', 2, ' ', 1),
(19, 24, 'TERKIRIM', 2, ' ', 1),
(20, 25, 'TERKIRIM', 2, ' ', 1),
(21, 26, 'TERKIRIM', 2, ' ', 1),
(22, 27, 'HADIR', 2, ' ', 1),
(23, 28, 'TERKIRIM', 2, ' ', 1),
(24, 29, 'TERKIRIM', 2, ' ', 1),
(25, 30, 'TERKIRIM', 2, ' ', 1),
(26, 9, 'TERKIRIM', 2, ' ', 2),
(27, 13, 'TERKIRIM', 2, ' ', 2),
(28, 14, 'HADIR', 2, ' ', 2),
(29, 23, 'TERKIRIM', 2, ' ', 2),
(30, 27, 'HADIR', 1, ' ', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kesesuaianmateri`
--

CREATE TABLE `t_kesesuaianmateri` (
  `id_kesesuaianmateri` bigint(20) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `realisasi` double NOT NULL,
  `id_portofolio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kontribusi`
--

CREATE TABLE `t_kontribusi` (
  `id_kl` int(12) NOT NULL,
  `id_dosen` int(12) NOT NULL,
  `total_mhs` varchar(20) NOT NULL,
  `jml_bimbingan` int(11) NOT NULL,
  `prosentase` varchar(20) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_kontribusi`
--

INSERT INTO `t_kontribusi` (`id_kl`, `id_dosen`, `total_mhs`, `jml_bimbingan`, `prosentase`, `tahun`) VALUES
(1, 14, '3', 15, '20', 3),
(2, 23, '3', 17, '17.65', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kpi`
--

CREATE TABLE `t_kpi` (
  `id_kpi` int(22) NOT NULL,
  `indikator` varchar(255) NOT NULL,
  `prodi` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_kpi`
--

INSERT INTO `t_kpi` (`id_kpi`, `indikator`, `prodi`, `status`) VALUES
(1, 'Jumlah Bimbingan Proyek Akhir', 1, 'ON'),
(2, 'Kehadiran dalam Rapat Prodi atau Undangan Prodi', 1, 'ON'),
(3, 'Mengikuti Koordinasi Koordinator MK', 1, 'ON'),
(4, 'Rapat Koordinasi MK', 1, 'ON'),
(5, 'Kontribusi Kelulusan Tepat Waktu Mahasiswa', 1, 'ON'),
(6, 'Kelengkapan RPS', 1, 'ON'),
(7, 'Ketersediaan Modul Praktikum', 1, 'ON'),
(8, 'Pembuatan Soal Asessment dan Penyerahan Soal Asessment Ke LAK', 1, 'ON'),
(9, 'Ketersedian Bahan Perkuliahan', 1, 'ON'),
(10, 'Penyerahan Usulan Soal Asessment Ke Dosen Koordinator', 1, 'ON'),
(11, 'Penyerahan Nilai Asessment Ke Koordinator', 1, 'ON'),
(12, 'Jumlah Menguji DE', 1, 'ON'),
(13, 'Jumlah Menguji Sidang', 1, 'ON'),
(14, 'Perwalian', 1, 'ON'),
(15, 'LKS Dosen', 1, 'ON'),
(16, 'EDOM', 1, 'ON'),
(17, 'Input BAP', 1, 'ON');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_lampiran`
--

CREATE TABLE `t_lampiran` (
  `id_lampiran` bigint(20) NOT NULL,
  `jenis_lampiran` varchar(20) NOT NULL,
  `nama_berkas` text NOT NULL,
  `waktu_unggah` datetime NOT NULL,
  `id_portofolio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_lks`
--

CREATE TABLE `t_lks` (
  `id_lks` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `file_lks` text NOT NULL,
  `tgl_lks` date NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'waiting',
  `keterangan` text,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_lks`
--

INSERT INTO `t_lks` (`id_lks`, `id_dosen`, `file_lks`, `tgl_lks`, `status`, `keterangan`, `id_semester`) VALUES
(1, 27, 'LKS_WIU_2018-06-03_11-36-21.JPG', '2018-06-03', 'approved', NULL, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_matakuliah`
--

CREATE TABLE `t_matakuliah` (
  `id_matakuliah` int(11) NOT NULL,
  `kode_matakuliah` varchar(10) NOT NULL,
  `nama_matakuliah` varchar(150) NOT NULL,
  `sks_matakuliah` int(2) NOT NULL,
  `status_wajib` varchar(20) NOT NULL,
  `tipe_matakuliah` varchar(20) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_matakuliah`
--

INSERT INTO `t_matakuliah` (`id_matakuliah`, `kode_matakuliah`, `nama_matakuliah`, `sks_matakuliah`, `status_wajib`, `tipe_matakuliah`, `id_semester`) VALUES
(31, 'KA1011', 'APLIKASI PERKANTORAN', 1, 'Wajib Prodi', 'Tingkat 1', 5),
(32, 'KU1012', 'BAHASA INGGRIS 1', 2, 'Wajib Prodi', 'Tingkat 1', 5),
(33, 'MI1264', 'DASAR ALGORITMA DAN PEMROGRAMAN', 4, 'Wajib Prodi', 'Tingkat 1', 5),
(34, 'DU1023', 'MATEMATIKA DISKRIT', 3, 'Wajib Prodi', 'Tingkat 1', 5),
(35, 'MI1023', 'SISTEM INFORMASI MANAJEMEN', 3, 'Wajib Prodi', 'Tingkat 1', 5),
(36, 'DPH1C4', 'PEMROGRAMAN BERORIENTASI OBYEK', 4, 'Wajib Prodi', 'Tingkat 2', 5),
(37, 'DPH2I4', 'PEMROGRAMAN WEB DASAR', 4, 'Wajib Prodi', 'Tingkat 2', 5),
(38, 'DMH2A3', 'PENGOLAHAN BASIS DATA', 3, 'Wajib Prodi', 'Tingkat 2', 5),
(39, 'DMH1D3', 'PROSES BISNIS', 3, 'Wajib Prodi', 'Tingkat 2', 5),
(40, 'DAH2B2', 'BAHASA INGGRIS IV', 2, 'Wajib Prodi', 'Tingkat 3', 5),
(41, 'DMH3B2', 'ETIKA PROFESI', 2, 'Wajib Prodi', 'Tingkat 3', 5),
(42, 'DIH3A3', 'IMPLEMENTASI USER EXPERIENCE DESIGN', 3, 'Wajib Prodi', 'Tingkat 3', 5),
(43, 'DMH3C3', 'METODOLOGI PENELITIAN', 3, 'Wajib Prodi', 'Tingkat 3', 5),
(44, 'DPH3A4', 'PEMROGRAMAN PERANGKAT BERGERAK', 4, 'Wajib Prodi', 'Tingkat 3', 5),
(45, 'DMH3C2', 'PENGANTAR AUDIT SISTEM INFORMASI', 2, 'Wajib Prodi', 'Tingkat 3', 5),
(77, 'DAH2A2', 'BAHASA INGGRIS III', 2, 'Wajib Prodi', 'Tingkat 2', 6),
(78, 'DUH1A2', 'LITERASI TIK', 2, 'Wajib Prodi', 'Tingkat 1', 6),
(79, 'DMH2B2', 'PERILAKU ORGANISASI', 2, 'Wajib Prodi', 'Tingkat 2', 6),
(81, 'DPH1C4', 'PEMROGRAMAN BERORIENTASI OBYEK', 4, 'Wajib Prodi', 'Tingkat 1', 6),
(82, 'DMH1K3', 'PERANCANGAN BASIS DATA', 3, 'Wajib Prodi', 'Tingkat 1', 6),
(83, 'DMH2C3', 'PEMROGRAMAN BASIS DATA', 3, 'Wajib Prodi', 'Tingkat 2', 6),
(84, 'DMH2E2', 'PROYEK II', 2, 'Wajib Prodi', 'Tingkat 2', 6),
(85, 'DPH2C2', 'PENGUJIAN PERANGKAT LUNAK', 2, 'Wajib Prodi', 'Tingkat 2', 6),
(86, 'DIH1A4', 'IMPLEMENTASI DESAIN ANTARMUKA PENGGUNA', 4, 'Wajib Prodi', 'Tingkat 1', 6),
(87, 'DMH1D3', 'PROSES BISNIS', 3, 'Wajib Prodi', 'Tingkat 1', 6),
(88, 'DMH2F3', 'ANALISIS DAN PERANCANGAN SISTEM INFORMASI', 3, 'Wajib Prodi', 'Tingkat 2', 6),
(89, 'DMH1E2', 'PROYEK I', 2, 'Wajib Prodi', 'Tingkat 1', 6),
(90, 'DUH2A2', 'KEWIRAUSAHAAN', 2, 'Wajib Prodi', 'Tingkat 2', 6),
(91, 'DPH2B4', 'PEMROGRAMAN WEB LANJUT', 4, 'Wajib Prodi', 'Tingkat 2', 6),
(92, 'LUH1B2', 'BAHASA INGGRIS I', 2, 'Wajib Prodi', 'Tingkat 1', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_mhs_pa`
--

CREATE TABLE `t_mhs_pa` (
  `id_mhs` int(11) NOT NULL,
  `nim_mhs` bigint(20) NOT NULL,
  `nama_mhs` varchar(49) NOT NULL,
  `judul_pa` text NOT NULL,
  `id_doping1` int(11) DEFAULT NULL,
  `id_doping2` int(11) DEFAULT NULL,
  `tahap` int(11) NOT NULL,
  `angkatan` varchar(9) NOT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `grup` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_mhs_pa`
--

INSERT INTO `t_mhs_pa` (`id_mhs`, `nim_mhs`, `nama_mhs`, `judul_pa`, `id_doping1`, `id_doping2`, `tahap`, `angkatan`, `id_tahunajaran`, `grup`) VALUES
(1, 6701152089, 'RIO FRIYANDO S.', 'Analisis Pengunjung Berdasarkan Gender', 3, 25, 4, '2015', 3, 1),
(2, 6701154074, 'ERZA A FAHREZI', 'Analisis Pengunjung Berdasarkan Gender', 3, 25, 1, '2015', 3, 1),
(3, 6701152081, 'Samuel Firma Windu Nainggolan', 'Aplikasi berbasis web  Pengadaan Obat Studi kasus : Apotek Siliwangi ', 9, 2, 1, '2015', 3, 2),
(4, 6701150046, 'Septhian Manroe', 'Aplikasi Instalasi Rawat Inap Klinik Siliwangi', 9, 2, 1, '2015', 3, 2),
(5, 6701154181, 'Andreas Togos Silalahi', 'Aplikasi Pengolahan Data Pasien Rawat Jalan Klinik Siliwangi Baleendah Bandung Berbasis Web dan SMS Gateway', 9, 2, 1, '2015', 3, 2),
(6, 6701150024, 'MUNIRSYAH', 'Aplikasi Berbasis Web Inventaris Aset Desa (Studi Kasus: Kecamatan Dayeuhkolot)', 27, 29, 1, '2015', 3, 3),
(7, 6701150109, 'ADELYA ASTARI', 'Aplikasi Berbasis Web Inventaris Aset Desa (Studi Kasus: Kecamatan Dayeuhkolot)', 27, 29, 1, '2015', 3, 3),
(8, 6701150131, 'Alika Safitri', 'Aplikasi berbasis web Penyandang Masalah Kesejahteraan Sosial Studi Kasus : Dinas Sosial Kabupaten Bandung Modul Dinas Sosial', 27, 29, 4, '2015', 3, 4),
(9, 6701154171, 'Dinda Lestari Zaenal', 'Aplikasi berbasis web Penyandang Masalah Kesejahteraan Sosial Studi Kasus : Dinas Sosial Kabupaten Bandung Modul Kecamatan dan Desa', 27, 29, 4, '2015', 3, 4),
(10, 6701150107, 'Dhea Widyana Putri', 'Aplikasi E-Recruitment Pegawai Baru dan Pengelolaan Data Pegawai Berbasis WEB (Modul Pengelolaan Data Pegawai)', 20, 24, 4, '2015', 3, 5),
(11, 6701154142, 'Vivi Rizqia', 'Aplikasi E-Recruitment Pegawai Baru dan Pengelolaan Data Pegawai Berbasis WEB (Modul Rekruitasi Pegawai)', 20, 24, 1, '2015', 3, 5),
(12, 6701150041, 'Timmie Siswandi', 'Aplikasi Inventaris Toko Buku Zahra Berbasis Web Studi kasus : Zahra Books Jombang', 16, 6, 1, '2015', 3, 6),
(13, 6701151061, 'Aji Bagus Panuntun', 'Aplikasi Penjualan Buku di Zahra Book Berbasis Web', 16, 6, 1, '2015', 3, 6),
(14, 6701150016, 'isna nurdiansyah', 'Aplikasi Siklus Percetakan Buku Studi Kasus : Zahra Books Jombang', 16, 6, 1, '2015', 3, 6),
(15, 6701154101, 'Ario Barnas Hardi', 'Aplikasi Layanan Kesehatan Klinik Brimedika Bandung Modul Apotek', 11, 13, 1, '2015', 3, 7),
(16, 6701152201, 'Gilda Marsaulina Hutabarat', 'Aplikasi Layanan Kesehatan Klinik Brimedika Bandung Modul Rawat Jalan Berbasis Web', 11, 13, 1, '2015', 3, 7),
(17, 6701154166, 'Pratiwi Syifa', 'Aplikasi Layanan Kesehatan Klinik Brimedika Bandung untuk Modul Medical Check- Up', 11, 13, 1, '2015', 3, 7),
(18, 6701150025, 'JOSUA NUGRAHA P', 'Aplikasi pelaporan OPT Holtikultura Studi Kasus : BPTPH', 27, 29, 1, '2015', 3, 8),
(19, 6701152090, 'I KADEK WIJAYA K', 'Aplikasi pelaporan OPT Pangan Studi Kasus : BPTPH', 27, 29, 1, '2015', 3, 8),
(20, 6701150106, 'Ai Siti Sopiah', 'Aplikasi pemesanan dan Delivery Makanan di Kantin Universitas Telkom Modul Pelanggan dan Deliv-Man', 23, 26, 1, '2015', 3, 9),
(21, 6701151056, 'Asido Sibuea', 'Aplikasi pemesanan dan Delivery Makanan di Kantin Universitas Telkom Modul Pemesanan ', 23, 26, 1, '2015', 3, 9),
(22, 6701154137, 'Tiara Sabila', 'Aplikasi Pendaftaran dan Transaksi Klinik Hewan Berbasis Android ', 10, 14, 1, '2015', 3, 10),
(23, 6701154152, 'Katarina Astridya', 'Aplikasi Pendaftaran dan Transaksi Klinik Hewan Berbasis Web (Modul Pengelolaan Data Klinik) ', 10, 14, 1, '2015', 3, 10),
(24, 6701150127, 'Nabila Rizkita', 'Aplikasi Pendaftaran dan Transaksi Klinik Hewan Berbasis Web (Modul Pengelolaan Data Pasien dan Transaksi) ', 10, 14, 1, '2015', 3, 10),
(25, 6701152082, 'NOVALDI AMIRULLAH', 'APLIKASI PENDATAAN PUSAT LAYANAN \nKESEHATAN DI KABUPATEN BANDUNG \nBERBASIS ANDROID', 2, 6, 1, '2015', 3, 11),
(26, 6701151057, 'FAHSYA PERMANA PUTRA', 'APLIKASI PENDATAAN PUSAT LAYANAN \nKESEHATAN DI KABUPATEN BANDUNG \nBERBASIS WEB MODUL : KLINIK, POSYANDU, PUSKESMAS)', 14, 4, 1, '2015', 3, 11),
(27, 6701151062, 'FIKRI HARJO YUDHANTO', 'APLIKASI PENDATAAN PUSAT LAYANAN KESEHATAN DI KABUPATEN BANDUNG BERBSIS WEB MODUL:  RUMAH SAKIT', 14, 4, 1, '2015', 3, 11),
(28, 6701152146, 'ayidha elvira syam', 'Aplikasi Pengadaan Barang/Jasa Studi Kasus PT Bhakti Unggul Teknovasi Modul Pemesanan dan Pengelolaan Pengadaan barang ', 20, 24, 1, '2015', 3, 12),
(29, 6701154156, 'gundari amboro sati', 'Aplikasi Pengadaan Barang/Jasa Studi Kasus PT Bhakti Unggul Teknovasi Modul Pengadaan Barang', 20, 24, 1, '2015', 3, 12),
(30, 6701154196, 'Ari Nur Fatwa', 'Aplikasi pengelolaan aktivitas produksi berbasis web pada perusahaan zaemerci', 11, 13, 1, '2015', 3, 13),
(31, 6701154066, 'Hanif Farras Alfalah', 'Aplikasi pengelolaan keuangan dan penjualan di clothing line zaemerci berbasis web', 11, 13, 1, '2015', 3, 13),
(32, 6701154167, 'Fakhrunnisa Nur Afra', 'Aplikasi Pengelolaan Dan Reservasi Hotel di Bandung Berbasis Web (Modul Pengelolaan Hotel)', 14, 10, 1, '2015', 3, 14),
(33, 6701154162, 'Rhadiathul Hasnah', 'Aplikasi Pengelolaan Dan Reservasi Hotel di Bandung Berbasis Web (Modul Reservasi Hotel)', 14, 10, 1, '2015', 3, 14),
(34, 6701150042, 'Lazzuardi Rifqian Naufal', 'Aplikasi Pengelolaan Pencucian Mobil Di Kota Bandung Berbasis Android', 14, 9, 1, '2015', 3, 15),
(35, 6701152192, 'Assyifannisa', 'Aplikasi Pengelolaan Pencucian Mobil Di Kota Bandung Berbasis Web', 14, 9, 1, '2015', 3, 15),
(36, 6701152176, 'Adhitya Candra PP', 'Aplikasi Pengolahan Sertifikasi dan Pelatihan di Laboratorium Aplikasi Terapan Studi kasus : Laboratorium FIT Modul: Pelatihan', 11, 13, 1, '2015', 3, 16),
(37, 6701154141, 'Revina Yoga', 'Aplikasi Pengolahan Sertifikasi dan Pelatihan di Laboratorium Aplikasi Terapan Studi kasus : Laboratorium FIT Modul: sertifikasi ', 11, 13, 1, '2015', 3, 16),
(38, 6701154094, 'VALENTINO ROSSI FIERDAUS SAEPUDIN', 'Aplikasi Sistem Informasi Akademik Berbasis Android', 20, 24, 1, '2015', 3, 17),
(39, 6701150104, 'HESTY KHAIRUNISA', 'Aplikasi Sistem Informasi Akademik Berbasis Web', 20, 24, 1, '2015', 3, 17),
(40, 6701154103, 'KHULAFAUR RASYIDIN', 'Aplikasi Virtual Market untuk Pengembanan UKM (Pembeli)', 11, 13, 1, '2015', 3, 18),
(41, 6701151134, 'LAILY INDARYANI', 'Aplikasi Virtual Market untuk Pengembangan UKM (Pengelola)', 11, 13, 1, '2015', 3, 18),
(42, 6701150113, 'LARAS ARDINA', 'Aplikasi Virtual Market untuk Pengembangan UKM (Penyalur)', 11, 13, 1, '2015', 3, 18),
(43, 6701150125, 'NONITA SARI SIHITE', 'Aplikasi Virtual Market Untuk Pengembangan UMKM (Penyalur)', 11, 13, 1, '2015', 3, 18),
(44, 6701154145, 'DINDA AISYAH J A', 'Aplikasi Virtual Market Untuk Pengembangan UMKM (Penyedia)', 11, 13, 1, '2015', 3, 18),
(45, 6701154170, 'RAHMAD GERI K', 'Aplikasi Virtual Market Untuk Pengembangan UMKM (Penyedia)', 11, 13, 1, '2015', 3, 18),
(46, 6701154183, 'GITA VHANIE ADISTY', 'Bot Tools untuk Auto Delete Comment yang Tidak Diinginkan', 23, 26, 1, '2015', 3, 19),
(47, 6701154158, 'ELSA TRIANA SINAGA', 'Bot Tools untuk Auto Delete Friend dengan Kriteria Tertentu', 23, 26, 4, '2015', 3, 19),
(48, 6701154067, 'Shafrian Dwitama', 'MUSLIM ASSISTANT Modul : Admin', 21, 8, 1, '2015', 3, 20),
(49, 6701154102, 'Irfan Fahmi Wijaya', 'MUSLIM ASSISTANT Modul : Doa, Lokasi Masjid, Arah Kiblat, Penginapan Syariah, Restoran Halal, Jadwal Solat ', 21, 8, 4, '2015', 3, 20),
(50, 6701152077, 'Mughny Mubarak', 'MUSLIM ASSISTANT Modul : Qur''an dan Hadits', 21, 8, 1, '2015', 3, 20),
(51, 6701150002, 'RIO CIKAL GIGIH PERMANA', 'Mustream – Website Client Music Streaming', 21, 8, 1, '2015', 3, 21),
(52, 6701150007, 'Tegar Abdillah', 'MuStream Aplikasi Server Music Streaming Modul ????', 21, 8, 1, '2015', 3, 21),
(53, 6701150017, 'Yuwanda Fajar', 'MuStream Aplikasi Server Music Streaming untuk Modul Manajemen Konten dan Royalti', 21, 8, 1, '2015', 3, 21),
(54, 6701152087, 'Zulhilmi Ghiffary', 'Aplikasi Streaming Music untuk Komunikasi dan Pengolahan data Streaming pada server (Ampache)', 21, 8, 1, '2015', 3, 21),
(55, 6701154182, 'Fadhil Cahya Kesuma', 'Panti App (Aplikasi Managemen Operasional & Donasi Panti Sosial) Modul  Operasional Panti', 21, 8, 1, '2015', 3, 22),
(56, 6701150032, 'Arif Rizqi Satriana', 'Panti App (Aplikasi Managemen Operasional & Donasi Panti Sosial) Modul Donasi', 21, 8, 1, '2015', 3, 22),
(57, 6701150132, 'Anggi Prayudi', 'Panti App (Aplikasi Managemen Operasional & Donasi Panti Sosial) Modul Service Aplikasi', 21, 8, 1, '2015', 3, 22),
(58, 6701150019, 'ANDI KURNIANTO', 'Pembelajaran Bahasa Isyarat', 3, 25, 1, '2015', 3, 23),
(59, 6701150023, 'RIYANTO PRASTIYO', 'Pembelajaran Bahasa Isyarat', 3, 25, 1, '2015', 3, 23),
(60, 6701150029, 'FADHLI FAUZAN', 'Pembelajaran Jari Tangan untu Bayi Di Bawah Dua Tahun', 3, 25, 1, '2015', 3, 24),
(61, 6701150044, 'SAFII YOGI SAPUTRA', 'Pembelajaran Jari Tangan untu Bayi Di Bawah Dua Tahun', 3, 25, 1, '2015', 3, 24),
(62, 6701150012, 'MUHAMMAD NAUFAL AL FARISI', 'SISTEM INFORMASI GEOGRAFIS & BANDUNG TOUR GUIDE Berbasis Android', 24, 20, 1, '2015', 3, 25),
(63, 6701150122, 'ERIN KARINA NUR FADILLAH', 'SISTEM INFORMASI GEOGRAFIS & BANDUNG TOUR GUIDE Berbasus Web', 24, 20, 1, '2015', 3, 25),
(64, 6701154097, 'Puger Pamungkas', 'SISTEM INFORMASI KERUSAKAN JALAN BERBASIS ANDROID (STUDI KASUS PEMERINTAHAN KAB. BANDUNG)', 14, 25, 1, '2015', 3, 26),
(65, 6701152187, 'Nur Adin', 'Sistem Informasi Kerusakan Jalan Berbasis Website  (Studi kasus Dinas Pekerjaan Umum dan Penataan Ruang Kabupaten Bandung)', 14, 20, 1, '2015', 3, 26),
(66, 6701150001, 'Furqon al khudzaefi', 'Human Resource Information System at Bandung Techno Park for Competency Based Human Resource Management 2', 23, 26, 1, '2015', 3, 27),
(67, 6701154151, 'Niken Galuh Sinta Wardani', 'Sistem Informasi Sumber Daya Manusia di Bandung Techno Park modul Layanan Diri Karyawan', 23, 26, 1, '2015', 3, 27),
(68, 6701150051, 'T M Reza Rahadian', 'Sistem Informasi Sumber Daya Manusia di Bandung Techno Park untuk modul pengelolaan kompetensi sumberdaya manusia 1', 23, 26, 1, '2015', 3, 27),
(69, 6701154200, 'AHMAD ADLI', 'Sistem Manajemen Perangkat di Laboratorium Fakultas Ilmu Terapan', 13, 11, 1, '2015', 3, 28),
(70, 6701152190, 'DHEA KHAIRINNISA', 'Sistem Procurement Perangkat di Laboratorium Fakultas Ilmu terapan', 13, 11, 1, '2015', 3, 28),
(71, 6701150045, 'FAKHRI NAUFAL ALI', 'Aplikasi Pemesanan Dan Monitoring Data Barang Berbasis Web', 13, 11, 1, '2015', 3, 29),
(72, 6701154165, 'IRSAN MARDIANSYAH', 'Aplikasi Pemesanan dan Pelaporan Data Barang Berbasis Android', 13, 11, 1, '2015', 3, 29),
(73, 6701152180, 'RAJA IRFAN ADIPUTRA R', 'Aplikasi Pengelolaan Data Produksi CV HFM', 13, 11, 1, '2015', 3, 29),
(74, 6701150114, 'NYOMAN IRVIANTI WINDAPUTRI', 'Aplikasi “Rumahku” Pembelajaran Interaktif Mengenal Bagian Rumah Berbasis Android', 29, 27, 1, '2015', 3, 30),
(75, 6701152199, 'SANTI AL ARIF', 'Aplikasi Edukasi Pengenalan Binatang (Studi Kasus: PGTK Darul Hikam)', 29, 27, 1, '2015', 3, 30),
(76, 6701152149, 'FABIOLA MATARA', 'Aplikasi Media Interaktif Pengenalan Macam-macam Alat Komunikasi', 29, 27, 1, '2015', 3, 30),
(77, 6701154144, 'ANNISA ROHMAH', 'Aplikasi Pembelajaran Untuk Anak Usia Dini Dalam Mengenal Profesi Atau Cita-cita', 29, 27, 1, '2015', 3, 30),
(78, 6701150049, 'MUHAMMAD DALLA LILHAQ', 'Aplikasi Pengenalan Alam Semesta Ciptaan Allah (Air, Api, Udara, Benda Langit, dan Gejala Alam) (Studi Kasus: TK Darul Hikam)', 29, 27, 1, '2015', 3, 30),
(79, 6701154069, 'MUHAMMAD APRIAWAN DERAJAT', 'Aplikasi Pengenalan dan Pembelajaran Budaya Nusantara', 29, 27, 1, '2015', 3, 30),
(80, 6701150039, 'ERVAN ADITYA NUGRAHA', 'Aplikasi Pengenalan Tanaman Hias, Umbi-umbian, Apotik Hidup dan Sayuran (Studi kasus: TK Darul Hikam)', 29, 27, 1, '2015', 3, 30),
(81, 6701154154, 'NYSSA RAMADHANTI', 'Media Pembelajaran Hebatnya Alat Transportasi (Studi Kasus: TK Darul Hikam)', 29, 27, 1, '2015', 3, 30),
(82, 6701150047, 'LALU GDE MUHAMMAD FARIZT', 'Aplikasi Pengelolaan Pencapaian Kinerja Dosen Bidang Pengajaran', 14, 23, 7, '2015', 3, 31),
(83, 6701151059, 'DILRAJ PUTRA', 'Aplikasi Pengelolaan Pencapaian Kinerja Dosen Bidang Pengajaran', 14, 23, 7, '2015', 3, 31),
(84, 6701151054, 'MUHAMMAD DERRY SALMAN SYAHID', 'Aplikasi Portofolio Pengajaran Dosen', 23, 14, 7, '2015', 3, 31),
(85, 6701150174, 'IRFAN FATHUR RAHMAN', 'Dashboard Monitoring Kinerja Dosen Bidang Pengajaran', 24, 14, 4, '2015', 3, 31),
(86, 6701150034, 'RIZKY WAHYUDI', 'Pengenalan Wajah untuk Presensi Mahasiswa', 3, 25, 1, '2015', 3, 32),
(87, 6701151064, 'DEAN ARIFIN SIBURIAN', 'Pengenalan Wajah untuk Presensi Pegawai', 3, 25, 1, '2015', 3, 32),
(88, 6701152078, 'DILLA FAJRI', 'Kepuasan Pelanggan', 3, 25, 1, '2015', 3, 33),
(89, 6701150028, 'IMAM RIFA''I', 'Kepuasan Pelanggan Pada Sentra Industri Konveksi', 3, 25, 1, '2015', 3, 33),
(90, 6701154095, 'DEBY MUHAMMAD BUDI', 'Pengukuran Kepuasan Layanan Pelanggan Menggunakan Depth Camera', 3, 25, 1, '2015', 3, 33),
(91, 6701154073, 'SATYA ANGGRAHITA', 'Aplikasi Virtual Assistant Berbasis Android', 23, 26, 1, '2015', 3, NULL),
(92, 6701140102, 'Putri dewi maharani', ' Aplikasi Pembelajaran bahasa jepang tingkat N5 (pemula) berbasis android', 16, 6, 1, '2015', 3, NULL),
(93, 6301130082, 'BRAM PURBA', 'Aplikasi Administrasi Keanggotaan BBC Fitness Centre Berbasis Web', 6, 16, 1, '2015', 3, NULL),
(94, 6701140208, 'Jonathan Juniko Wijaya', 'APLIKASI ADMINISTRASI PEMBAYARAN SUMBANGAN PEMBINAAN PENDIDIKAN (SPP) DAN DANA SUMBANGAN PEMBANGUNAN (DSP) DI SD ANANDA BOJONG KULUR KABUPATEN BOGOR', NULL, NULL, 1, '2015', 3, NULL),
(95, 6701140028, 'Faris Zulfikar Hidayat', 'Aplikasi Centra Meubel Online', 26, 23, 1, '2015', 3, NULL),
(96, 6701150027, 'Okananda Rizky Perdana', 'Aplikasi Dokumen Manajemen\nSystem (DMS) berbasis Tata\nkelola Data DAMA International\npada perusahaan ', 13, 11, 1, '2015', 3, NULL),
(97, 6701150121, 'Vina Dwi Mercuri', 'Aplikasi EatMe Berbasis Web pada Wagoon Coffee Buah Batu', 20, 24, 1, '2015', 3, NULL),
(98, 6701150011, 'Kiki Sahnakri ', 'Aplikasi Latihan dan Simulasi Uji Kemampuan Bahasa Indonesia Berbasis Web', 27, 29, 1, '2015', 3, NULL),
(99, 6701150036, 'Agung Harianto S', 'Aplikasi Manejemen penghuni perumahaan Studi Kasus : Perumahan Permata Buah Batu ', 6, 16, 1, '2015', 3, NULL),
(100, 6701152193, 'AJENG ANJANI R', 'Aplikasi Monitoring dan Pemesanan Tiket Bus dengan Metode FIFO Berbasis Web pada PT Bus Budiman', 6, 16, 1, '2015', 3, NULL),
(101, 6701150110, 'LUH PUTU YULI ASTRINI', 'Aplikasi Pelayanan Masyarakat Desa Cipagalo Berbasis Android', 6, 16, 1, '2015', 3, NULL),
(102, 6701154198, 'NOVIA NUR HIDAYAH', 'Aplikasi Pembelajaran Bahasa Jepang Berbasis Android', 21, 8, 1, '2015', 3, NULL),
(103, 6701154143, 'NAFINA SALMA SETIAWAN', 'Aplikasi Pembelajaran Huruf Hijaiyah Berbasis Android Bertemakan Flat Design', 24, 20, 1, '2015', 3, NULL),
(104, 6701150005, 'GIFARI BAGASKARA', 'Aplikasi Pembuatan Kode Buku pada Dinas Perpustakaan dan Kearsipan Kota Bandung', 24, 20, 1, '2015', 3, NULL),
(105, 6701150116, 'Elisabet Patrisia', 'Aplikasi Pemesanan Tiket Bus Secara Online Berbasis Web Di CV Harum Prima Bandung', 29, 27, 1, '2015', 3, NULL),
(106, 6701150040, 'ARVIN LAZAWARDI P', 'Aplikasi Peminjaman Ruangan dan Aset Laboratorium', 13, 11, 1, '2015', 3, NULL),
(107, 6701154159, 'GINSHA ASMAUL SITI KHASANAH', 'Aplikasi Peminjaman Sepeda Berbasis Android Pada Studi Kasus Telkom University', 29, 27, 1, '2015', 3, NULL),
(108, 6701154150, 'ZSAZSA DWI LINGGASARI', 'Aplikasi penatausahaan Pengadaan Barang Bagian Badan Pengelolaan Keuangan dan Aset Daerah (BPKAD) Pemerintahan Kota Bandung Berbasis Web Menggunakan Metode Just In Time', 6, 16, 1, '2015', 3, NULL),
(109, 6701150105, 'TIKA HANDAYANI P', 'Aplikasi Pencatatan Data Aspirasi Masyarakat: Komplain, Kritik, dan Saran Desa Cipagalo Berbasis Web', 6, 16, 1, '2015', 3, NULL),
(110, 6701152080, 'DEDE SURYAWAN A P', 'Aplikasi Pencatatan Pelanggaran Siswa Berbasis Web', 20, 24, 1, '2015', 3, NULL),
(111, 6701154184, 'VIONA REWITA', 'Aplikasi Pendaftaran Berobat Online dan Penjadwalan Dokter Berbasis Web (Studi Kasus: Klinik dan Apotik Bona Mitra Keluarga)', 9, 18, 1, '2015', 3, NULL),
(112, 6701154155, 'MADE SINTAYATI', 'Aplikasi Pendataan dan Pengelolaan Imunisasi Rutin di Puskesmas Bojongsoang Berbasis Web', 6, 16, 1, '2015', 3, NULL),
(113, 6701150130, 'LIDYA NURINNAHAR', 'Aplikasi Pendataan Ketersediaan Alat dan Bahan Imunisasi Rutin di Puskesmas Bojongsoang Berbasis Web', 6, 16, 1, '2015', 3, NULL),
(114, 6701150015, 'IQBAL AL-HAKIM', 'Aplikasi Pengelolaan Administrasi Puskesmas Bojongsoang', 8, 21, 1, '2015', 3, NULL),
(115, 6701150026, 'Ahmad Nadliri', 'Aplikasi Pengelolaan Belanja kebutuhan ATK Studi kasus : Dinas Pendidikan Kabupaten Jombang', 18, 21, 1, '2015', 3, NULL),
(116, 6701152177, 'Rinda Firma Violita', 'Aplikasi Pengelolaan Biaya Pendidikan di Madrasah Tsnawiyah PERSIS Ciganitri-Bandung', 27, 18, 1, '2015', 3, NULL),
(117, 6701152147, 'RIZKA DWI WUANDARI S.', 'Aplikasi Pengelolaan Dana Bantuan Operasional Sekolah pada Madrasah Tsanawiyah Pesantren Persatuan Islam Ciganitri', 2, 9, 1, '2015', 3, NULL),
(118, 6701150120, 'BELLA ANGGRYENI', 'Aplikasi pengelolaan data dan informasi kepegawaian Badan Pengelola Keuangan dan Aset (BPKA) daerah kota Bandung berbasis Web', 16, 6, 1, '2015', 3, NULL),
(119, 6701150119, 'RIMA NOVIANTI', 'Aplikasi Pengelolaan Data Kepegawaian Berbasis Web (Studi Kasus: Dinas Perpustakaan dan Kearsipan Daerah)', 10, 18, 1, '2015', 3, NULL),
(120, 6701152175, 'REVI CHANDRA RIANA', 'Aplikasi Pengelolaan E-KTP Kelurahan Kujangsari', 26, 10, 1, '2015', 3, NULL),
(121, 6701151058, 'SETYO AJI', 'Aplikasi Pengelolaan Iklan Virtual Berbasis Media Sosial', 18, 21, 1, '2015', 3, NULL),
(122, 6701154195, 'HAFIYANIDA RAHMANI', 'Aplikasi Pengelolaan Keuangan Untuk Pengajuan Kebutuhan dari Masyarakat Desa Cipagalo Berbasis Web', 16, 6, 1, '2015', 3, NULL),
(123, 6701154100, 'MUBAROQ IQBAL', 'Aplikasi Pengelolaan Pendaftaran Ulang SMKN 2 Kepahiang', 24, 20, 1, '2015', 3, NULL),
(124, 6701154140, 'LATIFA NUR FITRIANA', 'Aplikasi Pengelolaan Penerimaan Dana Bantuan Untuk Desa Cipagalo Berbasis Web', 16, 6, 1, '2015', 3, NULL),
(125, 6701154160, 'NADIA SALSABILA', 'Aplikasi Pengelolaan Pengajuan Dana Keuangan untuk Desa Cipagalo dari Pemerintah Kabupaten Bandung Selatan Berbasis Web', 16, 6, 1, '2015', 3, NULL),
(126, 6701150117, 'Ficky Azizah', 'Aplikasi Pengelolaan Presensi dan Penggajian Karyawan Berbasis Web', 26, 10, 1, '2015', 3, NULL),
(127, 6701150020, 'MUHAMAD IKHSAN M', 'Aplikasi Pengelolaan Rental Mobil', 26, 10, 1, '2015', 3, NULL),
(128, 6701150021, 'Arrahman Ayuskhan', 'Aplikasi Pengelolaan Stok Barang Nun Jilbab Bandung', 10, 18, 1, '2015', 3, NULL),
(129, 6701150115, 'NIZMA AINUN', 'Aplikasi Pengelolaan Surat Dinas Masuk dan Surat Dinas Keluar di Badan Pengelolaan Keuangan dan Aset Daerah Kota Bandung Berbasis Web', 16, 18, 1, '2015', 3, NULL),
(130, 6701152185, 'MOCHAMAD SYAFIQ A', 'Aplikasi Penggajian Berdasarkan Absensi PT Sekawan Karya Cipta', 18, 10, 1, '2015', 3, NULL),
(131, 6701154153, 'ANNISA ROMADONA FAUZIA', 'Aplikasi Penghitung HPP dan Studi Kelayakan Produk untuk UMKM Berbasis Desktop', 13, 11, 1, '2015', 3, NULL),
(132, 6701150111, 'AMBAR SORAYA', 'Aplikasi pengolahan cek in, cek out, dan housekeeping di hotel gurame berbasis web ', 3, 25, 1, '2015', 3, NULL),
(133, 6701140073, 'Adinda Indah Permata', 'Aplikasi Pengolahan Data Obat Berbasis Web pada klinik perawatan Dermabrasy Therapy Skincare ', 26, 23, 1, '2015', 3, NULL),
(134, 6701152148, 'DESSY YUSSELA M Y', 'Aplikasi Pengolahan Nilai Rapor SMP BPI 1 Bandung', 21, 8, 1, '2015', 3, NULL),
(135, 6701150043, 'ILHAM FIRDAUS', 'Aplikasi Penjualan Online Berbasis Web pada Toko Citra Oniq', 10, 18, 1, '2015', 3, NULL),
(136, 6701151060, 'RISWAN ARDINATA', 'Aplikasi Perhitungan Gaji Untuk Armada Truk Berdasarkan Pendapatan Per unit PT. BKW Berbasis Web', 18, 10, 1, '2015', 3, NULL),
(137, 6701150108, 'PIO ANDINA SEREPINA SIMANULLANG', 'Aplikasi Perhitungan Masa Subur Wanita', 10, 23, 1, '2015', 3, NULL),
(138, 6701150006, 'Dicky Muhammad Sofian', 'Aplikasi Perpustakaan Di SMAN 27 Bandung berbasis Sms Gateway', 8, 21, 1, '2015', 3, NULL),
(139, 6701150038, 'RAHMAWAN A', 'Aplikasi Prediksi Pemintatan di SMA dengan Menggunakan Algoritma K-Nearest Neighbor', 10, 20, 1, '2015', 3, NULL),
(140, 6701154099, 'LUTHFI ANUGERAH MIRDA', 'Aplikasi Presensi dan Interaktif Greeting', 3, 25, 1, '2015', 3, NULL),
(141, 6701150124, 'HARTATI SEPDIYANTI', 'Aplikasi Referensi Obat, Penjualan, dan Pendataan Obat Berbasis Web (Studi Kasus: Apotik Nusa Farma bandung)', 2, 9, 1, '2015', 3, NULL),
(142, 6701150118, 'RINEZ ASPRINOLA', 'Aplikasi Rekomendasi Makanan pada Solusi Penyakit Menggunakan Metode Forward Chaining', 16, 18, 1, '2015', 3, NULL),
(143, 6701152179, 'AYU KARTIKA SANDHY', 'Aplikasi Simpan Pinjam Koperasi', 2, 9, 1, '2015', 3, NULL),
(144, 6701150133, 'MEGA CANDRA DEWI', 'Aplikasi Toko Obat Berbasis Android', 24, 20, 1, '2015', 3, NULL),
(145, 6701154072, 'Akbar Maulana', 'Aplikasi Ujian Online dan Analisis Soal SMAN 1 CIBINONG', 18, 11, 1, '2015', 3, NULL),
(146, 6701150112, 'Lia Maulita R', 'Dashboard Aplikasi Posyandu : Monitoring dan Controlling Tumbuh Kembang anak, Pemberian imunisasi dan Vitamin A', 27, 29, 1, '2015', 3, NULL),
(147, 6701151135, 'LAILA FATHHIYANA A', 'Integrasi Aplikasi', 24, 20, 1, '2015', 3, NULL),
(148, 6701154138, 'VIYONA RASINTA MAGDALENA PURBA', 'Media Pembelajaran Interaktif Pengenalan Peta dan Budaya Indonesia (Studi Kasus: SD Tiara Bunda, Batununggal Bandung)', 9, 4, 1, '2015', 3, NULL),
(149, 6701152202, 'Jihad Dwi Cahyono', 'PEMBANGUNAN APLIKASI ASSESMENT KUALITAS DATA DENGAN METODE CALDEA EVAMELCA BERBASIS WEB\n(Studi Kasus: Telkom University)', 13, 11, 1, '2015', 3, NULL),
(150, 6701151053, 'MADE DWI DHARMA SREYA', 'Pembangunan Aplikasi Manajemen Proyek untuk Perusahaan Roketin Bandung', 18, 10, 1, '2015', 3, NULL),
(151, 6701154070, 'AULIA MEI AZIZI', 'Pengenalan Muka Untuk Presensi Mahasiswa', 3, 25, 1, '2015', 3, NULL),
(152, 6701154096, 'Sultan Habib Putera Kesuma', 'Portal Data Pemerintah Kabupaten Bandung', 27, 29, 1, '2015', 3, NULL),
(153, 6701154139, 'NINING KARTIKA', 'Prix Simanday', 18, 20, 1, '2015', 3, NULL),
(154, 6701154071, 'rahmat alifuddin nur', 'Reservasi Online Pemesanan Makanan d Warung Shinju Ramen Bandung', 2, 9, 1, '2015', 3, NULL),
(155, 6701150010, 'ILHAM WAHYUDI', 'Sistem Informasi Administrasi Data Kependudukan pada Kantor Kelurahan Desa Kujangsari Kab. Bandung', 26, 23, 1, '2015', 3, NULL),
(156, 6701150014, 'AGRI SYAHRIAL', 'Aplikasi Monitoring Sampah Rumah Tangga', 9, 29, 1, '2015', 3, NULL),
(157, 6701154068, 'HENDRI SUSANTO', 'Aplikasi Transaksi dan Penjualan Berbasis Web (Studi Kasus: PT Pos Indonesia Canag Kota Cimahi)', 26, 23, 1, '2015', 3, NULL),
(158, 6701150008, 'JIMMY PUTERA MANURUNG', 'Aplikasi Rawat Jalan di Klinik Mulia Sehat Berbasis Web', 9, 2, 1, '2015', 3, NULL),
(159, 6701150018, 'A. RIZALDY MUQORROBIN', 'Aplikasi Pembelajaran Tata Surya Bagi Siswa Dasar Kelas 6 Menggunakan Augmented Reality', 11, 17, 1, '2015', 3, NULL),
(160, 6701150033, 'MESA BASTIAN', 'Aplikasi Pengenalan Hewan Menggunakan Augmented Reality Berbasis Android', 23, 4, 1, '2015', 3, NULL),
(161, 6701150048, 'KHALIFASYA GIBRANSAM', 'Aplikasi Pasar Item Game Online', 21, 8, 1, '2015', 3, NULL),
(162, 6701150123, 'EVITA CHRISTI MATONDANG', 'Aplikasi Penyewaan Rumah di Perumahan Sekitar Universitas Telkom (Studi Kasus Perumahan Sekitar Universitas Telkom)', 24, 28, 1, '2015', 3, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_modul`
--

CREATE TABLE `t_modul` (
  `id_modul` int(5) NOT NULL,
  `fileName` text NOT NULL,
  `date` date NOT NULL,
  `status_modul` varchar(150) NOT NULL,
  `keterangan` text,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_modul`
--

INSERT INTO `t_modul` (`id_modul`, `fileName`, `date`, `status_modul`, `keterangan`, `id_matakuliah`) VALUES
(1, '001-DONA3.docx', '2018-06-03', 'APPROVE', NULL, 83);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_nilai`
--

CREATE TABLE `t_nilai` (
  `id_nilai` int(12) NOT NULL,
  `id_dosen` int(12) NOT NULL,
  `nilai_bap` double DEFAULT NULL,
  `nilai_rapat_prodi` double DEFAULT NULL,
  `nilai_rapat_koor` double DEFAULT NULL,
  `nilai_rapat_prodi2` double DEFAULT NULL,
  `nilai_rapat_koor2` double DEFAULT NULL,
  `nilai_jml_bimb` double DEFAULT NULL,
  `nilai_kk` double DEFAULT NULL,
  `nilai_usulan` double DEFAULT NULL,
  `nilai_penyerahan` double DEFAULT NULL,
  `nilai_edom` double DEFAULT NULL,
  `nilai_jml_de` double DEFAULT NULL,
  `nilai_jml_sidang` double DEFAULT NULL,
  `nilai_perwalian` double DEFAULT NULL,
  `nilai_lks` double DEFAULT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data untuk tabel `t_nilai`
--

INSERT INTO `t_nilai` (`id_nilai`, `id_dosen`, `nilai_bap`, `nilai_rapat_prodi`, `nilai_rapat_koor`, `nilai_rapat_prodi2`, `nilai_rapat_koor2`, `nilai_jml_bimb`, `nilai_kk`, `nilai_usulan`, `nilai_penyerahan`, `nilai_edom`, `nilai_jml_de`, `nilai_jml_sidang`, `nilai_perwalian`, `nilai_lks`, `id_semester`) VALUES
(26, 2, NULL, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 232, 0, 0, NULL, NULL, 6),
(27, 4, 85.63, 0, NULL, NULL, NULL, 80, NULL, NULL, NULL, 882, 80, 80, NULL, NULL, 6),
(28, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 912, NULL, NULL, NULL, NULL, 6),
(29, 6, 87.8, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 90, 90, 80, NULL, NULL, 6),
(30, 10, 97.22, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 902, 0, 0, NULL, NULL, 6),
(31, 9, 96.39, 0, 0, NULL, NULL, 95, NULL, NULL, NULL, 82, 50, 0, NULL, NULL, 6),
(32, 8, 91.05, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 772, 0, 0, NULL, NULL, 6),
(33, 11, 93.54, 0, NULL, NULL, NULL, 95, NULL, NULL, 11.11, 88, 50, 0, NULL, NULL, 6),
(34, 13, 93.31, 0, 0, NULL, NULL, 95, NULL, NULL, NULL, 90, 50, 0, NULL, NULL, 6),
(35, 14, 96.78, 100, 100, NULL, NULL, 95, 0, NULL, NULL, 90, 50, 0, NULL, NULL, 6),
(36, 16, 100, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 86, 90, 80, NULL, NULL, 6),
(37, 17, 93.66, 0, NULL, NULL, NULL, 80, NULL, NULL, NULL, 9, 0, 0, NULL, NULL, 6),
(38, 29, 62.53, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 87, 50, 0, NULL, NULL, 6),
(39, 18, 70.27, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 86, 0, 0, NULL, NULL, 6),
(40, 20, 81.04, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 87, 50, 0, NULL, NULL, 6),
(41, 21, 86.88, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 88, 0, 0, NULL, NULL, 6),
(42, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, 6),
(43, 23, 94.6, 0, 0, NULL, NULL, 95, 0, NULL, NULL, 89, 50, 0, NULL, NULL, 6),
(44, 24, 85.77, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 82, 0, 0, NULL, NULL, 6),
(45, 25, NULL, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 82, 0, 0, NULL, NULL, 6),
(46, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, NULL, NULL, NULL, NULL, 6),
(47, 26, 95.42, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, 92, 80, 80, NULL, NULL, 6),
(48, 27, 97.17, 100, 100, 50, 60, 95, NULL, 13.33, 50, 90, 50, 0, 90, 60, 6),
(49, 28, 57.5, 0, NULL, NULL, NULL, 80, NULL, NULL, NULL, 2, 50, 0, NULL, NULL, 6),
(50, 3, 100, 0, NULL, NULL, NULL, 95, NULL, NULL, NULL, NULL, 50, 0, NULL, NULL, 6),
(51, 12, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 6),
(52, 19, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 6),
(53, 30, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_nilai2`
--

CREATE TABLE `t_nilai2` (
  `id_nilai` bigint(20) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `assessment1` double NOT NULL,
  `assessment2` double NOT NULL,
  `assessment3` double NOT NULL,
  `indeks` varchar(2) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `id_portofolio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_nilaiasessment`
--

CREATE TABLE `t_nilaiasessment` (
  `id_nilaiasessment` int(11) NOT NULL,
  `id_matakuliah` int(11) NOT NULL,
  `fileName` text NOT NULL,
  `asessment` varchar(150) NOT NULL,
  `status_nilai` varchar(150) NOT NULL,
  `keterangan` text,
  `date` date NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_nilaiasessment`
--

INSERT INTO `t_nilaiasessment` (`id_nilaiasessment`, `id_matakuliah`, `fileName`, `asessment`, `status_nilai`, `keterangan`, `date`, `id_dosen`) VALUES
(12, 88, 'nilai1.pdf', 'asessment1', 'APPROVE', NULL, '2018-06-10', 27),
(13, 88, 'nilai2.pdf', 'asessment2', 'APPROVE', NULL, '2018-06-10', 27),
(14, 88, 'nilai3.pdf', 'asessment3', 'APPROVE', NULL, '2018-06-10', 27),
(18, 88, 'nilai7.pdf', 'asessment1', 'APPROVE', NULL, '2018-06-10', 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_nilai_koor`
--

CREATE TABLE `t_nilai_koor` (
  `id_nilai_koor` int(12) NOT NULL,
  `id_dosen` int(12) NOT NULL,
  `nilai_koor_rapat` double DEFAULT NULL,
  `nilai_rps` double DEFAULT NULL,
  `nilai_bahan` double DEFAULT NULL,
  `nilai_modul` double DEFAULT NULL,
  `nilai_soal` double DEFAULT NULL,
  `id_matakuliah` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data untuk tabel `t_nilai_koor`
--

INSERT INTO `t_nilai_koor` (`id_nilai_koor`, `id_dosen`, `nilai_koor_rapat`, `nilai_rps`, `nilai_bahan`, `nilai_modul`, `nilai_soal`, `id_matakuliah`, `id_semester`) VALUES
(1, 27, 80, NULL, NULL, NULL, NULL, 84, 6),
(2, 10, NULL, 60, 60, 60, 60, 83, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_partisipasi`
--

CREATE TABLE `t_partisipasi` (
  `id_partisipasi` bigint(20) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `realisasi_dosen` double NOT NULL,
  `realisasi_mahasiswa` double NOT NULL,
  `id_portofolio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pengajaran`
--

CREATE TABLE `t_pengajaran` (
  `id_pengajaran` bigint(20) NOT NULL,
  `status_koordinator` varchar(5) NOT NULL,
  `id_matakuliah` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_pengajaran`
--

INSERT INTO `t_pengajaran` (`id_pengajaran`, `status_koordinator`, `id_matakuliah`, `id_dosen`) VALUES
(92, 'n', 31, 2),
(93, 'y', 31, 9),
(94, 'n', 31, 1),
(95, 'n', 32, 4),
(96, 'y', 32, 28),
(97, 'n', 40, 4),
(98, 'y', 40, 28),
(99, 'y', 33, 6),
(100, 'n', 33, 20),
(101, 'n', 33, 22),
(102, 'n', 33, 26),
(103, 'n', 41, 17),
(104, 'n', 41, 19),
(105, 'y', 41, 23),
(106, 'n', 42, 18),
(107, 'y', 42, 21),
(108, 'n', 42, 22),
(109, 'y', 34, 13),
(110, 'n', 34, 14),
(111, 'n', 34, 26),
(112, 'n', 43, 3),
(113, 'n', 43, 13),
(114, 'n', 43, 14),
(115, 'n', 43, 29),
(116, 'y', 43, 27),
(117, 'y', 36, 20),
(118, 'n', 36, 22),
(119, 'n', 36, 26),
(120, 'n', 44, 18),
(121, 'y', 44, 29),
(122, 'n', 44, 20),
(123, 'n', 37, 2),
(124, 'n', 37, 18),
(125, 'y', 37, 21),
(126, 'n', 45, 3),
(127, 'y', 45, 13),
(128, 'y', 38, 9),
(129, 'n', 38, 13),
(130, 'n', 38, 25),
(131, 'y', 39, 13),
(132, 'n', 39, 10),
(133, 'n', 39, 23),
(134, 'n', 35, 8),
(135, 'n', 35, 11),
(136, 'y', 35, 12),
(137, 'n', 77, 4),
(138, 'n', 92, 4),
(139, 'n', 78, 3),
(140, 'n', 81, 6),
(141, 'y', 78, 6),
(142, 'n', 82, 10),
(143, 'y', 83, 10),
(144, 'n', 78, 9),
(145, 'n', 83, 9),
(146, 'n', 84, 9),
(147, 'y', 85, 8),
(148, 'n', 86, 11),
(149, 'n', 87, 11),
(150, 'n', 88, 11),
(151, 'n', 87, 13),
(152, 'n', 89, 13),
(153, 'n', 78, 13),
(154, 'n', 84, 13),
(155, 'n', 87, 14),
(156, 'n', 89, 14),
(157, 'n', 84, 14),
(158, 'y', 81, 16),
(159, 'n', 85, 16),
(160, 'y', 79, 17),
(161, 'y', 90, 17),
(162, 'n', 81, 29),
(163, 'n', 91, 29),
(164, 'y', 91, 18),
(165, 'n', 81, 20),
(166, 'n', 91, 20),
(167, 'y', 86, 21),
(168, 'n', 91, 21),
(169, 'y', 87, 23),
(170, 'n', 84, 23),
(171, 'n', 88, 23),
(172, 'n', 78, 23),
(173, 'n', 86, 24),
(174, 'n', 83, 24),
(175, 'n', 90, 24),
(176, 'y', 89, 25),
(177, 'n', 89, 26),
(178, 'y', 82, 26),
(179, 'y', 84, 27),
(180, 'y', 88, 27),
(181, 'n', 92, 28),
(182, 'n', 77, 28),
(183, 'y', 92, 30),
(184, 'y', 77, 30),
(185, 'n', 83, 20);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_perwalian`
--

CREATE TABLE `t_perwalian` (
  `id_perwalian` int(12) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `file_perwalian` text NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'waiting',
  `keterangan` varchar(255) DEFAULT NULL,
  `id_dosen` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_perwalian`
--

INSERT INTO `t_perwalian` (`id_perwalian`, `kelas`, `file_perwalian`, `tanggal`, `status`, `keterangan`, `id_dosen`, `id_semester`) VALUES
(1, 'D3MI-39-02', 'Perwalian_D3MI-39-02_WIU_2018-06-03_11-36-31.JPG', '2018-06-03', 'approved', NULL, 27, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_portofolio`
--

CREATE TABLE `t_portofolio` (
  `id_portofolio` bigint(20) NOT NULL,
  `pendahuluan` text NOT NULL,
  `metodepembelajaran` text NOT NULL,
  `mediapembelajaran` text NOT NULL,
  `evaluasipembelajaran` text NOT NULL,
  `statistik` text NOT NULL,
  `umpanbalikmahasiswa` text NOT NULL,
  `refleksidansolusi` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `komentar` text NOT NULL,
  `tgl_pembuatan` datetime NOT NULL,
  `tgl_approve` datetime NOT NULL,
  `id_pengajaran` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_presensi`
--

CREATE TABLE `t_presensi` (
  `id_presensi` bigint(20) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `persentase` double NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `id_portofolio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_prodi`
--

CREATE TABLE `t_prodi` (
  `id_prodi` int(11) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_prodi`
--

INSERT INTO `t_prodi` (`id_prodi`, `prodi`, `status`) VALUES
(1, 'MI', 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rps`
--

CREATE TABLE `t_rps` (
  `id_rps` int(11) NOT NULL,
  `fileName` text NOT NULL,
  `date` date NOT NULL,
  `status_rps` varchar(150) NOT NULL,
  `keterangan` text,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_rps`
--

INSERT INTO `t_rps` (`id_rps`, `fileName`, `date`, `status_rps`, `keterangan`, `id_matakuliah`) VALUES
(1, '001-DONA5.docx', '2018-06-03', 'APPROVE', NULL, 83);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_semester`
--

CREATE TABLE `t_semester` (
  `id_semester` int(11) NOT NULL,
  `semester` varchar(6) NOT NULL,
  `status` varchar(15) NOT NULL,
  `tgl_deadline` date NOT NULL,
  `id_tahunajaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_semester`
--

INSERT INTO `t_semester` (`id_semester`, `semester`, `status`, `tgl_deadline`, `id_tahunajaran`) VALUES
(1, 'Ganjil', 'Tidak Aktif', '2015-12-31', 1),
(2, 'Genap', 'Tidak Aktif', '2016-06-30', 1),
(3, 'Ganjil', 'Tidak Aktif', '2016-12-31', 2),
(4, 'Genap', 'Tidak Aktif', '2017-06-30', 2),
(5, 'Ganjil', 'Tidak Aktif', '2018-05-31', 3),
(6, 'Genap', 'Aktif', '2018-05-31', 3),
(12, 'Ganjil', 'Tidak Aktif', '2018-08-31', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_silabussap`
--

CREATE TABLE `t_silabussap` (
  `id_silabussap` bigint(20) NOT NULL,
  `jenis_data` varchar(10) NOT NULL,
  `nama_berkas` text NOT NULL,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_silabussap`
--

INSERT INTO `t_silabussap` (`id_silabussap`, `jenis_data`, `nama_berkas`, `id_matakuliah`) VALUES
(1, 'silabus', 'dmh2c3_SILABUS,_SAP,_INDIKATOR,_RPP_PemrogramanBasisData_17-05-2018_13-44-30.xlsx', 83),
(2, 'sap', 'dmh2c3_SILABUS,_SAP,_INDIKATOR,_RPP_PemrogramanBasisData_17-05-2018_13-45-59.xlsx', 83);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_soal`
--

CREATE TABLE `t_soal` (
  `id_soal` int(5) NOT NULL,
  `fileName` text NOT NULL,
  `bukti` text NOT NULL,
  `date` date NOT NULL,
  `status_soal` varchar(150) NOT NULL,
  `keterangan` text,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_soal`
--

INSERT INTO `t_soal` (`id_soal`, `fileName`, `bukti`, `date`, `status_soal`, `keterangan`, `id_matakuliah`) VALUES
(1, '6701150047 Registrasi _ Telkom University.pdf', 'reza.jpg', '2018-06-03', 'APPROVE', NULL, 83);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_statusbahan`
--

CREATE TABLE `t_statusbahan` (
  `id_statusbahan` int(11) NOT NULL,
  `id_bahan` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_statusbahan`
--

INSERT INTO `t_statusbahan` (`id_statusbahan`, `id_bahan`, `id_dosen`, `status`) VALUES
(1, 1, 9, 'SETUJU'),
(2, 1, 24, 'SETUJU'),
(3, 1, 20, 'SETUJU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_statusmodul`
--

CREATE TABLE `t_statusmodul` (
  `id_statusmodul` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `id_modul` int(5) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_statusmodul`
--

INSERT INTO `t_statusmodul` (`id_statusmodul`, `id_dosen`, `id_modul`, `status`) VALUES
(1, 9, 1, 'SETUJU'),
(2, 24, 1, 'SETUJU'),
(3, 20, 1, 'SETUJU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_statusnilaiasessment`
--

CREATE TABLE `t_statusnilaiasessment` (
  `id_statusnilaiasessment` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `id_nilaiasessment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_statusnilaiasessment`
--

INSERT INTO `t_statusnilaiasessment` (`id_statusnilaiasessment`, `id_dosen`, `status`, `id_nilaiasessment`) VALUES
(12, 27, 'SETUJU', 12),
(13, 27, 'SETUJU', 13),
(14, 27, 'SETUJU', 14),
(18, 11, 'SETUJU', 18);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_statusrps`
--

CREATE TABLE `t_statusrps` (
  `id_statusrps` int(11) NOT NULL,
  `id_rps` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_statusrps`
--

INSERT INTO `t_statusrps` (`id_statusrps`, `id_rps`, `id_dosen`, `status`) VALUES
(1, 1, 9, 'SETUJU'),
(2, 1, 24, 'SETUJU'),
(3, 1, 20, 'SETUJU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_statussoal`
--

CREATE TABLE `t_statussoal` (
  `id_statussoal` int(11) NOT NULL,
  `id_soal` int(5) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_statussoal`
--

INSERT INTO `t_statussoal` (`id_statussoal`, `id_soal`, `id_dosen`, `status`) VALUES
(1, 1, 9, 'SETUJU'),
(2, 1, 24, 'SETUJU'),
(3, 1, 20, 'SETUJU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_statususulan`
--

CREATE TABLE `t_statususulan` (
  `id_statususulan` int(11) NOT NULL,
  `id_usulan` int(5) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_statususulan`
--

INSERT INTO `t_statususulan` (`id_statususulan`, `id_usulan`, `id_dosen`, `status`) VALUES
(3, 3, 27, 'SETUJU'),
(4, 4, 23, 'SETUJU'),
(6, 6, 11, 'SETUJU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_tahunajaran`
--

CREATE TABLE `t_tahunajaran` (
  `id_tahunajaran` int(11) NOT NULL,
  `tahunajaran` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_tahunajaran`
--

INSERT INTO `t_tahunajaran` (`id_tahunajaran`, `tahunajaran`) VALUES
(1, '2015/2016'),
(2, '2016/2017'),
(3, '2017/2018'),
(7, '2018/2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_template`
--

CREATE TABLE `t_template` (
  `id_template` int(11) NOT NULL,
  `pendahuluan` text NOT NULL,
  `metodepembelajaran` text NOT NULL,
  `mediapembelajaran` text NOT NULL,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_template`
--

INSERT INTO `t_template` (`id_template`, `pendahuluan`, `metodepembelajaran`, `mediapembelajaran`, `id_matakuliah`) VALUES
(1, '<p style="margin-left:0cm; text-align:justify; margin:5pt 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Matakuliah Pemrograman Basis Data (DMH2C3) diselenggarakan pada semester 4 pada kurikulum Program Studi D3 Manajemen Informatika. Matakuliah ini memiliki bobot 3 SKS dengan pelaksanaan 2 SKS kuliah selama 2 jam dan 1 SKS praktikum selama 4 jam. Praktikum menggunakan alat bantu/software Oracle Database 10g atau 11g. Sesi perkuliahan/tutorial dipandu oleh dosen dan sesi praktikum dipandu oleh dosen dan asisten untuk membantu mahasiswa/praktikan.</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Mata kuliah Pemrograman Basis Data memberikan keterampilan dan pengetahuan dasar kepada mahasiswa mengenai cara membuat pemrograman blok PL/SQL baik secara non modular maupun modular. Pemrograman non modular berupa anonymous block sedangkan bentuk modular berupa <i>procedure, function</i>, <i>package, </i>dan <i>trigger</i>. Dalam mata kuliah ini diajarkan pula teknik percabangan, teknik perulangan, <i>cursor</i>, dan penanganan <i>exception</i>. Standar kompetensi yang diharapkan adalah mahasiswa mampu membangun pemrograman basis data secara non-modular maupun secara modular dengan menggunakan PL/SQL.</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">&nbsp;</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 10pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Materi dalam mata kuliah Pemrograman Basis Data ini sebagian besar diambil dari materi training PL/SQL dari Oracle Corporation. Materi terbagi menjadi tiga (3) kajian, yaitu: (1) Pemrograman non-modular berupa anonymous block dan teknik-teknik pemrograman; (2) Pemrograman modular berupa <i>procedure </i>dan<i> function</i>; dan (3) Pemrograman Modular dalam bentuk <i>package </i>dan <i>trigger</i>. Pelaksanaan ujian dibagi menjadi 3 bagian, yaitu assessment 1 terdiri dari materi kajian (1), assessment 2 terdiri dari materi kajian (2), assessment 3 terdiri materi kajian (1), (2), dan (3).</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:5pt 0cm 10pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Kelas matakuliah yang diselenggarakan berjumlah lima (5) kelas. Pengajar setiap kelas ditampilkan dalam tabel berikut.</span></span></span></p>\r\n\r\n<p align="center" class="MsoCaption" style="text-align:center; margin:5pt 0cm 10pt"><span style="font-size:8pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:#365f91"><span style="font-weight:bold"><span style="line-height:115%">Tabel </span><span style="line-height:115%">1</span><span lang="EN-US" style="line-height:115%">. Daftar Kelas dan Pengajar</span></span></span></span></span></span></p>\r\n\r\n<table align="center" class="Table" style="width:263.9pt; border-collapse:collapse; border:solid windowtext 1.0pt" width="352">\r\n	<tbody>\r\n		<tr style="height:15.0pt">\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:23.75pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="32">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">No</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:69.15pt; border-left:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="92">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Kelas</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:4.0cm; border-left:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="151">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Pengajar</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:57.6pt; border-left:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="77">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Keterangan</span></span></span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr style="height:15.0pt">\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:23.75pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="32">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">1</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:69.15pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="92">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">D3MI-40-01</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:4.0cm; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="151">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Ely Rosely</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:57.6pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="77">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Reguler</span></span></span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr style="height:15.0pt">\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:23.75pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="32">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">2</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:69.15pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="92">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">D3MI-40-02</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:4.0cm; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="151">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Suryatiningsih</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:57.6pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="77">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Reguler</span></span></span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr style="height:15.0pt">\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:23.75pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="32">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">3</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:69.15pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="92">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">D3MI-40-03</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:4.0cm; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="151"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Elis Hernawati</span></span></span></span></td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:57.6pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="77">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Reguler</span></span></span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr style="height:15.0pt">\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:23.75pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="32">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">4</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:69.15pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="92">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">D3MI-40-04</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:4.0cm; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="151"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Elis Hernawati</span></span></span></span></td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:57.6pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="77">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Reguler</span></span></span></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr style="height:15.0pt">\r\n			<td nowrap="nowrap" style="border:solid windowtext 1.0pt; width:23.75pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="32">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">5</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:69.15pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="92">\r\n			<p align="center" style="margin:0cm; margin-bottom:.0001pt; text-align:center"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">D3MI-40-05</span></span></span></span></p>\r\n			</td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:4.0cm; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="151"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Ely Rosely</span></span></span></span></td>\r\n			<td nowrap="nowrap" style="border-bottom:solid windowtext 1.0pt; width:57.6pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt; height:15.0pt" valign="bottom" width="77">\r\n			<p style="margin:0cm; margin-bottom:.0001pt"><span style="font-size:10pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Reguler</span></span></span></span></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p align="center" style="margin-top:0cm; margin-right:0cm; margin-bottom:10.0pt; margin-left:0cm; text-align:center; margin:5pt 0cm 10pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 10pt 36pt"><span lang="EN-US" style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Mata kuliah ini merupakan mata kuliah parallel sehingga penyelenggaraannya mengikuti ketentuan yang telah dikoordinasikan sebelumnya dengan Koordinator Mata Kuliah, yaitu </span></span></span><span style="font-size:10.0pt"><span style="line-height:normal"><span style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:black">Ely Rosely</span></span></span></span><span lang="EN-US" style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">. Ketentuan yang ditetapkan berupa silabus, S</span></span></span><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">atuan </span></span></span><span lang="EN-US" style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">A</span></span></span><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">cara </span></span></span><span lang="EN-US" style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">P</span></span></span><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">engajaran (SAP)</span></span></span><span lang="EN-US" style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">, slide, komponen penilaian dan bobot penilaian&nbsp;</span></span></span></p>\r\n', '<p style="margin-left:0cm; text-align:justify; margin:5pt 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Metode pembelajaran yang digunakan adalah ceramah plus latihan dan tugas, serta praktikum. Metode ceramah dilakukan dalam bentuk penjelasan secara lisan berdasarkan media visual diam (slide). Ceramah digunakan untuk menjelaskan teori dari materi Pemrograman Basis Data. Setelah kegiatan ceramah dilanjutkan dengan latihan soal yang diberikan oleh dosen agar mahasiswa dapat mempraktekan sendiri teori-teori yang telah dipelajari sebelumnya. Untuk memahami soal yang lebih kompleks maka diberikan soal untuk dikerjakan di rumah dan dikumpulkan besoknya melalui media elektronik Edmodo. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Untuk kegiatan praktikum mahasiswa sudah menerima sebagian modul praktikum sehingga dapat mengikuti langkah-langkah berupa praktek dengan menggunakan komputer yang telah disiapkan.&nbsp; Sebagian modul lagi diberikan langsung di kelas berupa studi kasus yang harus diselesaikan selama kelas berlangsung. Sebelum praktikum dimulai, mahasiswa diberi tes pendahuluan untuk mengukur kesiapan mahasiswa mengikuti kegiatan praktikum. Setelah selesai praktikum, mahasiswa diberikan tes akhir untuk mengukur penyerapan materi yang dikuasainya. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Perkuliahan dan praktikum dilakukan pada sesi yang terpisah, perkuliahan dilakukan selama 2 jam, begitu juga &nbsp;praktikum &nbsp;dilakukan selama 3 jam. Dosen memberikan teori dan latihan di sesi perkuliahan, sedangkan di sesi praktikum, mahasiswa akan mengerjakan praktikum dengan didampingi oleh asisten, sedangkan dosen hanya mengawasi saja. Tugas yang dikerjakan di luar kelas dikerjakan secara mandiri oleh mahasiswa. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Kegiatan-kegiatan yang dilakukan selama perkuliahan, dijelaskan secara rinci di bawah ini.</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>P</b><b>ertemuan </b><b>ke-</b><b>1</b><b>: </b>Penjelasan tentang aturan perkuliahan dan Satuan Acara Perkuliahan, dilanjutkan dengan pembuatan akun kelas oleh dosen di Edmodo sebagai teacher, kemudian mahasiswa membuat akun di Edmodo sebagai Student. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>Pertemuan ke-</b><b>2</b><b> s.d ke-</b><b>5</b><b>:</b> Perkuliahan materi bahasan kajian 1, yang terdiri dari struktur anonymous block dan tipe data, struktur percabangan, struktur perulangan, exception, dan cursor. Dosen menjelaskan teori terkait topik bahasan di kelas, kemudian dilanjutkan dengan latihan tertulis dan praktek. Kegiatan perkuliahan dilanjutkan dengan praktikum pada sesi berikutnya. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">&nbsp;</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>Pertemuan ke-</b><b>6</b><b>&nbsp; </b>: Assessment 1 diberikan dalam bentuk ujian tulis. Peserta assessment dibagi menjadi 2 sesi, dimana sesi ke 1 dilakukan pada pertemuan 7 sedangakan sesi ke 2 dilakukan pada pertemuan ke 7 untuk praktikum. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>Pertemuan ke-</b><b>7</b><b> s.d ke-</b><b>10</b><b> </b>: Perkuliahan materi bahasan kajian 2 adalah <i>procedure </i>tanpa parameter, <i>procedure </i>dengan parameter <i>, function</i> tanpa parameter<i>, function</i> dengan parameter,<i> package,</i> dan<i> trigger</i>. Ada materi tambahan tetapi tidak diujikan di assessment, yaitu UTL_FILE. Seperti biasa, dosen menjelaskan teori terkait materi &nbsp;bahasan di kelas, kemudian dilanjutkan dengan latihan tertulis dan praktek. Kegiatan perkuliahan dilanjutkan dengan praktikum pada sesi berikutnya. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>Pertemuan ke-</b><b>11</b>: Assessment 2 diberikan dalam bentuk ujian tulis dan peserta assessment dibagi menjadi 2 sesi, sama seperti assessment 1.</span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>Pertemuan ke-</b><b>12</b><b> s.d ke-</b><b>15</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>: Perkuliahan materi bahasan kajian 3 adalah <i>package</i> dan<i> trigger</i>. Ada materi tambahan tetapi tidak diujikan di assessment, yaitu UTL_FILE. Seperti biasa, dosen menjelaskan teori terkait materi &nbsp;bahasan di kelas, kemudian dilanjutkan dengan latihan tertulis dan praktek. Kegiatan perkuliahan dilanjutkan dengan praktikum pada sesi berikutnya. </span></span></span></p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 0.0001pt 36pt">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; text-align:justify; margin:0cm 0cm 10pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif"><b>Pertemuan ke-</b><b>16</b>: Assessment 3 diberikan dalam bentuk presentasi tugas besar bergabung bersama mata kuliah Proyek 2.</span></span></span></p>\r\n', '<p style="margin-left:0cm; text-align:justify; margin:5pt 0cm 10pt 36pt"><span style="font-size:10pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">Media yang digunakan untuk perkuliahan adalah visual. Alat bantu penyampaian materi menggunakan visual cetak berupa slide, modul praktikum, modul SAP, proyektor, dan omputer. Dalam penyampaian materi digunakan proyektor. Materi dalam bentuk softcopy diberikan kepada mahasiswa melalui media online Edmodo. Edmodo ini juga digunakan untuk kegiatan pemberian tugas oleh dosen dan pengiriman tugas oleh mahasiswa. Untuk kegiatan Praktikum digunakan perangkat lunak Oracle Database 11g yang telah terpasang disetiap komputer dimana setiap mahasiswa mennggunakan satu set komputer. Kegiatan perkuliahan dan praktikum dilaksanakan di laboratorium Basis Data.</span></span></span></p>\r\n', 83);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_undangan`
--

CREATE TABLE `t_undangan` (
  `id_undangan` int(11) NOT NULL,
  `agenda` varchar(30) NOT NULL,
  `tanggal_rapat` datetime NOT NULL,
  `ruangan` varchar(20) NOT NULL,
  `tipe` int(11) NOT NULL,
  `bukti` text,
  `id_matakuliah` int(11) DEFAULT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_undangan`
--

INSERT INTO `t_undangan` (`id_undangan`, `agenda`, `tanggal_rapat`, `ruangan`, `tipe`, `bukti`, `id_matakuliah`, `id_semester`) VALUES
(1, 'Workshop Portofolio', '2018-06-03 16:18:00', 'Ruangan Admin', 2, 'x', NULL, 6),
(2, 'Soal Assesment', '2018-06-03 16:20:00', 'G 59', 1, 'x', 84, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_usulansoal`
--

CREATE TABLE `t_usulansoal` (
  `id_usulan` int(5) NOT NULL,
  `fileName` text NOT NULL,
  `date` date NOT NULL,
  `asessment` varchar(150) NOT NULL,
  `status_usulan` varchar(150) NOT NULL,
  `keterangan` text,
  `id_matakuliah` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `t_usulansoal`
--

INSERT INTO `t_usulansoal` (`id_usulan`, `fileName`, `date`, `asessment`, `status_usulan`, `keterangan`, `id_matakuliah`, `id_dosen`) VALUES
(3, 'usulan_soal2.pdf', '2018-06-10', 'asessment1', 'APPROVE', NULL, 88, 27),
(4, 'usulan_soal5.pdf', '2018-06-10', 'asessment1', 'APPROVE', NULL, 88, 23),
(6, 'usulan_soal7.pdf', '2018-06-10', 'asessment1', 'APPROVE', NULL, 88, 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_angkatan`
--
ALTER TABLE `t_angkatan`
  ADD PRIMARY KEY (`id_angkatan`),
  ADD UNIQUE KEY `angkatan` (`angkatan`),
  ADD UNIQUE KEY `id_angkatan_2` (`id_angkatan`),
  ADD KEY `id_prodi` (`id_prodi`),
  ADD KEY `id_angkatan` (`id_angkatan`);

--
-- Indexes for table `t_bahanperkuliahan`
--
ALTER TABLE `t_bahanperkuliahan`
  ADD PRIMARY KEY (`id_bahan`) USING BTREE,
  ADD KEY `id_matakuliah` (`id_matakuliah`) USING BTREE;

--
-- Indexes for table `t_bap`
--
ALTER TABLE `t_bap`
  ADD PRIMARY KEY (`id_bap`) USING BTREE,
  ADD KEY `semester` (`semester`) USING BTREE,
  ADD KEY `id_dosen` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_bobot`
--
ALTER TABLE `t_bobot`
  ADD PRIMARY KEY (`id_bobot`),
  ADD KEY `id_matakuliah` (`id_matakuliah`);

--
-- Indexes for table `t_deadlinenilai`
--
ALTER TABLE `t_deadlinenilai`
  ADD PRIMARY KEY (`id_deadlinenilai`),
  ADD KEY `id_matakuliah` (`id_matakuliah`);

--
-- Indexes for table `t_deadlineusulan`
--
ALTER TABLE `t_deadlineusulan`
  ADD PRIMARY KEY (`id_deadlineusulan`),
  ADD KEY `id_matakuliah` (`id_matakuliah`);

--
-- Indexes for table `t_dosen`
--
ALTER TABLE `t_dosen`
  ADD PRIMARY KEY (`id_dosen`),
  ADD UNIQUE KEY `kode_dosen` (`kode_dosen`);

--
-- Indexes for table `t_edom`
--
ALTER TABLE `t_edom`
  ADD PRIMARY KEY (`id_edom`) USING BTREE,
  ADD KEY `tahun` (`semester`) USING BTREE,
  ADD KEY `t_edom_ibfk_1` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_edom2`
--
ALTER TABLE `t_edom2`
  ADD PRIMARY KEY (`id_edom`),
  ADD KEY `id_portofolio` (`id_portofolio`);

--
-- Indexes for table `t_jadwal_de`
--
ALTER TABLE `t_jadwal_de`
  ADD PRIMARY KEY (`id_jadwal_de`) USING BTREE,
  ADD KEY `id_mhs` (`id_mhs`) USING BTREE,
  ADD KEY `fk_dosen_puj1_de` (`PUJ1`) USING BTREE,
  ADD KEY `fk_dosen_puj2_de` (`PUJ2`) USING BTREE,
  ADD KEY `id_tahun_de` (`tahun`) USING BTREE;

--
-- Indexes for table `t_jadwal_sidang`
--
ALTER TABLE `t_jadwal_sidang`
  ADD PRIMARY KEY (`id_jadwal_sidang`) USING BTREE,
  ADD KEY `fk_dosen_puj1_sidang` (`PUJ1`) USING BTREE,
  ADD KEY `fk_dosen_puj2_sidang` (`PUJ2`) USING BTREE,
  ADD KEY `fk_id_mhs_sidang` (`id_mhs`) USING BTREE,
  ADD KEY `fk_id_tahun_sidang` (`tahun`) USING BTREE;

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD PRIMARY KEY (`id_kategori`) USING BTREE,
  ADD KEY `fk_kpi` (`id_kpi`) USING BTREE;

--
-- Indexes for table `t_kehadiran`
--
ALTER TABLE `t_kehadiran`
  ADD PRIMARY KEY (`id_kehadiran`) USING BTREE,
  ADD KEY `fk_undangan` (`id_undangan`) USING BTREE,
  ADD KEY `fk_data_dosen` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_kesesuaianmateri`
--
ALTER TABLE `t_kesesuaianmateri`
  ADD PRIMARY KEY (`id_kesesuaianmateri`),
  ADD KEY `id_portofolio` (`id_portofolio`);

--
-- Indexes for table `t_kontribusi`
--
ALTER TABLE `t_kontribusi`
  ADD PRIMARY KEY (`id_kl`) USING BTREE,
  ADD KEY `fk_id_tahunajaran_kontribusi` (`tahun`) USING BTREE,
  ADD KEY `fk_id_dosen_kontribusi` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_kpi`
--
ALTER TABLE `t_kpi`
  ADD PRIMARY KEY (`id_kpi`) USING BTREE,
  ADD KEY `fk_id_prodi_kpi` (`prodi`) USING BTREE;

--
-- Indexes for table `t_lampiran`
--
ALTER TABLE `t_lampiran`
  ADD PRIMARY KEY (`id_lampiran`),
  ADD KEY `id_portofolio` (`id_portofolio`);

--
-- Indexes for table `t_lks`
--
ALTER TABLE `t_lks`
  ADD PRIMARY KEY (`id_lks`) USING BTREE,
  ADD KEY `fk_dosen_lks` (`id_dosen`) USING BTREE,
  ADD KEY `fk_semester_lks` (`id_semester`) USING BTREE;

--
-- Indexes for table `t_matakuliah`
--
ALTER TABLE `t_matakuliah`
  ADD PRIMARY KEY (`id_matakuliah`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `t_mhs_pa`
--
ALTER TABLE `t_mhs_pa`
  ADD PRIMARY KEY (`id_mhs`) USING BTREE,
  ADD KEY `fk_id_pembimbing1` (`id_doping1`) USING BTREE,
  ADD KEY `fk_id_pembimbing2` (`id_doping2`) USING BTREE,
  ADD KEY `fk_id_tahun` (`id_tahunajaran`) USING BTREE;

--
-- Indexes for table `t_modul`
--
ALTER TABLE `t_modul`
  ADD PRIMARY KEY (`id_modul`) USING BTREE,
  ADD KEY `foreign key` (`id_matakuliah`) USING BTREE;

--
-- Indexes for table `t_nilai`
--
ALTER TABLE `t_nilai`
  ADD PRIMARY KEY (`id_nilai`) USING BTREE,
  ADD KEY `id_dosen` (`id_dosen`) USING BTREE,
  ADD KEY `fk_id_sem` (`id_semester`) USING BTREE;

--
-- Indexes for table `t_nilai2`
--
ALTER TABLE `t_nilai2`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_portofolio` (`id_portofolio`);

--
-- Indexes for table `t_nilaiasessment`
--
ALTER TABLE `t_nilaiasessment`
  ADD PRIMARY KEY (`id_nilaiasessment`) USING BTREE,
  ADD KEY `id_matakuliah` (`id_matakuliah`) USING BTREE,
  ADD KEY `id_dosen` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_nilai_koor`
--
ALTER TABLE `t_nilai_koor`
  ADD PRIMARY KEY (`id_nilai_koor`) USING BTREE,
  ADD KEY `fk_id_dosen_koor_nilai` (`id_dosen`) USING BTREE,
  ADD KEY `fk_id_semester_koor_nilai` (`id_semester`) USING BTREE,
  ADD KEY `fk_id_matakuliah_nilai` (`id_matakuliah`) USING BTREE;

--
-- Indexes for table `t_partisipasi`
--
ALTER TABLE `t_partisipasi`
  ADD PRIMARY KEY (`id_partisipasi`),
  ADD KEY `id_portofolio` (`id_portofolio`);

--
-- Indexes for table `t_pengajaran`
--
ALTER TABLE `t_pengajaran`
  ADD PRIMARY KEY (`id_pengajaran`),
  ADD KEY `id_matakuliah` (`id_matakuliah`),
  ADD KEY `id_dosen` (`id_dosen`);

--
-- Indexes for table `t_perwalian`
--
ALTER TABLE `t_perwalian`
  ADD PRIMARY KEY (`id_perwalian`) USING BTREE,
  ADD KEY `fk_id_dosen_wali` (`id_dosen`) USING BTREE,
  ADD KEY `fk_id_semester_perwalian` (`id_semester`) USING BTREE;

--
-- Indexes for table `t_portofolio`
--
ALTER TABLE `t_portofolio`
  ADD PRIMARY KEY (`id_portofolio`),
  ADD KEY `id_pengajaran` (`id_pengajaran`);

--
-- Indexes for table `t_presensi`
--
ALTER TABLE `t_presensi`
  ADD PRIMARY KEY (`id_presensi`),
  ADD KEY `id_portofolio` (`id_portofolio`);

--
-- Indexes for table `t_prodi`
--
ALTER TABLE `t_prodi`
  ADD PRIMARY KEY (`id_prodi`) USING BTREE;

--
-- Indexes for table `t_rps`
--
ALTER TABLE `t_rps`
  ADD PRIMARY KEY (`id_rps`) USING BTREE,
  ADD KEY `foreign key` (`id_matakuliah`) USING BTREE;

--
-- Indexes for table `t_semester`
--
ALTER TABLE `t_semester`
  ADD PRIMARY KEY (`id_semester`),
  ADD KEY `id_tahunajaran` (`id_tahunajaran`);

--
-- Indexes for table `t_silabussap`
--
ALTER TABLE `t_silabussap`
  ADD PRIMARY KEY (`id_silabussap`),
  ADD KEY `id_matakuliah` (`id_matakuliah`);

--
-- Indexes for table `t_soal`
--
ALTER TABLE `t_soal`
  ADD PRIMARY KEY (`id_soal`) USING BTREE,
  ADD KEY `foreign key` (`id_matakuliah`) USING BTREE;

--
-- Indexes for table `t_statusbahan`
--
ALTER TABLE `t_statusbahan`
  ADD PRIMARY KEY (`id_statusbahan`) USING BTREE,
  ADD KEY `foreign key` (`id_bahan`) USING BTREE,
  ADD KEY `fk` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_statusmodul`
--
ALTER TABLE `t_statusmodul`
  ADD PRIMARY KEY (`id_statusmodul`) USING BTREE,
  ADD KEY `fk` (`id_modul`) USING BTREE,
  ADD KEY `foreign key` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_statusnilaiasessment`
--
ALTER TABLE `t_statusnilaiasessment`
  ADD PRIMARY KEY (`id_statusnilaiasessment`) USING BTREE,
  ADD KEY `id_nilaiasessment` (`id_nilaiasessment`) USING BTREE,
  ADD KEY `id_dosen` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_statusrps`
--
ALTER TABLE `t_statusrps`
  ADD PRIMARY KEY (`id_statusrps`) USING BTREE,
  ADD KEY `foreign key` (`id_dosen`) USING BTREE,
  ADD KEY `fk` (`id_rps`) USING BTREE;

--
-- Indexes for table `t_statussoal`
--
ALTER TABLE `t_statussoal`
  ADD PRIMARY KEY (`id_statussoal`) USING BTREE,
  ADD KEY `foreign key` (`id_soal`) USING BTREE,
  ADD KEY `fk` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_statususulan`
--
ALTER TABLE `t_statususulan`
  ADD PRIMARY KEY (`id_statususulan`) USING BTREE,
  ADD KEY `foreign key` (`id_usulan`) USING BTREE,
  ADD KEY `fk` (`id_dosen`) USING BTREE;

--
-- Indexes for table `t_tahunajaran`
--
ALTER TABLE `t_tahunajaran`
  ADD PRIMARY KEY (`id_tahunajaran`),
  ADD UNIQUE KEY `tahunajaran` (`tahunajaran`);

--
-- Indexes for table `t_template`
--
ALTER TABLE `t_template`
  ADD PRIMARY KEY (`id_template`),
  ADD KEY `id_portofolio` (`id_matakuliah`);

--
-- Indexes for table `t_undangan`
--
ALTER TABLE `t_undangan`
  ADD PRIMARY KEY (`id_undangan`) USING BTREE,
  ADD KEY `fk_id_semester_undangan` (`id_semester`) USING BTREE,
  ADD KEY `fk_id_matakuliah_undangan` (`id_matakuliah`) USING BTREE;

--
-- Indexes for table `t_usulansoal`
--
ALTER TABLE `t_usulansoal`
  ADD PRIMARY KEY (`id_usulan`) USING BTREE,
  ADD KEY `foreign key` (`id_matakuliah`) USING BTREE,
  ADD KEY `fk` (`id_dosen`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_angkatan`
--
ALTER TABLE `t_angkatan`
  MODIFY `id_angkatan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_bahanperkuliahan`
--
ALTER TABLE `t_bahanperkuliahan`
  MODIFY `id_bahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_bap`
--
ALTER TABLE `t_bap`
  MODIFY `id_bap` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_bobot`
--
ALTER TABLE `t_bobot`
  MODIFY `id_bobot` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_deadlinenilai`
--
ALTER TABLE `t_deadlinenilai`
  MODIFY `id_deadlinenilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_deadlineusulan`
--
ALTER TABLE `t_deadlineusulan`
  MODIFY `id_deadlineusulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_dosen`
--
ALTER TABLE `t_dosen`
  MODIFY `id_dosen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `t_edom`
--
ALTER TABLE `t_edom`
  MODIFY `id_edom` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_edom2`
--
ALTER TABLE `t_edom2`
  MODIFY `id_edom` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_jadwal_de`
--
ALTER TABLE `t_jadwal_de`
  MODIFY `id_jadwal_de` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `t_jadwal_sidang`
--
ALTER TABLE `t_jadwal_sidang`
  MODIFY `id_jadwal_sidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
  MODIFY `id_kategori` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `t_kehadiran`
--
ALTER TABLE `t_kehadiran`
  MODIFY `id_kehadiran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `t_kesesuaianmateri`
--
ALTER TABLE `t_kesesuaianmateri`
  MODIFY `id_kesesuaianmateri` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_kontribusi`
--
ALTER TABLE `t_kontribusi`
  MODIFY `id_kl` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kpi`
--
ALTER TABLE `t_kpi`
  MODIFY `id_kpi` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `t_lampiran`
--
ALTER TABLE `t_lampiran`
  MODIFY `id_lampiran` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_lks`
--
ALTER TABLE `t_lks`
  MODIFY `id_lks` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_matakuliah`
--
ALTER TABLE `t_matakuliah`
  MODIFY `id_matakuliah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `t_mhs_pa`
--
ALTER TABLE `t_mhs_pa`
  MODIFY `id_mhs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT for table `t_modul`
--
ALTER TABLE `t_modul`
  MODIFY `id_modul` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_nilai`
--
ALTER TABLE `t_nilai`
  MODIFY `id_nilai` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `t_nilai2`
--
ALTER TABLE `t_nilai2`
  MODIFY `id_nilai` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_nilaiasessment`
--
ALTER TABLE `t_nilaiasessment`
  MODIFY `id_nilaiasessment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `t_nilai_koor`
--
ALTER TABLE `t_nilai_koor`
  MODIFY `id_nilai_koor` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_partisipasi`
--
ALTER TABLE `t_partisipasi`
  MODIFY `id_partisipasi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_pengajaran`
--
ALTER TABLE `t_pengajaran`
  MODIFY `id_pengajaran` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT for table `t_perwalian`
--
ALTER TABLE `t_perwalian`
  MODIFY `id_perwalian` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_portofolio`
--
ALTER TABLE `t_portofolio`
  MODIFY `id_portofolio` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_presensi`
--
ALTER TABLE `t_presensi`
  MODIFY `id_presensi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_prodi`
--
ALTER TABLE `t_prodi`
  MODIFY `id_prodi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_rps`
--
ALTER TABLE `t_rps`
  MODIFY `id_rps` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_semester`
--
ALTER TABLE `t_semester`
  MODIFY `id_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_silabussap`
--
ALTER TABLE `t_silabussap`
  MODIFY `id_silabussap` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_soal`
--
ALTER TABLE `t_soal`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_statusbahan`
--
ALTER TABLE `t_statusbahan`
  MODIFY `id_statusbahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_statusmodul`
--
ALTER TABLE `t_statusmodul`
  MODIFY `id_statusmodul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_statusnilaiasessment`
--
ALTER TABLE `t_statusnilaiasessment`
  MODIFY `id_statusnilaiasessment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `t_statusrps`
--
ALTER TABLE `t_statusrps`
  MODIFY `id_statusrps` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_statussoal`
--
ALTER TABLE `t_statussoal`
  MODIFY `id_statussoal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_statususulan`
--
ALTER TABLE `t_statususulan`
  MODIFY `id_statususulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_tahunajaran`
--
ALTER TABLE `t_tahunajaran`
  MODIFY `id_tahunajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_template`
--
ALTER TABLE `t_template`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_undangan`
--
ALTER TABLE `t_undangan`
  MODIFY `id_undangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_usulansoal`
--
ALTER TABLE `t_usulansoal`
  MODIFY `id_usulan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `t_angkatan`
--
ALTER TABLE `t_angkatan`
  ADD CONSTRAINT `t_angkatan_ibfk_1` FOREIGN KEY (`id_prodi`) REFERENCES `t_prodi` (`id_prodi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_bahanperkuliahan`
--
ALTER TABLE `t_bahanperkuliahan`
  ADD CONSTRAINT `t_bahanperkuliahan_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_bap`
--
ALTER TABLE `t_bap`
  ADD CONSTRAINT `t_bap_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_bap_ibfk_3` FOREIGN KEY (`semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_bobot`
--
ALTER TABLE `t_bobot`
  ADD CONSTRAINT `t_bobot_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_deadlinenilai`
--
ALTER TABLE `t_deadlinenilai`
  ADD CONSTRAINT `t_deadlinenilai_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_deadlineusulan`
--
ALTER TABLE `t_deadlineusulan`
  ADD CONSTRAINT `t_deadlineusulan_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_edom`
--
ALTER TABLE `t_edom`
  ADD CONSTRAINT `t_edom_ibfk_1` FOREIGN KEY (`semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_edom_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_jadwal_de`
--
ALTER TABLE `t_jadwal_de`
  ADD CONSTRAINT `t_jadwal_de_ibfk_1` FOREIGN KEY (`PUJ1`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_jadwal_de_ibfk_2` FOREIGN KEY (`PUJ2`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_jadwal_de_ibfk_3` FOREIGN KEY (`tahun`) REFERENCES `t_tahunajaran` (`id_tahunajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_jadwal_sidang`
--
ALTER TABLE `t_jadwal_sidang`
  ADD CONSTRAINT `t_jadwal_sidang_ibfk_1` FOREIGN KEY (`PUJ1`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_jadwal_sidang_ibfk_2` FOREIGN KEY (`PUJ2`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_jadwal_sidang_ibfk_3` FOREIGN KEY (`tahun`) REFERENCES `t_tahunajaran` (`id_tahunajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD CONSTRAINT `t_kategori_ibfk_1` FOREIGN KEY (`id_kpi`) REFERENCES `t_kpi` (`id_kpi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_kehadiran`
--
ALTER TABLE `t_kehadiran`
  ADD CONSTRAINT `t_kehadiran_ibfk_1` FOREIGN KEY (`id_undangan`) REFERENCES `t_undangan` (`id_undangan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_kesesuaianmateri`
--
ALTER TABLE `t_kesesuaianmateri`
  ADD CONSTRAINT `t_kesesuaianmateri_ibfk_1` FOREIGN KEY (`id_portofolio`) REFERENCES `t_portofolio` (`id_portofolio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_kontribusi`
--
ALTER TABLE `t_kontribusi`
  ADD CONSTRAINT `t_kontribusi_ibfk_1` FOREIGN KEY (`tahun`) REFERENCES `t_tahunajaran` (`id_tahunajaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_kontribusi_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_kpi`
--
ALTER TABLE `t_kpi`
  ADD CONSTRAINT `t_kpi_ibfk_1` FOREIGN KEY (`prodi`) REFERENCES `t_prodi` (`id_prodi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_lampiran`
--
ALTER TABLE `t_lampiran`
  ADD CONSTRAINT `t_lampiran_ibfk_1` FOREIGN KEY (`id_portofolio`) REFERENCES `t_portofolio` (`id_portofolio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_lks`
--
ALTER TABLE `t_lks`
  ADD CONSTRAINT `t_lks_ibfk_1` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_matakuliah`
--
ALTER TABLE `t_matakuliah`
  ADD CONSTRAINT `t_matakuliah_ibfk_1` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_mhs_pa`
--
ALTER TABLE `t_mhs_pa`
  ADD CONSTRAINT `t_mhs_pa_ibfk_1` FOREIGN KEY (`id_doping1`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_mhs_pa_ibfk_2` FOREIGN KEY (`id_doping2`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_mhs_pa_ibfk_3` FOREIGN KEY (`id_tahunajaran`) REFERENCES `t_tahunajaran` (`id_tahunajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_modul`
--
ALTER TABLE `t_modul`
  ADD CONSTRAINT `t_modul_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_nilai`
--
ALTER TABLE `t_nilai`
  ADD CONSTRAINT `t_nilai_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_nilai_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_nilaiasessment`
--
ALTER TABLE `t_nilaiasessment`
  ADD CONSTRAINT `t_nilaiasessment_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_nilaiasessment_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_nilai_koor`
--
ALTER TABLE `t_nilai_koor`
  ADD CONSTRAINT `t_nilai_koor_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_nilai_koor_ibfk_2` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_nilai_koor_ibfk_3` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_nilai_koor_ibfk_4` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_partisipasi`
--
ALTER TABLE `t_partisipasi`
  ADD CONSTRAINT `t_partisipasi_ibfk_1` FOREIGN KEY (`id_portofolio`) REFERENCES `t_portofolio` (`id_portofolio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_pengajaran`
--
ALTER TABLE `t_pengajaran`
  ADD CONSTRAINT `t_pengajaran_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pengajaran_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_perwalian`
--
ALTER TABLE `t_perwalian`
  ADD CONSTRAINT `t_perwalian_ibfk_1` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perwalian_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_rps`
--
ALTER TABLE `t_rps`
  ADD CONSTRAINT `t_rps_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_semester`
--
ALTER TABLE `t_semester`
  ADD CONSTRAINT `t_semester_ibfk_1` FOREIGN KEY (`id_tahunajaran`) REFERENCES `t_tahunajaran` (`id_tahunajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_soal`
--
ALTER TABLE `t_soal`
  ADD CONSTRAINT `t_soal_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_statusbahan`
--
ALTER TABLE `t_statusbahan`
  ADD CONSTRAINT `t_statusbahan_ibfk_1` FOREIGN KEY (`id_bahan`) REFERENCES `t_bahanperkuliahan` (`id_bahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_statusbahan_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_statusmodul`
--
ALTER TABLE `t_statusmodul`
  ADD CONSTRAINT `t_statusmodul_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_statusmodul_ibfk_2` FOREIGN KEY (`id_modul`) REFERENCES `t_modul` (`id_modul`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_statusnilaiasessment`
--
ALTER TABLE `t_statusnilaiasessment`
  ADD CONSTRAINT `t_statusnilaiasessment_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_statusnilaiasessment_ibfk_2` FOREIGN KEY (`id_nilaiasessment`) REFERENCES `t_nilaiasessment` (`id_nilaiasessment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_statusrps`
--
ALTER TABLE `t_statusrps`
  ADD CONSTRAINT `t_statusrps_ibfk_1` FOREIGN KEY (`id_rps`) REFERENCES `t_rps` (`id_rps`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_statusrps_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_statussoal`
--
ALTER TABLE `t_statussoal`
  ADD CONSTRAINT `t_statussoal_ibfk_1` FOREIGN KEY (`id_soal`) REFERENCES `t_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_statussoal_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_statususulan`
--
ALTER TABLE `t_statususulan`
  ADD CONSTRAINT `t_statususulan_ibfk_1` FOREIGN KEY (`id_usulan`) REFERENCES `t_usulansoal` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_statususulan_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_template`
--
ALTER TABLE `t_template`
  ADD CONSTRAINT `t_template_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_undangan`
--
ALTER TABLE `t_undangan`
  ADD CONSTRAINT `t_undangan_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_undangan_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id_semester`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_usulansoal`
--
ALTER TABLE `t_usulansoal`
  ADD CONSTRAINT `t_usulansoal_ibfk_1` FOREIGN KEY (`id_matakuliah`) REFERENCES `t_matakuliah` (`id_matakuliah`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_usulansoal_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
