/*
 Navicat Premium Data Transfer

 Source Server         : appkd
 Source Server Type    : MySQL
 Source Server Version : 100128
 Source Host           : localhost:3306
 Source Schema         : db_kinerja2

 Target Server Type    : MySQL
 Target Server Version : 100128
 File Encoding         : 65001

 Date: 17/04/2018 14:38:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_mhs_pa
-- ----------------------------
DROP TABLE IF EXISTS `t_mhs_pa`;
CREATE TABLE `t_mhs_pa`  (
  `id_mhs` int(11) NOT NULL AUTO_INCREMENT,
  `nim_mhs` bigint(20) NOT NULL,
  `nama_mhs` varchar(49) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul_pa` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_doping1` int(11) NULL DEFAULT NULL,
  `id_doping2` int(11) NULL DEFAULT NULL,
  `tahap` int(11) NOT NULL,
  `angkatan` varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `grup` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_mhs`) USING BTREE,
  INDEX `fk_id_pembimbing1`(`id_doping1`) USING BTREE,
  INDEX `fk_id_pembimbing2`(`id_doping2`) USING BTREE,
  INDEX `fk_id_tahun`(`id_tahunajaran`) USING BTREE,
  CONSTRAINT `fk_id_pembimbing1` FOREIGN KEY (`id_doping1`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_id_pembimbing2` FOREIGN KEY (`id_doping2`) REFERENCES `t_dosen` (`id_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_id_tahun` FOREIGN KEY (`id_tahunajaran`) REFERENCES `t_tahunajaran` (`id_tahunajaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 176 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_mhs_pa
-- ----------------------------
INSERT INTO `t_mhs_pa` VALUES (1, 6701152089, 'RIO FRIYANDO S.', 'Analisis Pengunjung Berdasarkan Gender', 3, 25, 1, '2018', 3, 1);
INSERT INTO `t_mhs_pa` VALUES (2, 6701154074, 'ERZA A FAHREZI', 'Analisis Pengunjung Berdasarkan Gender', 3, 25, 5, '2015', 3, 1);
INSERT INTO `t_mhs_pa` VALUES (3, 6701152081, 'Samuel Firma Windu Nainggolan', 'Aplikasi berbasis web  Pengadaan Obat Studi kasus : Apotek Siliwangi ', 9, 2, 3, '2019', 3, 2);
INSERT INTO `t_mhs_pa` VALUES (4, 6701150046, 'Septhian Manroe', 'Aplikasi Instalasi Rawat Inap Klinik Siliwangi', 9, 2, 2, '2015', 3, 2);
INSERT INTO `t_mhs_pa` VALUES (5, 6701154181, 'Andreas Togos Silalahi', 'Aplikasi Pengolahan Data Pasien Rawat Jalan Klinik Siliwangi Baleendah Bandung Berbasis Web dan SMS Gateway', 9, 2, 4, '2015', 3, 2);
INSERT INTO `t_mhs_pa` VALUES (6, 6701150024, 'MUNIRSYAH', 'Aplikasi Berbasis Web Inventaris Aset Desa (Studi Kasus: Kecamatan Dayeuhkolot)', 27, 29, 1, '2015', 3, 3);
INSERT INTO `t_mhs_pa` VALUES (7, 6701150109, 'ADELYA ASTARI', 'Aplikasi Berbasis Web Inventaris Aset Desa (Studi Kasus: Kecamatan Dayeuhkolot)', 27, 29, 1, '2015', 3, 3);
INSERT INTO `t_mhs_pa` VALUES (8, 6701150131, 'Alika Safitri', 'Aplikasi berbasis web Penyandang Masalah Kesejahteraan Sosial Studi Kasus : Dinas Sosial Kabupaten Bandung Modul Dinas Sosial', 27, 29, 1, '2015', 3, 4);
INSERT INTO `t_mhs_pa` VALUES (9, 6701154171, 'Dinda Lestari Zaenal', 'Aplikasi berbasis web Penyandang Masalah Kesejahteraan Sosial Studi Kasus : Dinas Sosial Kabupaten Bandung Modul Kecamatan dan Desa', 27, 29, 1, '2015', 3, 4);
INSERT INTO `t_mhs_pa` VALUES (10, 6701150107, 'Dhea Widyana Putri', 'Aplikasi E-Recruitment Pegawai Baru dan Pengelolaan Data Pegawai Berbasis WEB (Modul Pengelolaan Data Pegawai)', 20, 24, 1, '2015', 3, 5);
INSERT INTO `t_mhs_pa` VALUES (11, 6701154142, 'Vivi Rizqia', 'Aplikasi E-Recruitment Pegawai Baru dan Pengelolaan Data Pegawai Berbasis WEB (Modul Rekruitasi Pegawai)', 20, 24, 6, '2015', 3, 5);
INSERT INTO `t_mhs_pa` VALUES (12, 6701150041, 'Timmie Siswandi', 'Aplikasi Inventaris Toko Buku Zahra Berbasis Web Studi kasus : Zahra Books Jombang', 16, 6, 1, '2015', 3, 6);
INSERT INTO `t_mhs_pa` VALUES (13, 6701151061, 'Aji Bagus Panuntun', 'Aplikasi Penjualan Buku di Zahra Book Berbasis Web', 16, 6, 1, '2015', 3, 6);
INSERT INTO `t_mhs_pa` VALUES (14, 6701150016, 'isna nurdiansyah', 'Aplikasi Siklus Percetakan Buku Studi Kasus : Zahra Books Jombang', 16, 6, 1, '2015', 3, 6);
INSERT INTO `t_mhs_pa` VALUES (15, 6701154101, 'Ario Barnas Hardi', 'Aplikasi Layanan Kesehatan Klinik Brimedika Bandung Modul Apotek', 11, 13, 1, '2015', 3, 7);
INSERT INTO `t_mhs_pa` VALUES (16, 6701152201, 'Gilda Marsaulina Hutabarat', 'Aplikasi Layanan Kesehatan Klinik Brimedika Bandung Modul Rawat Jalan Berbasis Web', 11, 13, 1, '2015', 3, 7);
INSERT INTO `t_mhs_pa` VALUES (17, 6701154166, 'Pratiwi Syifa', 'Aplikasi Layanan Kesehatan Klinik Brimedika Bandung untuk Modul Medical Check- Up', 11, 13, 1, '2015', 3, 7);
INSERT INTO `t_mhs_pa` VALUES (18, 6701150025, 'JOSUA NUGRAHA P', 'Aplikasi pelaporan OPT Holtikultura Studi Kasus : BPTPH', 27, 29, 1, '2015', 3, 8);
INSERT INTO `t_mhs_pa` VALUES (19, 6701152090, 'I KADEK WIJAYA K', 'Aplikasi pelaporan OPT Pangan Studi Kasus : BPTPH', 27, 29, 1, '2015', 3, 8);
INSERT INTO `t_mhs_pa` VALUES (20, 6701150106, 'Ai Siti Sopiah', 'Aplikasi pemesanan dan Delivery Makanan di Kantin Universitas Telkom Modul Pelanggan dan Deliv-Man', 23, 26, 1, '2015', 3, 9);
INSERT INTO `t_mhs_pa` VALUES (21, 6701151056, 'Asido Sibuea', 'Aplikasi pemesanan dan Delivery Makanan di Kantin Universitas Telkom Modul Pemesanan ', 23, 26, 1, '2015', 3, 9);
INSERT INTO `t_mhs_pa` VALUES (22, 6701154137, 'Tiara Sabila', 'Aplikasi Pendaftaran dan Transaksi Klinik Hewan Berbasis Android ', 10, 14, 5, '2015', 3, 10);
INSERT INTO `t_mhs_pa` VALUES (23, 6701154152, 'Katarina Astridya', 'Aplikasi Pendaftaran dan Transaksi Klinik Hewan Berbasis Web (Modul Pengelolaan Data Klinik) ', 10, 14, 1, '2015', 3, 10);
INSERT INTO `t_mhs_pa` VALUES (24, 6701150127, 'Nabila Rizkita', 'Aplikasi Pendaftaran dan Transaksi Klinik Hewan Berbasis Web (Modul Pengelolaan Data Pasien dan Transaksi) ', 10, 14, 1, '2015', 3, 10);
INSERT INTO `t_mhs_pa` VALUES (25, 6701152082, 'NOVALDI AMIRULLAH', 'APLIKASI PENDATAAN PUSAT LAYANAN \nKESEHATAN DI KABUPATEN BANDUNG \nBERBASIS ANDROID', 2, 6, 1, '2015', 3, 11);
INSERT INTO `t_mhs_pa` VALUES (26, 6701151057, 'FAHSYA PERMANA PUTRA', 'APLIKASI PENDATAAN PUSAT LAYANAN \nKESEHATAN DI KABUPATEN BANDUNG \nBERBASIS WEB MODUL : KLINIK, POSYANDU, PUSKESMAS)', 14, 4, 1, '2015', 3, 11);
INSERT INTO `t_mhs_pa` VALUES (27, 6701151062, 'FIKRI HARJO YUDHANTO', 'APLIKASI PENDATAAN PUSAT LAYANAN KESEHATAN DI KABUPATEN BANDUNG BERBSIS WEB MODUL:  RUMAH SAKIT', 14, 4, 1, '2015', 3, 11);
INSERT INTO `t_mhs_pa` VALUES (28, 6701152146, 'ayidha elvira syam', 'Aplikasi Pengadaan Barang/Jasa Studi Kasus PT Bhakti Unggul Teknovasi Modul Pemesanan dan Pengelolaan Pengadaan barang ', 20, 24, 1, '2015', 3, 12);
INSERT INTO `t_mhs_pa` VALUES (29, 6701154156, 'gundari amboro sati', 'Aplikasi Pengadaan Barang/Jasa Studi Kasus PT Bhakti Unggul Teknovasi Modul Pengadaan Barang', 20, 24, 1, '2015', 3, 12);
INSERT INTO `t_mhs_pa` VALUES (30, 6701154196, 'Ari Nur Fatwa', 'Aplikasi pengelolaan aktivitas produksi berbasis web pada perusahaan zaemerci', 11, 13, 1, '2015', 3, 13);
INSERT INTO `t_mhs_pa` VALUES (31, 6701154066, 'Hanif Farras Alfalah', 'Aplikasi pengelolaan keuangan dan penjualan di clothing line zaemerci berbasis web', 11, 13, 1, '2015', 3, 13);
INSERT INTO `t_mhs_pa` VALUES (32, 6701154167, 'Fakhrunnisa Nur Afra', 'Aplikasi Pengelolaan Dan Reservasi Hotel di Bandung Berbasis Web (Modul Pengelolaan Hotel)', 14, 10, 1, '2015', 3, 14);
INSERT INTO `t_mhs_pa` VALUES (33, 6701154162, 'Rhadiathul Hasnah', 'Aplikasi Pengelolaan Dan Reservasi Hotel di Bandung Berbasis Web (Modul Reservasi Hotel)', 14, 10, 1, '2015', 3, 14);
INSERT INTO `t_mhs_pa` VALUES (34, 6701150042, 'Lazzuardi Rifqian Naufal', 'Aplikasi Pengelolaan Pencucian Mobil Di Kota Bandung Berbasis Android', 14, 9, 1, '2015', 3, 15);
INSERT INTO `t_mhs_pa` VALUES (35, 6701152192, 'Assyifannisa', 'Aplikasi Pengelolaan Pencucian Mobil Di Kota Bandung Berbasis Web', 14, 9, 1, '2015', 3, 15);
INSERT INTO `t_mhs_pa` VALUES (36, 6701152176, 'Adhitya Candra PP', 'Aplikasi Pengolahan Sertifikasi dan Pelatihan di Laboratorium Aplikasi Terapan Studi kasus : Laboratorium FIT Modul: Pelatihan', 11, 13, 1, '2015', 3, 16);
INSERT INTO `t_mhs_pa` VALUES (37, 6701154141, 'Revina Yoga', 'Aplikasi Pengolahan Sertifikasi dan Pelatihan di Laboratorium Aplikasi Terapan Studi kasus : Laboratorium FIT Modul: sertifikasi ', 11, 13, 1, '2015', 3, 16);
INSERT INTO `t_mhs_pa` VALUES (38, 6701154094, 'VALENTINO ROSSI FIERDAUS SAEPUDIN', 'Aplikasi Sistem Informasi Akademik Berbasis Android', 20, 24, 1, '2015', 3, 17);
INSERT INTO `t_mhs_pa` VALUES (39, 6701150104, 'HESTY KHAIRUNISA', 'Aplikasi Sistem Informasi Akademik Berbasis Web', 20, 24, 1, '2015', 3, 17);
INSERT INTO `t_mhs_pa` VALUES (40, 6701154103, 'KHULAFAUR RASYIDIN', 'Aplikasi Virtual Market untuk Pengembanan UKM (Pembeli)', 11, 13, 1, '2015', 3, 18);
INSERT INTO `t_mhs_pa` VALUES (41, 6701151134, 'LAILY INDARYANI', 'Aplikasi Virtual Market untuk Pengembangan UKM (Pengelola)', 11, 13, 1, '2015', 3, 18);
INSERT INTO `t_mhs_pa` VALUES (42, 6701150113, 'LARAS ARDINA', 'Aplikasi Virtual Market untuk Pengembangan UKM (Penyalur)', 11, 13, 1, '2015', 3, 18);
INSERT INTO `t_mhs_pa` VALUES (43, 6701150125, 'NONITA SARI SIHITE', 'Aplikasi Virtual Market Untuk Pengembangan UMKM (Penyalur)', 11, 13, 1, '2015', 3, 18);
INSERT INTO `t_mhs_pa` VALUES (44, 6701154145, 'DINDA AISYAH J A', 'Aplikasi Virtual Market Untuk Pengembangan UMKM (Penyedia)', 11, 13, 1, '2015', 3, 18);
INSERT INTO `t_mhs_pa` VALUES (45, 6701154170, 'RAHMAD GERI K', 'Aplikasi Virtual Market Untuk Pengembangan UMKM (Penyedia)', 11, 13, 1, '2015', 3, 18);
INSERT INTO `t_mhs_pa` VALUES (46, 6701154183, 'GITA VHANIE ADISTY', 'Bot Tools untuk Auto Delete Comment yang Tidak Diinginkan', 23, 26, 1, '2015', 3, 19);
INSERT INTO `t_mhs_pa` VALUES (47, 6701154158, 'ELSA TRIANA SINAGA', 'Bot Tools untuk Auto Delete Friend dengan Kriteria Tertentu', 23, 26, 1, '2015', 3, 19);
INSERT INTO `t_mhs_pa` VALUES (48, 6701154067, 'Shafrian Dwitama', 'MUSLIM ASSISTANT Modul : Admin', 21, 8, 1, '2015', 3, 20);
INSERT INTO `t_mhs_pa` VALUES (49, 6701154102, 'Irfan Fahmi Wijaya', 'MUSLIM ASSISTANT Modul : Doa, Lokasi Masjid, Arah Kiblat, Penginapan Syariah, Restoran Halal, Jadwal Solat ', 21, 8, 1, '2015', 3, 20);
INSERT INTO `t_mhs_pa` VALUES (50, 6701152077, 'Mughny Mubarak', 'MUSLIM ASSISTANT Modul : Qur\'an dan Hadits', 21, 8, 1, '2015', 3, 20);
INSERT INTO `t_mhs_pa` VALUES (51, 6701150002, 'RIO CIKAL GIGIH PERMANA', 'Mustream – Website Client Music Streaming', 21, 8, 1, '2015', 3, 21);
INSERT INTO `t_mhs_pa` VALUES (52, 6701150007, 'Tegar Abdillah', 'MuStream Aplikasi Server Music Streaming Modul ????', 21, 8, 1, '2015', 3, 21);
INSERT INTO `t_mhs_pa` VALUES (53, 6701150017, 'Yuwanda Fajar', 'MuStream Aplikasi Server Music Streaming untuk Modul Manajemen Konten dan Royalti', 21, 8, 1, '2015', 3, 21);
INSERT INTO `t_mhs_pa` VALUES (54, 6701152087, 'Zulhilmi Ghiffary', 'Aplikasi Streaming Music untuk Komunikasi dan Pengolahan data Streaming pada server (Ampache)', 21, 8, 1, '2015', 3, 21);
INSERT INTO `t_mhs_pa` VALUES (55, 6701154182, 'Fadhil Cahya Kesuma', 'Panti App (Aplikasi Managemen Operasional & Donasi Panti Sosial) Modul  Operasional Panti', 21, 8, 1, '2015', 3, 22);
INSERT INTO `t_mhs_pa` VALUES (56, 6701150032, 'Arif Rizqi Satriana', 'Panti App (Aplikasi Managemen Operasional & Donasi Panti Sosial) Modul Donasi', 21, 8, 1, '2015', 3, 22);
INSERT INTO `t_mhs_pa` VALUES (57, 6701150132, 'Anggi Prayudi', 'Panti App (Aplikasi Managemen Operasional & Donasi Panti Sosial) Modul Service Aplikasi', 21, 8, 1, '2015', 3, 22);
INSERT INTO `t_mhs_pa` VALUES (58, 6701150019, 'ANDI KURNIANTO', 'Pembelajaran Bahasa Isyarat', 3, 25, 1, '2015', 3, 23);
INSERT INTO `t_mhs_pa` VALUES (59, 6701150023, 'RIYANTO PRASTIYO', 'Pembelajaran Bahasa Isyarat', 3, 25, 1, '2015', 3, 23);
INSERT INTO `t_mhs_pa` VALUES (60, 6701150029, 'FADHLI FAUZAN', 'Pembelajaran Jari Tangan untu Bayi Di Bawah Dua Tahun', 3, 25, 1, '2015', 3, 24);
INSERT INTO `t_mhs_pa` VALUES (61, 6701150044, 'SAFII YOGI SAPUTRA', 'Pembelajaran Jari Tangan untu Bayi Di Bawah Dua Tahun', 3, 25, 1, '2015', 3, 24);
INSERT INTO `t_mhs_pa` VALUES (62, 6701150012, 'MUHAMMAD NAUFAL AL FARISI', 'SISTEM INFORMASI GEOGRAFIS & BANDUNG TOUR GUIDE Berbasis Android', 24, 20, 1, '2015', 3, 25);
INSERT INTO `t_mhs_pa` VALUES (63, 6701150122, 'ERIN KARINA NUR FADILLAH', 'SISTEM INFORMASI GEOGRAFIS & BANDUNG TOUR GUIDE Berbasus Web', 24, 20, 1, '2015', 3, 25);
INSERT INTO `t_mhs_pa` VALUES (64, 6701154097, 'Puger Pamungkas', 'SISTEM INFORMASI KERUSAKAN JALAN BERBASIS ANDROID (STUDI KASUS PEMERINTAHAN KAB. BANDUNG)', 14, 25, 1, '2015', 3, 26);
INSERT INTO `t_mhs_pa` VALUES (65, 6701152187, 'Nur Adin', 'Sistem Informasi Kerusakan Jalan Berbasis Website  (Studi kasus Dinas Pekerjaan Umum dan Penataan Ruang Kabupaten Bandung)', 14, 20, 1, '2015', 3, 26);
INSERT INTO `t_mhs_pa` VALUES (66, 6701150001, 'Furqon al khudzaefi', 'Human Resource Information System at Bandung Techno Park for Competency Based Human Resource Management 2', 23, 26, 1, '2015', 3, 27);
INSERT INTO `t_mhs_pa` VALUES (67, 6701154151, 'Niken Galuh Sinta Wardani', 'Sistem Informasi Sumber Daya Manusia di Bandung Techno Park modul Layanan Diri Karyawan', 23, 26, 1, '2015', 3, 27);
INSERT INTO `t_mhs_pa` VALUES (68, 6701150051, 'T M Reza Rahadian', 'Sistem Informasi Sumber Daya Manusia di Bandung Techno Park untuk modul pengelolaan kompetensi sumberdaya manusia 1', 23, 26, 1, '2015', 3, 27);
INSERT INTO `t_mhs_pa` VALUES (69, 6701154200, 'AHMAD ADLI', 'Sistem Manajemen Perangkat di Laboratorium Fakultas Ilmu Terapan', 13, 11, 1, '2015', 3, 28);
INSERT INTO `t_mhs_pa` VALUES (70, 6701152190, 'DHEA KHAIRINNISA', 'Sistem Procurement Perangkat di Laboratorium Fakultas Ilmu terapan', 13, 11, 1, '2015', 3, 28);
INSERT INTO `t_mhs_pa` VALUES (71, 6701150045, 'FAKHRI NAUFAL ALI', 'Aplikasi Pemesanan Dan Monitoring Data Barang Berbasis Web', 13, 11, 1, '2015', 3, 29);
INSERT INTO `t_mhs_pa` VALUES (72, 6701154165, 'IRSAN MARDIANSYAH', 'Aplikasi Pemesanan dan Pelaporan Data Barang Berbasis Android', 13, 11, 1, '2015', 3, 29);
INSERT INTO `t_mhs_pa` VALUES (73, 6701152180, 'RAJA IRFAN ADIPUTRA R', 'Aplikasi Pengelolaan Data Produksi CV HFM', 13, 11, 1, '2015', 3, 29);
INSERT INTO `t_mhs_pa` VALUES (74, 6701150114, 'NYOMAN IRVIANTI WINDAPUTRI', 'Aplikasi “Rumahku” Pembelajaran Interaktif Mengenal Bagian Rumah Berbasis Android', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (75, 6701152199, 'SANTI AL ARIF', 'Aplikasi Edukasi Pengenalan Binatang (Studi Kasus: PGTK Darul Hikam)', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (76, 6701152149, 'FABIOLA MATARA', 'Aplikasi Media Interaktif Pengenalan Macam-macam Alat Komunikasi', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (77, 6701154144, 'ANNISA ROHMAH', 'Aplikasi Pembelajaran Untuk Anak Usia Dini Dalam Mengenal Profesi Atau Cita-cita', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (78, 6701150049, 'MUHAMMAD DALLA LILHAQ', 'Aplikasi Pengenalan Alam Semesta Ciptaan Allah (Air, Api, Udara, Benda Langit, dan Gejala Alam) (Studi Kasus: TK Darul Hikam)', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (79, 6701154069, 'MUHAMMAD APRIAWAN DERAJAT', 'Aplikasi Pengenalan dan Pembelajaran Budaya Nusantara', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (80, 6701150039, 'ERVAN ADITYA NUGRAHA', 'Aplikasi Pengenalan Tanaman Hias, Umbi-umbian, Apotik Hidup dan Sayuran (Studi kasus: TK Darul Hikam)', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (81, 6701154154, 'NYSSA RAMADHANTI', 'Media Pembelajaran Hebatnya Alat Transportasi (Studi Kasus: TK Darul Hikam)', 29, 27, 1, '2015', 3, 30);
INSERT INTO `t_mhs_pa` VALUES (82, 6701150047, 'LALU GDE MUHAMMAD FARIZT', 'Aplikasi Pengelolaan Pencapaian Kinerja Dosen Bidang Pengajaran', 14, 23, 1, '2015', 3, 31);
INSERT INTO `t_mhs_pa` VALUES (83, 6701151059, 'DILRAJ PUTRA', 'Aplikasi Pengelolaan Pencapaian Kinerja Dosen Bidang Pengajaran', 14, 23, 7, '2015', 3, 31);
INSERT INTO `t_mhs_pa` VALUES (84, 6701151054, 'MUHAMMAD DERRY SALMAN SYAHID', 'Aplikasi Portofolio Pengajaran Dosen', 23, 14, 7, '2015', 3, 31);
INSERT INTO `t_mhs_pa` VALUES (85, 6701150174, 'IRFAN FATHUR RAHMAN', 'Dashboard Monitoring Kinerja Dosen Bidang Pengajaran', 24, 14, 1, '2015', 3, 31);
INSERT INTO `t_mhs_pa` VALUES (86, 6701150034, 'RIZKY WAHYUDI', 'Pengenalan Wajah untuk Presensi Mahasiswa', 3, 25, 1, '2015', 3, 32);
INSERT INTO `t_mhs_pa` VALUES (87, 6701151064, 'DEAN ARIFIN SIBURIAN', 'Pengenalan Wajah untuk Presensi Pegawai', 3, 25, 1, '2015', 3, 32);
INSERT INTO `t_mhs_pa` VALUES (88, 6701152078, 'DILLA FAJRI', 'Kepuasan Pelanggan', 3, 25, 1, '2015', 3, 33);
INSERT INTO `t_mhs_pa` VALUES (89, 6701150028, 'IMAM RIFA\'I', 'Kepuasan Pelanggan Pada Sentra Industri Konveksi', 3, 25, 1, '2015', 3, 33);
INSERT INTO `t_mhs_pa` VALUES (90, 6701154095, 'DEBY MUHAMMAD BUDI', 'Pengukuran Kepuasan Layanan Pelanggan Menggunakan Depth Camera', 3, 25, 1, '2015', 3, 33);
INSERT INTO `t_mhs_pa` VALUES (91, 6701154073, 'SATYA ANGGRAHITA', 'Aplikasi Virtual Assistant Berbasis Android', 23, 26, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (92, 6701140102, 'Putri dewi maharani', ' Aplikasi Pembelajaran bahasa jepang tingkat N5 (pemula) berbasis android', 16, 6, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (93, 6301130082, 'BRAM PURBA', 'Aplikasi Administrasi Keanggotaan BBC Fitness Centre Berbasis Web', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (94, 6701140208, 'Jonathan Juniko Wijaya', 'APLIKASI ADMINISTRASI PEMBAYARAN SUMBANGAN PEMBINAAN PENDIDIKAN (SPP) DAN DANA SUMBANGAN PEMBANGUNAN (DSP) DI SD ANANDA BOJONG KULUR KABUPATEN BOGOR', NULL, NULL, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (95, 6701140028, 'Faris Zulfikar Hidayat', 'Aplikasi Centra Meubel Online', 26, 23, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (96, 6701150027, 'Okananda Rizky Perdana', 'Aplikasi Dokumen Manajemen\nSystem (DMS) berbasis Tata\nkelola Data DAMA International\npada perusahaan ', 13, 11, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (97, 6701150121, 'Vina Dwi Mercuri', 'Aplikasi EatMe Berbasis Web pada Wagoon Coffee Buah Batu', 20, 24, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (98, 6701150011, 'Kiki Sahnakri ', 'Aplikasi Latihan dan Simulasi Uji Kemampuan Bahasa Indonesia Berbasis Web', 27, 29, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (99, 6701150036, 'Agung Harianto S', 'Aplikasi Manejemen penghuni perumahaan Studi Kasus : Perumahan Permata Buah Batu ', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (100, 6701152193, 'AJENG ANJANI R', 'Aplikasi Monitoring dan Pemesanan Tiket Bus dengan Metode FIFO Berbasis Web pada PT Bus Budiman', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (101, 6701150110, 'LUH PUTU YULI ASTRINI', 'Aplikasi Pelayanan Masyarakat Desa Cipagalo Berbasis Android', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (102, 6701154198, 'NOVIA NUR HIDAYAH', 'Aplikasi Pembelajaran Bahasa Jepang Berbasis Android', 21, 8, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (103, 6701154143, 'NAFINA SALMA SETIAWAN', 'Aplikasi Pembelajaran Huruf Hijaiyah Berbasis Android Bertemakan Flat Design', 24, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (104, 6701150005, 'GIFARI BAGASKARA', 'Aplikasi Pembuatan Kode Buku pada Dinas Perpustakaan dan Kearsipan Kota Bandung', 24, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (105, 6701150116, 'Elisabet Patrisia', 'Aplikasi Pemesanan Tiket Bus Secara Online Berbasis Web Di CV Harum Prima Bandung', 29, 27, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (106, 6701150040, 'ARVIN LAZAWARDI P', 'Aplikasi Peminjaman Ruangan dan Aset Laboratorium', 13, 11, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (107, 6701154159, 'GINSHA ASMAUL SITI KHASANAH', 'Aplikasi Peminjaman Sepeda Berbasis Android Pada Studi Kasus Telkom University', 29, 27, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (108, 6701154150, 'ZSAZSA DWI LINGGASARI', 'Aplikasi penatausahaan Pengadaan Barang Bagian Badan Pengelolaan Keuangan dan Aset Daerah (BPKAD) Pemerintahan Kota Bandung Berbasis Web Menggunakan Metode Just In Time', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (109, 6701150105, 'TIKA HANDAYANI P', 'Aplikasi Pencatatan Data Aspirasi Masyarakat: Komplain, Kritik, dan Saran Desa Cipagalo Berbasis Web', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (110, 6701152080, 'DEDE SURYAWAN A P', 'Aplikasi Pencatatan Pelanggaran Siswa Berbasis Web', 20, 24, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (111, 6701154184, 'VIONA REWITA', 'Aplikasi Pendaftaran Berobat Online dan Penjadwalan Dokter Berbasis Web (Studi Kasus: Klinik dan Apotik Bona Mitra Keluarga)', 9, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (112, 6701154155, 'MADE SINTAYATI', 'Aplikasi Pendataan dan Pengelolaan Imunisasi Rutin di Puskesmas Bojongsoang Berbasis Web', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (113, 6701150130, 'LIDYA NURINNAHAR', 'Aplikasi Pendataan Ketersediaan Alat dan Bahan Imunisasi Rutin di Puskesmas Bojongsoang Berbasis Web', 6, 16, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (114, 6701150015, 'IQBAL AL-HAKIM', 'Aplikasi Pengelolaan Administrasi Puskesmas Bojongsoang', 8, 21, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (115, 6701150026, 'Ahmad Nadliri', 'Aplikasi Pengelolaan Belanja kebutuhan ATK Studi kasus : Dinas Pendidikan Kabupaten Jombang', 18, 21, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (116, 6701152177, 'Rinda Firma Violita', 'Aplikasi Pengelolaan Biaya Pendidikan di Madrasah Tsnawiyah PERSIS Ciganitri-Bandung', 27, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (117, 6701152147, 'RIZKA DWI WUANDARI S.', 'Aplikasi Pengelolaan Dana Bantuan Operasional Sekolah pada Madrasah Tsanawiyah Pesantren Persatuan Islam Ciganitri', 2, 9, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (118, 6701150120, 'BELLA ANGGRYENI', 'Aplikasi pengelolaan data dan informasi kepegawaian Badan Pengelola Keuangan dan Aset (BPKA) daerah kota Bandung berbasis Web', 16, 6, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (119, 6701150119, 'RIMA NOVIANTI', 'Aplikasi Pengelolaan Data Kepegawaian Berbasis Web (Studi Kasus: Dinas Perpustakaan dan Kearsipan Daerah)', 10, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (120, 6701152175, 'REVI CHANDRA RIANA', 'Aplikasi Pengelolaan E-KTP Kelurahan Kujangsari', 26, 10, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (121, 6701151058, 'SETYO AJI', 'Aplikasi Pengelolaan Iklan Virtual Berbasis Media Sosial', 18, 21, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (122, 6701154195, 'HAFIYANIDA RAHMANI', 'Aplikasi Pengelolaan Keuangan Untuk Pengajuan Kebutuhan dari Masyarakat Desa Cipagalo Berbasis Web', 16, 6, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (123, 6701154100, 'MUBAROQ IQBAL', 'Aplikasi Pengelolaan Pendaftaran Ulang SMKN 2 Kepahiang', 24, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (124, 6701154140, 'LATIFA NUR FITRIANA', 'Aplikasi Pengelolaan Penerimaan Dana Bantuan Untuk Desa Cipagalo Berbasis Web', 16, 6, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (125, 6701154160, 'NADIA SALSABILA', 'Aplikasi Pengelolaan Pengajuan Dana Keuangan untuk Desa Cipagalo dari Pemerintah Kabupaten Bandung Selatan Berbasis Web', 16, 6, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (126, 6701150117, 'Ficky Azizah', 'Aplikasi Pengelolaan Presensi dan Penggajian Karyawan Berbasis Web', 26, 10, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (127, 6701150020, 'MUHAMAD IKHSAN M', 'Aplikasi Pengelolaan Rental Mobil', 26, 10, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (128, 6701150021, 'Arrahman Ayuskhan', 'Aplikasi Pengelolaan Stok Barang Nun Jilbab Bandung', 10, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (129, 6701150115, 'NIZMA AINUN', 'Aplikasi Pengelolaan Surat Dinas Masuk dan Surat Dinas Keluar di Badan Pengelolaan Keuangan dan Aset Daerah Kota Bandung Berbasis Web', 16, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (130, 6701152185, 'MOCHAMAD SYAFIQ A', 'Aplikasi Penggajian Berdasarkan Absensi PT Sekawan Karya Cipta', 18, 10, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (131, 6701154153, 'ANNISA ROMADONA FAUZIA', 'Aplikasi Penghitung HPP dan Studi Kelayakan Produk untuk UMKM Berbasis Desktop', 13, 11, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (132, 6701150111, 'AMBAR SORAYA', 'Aplikasi pengolahan cek in, cek out, dan housekeeping di hotel gurame berbasis web ', 3, 25, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (133, 6701140073, 'Adinda Indah Permata', 'Aplikasi Pengolahan Data Obat Berbasis Web pada klinik perawatan Dermabrasy Therapy Skincare ', 26, 23, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (134, 6701152148, 'DESSY YUSSELA M Y', 'Aplikasi Pengolahan Nilai Rapor SMP BPI 1 Bandung', 21, 8, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (135, 6701150043, 'ILHAM FIRDAUS', 'Aplikasi Penjualan Online Berbasis Web pada Toko Citra Oniq', 10, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (136, 6701151060, 'RISWAN ARDINATA', 'Aplikasi Perhitungan Gaji Untuk Armada Truk Berdasarkan Pendapatan Per unit PT. BKW Berbasis Web', 18, 10, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (137, 6701150108, 'PIO ANDINA SEREPINA SIMANULLANG', 'Aplikasi Perhitungan Masa Subur Wanita', 10, 23, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (138, 6701150006, 'Dicky Muhammad Sofian', 'Aplikasi Perpustakaan Di SMAN 27 Bandung berbasis Sms Gateway', 8, 21, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (139, 6701150038, 'RAHMAWAN A', 'Aplikasi Prediksi Pemintatan di SMA dengan Menggunakan Algoritma K-Nearest Neighbor', 10, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (140, 6701154099, 'LUTHFI ANUGERAH MIRDA', 'Aplikasi Presensi dan Interaktif Greeting', 3, 25, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (141, 6701150124, 'HARTATI SEPDIYANTI', 'Aplikasi Referensi Obat, Penjualan, dan Pendataan Obat Berbasis Web (Studi Kasus: Apotik Nusa Farma bandung)', 2, 9, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (142, 6701150118, 'RINEZ ASPRINOLA', 'Aplikasi Rekomendasi Makanan pada Solusi Penyakit Menggunakan Metode Forward Chaining', 16, 18, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (143, 6701152179, 'AYU KARTIKA SANDHY', 'Aplikasi Simpan Pinjam Koperasi', 2, 9, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (144, 6701150133, 'MEGA CANDRA DEWI', 'Aplikasi Toko Obat Berbasis Android', 24, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (145, 6701154072, 'Akbar Maulana', 'Aplikasi Ujian Online dan Analisis Soal SMAN 1 CIBINONG', 18, 11, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (146, 6701150112, 'Lia Maulita R', 'Dashboard Aplikasi Posyandu : Monitoring dan Controlling Tumbuh Kembang anak, Pemberian imunisasi dan Vitamin A', 27, 29, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (147, 6701151135, 'LAILA FATHHIYANA A', 'Integrasi Aplikasi', 24, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (148, 6701154138, 'VIYONA RASINTA MAGDALENA PURBA', 'Media Pembelajaran Interaktif Pengenalan Peta dan Budaya Indonesia (Studi Kasus: SD Tiara Bunda, Batununggal Bandung)', 9, 4, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (149, 6701152202, 'Jihad Dwi Cahyono', 'PEMBANGUNAN APLIKASI ASSESMENT KUALITAS DATA DENGAN METODE CALDEA EVAMELCA BERBASIS WEB\n(Studi Kasus: Telkom University)', 13, 11, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (150, 6701151053, 'MADE DWI DHARMA SREYA', 'Pembangunan Aplikasi Manajemen Proyek untuk Perusahaan Roketin Bandung', 18, 10, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (151, 6701154070, 'AULIA MEI AZIZI', 'Pengenalan Muka Untuk Presensi Mahasiswa', 3, 25, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (152, 6701154096, 'Sultan Habib Putera Kesuma', 'Portal Data Pemerintah Kabupaten Bandung', 27, 29, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (153, 6701154139, 'NINING KARTIKA', 'Prix Simanday', 18, 20, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (154, 6701154071, 'rahmat alifuddin nur', 'Reservasi Online Pemesanan Makanan d Warung Shinju Ramen Bandung', 2, 9, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (155, 6701150010, 'ILHAM WAHYUDI', 'Sistem Informasi Administrasi Data Kependudukan pada Kantor Kelurahan Desa Kujangsari Kab. Bandung', 26, 23, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (156, 6701150014, 'AGRI SYAHRIAL', 'Aplikasi Monitoring Sampah Rumah Tangga', 9, 29, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (157, 6701154068, 'HENDRI SUSANTO', 'Aplikasi Transaksi dan Penjualan Berbasis Web (Studi Kasus: PT Pos Indonesia Canag Kota Cimahi)', 26, 23, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (158, 6701150008, 'JIMMY PUTERA MANURUNG', 'Aplikasi Rawat Jalan di Klinik Mulia Sehat Berbasis Web', 9, 2, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (159, 6701150018, 'A. RIZALDY MUQORROBIN', 'Aplikasi Pembelajaran Tata Surya Bagi Siswa Dasar Kelas 6 Menggunakan Augmented Reality', 11, 17, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (160, 6701150033, 'MESA BASTIAN', 'Aplikasi Pengenalan Hewan Menggunakan Augmented Reality Berbasis Android', 23, 4, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (161, 6701150048, 'KHALIFASYA GIBRANSAM', 'Aplikasi Pasar Item Game Online', 21, 8, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (162, 6701150123, 'EVITA CHRISTI MATONDANG', 'Aplikasi Penyewaan Rumah di Perumahan Sekitar Universitas Telkom (Studi Kasus Perumahan Sekitar Universitas Telkom)', 24, 28, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (163, 6701150128, 'YOSI PRIHATININGSIH', 'Aplikasi D\'Gallery Futsal Berbasis Web (Lingkungan Telkom University)', 18, 28, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (164, 6701152083, 'GIAN NANDA FATRA WADANA', 'Aplikasi Pengolahan Data Donatur dan Anak Asuh di Panti Asuhan Amanah Berbasis Web', 13, 17, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (165, 6701152178, 'OLGHA MAGRISA', 'Aplikasi Berbasis Web untuk Penyewaan Kafe/Restauran', 25, 17, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (166, 6701152203, 'INRA AYU', 'Aplikasi Pembagian dan Perhitungan Harta Warisan Online Berdasarkan Konsep Fiqh Mawaris', 2, 28, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (167, 6701154093, 'M MUTHI AMMAR NAUFAL', 'Aplikasi Penjualan Kue Berbasis Web pada Toko Yanie\'s Cakes and Cookies', 9, 17, 1, '2015', 3, NULL);
INSERT INTO `t_mhs_pa` VALUES (168, 23, '23', '222333', 14, 13, 2, '2015', 3, 33322);
INSERT INTO `t_mhs_pa` VALUES (169, 3, '3', '3', 3, 25, 1, '2015', 3, 3);
INSERT INTO `t_mhs_pa` VALUES (170, 787787, '78787', '78787', 8, 3, 7, '', 3, 8787878);
INSERT INTO `t_mhs_pa` VALUES (171, 1231231, '1231231', '1231231', 6, 3, 1, '2017', 3, 200);
INSERT INTO `t_mhs_pa` VALUES (172, 6700151059, 'Simon Dominic', 'Yaman', 3, 9, 1, '2018', 3, 100);
INSERT INTO `t_mhs_pa` VALUES (173, 333, '33', '33', 9, 10, 3, '33', 3, 33);
INSERT INTO `t_mhs_pa` VALUES (174, 1111111, 'Tes Tes', 'Tes Application', 6, 10, 7, '2015', 3, 9);
INSERT INTO `t_mhs_pa` VALUES (175, 1, '2', '4', 3, 8, 1, '3', 3, 5);

SET FOREIGN_KEY_CHECKS = 1;
